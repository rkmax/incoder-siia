start =
  EXPRESSION

NUMBER_EXP =
    OCTAL_EXP
  / DECIMAL_EXP

DECIMAL_EXP =
    "0" { return 0; }
  / first:[1-9] rest:[0-9]* { return parseInt(first + rest.join("")); }

OCTAL_EXP =
    "0" digits:[0-7]+ { return parseInt(digits.join(""), 8); }

BOOL_EXP =
    "true"  { return true; }
  / "false" { return false; }

STRING_EXP =
    [\"\']? chars:[0-9a-zA-Z_\ ]+ [\"\']? { return chars.join(""); }

STRING_EXP_ =
    chars:[0-9a-zA-Z_*]+ { return chars.join(""); }

PATH_EXP =
    first:STRING_EXP_ rest:("." s:STRING_EXP_ { return "." + s; })?
        { return {PATH: first + (rest || "")}; }

ARRAY_EXP =
    "[" first:PRIMARY_EXP rest:("," SS s:PRIMARY_EXP { return s; })? "]"
        { if (rest !== "") { return [first].concat(rest);} else {return [first];} }

PROPERTY_EXP =
    f:[\:] chars:[0-9a-zA-Z_]+ { return {PROP: f + chars.join("")}; }

OPER_EXP =
    "==" / "HAS" / ">=" / "<=" / "<>" / ">" / "<"

SS =
    " "*

PRIMARY_EXP =
    NUMBER_EXP
  / BOOL_EXP
  / OBJECT_EXP
  / STRING_EXP

SECOND_EXP =
    PRIMARY_EXP
  / ARRAY_EXP

OBJECT_EXP =
    PROPERTY_EXP
  / PATH_EXP

COMP_EXP =
    SS L:OBJECT_EXP SS COMP:OPER_EXP SS R:PRIMARY_EXP SS
        { return {OPERATOR: COMP, L:L, R:R}; }

IF_EXP = "IF" / "THEN" / "END"
EVENT_EXP = "ON CHANGE" / "THEN" / "END"

VALID_FUNC =
    "DOMAIN"

FUNCT_EXP =
    SS FUNC:VALID_FUNC "(" VAL:PRIMARY_EXP ")" SS
        { return {FUNC:FUNC, VAL:VAL}; }

ASSIGN_EXP =
    SS "SET" SS L:PRIMARY_EXP SS "=" SS R:SECOND_EXP SS "WHERE" COMP:COMP_EXP
        { return {NAME: "ASSIGN",L:L, R:R, IF: COMP}; }
  / SS "SET"? SS L:PRIMARY_EXP SS "=" SS R:FUNCT_EXP SS
        { return {NAME: "ASSIGN",L:L, R:R}; }
  / SS "SET"? SS L:PRIMARY_EXP SS "=" SS R:SECOND_EXP SS
        { return {NAME: "ASSIGN",L:L, R:R}; }

EXPRESSION =
    IF_EXP COMP:COMP_EXP IF_EXP FUNC:ASSIGN_EXP IF_EXP?
        { return {CONDITION: COMP, ACTION: FUNC}; }
  / EVENT_EXP SS PATH:OBJECT_EXP SS EVENT_EXP FUNC:ASSIGN_EXP EVENT_EXP?
        { return {EVENT: PATH, ACTION: FUNC}; }
