;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsList', ['$rootScope', 'utils', 'builder', function($rootScope, utils) {

        return {
            restrict: 'A',
            require: '^rrsForm',
            scope: {
                list: '=rrsList',
                model: '=rrsModel'
            },
            replace: true,
            templateUrl: utils.partial('list'),
            link: function(scope) {
                scope.$watch('list.domain', function(domainName) {

                    scope.list.options = _.filter($rootScope.domains, function(d) {
                        return d.domain === domainName;
                    }).map(function(o) {
                        return {
                            id: utils.guid(),
                            label: o.value
                        };
                    });
                });
            },
            controller: function($scope) {

                $scope.list.id = utils.guid();
                $scope.list.checkbox = {};

                // para el modelo
                $scope.$watch('model', function(newVal, oldVal) {

                    switch($scope.list.type) {
                        case 'radio':
                            $scope.list.option = newVal;
                            break;
                        case 'checkbox':
                            if (!_.isArray(newVal)) {
                                newVal = [newVal];
                            }

                            var _options = {};

                            for (var i = 0; i < newVal.length; i++) {
                                _options[newVal[i]] = true;
                            }

                            $scope.list.checkbox = _.extend($scope.list.checkbox, _options);
                            break;
                    }
                });

                // Para los Radio
                $scope.$watch('list.radio', function(newVal, oldVal) {
                    if (newVal === oldVal) return;
                    $scope.model = newVal;
                }, true);

                // Para los checkbox
                $scope.$watch('list.checkbox', function(newVal, oldVal) {

                    if (newVal === oldVal) return;

                    var _data = [];

                    _.each(newVal, function(v, k) {
                        if (v) _data.push(k);
                    });

                    $scope.model = _data;

                }, true);
            }
        };
    }]);
})();
