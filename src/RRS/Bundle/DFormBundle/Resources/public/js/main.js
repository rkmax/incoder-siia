;(function() {
    "use strict";

    // Declara el modulo angular
    angular.module('app', [])


    // Definimos un controlador
    .controller('Main', function($scope) {
        $scope.greetings = "Hello from angular";
    })

    .factory('FORM', function() {

        var el_ids = {};

        var builder_element = function (el_, params) {
            var el =
                _.extend({__meta: params, label: params.label}, el_),
                base_tpml = "<div PARAMS=\"data." + el.id + "\"></div>"
            ;


            switch (params.element_type) {
                case 'textfield':
                    el.template = base_tpml.replace("PARAMS", "meta-textfield");
                    break;
                case 'choicefield':
                    if (el.multiple) {} else {
                        el.template = base_tpml.replace("PARAMS", "meta-choicefield");
                    }
                    el.domain = params.domain_values;
                    break;
                default:
                    throw "Elemento '" + params.element_type + "' no definido";
            }

            return el;
        };

        var builder_element_id = function (parent_name, el_name) {
            var
                id = (parent_name + "_" + el_name),
                name = (parent_name + "[" + el_name + "]")
            ;

            return {id: id, name: name};
        };

        var builder_build = function (params) {
            var
                form_name = params.name || 'form',
                form_url  = params.url  || '',
                form_type = params.type || 'form',
                form_elements = []
            ;

            if (_.isArray(params.element) && params.element.length === 0) {
                throw 'No hay elementos en el formulario';
            }

            _.each(params.element, function (element_data) {
                var
                    el_ = builder_element_id(form_name, element_data.name),
                    el = false
                ;

                if (!_.has(el_ids, el_.id)) {
                    el_ids[el_.id] = builder_element(el_, element_data);

                    if (el_ids[el_.id]) {
                        form_elements.push(el_ids[el_.id]);
                    }
                }
            });

            return form_elements;
        };

        return {
            build: function(params) {
                return builder_build(params);
            }
        };
    })

    // Directiva que lee el formulario "meta" y o convierte a elementos
    .directive('metaForm', function($http, FORM, $compile) {
        return {
            restrict: 'A',
            scope: {
                metaForm: '@'
            },
            controller: function($scope) {

            },
            transclude: true,
            replace: true,
            template: '<div><div class="form-header"><h1>{{title}}</h1></div><div class="form-content"><form method="post" class="pure-form pure-form-aligned"></form></div></div>',
            link: function (scope, el, attrs) {
                scope.data = {};
                scope.$watch('metaForm', function(provider) {
                    if (!provider) return;

                    $http({url: provider})
                        .success(function(data, status, headers, config){

                            scope.title = data.label;

                            // Construccion de elementos
                            _.each(FORM.build(data), function(_el) {
                                el.find('form').append(($compile(_el.template)(scope)));
                                delete _el.template;
                                scope.data[_el.id] = _el;

                            });

                            // Al enviar
                            el.on('submit', function(ev) {
                                ev.preventDefault();

                                console.log(scope.data);
                            });

                        })

                        .error(function(data, status, headers, config){
                            throw 'Ha ocurrido un error';
                        });
                });
            }
        };
    })

    .directive('metaTextfield', function () {
        return {
            restrict: 'EA',
            require: '^metaForm',
            templateUrl: "/app_dev.php/_partial/text",
            transclude: true,
            replace: true,
            scope: {
                metaTextfield: '=',
            }
        };
    })

    .directive('metaChoicefield', function() {
        return {
            restrict: 'EA',
            require: '^metaForm',
            templateUrl: "/app_dev.php/_partial/choice",
            transclude: true,
            replace: true,
            scope: {
                metaChoicefield: '='
            }
        };
    })

    ;
})();
