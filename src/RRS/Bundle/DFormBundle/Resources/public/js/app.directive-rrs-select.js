;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsSelect', ['$rootScope', 'utils', function($rootScope, utils) {

        var sort_order = function(a, b) {
            if (a.order < b.order) return 1;
            if (a.order > b.order) return -1;

            return 0;
        };

        var controller =function($scope) {
            $scope.setDomain = function(domainName, not_set_null) {
                $scope.select.domain_values = _.filter($rootScope.domains, function(d) {
                    return d.domain == domainName;
                });

                if (!not_set_null) {
                    $scope.model = null;
                }
            };

            $scope.$watch('select.domain', function(domainName, oldDomain) {

                if (oldDomain === domainName) return;

                $scope.setDomain(domainName);
            });
        };

        var rrsSelect = {
            restrict: 'A',
            require: '^rrsForm',
            scope: {
                select: '=rrsSelect',
                model: '=rrsModel'
            },
            templateUrl: utils.partial('select'),
            controller: controller,
            link: function(scope, el, attrs) {
                scope.setDomain(scope.select.domain, true);
            }
        };

        return rrsSelect;
    }]);
})();
