;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsInput', ['utils', function(utils) {

        var rrsInput = {
            restrict: 'A',
            require: '^rrsForm',
            replace: true,
            templateUrl: utils.partial('input'),
            scope: {
                input: '=rrsInput',
                model: '=rrsModel'
            },
            controller: function($scope) {
                $scope.input.id = utils.guid();
            }
        };

        return rrsInput;
    }]);
})();
