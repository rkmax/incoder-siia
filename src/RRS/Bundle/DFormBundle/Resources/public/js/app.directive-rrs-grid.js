;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsGrid', ['utils', 'builder', '$rootScope', function(utils, builder, $rootScope) {

        var rrsGrid = {
            require: '^rrsForm',
            restrict: 'A',
            scope: {
                grid: '=rrsGrid',
                model: '=rrsModel'
            },
            replace: true,
            templateUrl: utils.partial('grid'),
            link: function(scope, el, attrs) {
                var
                    $grid = $('<div>', {'class': 'pure-g-r'}),
                    cols = scope.grid.columns.length,
                    domain_values = _.filter($rootScope.domains, function(d) {
                        return d.domain == scope.grid.domain;
                    }),
                    col_autofill = _.filter(scope.grid.columns, function(c) {
                        return c.autofill === true;
                    }).map(function(c) {
                        return c.name;
                    })[0],
                    rowCount = 0,
                    addHeader = function() {


                        for (var i = 0; i < scope.grid.columns.length; i++) {
                            var $col = $('<div>', {'class': 'pure-u-1-' + cols});
                            $col.append($('<h4>', {text: scope.grid.columns[i].label}));

                            $grid.append($col);
                        }
                    },
                    addRow = function(data) {

                        for (var i = 0; i < scope.grid.columns.length; i++) {
                            var
                                $col = $('<div>', {'class': 'pure-u-1-' + cols}),
                                meta = _.extend({}, scope.grid.columns[i]),
                                _o_name = meta.name
                            ;

                            meta.name = "[" + rowCount + "]." + meta.name;

                            if (typeof scope.meta[rowCount] === "undefined") {
                                scope.meta[rowCount] = {};
                            }

                            if (_o_name === col_autofill) {
                                scope.data[rowCount] = {};
                                scope.data[rowCount][_o_name] = data.value;
                            }

                            scope.meta[rowCount][_o_name] = meta;
                            $col.append(builder.build([meta], scope, null, 'meta', 'data'));

                            $grid.append($col);
                        }
                        rowCount++;
                    }
                ;

                scope.meta = [];
                scope.data = [];

                addHeader();

                for (var j = 0; j < domain_values.length; j++) {
                    addRow(domain_values[j]);
                }

                el.append($grid);
            },
            controller: function($scope) {
                $scope.$watch('data', function(newVal, oldVal) {
                    if (JSON.stringify(newVal) === JSON.stringify(oldVal)) return;

                    $scope.model = newVal;

                }, true);
            }
        };

        return rrsGrid;
    }]);
})();
