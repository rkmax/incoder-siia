;(function() {
    "use strict";

    angular.module('app.factory')

    .factory('builder', ['$http', '$compile', function($http, $compile) {

        var build = function(root, elements, metainfo, datainfo) {

            if (elements.length === 0) return root;

            var
                element = elements.shift(),
                _meta = metainfo + element.name,
                _data = datainfo + element.name,
                _config = {}
            ;

            _config['data-rrs-' + element.class] = _meta;
            _config['data-rrs-model'] = _data;

            root.append($('<div>', _config));

            switch(element.class) {
                case 'input':
                    break;
                case 'fieldset':
                    elements = _.filter(elements, function(el) {return el.section !== element.name;});
                    break;
                case 'button':
                    break;
                case 'select':
                    break;
                case 'checkbox':
                    break;
                case 'list':
                    break;
                case 'grid':
                    break;
                case 'label':
                    break;
                default:
                    console.error("Ignoring " + element.name + " '" + element.class + "' class is not implemented yet!");
                    break;
            }

            return build(root, elements, metainfo, datainfo);
        };

        return {
            init: function(url) {

            },
            build_elements: function(els, scope, root) {
                return this.build(els, scope, root, 'meta.', 'data.');
            },
            build_actions: function(els, scope, root) {
                return this.build(els, scope, root, 'actions.', 'data.');
            },
            build: function(els, scope, root, metainfo, datainfo) {
                if (typeof root === "undefined" || root === null) {
                    root = $('<div>');
                }

                if (typeof metainfo === "undefined" || metainfo === null) {
                    throw 'metainfo must be defined';
                }

                if (typeof datainfo === "undefined" || datainfo === null) {
                    throw 'datainfo must be defined';
                }

                return ($compile(build(root, els, metainfo, datainfo))(scope));
            }
        };
    }]);
})();
