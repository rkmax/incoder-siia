;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsForm', ['$http', 'builder', '$rootScope', 'utils', 'rulemanager', function($http, builder, $rootScope, utils, rulemanager) {

        var sort_order = function(a) {
            return -a.order;
        };

        var link = function (scope, el, attrs) {

            // Al cambiar el proveedor el formulario se solicita e inicia el proceso
            // de construcción
            scope.$watch('rrsForm', function (url) {
                $http({url: url, method: 'GET'})
                    .success(function(data, status, headers, config) {

                        // Los datos que guarde el usuario van aqui
                        $rootScope.data = {};
                        if (data.data) {
                            scope.remote_data = data.data.data;
                        } else {
                            scope.remote_data = {};
                        }

                        // La informacion de los formularios va aqui
                        $rootScope.meta = {};
                        $rootScope.actions = {};
                        $rootScope.domains = data.domain;
                        $rootScope.rules = data.form.rules || [];
                        $rootScope.form = data.form || {};

                        var i;

                        var _els = _(data.form.elements).sortBy(sort_order);
                        for (i = _els.length - 1; i >= 0; i--) {
                            var _el = _els[i];
                            _el.html_name = attrs['formName'] + '[data][' + _el.name + ']';
                            $rootScope.meta[_el.name] = _el;
                        }

                        // Construimos el formulario
                        scope.title = $rootScope.form.label;
                        var elements = _($rootScope.meta).filter(function(el) {
                            return el.section === $rootScope.form.name;
                        });

                        el.append(builder.build_elements(elements, $rootScope));

                        rulemanager.init($rootScope.rules);

                        // Carga la data
                        if (data.data) {
                            _.each(data.data.data, function (v, k) {
                                // if (!_.isEmpty(v)) console.log(k, ":", "'" + v + "'");
                                $rootScope.data[k] = v;
                            });
                        }
                    })
                    .error(function(data, status, headers, config) {
                        // algo ha pasado con el servidor (404, 500, 401, etc)
                        // ...
                    })
                ;
            });
        };

        var rrsForm = {
            restrict: 'A',
            scope: {
                rrsForm: '@'
            },
            replace: true,
            // templateUrl: utils.partial('form'),
            link: link,
            controller: function($scope) {}
        };

        return rrsForm;
    }]);
})();
