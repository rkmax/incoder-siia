;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsLabel', ['utils', function(utils) {

        var rrsLabel = {
            restrict: 'A',
            require: '^rrsForm',
            replace: true,
            templateUrl: utils.partial('label'),
            scope: {
                label: '=rrsLabel',
                model: '=rrsModel'
            },
            controller: function($scope) {
                $scope.label.id = utils.guid();
            }
        };

        return rrsLabel;
    }]);
})();
