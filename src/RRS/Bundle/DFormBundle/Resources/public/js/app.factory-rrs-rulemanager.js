;(function() {
    "use strict";

    angular.module('app.factory')

    .factory('rulemanager', ['$rootScope', 'ruleparser', function($rootScope, ruleparser) {

        var OPERATOR = {
            "==": function(L, R) {
                return JSON.stringify(L) === JSON.stringify(R);
            },
            ">=": function(L, R) {
                return L >= R;
            },
            "<=": function(L, R) {
                return L <= R;
            },
            "!=": function(L, R) {
                return L != R;
            },
            "HAS": function(L, R) {
                return _.contains(L, R);
            }
        };

        var ACTIONS = {
            ASSIGN: function(L, R) {
                setValue(L, getValue(R));
            }
        };

        var EVENT = function(result) {
            if (!_.isObject(result.EVENT)) throw 'BAD PATH';

            return true;
        };

        var CONDITION = function(result) {
            if (!_.isObject(result.CONDITION.L) && !_.isObject(result.CONDITION.R)) throw 'BAD CONDITION';

            var
                L = getValue(result.CONDITION.L),
                R = getValue(result.CONDITION.R)
            ;

            return OPERATOR[result.CONDITION.OPERATOR](L, R);
        };

        var getValue = function(value) {
            if (_.isArray(value)) {
                return value;
            } else if (_.isObject(value)) {
                var value_info;
                try {
                    value_info = value.PATH.split('.');
                } catch (e) {
                    throw 'No se pudo realizar split() en "' + JSON.stringify(value) +'"';
                }

                if (typeof value_info[1] === "undefined") {
                    throw 'No implementado el soporte de valores genericos *.nombre_propiedad';
                } else {
                    if ( value_info[1] === 'value')  {
                        return $rootScope.data[value_info[0]];
                    } else {
                        return $rootScope.meta[value_info[0]][value_info[1]];
                    }
                }

            } else { return value; }
        };

        var setValue = function(member, value) {
            if (_.isObject(member)) {
                var member_info = member.PATH.split('.');
                if ( member_info[1] === 'value')  {
                    $rootScope.data[member_info[0]] = value;
                } else {
                    if (typeof $rootScope.meta[member_info[0]] === "undefined") {
                        throw 'El campo "' +  member_info[0] + '" no existe en meta';
                    } else {
                        $rootScope.meta[member_info[0]][member_info[1]] = value;
                    }

                }
            } else {
                throw 'Bad MEMBER';
            }
        };

        var execute = function(result) {
            if (!result.ACTION) throw 'NO ACTION';

            if (result.EVENT) {
                $rootScope.$watch(function() {
                    return getValue(result.EVENT);
                }, function(value) {
                    ACTIONS[result.ACTION.NAME](result.ACTION.L, result.ACTION.R);
                });
            }

            if (result.CONDITION) {
                $rootScope.$watch(function() {
                    return CONDITION(result);
                }, function(cond) {
                    if (cond) {
                        ACTIONS[result.ACTION.NAME](result.ACTION.L, result.ACTION.R);
                    }
                });
            }
        };

        var init = function(rules) {
            var results = ruleparser(rules);
            for (var i = results.length - 1; i >= 0; i--) {
                execute(results[i]);
            }
        };

        return {
            init: function(rules) {
                init(rules);
            }
        };
    }]);
})();
