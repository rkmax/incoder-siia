/**
 * Punto de entrada principal de la aplicación
 *
 * Configuraciones aqui
 */

;(function(window) {
    "use strict";

    $(document).find('[data-provide="datepicker"]')
        .datepicker({
            language: 'es',
            autoclose: true
        })
        .on('keydown', function(event) {
            if (event.keyIdentifier == "Down") {
                event.preventDefault()
            }
        }, false);

    $(document).find('input[type="date"]').attr({
        'type': 'text',
        'placeholder': 'dd/mm/yyyy'
    });

    var setClasses = function() {
        $('input[type="password"], input[type="email"], input[type="number"], input[type="text"], input[type="date"], select, textarea')
            .addClass('form-control');
    };

    $(document).find('[data-role="tagsinput"]').each(function(k, v) {
        var $el = $(v),
            autocomplete = $el.data().autocomplete;

        if (typeof autocomplete !== 'undefined') {
            $el.tagsinput({
                typeahead: SIIACAutocomplete($el)
            })
        } else {
            $el.tagsinput();
        }
    });

    $(function() {
        setClasses();

        $('[data-action]').on('click', function(e) {
            e.preventDefault();

            var fun = $(this).data().action;


            if (typeof window[fun] === 'function') {
                window[fun]();
            } else {
                throw 'Function "' + fun + "\" isn't defined!";
            }

        });
    })

    window.App = {
        setClasses: setClasses
    };

    $('form').submit(function() {

        var updateStatus = function(searchByText) {
            var $el = $('button:contains("' + searchByText + '")');
            $el.attr("disabled", "disabled");
            $el.text("Un momento por favor...");
        };

        updateStatus('Guardar');
        updateStatus('Actualizar');
    });

})(window);
