;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsCheckbox', ['utils', function(utils) {

        var rrsCheckbox = {
            restrict: 'A',
            require: '^rrsForm',
            scope: {
                checkbox: '=rrsCheckbox',
                model: '=rrsModel'
            },
            replace: true,
            controller: function($scope) {
                $scope.checkbox.id = utils.guid();
                $scope.$watch('checkbox.checked', function(isChecked) {
                    $scope.model = $scope.checkbox.label;
                });
            },
            templateUrl: utils.partial('checkbox')
        };

        return rrsCheckbox;
    }]);
})();
