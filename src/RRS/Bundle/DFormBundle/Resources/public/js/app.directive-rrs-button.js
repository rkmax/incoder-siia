;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsButton', ['utils', function(utils) {

        var rrsButton = {
            require: '^rrsForm',
            replace: true,
            templateUrl: utils.partial('button'),
            scope: {
                button: '=rrsButton'
            }
        };

        return rrsButton;
    }]);
})();
