;(function() {
    "use strict";

    angular.module('app.directive')

    .directive('rrsFieldset', ['builder', '$rootScope', 'utils', function(builder, $rootScope, utils) {

        var link = function(scope, el, attrs) {

            var sort_order = function(a, b) {
                if (a.order < b.order) return -1;
                if (a.order > b.order) return 1;
                return 0;
            };

            var filter = function(el) {
                return el.section === scope.fieldset.name;
            };

            var elements = _($rootScope.meta).filter(filter).sort(sort_order);

            el.append(builder.build_elements(elements, $rootScope));
        };

        var rrsFieldset = {
            restrict: 'A',
            require: '^rrsForm',
            replace: true,
            scope: {
                fieldset: '=rrsFieldset',
                model: '=rrsModel'
            },
            templateUrl: utils.partial('fieldset'),
            link: link,
            controller: function($scope) {}
        };

        return rrsFieldset;
    }]);
})();
