;(function() {
    "use strict";

    angular.module('app.factory')

    .factory('ruleparser', function() {
        return function(rules) {

            if (typeof DFORMS_ruleparser === "undefined") {
                throw 'Function ruleparser from Module DFORMS is not defined!';
            }

            return _.map(rules, function(rule) { return DFORMS_ruleparser.parse(rule);});
        };
    });
})();
