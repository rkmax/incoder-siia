// Autocomplete

String.prototype.supplant = function (o) {
    return this.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
};

;(function(window) {

    var autocomplete = function($input) {
        var ATTR_AUTOCOMPLETE = 'data-autocomplete',
            ATTR_TYPE = "name",
            BASE = (ENV === 'dev' ? '/app_dev.php' : ''),
            MASK = '{{GROUP}}',
            URL_API = BASE + '/auto/_/' + MASK,
            DOFA_URL_API = BASE + '/auto/dofa/_/',
            LOCAL_URL_API = BASE + '/auto/places/_/',
            TMPL_DESCRIPTION = "<b>{nombre_centro_poblado}</b> - {nombre_municipio}<br>{nombre_departamento} - <small>{nombre_tipo}</small>",
            typeahead_source,
            map = {};

        switch ($input.attr(ATTR_AUTOCOMPLETE)) {
            case 'dofa':
                var type = $input.attr(ATTR_TYPE).match(/dofa_actividad\[(\w+)\]/)[1],
                    dofa = {};
                typeahead_source = {
                    source: function (query, process) {
                        $.getJSON(DOFA_URL_API, {'query': query, 'type': type}, function(results, status) {
                            if (status !== 'success') return;
                            process($.map(results, function (dofa_el) {
                                dofa[dofa_el.descripcion] = dofa_el;
                                return dofa_el.descripcion;
                            }));
                        });
                    },
                    updater: function(item) {
                        $input.parents('li').find('input[type="hidden"]').val(dofa[item].id);
                        return dofa[item].descripcion;
                    }
                };
                break;
            case 'localizacion':
                typeahead_source = {
                    minLength: 3,
                    source: function (query, process) {
                        $.getJSON(LOCAL_URL_API, {'query': query}, function(results, status) {
                            if (status !== 'success') return;

                            process($.map(results, function (el) {
                                var descripcion = TMPL_DESCRIPTION.supplant(el);
                                map[descripcion] = el;

                                return descripcion;
                            }));
                        });
                    },
                    matcher: function(item) {
                        return true;
                    },
                    updater: function(item) {
                        var el = map[item],
                            $municipio = $('[name*="[municipio]"]'),
                            $departamento = $('[name*="[departamento]"]'),
                            $cp = $('[name*="[centro_poblado]"]');

                        $municipio.val(el.nombre_municipio);
                        $departamento.val(el.nombre_departamento);
                        $cp.val(el.nombre_centro_poblado);

                        return el.descripcion;
                    }
                };
                break;
            default:
                typeahead_source = {
                    source: function (query, process) {
                        if (typeof process !== 'undefined') {
                            $.getJSON(
                                URL_API.replace(MASK, $input.attr(ATTR_AUTOCOMPLETE)),
                                {'query': query},
                                function(results, status) {
                                    if (status !== 'success') return;

                                    process(results);
                                }
                            );
                            return null;
                        } else
                            return $.getJSON(url, {query: query}).promise();
                    }
                };
                break;
        }

        return typeahead_source;
    };

    var enableAutocomplete = function (v) {
        var $el = $(v),
            role = $el.data().role;

        if (!(typeof role !== 'undefined' && role == 'tagsinput')) {
            $el.typeahead(autocomplete($el));
        }
    }

    $(function() {
        $(document).find('[data-autocomplete]').each(function(k, v) {
            enableAutocomplete(v);
        });
    });

    window.SIIACAutocomplete = autocomplete;
    window.SIIACEnableAutocomplete = enableAutocomplete;
})(window);
