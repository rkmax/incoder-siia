;(function() {
    "use strict";

    angular.module('app.factory')

    .factory('utils', function() {

        var env = function() {
            return ENV === 'dev' ? '/app_dev.php' : '';
        }
        var s4 = function() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        };

        var guid = function() {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        };

        return {
            guid: guid,
            partial: function(partialName) {
                return (env() + '/_dev/_partial/' + partialName + '.html');
            }
        };
    });
})();
