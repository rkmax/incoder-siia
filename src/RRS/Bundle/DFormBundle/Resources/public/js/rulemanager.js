;(function($) {
    "use strict";

    var CONST = {
        VISIBILITY: 'data-visibility',
        EDITABILITY: 'data-editable'
    };

    var OPERATOR = {
        "==": function(L, R) {
            var upper = function(el) { return el.toUpperCase(); };

            if (_.isArray(L)) {
                return _.contains(_.map(L, upper), R.toUpperCase());
            }
            return JSON.stringify(L).toUpperCase() === JSON.stringify(R).toUpperCase();
        },
        ">=": function(L, R) {
            return L >= R;
        },
        "<=": function(L, R) {
            return L <= R;
        },
        "<>": function(L, R) {
            return !(this['=='](L, R));
        },
        "!=": function(L, R) {
            return L != R;
        },
        "HAS": function(L, R) {
            return _.contains(L, R);
        },
        "NOT_HAS": function(L, R) {
            return ! _.contains(L, R);
        }
    };

    var ACTIONS = {
        ASSIGN: function(L, R) {
            setValue(L, R);
        }
    };

    var WATCH = function(result, callback) {

        var $field = $('.field-' + result.CONDITION.L.PATH),
            handler = function handler() {
                var LVALUE = $field.val();
                callback(OPERATOR[result.CONDITION.OPERATOR](LVALUE, result.CONDITION.R));
            };

        if ($field.size() == 0) {
            throw 'El campo "'+ result.CONDITION.L.PATH + '" no existe!';
        } else if ($field.is('input') || $field.is('select')) {
            handler();
            $field.on('change', handler);
        } else {

            handler = function handler() {
                var $checkboxs = $field.find('input[type="checkbox"]:checked'),
                    LVALUE = [];

                $checkboxs.each(function(k, v) {
                    LVALUE.push($(v).val());
                });

                callback(OPERATOR[result.CONDITION.OPERATOR](LVALUE, result.CONDITION.R));
            };

            handler();
            $field.find('input[type="checkbox"]').on('change', handler);
        }
    };

    var COMMIT_EDITABILITY = function () {
        $('[' + CONST.EDITABILITY+ ']').map(function(k, v) {
            switch ($(v).attr(CONST.EDITABILITY)) {
                case '1':
                    $(v).prop('readonly', false);
                    break;
                case '0':
                    $(v).prop('readonly', true);
                    break;
            }
        });
    };

    var COMMIT_VISIBILITY = function() {
        $('[' + CONST.VISIBILITY + ']').map(function(k, v) {
            var $hideme = null,
                $nullme = null;

            if ($(v).is('input') || $(v).is('select')) {
                // TODO: Se debe definir que hacer con los valores de los campos ocultos
                //$nullme = $(v);
                $hideme = $(v).parent().parent();
            } else {
                $hideme = $(v);
            }

            switch ($(v).attr(CONST.VISIBILITY)) {
                case '1':
                    $hideme.fadeIn();
                    break;
                case '0':
                    $hideme.fadeOut(function() {
                        if (null !== $nullme) $nullme.val(null);
                    });
                    break;
            }
        });
    };

    var COMMIT = function() {
        COMMIT_VISIBILITY();
        COMMIT_EDITABILITY();
    };

    var setVisibility = function(path, value) {
        var fullpath = '.fieldset-' + path + ', .field-' + path,
            visibility = ['true', 1, true].indexOf(value) !== -1 ? 1 : 0;
        $(fullpath).attr(CONST.VISIBILITY, visibility);
        COMMIT_VISIBILITY();
    };

    var setValue = function(member, value) {
        if (_.isObject(member)) {
            var splitted = member.PATH.split('.');
            switch (splitted[1]) {
                case 'visible':
                    setVisibility(splitted[0], value);
                    break;
                default:
                    throw "Member '" + splitted[1] + "' on [" + splitted[0] + "] doesn't exist!";
                    break;
            }
        } else {
            throw 'Bad MEMBER';
        }
    };

    var execute = function(result) {
        if (!result.ACTION) throw 'NO ACTION';

        if (result.CONDITION) {
            WATCH(result, function(isTrue) {
                if (isTrue) {
                    ACTIONS[result.ACTION.NAME](result.ACTION.L, result.ACTION.R);
                }
            });
        }
    };

    $(function() {
        var $rules = $('.rule'),
            ruleLength = $rules.length;

        if (ruleLength > 0) {
            if (ENV == 'dev') console.log("rulemanager ", ruleLength);
        }

        $rules.each(function(k, ruleContainer) {
            var rule = $(ruleContainer).attr('data-rule'),
                result = DFORMS_ruleparser.parse(rule);
            execute(result);
            $(ruleContainer).remove();

            if (k == ruleLength - 1) COMMIT();
        })
    });

})(jQuery);
