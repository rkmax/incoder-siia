<?php

namespace RRS\Bundle\DFormBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\Common\Util\Inflector;

use RRS\Bundle\DFormBundle\Document;
use Incoder\Bundle\SiiaBundle\Entity\DForm;


class LoadRandomDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('data:random')
            ->setDescription("Carga datos random")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $faker = $this->getContainer()->get('davidbadura_faker.faker');


        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();

        for ($i = 0; $i < 10000; $i++) {
            $data = array(
                '__type' => 'asociado',
                "edad_asociado" => $faker->edad,
                "leer_escribir" => $faker->siNo,
                "departamento" => $faker->departamento,
                "municipio" => $faker->municipio,
                "nombre_asociado" => $faker->firstName,
                "apellidos_asociado" => $faker->lastName,
                "documento_identidad" => $faker->randomNumber(11),
                "tipo_documento" => $faker->tipoDocumento,
                "genero_asociado" => $faker->genero,
                "cuantos_hijos" => $faker->randomNumber(1),
                "rango_menor_6" => $faker->randomNumber(1),
                "rango_entre_6_15" => $faker->randomNumber(1),
                "rango_entre_15_18" => $faker->randomNumber(1),
                "actividad_tiempo" => $faker->actividadTiempo,
                "actividad_economica" => $faker->actividadTiempo,
                "familia_vive" => $faker->familiaVive,
                "estado_civil" => $faker->estadoCivil
            );

            $t = new Document\TransactionData();
            $t->setData($data);
            $dm->persist($t);
        }

        $dm->flush();
    }
}
