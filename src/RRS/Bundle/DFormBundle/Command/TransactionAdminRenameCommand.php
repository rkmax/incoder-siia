<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransactionAdminRenameCommand extends ContainerAwareCommand
{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var TransactionService
     */
    private $ts;

    /**
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('transaction:admin:rename')
            ->setDescription("Renombra las transacciones según su tipo")
        ;
    }

    private function init()
    {
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init();

        foreach ($this->getEntities() as $entityClass) {
            $entities = $this->em->getRepository($entityClass)->findAll();

            /** @var DFormEntity $entity */
            foreach ($entities as $entity) {
                $form = $this->ts->getBaseTransaction($entity);
                $t = $this->dm->find(SimpleTransaction::CLASS, $entity->getSlug());
                $t->setType($form->getName());
                $this->dm->persist($t);
            }
        }


        $this->dm->flush();
    }

    private function getEntities()
    {
        return [
            'asociacion' => 'Incoder\Bundle\SiiaBundle\Entity\Asociacion',
            'asociado' => 'Incoder\Bundle\SiiaBundle\Entity\Asociado',
            'diagnostico_tecnico' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico',
            'perfil_socioeconomico' => 'Incoder\Bundle\SiiaBundle\Entity\PerfilSocioEconomico',
            'cuadro_agricola' => 'Incoder\Bundle\SiiaBundle\Entity\CuadroAgricola',
            'cuadro_pecuario' => 'Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario',
        ];
    }
}