<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use JulianReyes\Lib\ParseUtils;
use RRS\Bundle\DFormBundle\Document\Domain;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Document\Form;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SyncDomainsTransactionsCommand extends ContainerAwareCommand
{
    const levenshtein = 'levenshtein';
    const TYPE = 'type';

    /** @var  DocumentManager */
    private $dm;

    /** @var  TransactionService */
    private $ts;

    /** @var  TwigEngine */
    private $templating;

    private $message;

    protected function configure()
    {
        $this
            ->setName('domain:admin:sync')
            ->setDescription("Encuentra dominios similares y los agrupa")
            ->addArgument(self::TYPE, InputArgument::OPTIONAL, "Tipo de transaccion", null)
            ->addOption(
                self::levenshtein,
                'l',
                InputOption::VALUE_REQUIRED,
                "Define la distancia levenshtein maxima",
                0
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $domains = $this->dm->getRepository(Domain::CLASS)->findAll();
        $total = count($domains);

        /** @var ProgressHelper $progress */
        $progress = $this->getHelperSet()->get('progress');
        $progress->setBarCharacter('<comment>=</comment>');
        $progress->setEmptyBarCharacter(' ');
        $progress->setProgressCharacter('|');
        $progress->setRedrawFrequency(500);
        $progress->start($output, $total);

        $type = $input->getArgument(self::TYPE);
        if ($type) { //Obtiene el formulario del tipo parametrizado
            $forms = $this->dm->getRepository(Form::CLASS)->findBy(array('name' => $type));
        }
        /* @var Domain $domain */
        foreach ($domains as $keyd => $domain) {

            $domainValue = $domain->getValue();
            //Sino envian el tipo se buscan todas las transacciones de los formularios que hagan uso de dicho dominio
            if (!$type) {
                //TODO: filtrar solo los dominios que usen el dominio
                //$forms = $this->dm->getRepository(Form::CLASS)->findAllByDomain($domain->getDomain());
                $forms = $this->dm->getRepository(Form::CLASS)->findAll();
            }
            /* Para los formularios se revisan los elementos que usan el dominio */
            /* @var Form $form */
            foreach ($forms as $fkey => $form) {

                /* @var Element $felement */
                foreach ($form->getElements() as $fekey => $felement) {

                    if ($felement->getDomain() && $felement->getDomain() == $domain->getDomain()) {

                        $elementName = $felement->getName();
                        //Encuentra todas las transacciones del tipo del formulario
                        //TODO: filtrar tambien todas las transacciones que tengan datos del elemento tambien
                        $trns = $this->dm->getRepository(SimpleTransaction::CLASS)
                            ->findBy(['type' => $form->getName()]);

                        /* @var SimpleTransaction $trn */
                        foreach ($trns as $tkey => $trn) {

                            $data = $trn->getData();
                            if ($data && isset($data[$elementName])) {

                                $trnDataValue = $data[$elementName];
                                //si la respuesta esta en un array se procesan todos los valores de este
                                if(is_array($trnDataValue)){

                                    foreach($trnDataValue as $tdkey => $tdvalue){

                                        $lev = ParseUtils::unsensibleLevenshteinCompare($tdvalue, $domainValue);
                                        //Si encuentra el valor seleccionado realiza el cambio y se sale para continuar con otra transaccion
                                        if ($lev == 0 && $domainValue != $tdvalue) {

                                            $output->writeln("Form: {$form->getName()}, Element: {$elementName} , Transaction: {$trn->getId()} , Value: [{$tdkey}]{$tdvalue}, Replaced with domain: [{$domain->getDomain()}]{$domainValue}");
                                            $trnDataValue[$tdkey] = $domainValue;
                                            $data = $this->persistNewData($trnDataValue, $data, $elementName, $trn);
                                            break;
                                        }
                                    }
                                }else{

                                    //Obtenemos el valor respondido en la transacción
                                    $lev = ParseUtils::unsensibleLevenshteinCompare($trnDataValue, $domainValue);
                                    //si encontramos un valor parecido lo reemplazamos y actualizamos la transacción
                                    if ($lev == 0 && $domainValue != $trnDataValue) {

                                        $output->writeln("Form: {$form->getName()}, Element: {$elementName} , Transaction: {$trn->getId()} , Value: {$trnDataValue}, Replaced with domain: [{$domain->getDomain()}]{$domainValue}");
                                        $data = $this->persistNewData($domainValue, $data, $elementName, $trn);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $progress->advance();
        }
        $progress->finish();
        $this->dm->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->templating = $this->getContainer()->get('templating');
    }

    /**
     * extrae en un array todos los nombres de los formularios
     * @param $forms
     * @return array
     */
    protected function getFormsNames($forms)
    {
        $types = [];
        /* @var Form $form */
        foreach ($forms as $fkey => $form) {
            $types[] = $form->getName();
        }
        return $types;
    }

    /**
     * @param $nuevaRespuesta
     * @param $respuestas
     * @param $pregunta
     * @param $trn
     * @return mixed
     */
    protected function persistNewData($nuevaRespuesta, $respuestas, $pregunta, $trn)
    {
        $respuestas[$pregunta] = $nuevaRespuesta;
        $trn->setData($respuestas);
        $this->dm->persist($trn);
        return $respuestas;
    }

}