<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use RRS\Bundle\DFormBundle\Document\Transaction;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransactionAdminCleanCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var TransactionService
     */
    private $ts;

    protected function configure()
    {
        $this
            ->setName('transaction:admin:clean')
            ->setDescription("Lee los formularios en yaml y los importa en la base de datos")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init();

        $slugs = [];
        $raw_slugs = [];

        $message = "<info>{$this->getName()}</info> %s";

        foreach ($this->getEntities() as $entityClass)
        {
            $iterator = $this->em
                ->getRepository($entityClass)
                ->createQueryBuilder('e')
                ->getQuery()
                ->iterate();

            $output->write(sprintf($message, "Getting  - {$entityClass} ..."));

            $current_slugs = [];

            // el slug del formulario base
            $tbase = $this->ts->getBaseTransaction($entityClass);
            $current_slugs[] = $tbase->getId();

            /** @var DFormEntity $entity */
            foreach ($iterator as $row)
            {
                $entity = $row[0];
                $current_slugs[] = new \MongoId($entity->getSlug());
                $raw_slugs[] = $entity->getSlug();
            }

            $current_slugs_count = count($current_slugs);
            $output->writeln("{$current_slugs_count}");

            $slugs = array_merge($slugs, $current_slugs);

            $this->em->clear($entityClass);
        }

        $slug_counter = count($slugs);
        $output->writeln(sprintf($message, "{$slug_counter} slugs founded!"));

        $iterator = $this->dm->getRepository(Transaction::CLASS)
            ->createQueryBuilder()
            ->field('_id')->notIn($slugs)
            ->getQuery()->getIterator();

        /** @var Transaction $transaccion */
        foreach ($iterator as $transaccion) {
            $this->dm->remove($transaccion);
            if ($transaccion->getData()) {
                $this->dm->remove($transaccion->getData());
            }
        }

        $this->dm->flush();
    }

    private function init()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
    }

    private function getEntities()
    {
        return [
            'asociacion' => 'Incoder\Bundle\SiiaBundle\Entity\Asociacion',
            'asociado' => 'Incoder\Bundle\SiiaBundle\Entity\Asociado',
            'diagnostico_tecnico' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico',
            'perfil_socioeconomico' => 'Incoder\Bundle\SiiaBundle\Entity\PerfilSocioEconomico',
            'cuadro_agricola' => 'Incoder\Bundle\SiiaBundle\Entity\CuadroAgricola',
            'cuadro_pecuario' => 'Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario',
        ];
    }
} 