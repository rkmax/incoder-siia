<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use RRS\Bundle\DFormBundle\Document\Domain;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DomainAdminCompactCommand extends ContainerAwareCommand
{
    /** @var  DocumentManager */
    private $dm;

    /** @var  TransactionService */
    private $ts;

    /** @var  TwigEngine */
    private $templating;

    private $message;

    const type = 'type';
    const prop = 'prop';
    const where = 'where';
    const set = 'set-value';
    const replace = 'update';
    const delete = 'delete';

    protected function configure()
    {
        $this
            ->setName('domain:admin:compact')
            ->setDescription("Compata dominios")
            ->addOption(self::type, 't', InputOption::VALUE_REQUIRED, "Tipo", null)
            ->addOption(self::prop, 'p', InputOption::VALUE_REQUIRED, "Property", null)
            ->addOption(self::where, 'w', InputOption::VALUE_REQUIRED, "Where", null)
            ->addOption(self::set, 'S', InputOption::VALUE_REQUIRED, "Set value", null)
            ->addOption(self::replace, null, InputOption::VALUE_NONE, "Reemplaza", null)
            ->addOption(self::delete, null, InputOption::VALUE_NONE, "Elimina el dominio reemplazado", null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pipeline = [];
        $type = $input->getOption(self::type);
        $prop = $input->getOption(self::prop);
        $where = $input->getOption(self::where);
        $set = $input->getOption(self::set);

        if ($set && $where && $type && $prop) {

            $element = $this->getElement($type, $prop);

            if (!$element) {
                throw new \Exception("No se encontro el elemento {$type}.{$prop}");
            }

            $output->writeln("Actualizando elemento tipo <info>{$element->getClass()}</info>...");

            if ($element->getClass() == 'list') {
                $this->updateListProperty($type, $prop, $set, $where);
            } else {
                $this->updateProperty($type, $prop, $set, $where);
            }

            if ($input->getOption(self::replace)) {
                $output->writeln("Reemplazando <info>{$element->getDomain()}</info> {$where} -> {$set}");
                $this->updateDomain($element->getDomain(), $where, $set);
            } else if ($input->getOption(self::delete)) {
                $output->writeln("Eliminando [<error>-</error>] <info>{$element->getDomain()}</info>: {$where}");
                $this->deleteDomain($element->getDomain(), $where);
            }
        } else {
            throw new \Exception(sprintf(
                "Las opciones %s %s",
                implode(", ", [self::type, self::prop, self::where, self::set]),
                "son obligatorias"
            ));
        }
    }

    public function updateDomain($domain, $value, $newValue)
    {
        if (!$domain) {
            throw new \Exception("EL nombre del dominio no puede ser vacio");
        }

        $this->dm->getRepository(Domain::CLASS)
            ->createQueryBuilder()
            ->update()
            ->field('domain')->equals($domain)
            ->field('value')->equals($value)
            ->field('value')->set($newValue)
            ->getQuery()->execute()
        ;
    }


    /**
     * @param $domain
     * @param $value
     */
    private function deleteDomain($domain, $value)
    {
        $this->dm->getRepository(Domain::CLASS)
            ->createQueryBuilder()
            ->remove()
            ->field('domain')->equals($domain)
            ->field('value')->equals($value)
            ->getQuery()->execute()
        ;
    }

    private function updateProperty($type, $prop, $set, $where)
    {
        $this->dm->getRepository(SimpleTransaction::CLASS)
            ->createQueryBuilder()
            ->update()
            ->multiple(true)
            ->field("data.{$prop}")->set($set)
            ->field("data.{$prop}")->equals($where)
            ->field("type")->equals($type)
            ->getQuery()->execute()
        ;
    }

    private function updateListProperty($type, $prop, $set, $where)
    {
        // Agrega el valor $set al array
        $this->dm->getRepository(SimpleTransaction::CLASS)
            ->createQueryBuilder()
            ->update()
            ->multiple(true)
            ->field("data.{$prop}")->addToSet($set)
            ->field("data.{$prop}")->in([$where])
            ->field("type")->equals($type)
            ->getQuery()->execute()
        ;

        // Elimina el valor $where del array
        $this->dm->getRepository(SimpleTransaction::CLASS)
            ->createQueryBuilder()
            ->update()
            ->multiple(true)
            ->field("data.{$prop}")->pull($where)
            ->field("data.{$prop}")->in([$where])
            ->field("type")->equals($type)
            ->getQuery()->execute()
        ;
    }


    /**
     * @param $type
     * @param $prop
     * @param callable $func
     * @return null|Element|\Closure
     * @throws \Exception
     */
    private function getElement($type, $prop, \Closure $func = null)
    {
        $form = $this->ts->getForm($type);

        if (!$form) {
            throw new \Exception("La propidad {$prop} no existe en {$type}");
        }

        /** @var Element $el */
        $el = $form->getElements()
            ->filter(function(Element $e) use ($prop) {
                return $e->getName() == $prop;
            })
            ->first();

        if (!$el) {
            return null;
        }

        if ($func) {
            return $func($el);
        }

        return $el;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->templating = $this->getContainer()->get('templating');
    }
}