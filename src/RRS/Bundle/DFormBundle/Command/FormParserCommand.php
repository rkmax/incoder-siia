<?php

namespace RRS\Bundle\DFormBundle\Command;

use Doctrine\Common\Util\Inflector;
use Doctrine\ODM\MongoDB\DocumentManager;
use RRS\Bundle\DFormBundle\Document;
use RRS\Bundle\DFormBundle\Document\Domain;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;


class FormParserCommand extends ContainerAwareCommand
{
    /**
     * @var DocumentManager
     */
    private $dm;

    protected function configure()
    {
        $this
            ->setName('form:parser')
            ->setDescription("Lee los formularios en yaml y los importa en la base de datos")
            ->addArgument('file', InputArgument::REQUIRED, "Archivo a parsear", null)
            ->addArgument('type', InputArgument::REQUIRED, "Tipo de objeto", null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('file');
        $type = $input->getArgument('type');

        if (!file_exists($path)) {
            $output->writeln("<error>El archivo '$path' no existe</error>");
        }

        $this->dm = $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        $data = Yaml::parse(file_get_contents($path));

        $formName = $data['form']['name'];

        // Dominios
        /** @var \RRS\Bundle\DFormBundle\Repository\Domain $dr */
        $dr = $this->dm->getRepository('DFormBundle:Domain');

        $previousDomains = $dr->findAll();
        $domainCount = count($dr->findAll());

        $domains = array_map(function(Domain $d) {
            return "{$d->getDomain()}:{$d->getValue()}";
        }, $previousDomains);

        $output->writeln("<info>Encontrados {$domainCount} dominios</info>");

        foreach ($data['domains'] as $value) {

            $domainName = trim($value['domain']);
            $domainValue = $this->mb_ucfirst(mb_strtolower(trim($value['value']), 'UTF-8'));

            if (!in_array("{$domainName}:{$domainValue}", $domains)) {

                $domain = new Document\Domain();
                $domain->setDomain($domainName);
                $domain->setValue($domainValue);
                $this->dm->persist($domain);
                $output->writeln("Agregando {$domain->getValue()} -> {$domain->getDomain()}");
            }
        }

        $this->dm->flush();

        /** @var Document\Transaction $transaction */
        $transaction = new Document\Transaction();
        $transaction->setType($type);
        $form = $this->fillForm(new Document\Form(), $data);
        $transaction->setForm($form);

        $dm->persist($form);
        $dm->persist($transaction);
        $dm->flush();

        if (!$this->getContainer()->get('d_form.transaction')->getForm($formName)) {
            $output->writeln('<error>La transacción no se guardó correctamente</error>');
        } else {
            $output->writeln('<info>El formulario se creo correctamente</info>');
        }
    }

    /**
     * @param Document\Form $form
     * @param array $data
     *
     * @return mixed
     */
    public function fillForm($form, $data)
    {
        $form->setName($data['form']['name']);
        $form->setLabel($data['form']['label']);

        foreach ($data['elements'] as $el) {
            $element = $this->buildElement($el);
            $form->addElement($element);
        }

        $form->setRules($data['rules']);

        return $form;
    }

    private function buildElement($data)
    {
        $element = new Document\Element();
        foreach ($data as $prop => $value) {
            try {
                switch ($prop) {
                    case 'autofill':
                    case 'data_type':
                        break;
                    case 'columns':
                        foreach ($value as $col) {
                            $colElement = $this->buildElement($col);

                            $element->addColumn($colElement);
                        }
                        break;
                    default:
                        call_user_func([$element, "set" . Inflector::classify($prop)], $value);
//                        if ($prop == 'queryable') {
//                            throw new \Exception(sprintf(
//                                "esto es %s(%s)",
//                                "set" . Inflector::classify($prop),
//                                gettype($value)
//                            ));
//                        }
                        break;
                }
            } catch (\Exception $e) {
                echo ">>>>>> {$prop}: " . json_encode($value) . PHP_EOL;
                throw $e;
            }
        }

        return $element;
    }

    private function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);

        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        } else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;

        return $str;
    }
}
