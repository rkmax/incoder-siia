<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;

class CheckDFormCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('form:check')
            ->setDescription("Verfifica el estado de los formularios")
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @SuppressWarnings("unused")
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $finder = new Finder();
        $files = $finder->files()->in(__DIR__ . "/../Resources/config/form");

        /** @var SplFileInfo $file */
        foreach ($files as $file)
        {
            $this->check($file);
        }
    }

    private function check(SplFileInfo $file)
    {
        $form = Yaml::parse(file_get_contents($file->getRealPath()));

        foreach ($form['elements'] as $element) {
            try {
                $this->checkElement($element);
            } catch (\Exception $e) {
                throw new \Exception("{$file->getBasename()}: {$e->getMessage()}");
            }
        }
    }

    private function checkElement(array $element)
    {
        $this->mustHaveProperty($element, 'name');
        $this->mustHaveProperty($element, 'section');
        $this->mustHaveProperty($element, 'label');

        switch ($element['class']) {
            case 'input':
                $this->mustHaveProperty($element, 'type');
                $this->propertyMustBe($element, 'type', ['text', 'number', 'date', 'email', 'hidden']);
                break;
            case 'list':
                $this->mustHaveProperty($element, 'type');
                $this->mustHaveProperty($element, 'domain');
                $this->propertyMustBe($element, 'type', ['checkbox', 'radio']);
                break;
            case 'select':
                $this->mustHaveProperty($element, 'domain');
                break;
        }
    }

    private function mustHaveProperty($element, $property)
    {
        if (!array_key_exists($property, $element)) {
            throw new \Exception("El elemento '{$element['name']}' está mal formado debe tener la propiedad '{$property}'");
        }
    }

    private function propertyMustBe($element, $property, $value)
    {
        $fail = false;
        $comparision = "";
        if (is_array($value)) {
            $comparision = implode(",", $value);

            if (!in_array($element[$property], $value))
            {
                $fail = true;
            }
        } else {
            if ($value !== $element[$property]) {
                $fail = true;
            }
        }

        if ($fail) {
            throw new \Exception(
                "El valor de la propiedad {$element['name']}['{$property}']({$element[$property]}) debe ser {$comparision}"
            );
        }
    }
} 
