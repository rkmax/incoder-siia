<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Document\Transaction;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransactionAdminMigrateCommand extends ContainerAwareCommand
{
    /**
     * @var DocumentManager
     */
    private $dm;

    protected function configure()
    {
        $this
            ->setName('transaction:admin:migrate')
            ->setDescription("Simplifica las transacciones")
        ;
    }

    private function init()
    {
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init();

        $transactionIterator = $this->dm->getRepository(Transaction::CLASS)
            ->createQueryBuilder()
            ->getQuery()->getIterator();

        $transactionSimpleRepo = $this->dm->getRepository(SimpleTransaction::CLASS);

        $migrationCounter = 0;

        /** @var Transaction $transacion */
        foreach ($transactionIterator as $transacion) {
            if (!($simpleTransaction = $transactionSimpleRepo->find($transacion->getId()))) {
                $simple = SimpleTransaction::migrate($transacion);
                $this->dm->persist($simple);
                $migrationCounter++;
            }
        }

        $output->writeln("{$this->getName()}: migrating {$migrationCounter} Transactions!");
        $this->dm->flush();
    }

} 