<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Incoder\Bundle\SiiaBundle\DSL\Caracterizacion\Factory;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\Asociado;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico;
use Incoder\Bundle\SiiaBundle\Entity\PerfilSocioEconomico;
use Incoder\Bundle\SiiaBundle\Repository\AsociadoRepository;
use RRS\Bundle\DFormBundle\Document\Domain;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Yaml\Yaml;


class LoadCsvCommand extends ContainerAwareCommand
{
    const FILE_PATH = 'file';
    const PROFILE = 'profile';
    const DELIMITER = 'delimiter';

    const ASOCIACION = 'caracterizacion_asociacion';
    const TECNICO = 'diagnostico_tecnico';
    const UNICO = 'registro_unico';
    const PERFIL_SE = 'perfil_socioeconomico';
    const ASESOR = 'to-asesor';
    const OPERADOR = 'to-operador';
    const DOMAINS = 'process-domains';
    const FATHER = 'father-reasign';

    const SW_PARSE = 'parse_csv';
    const SW_FLUSH = 'flush';
    const SW_DOMAINS = 'domains';
    const DUPLICATE = 'duplicados';
    private $row_messages;

    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var TransformedFinder
     */
    private $elastic;

    /**
     * @var array
     */
    private $domains = array();

    /**
     * @var array
     */
    private $formConfig = array();

    private $cache = array();

    protected function configure()
    {
        $this
            ->setName('load:csv')
            ->setDescription("Importa los datos segun perfiles de  carga")
            ->addArgument(self::FILE_PATH, InputArgument::REQUIRED, "Ruta del archivo CSV a parsear", null)
            ->addArgument(self::PROFILE, InputArgument::REQUIRED, "Perfil de carga", null)
            ->addArgument(self::DELIMITER, InputArgument::OPTIONAL, "Caracter separador de columnas", "\t")
            ->addOption(self::OPERADOR, 'o', InputOption::VALUE_REQUIRED, "A que operador asignar [ID]", null)
            ->addOption(self::ASESOR, 'a', InputOption::VALUE_REQUIRED, "A que asesor asignar [ID]", null)
            ->addOption(self::DOMAINS, 'd', InputOption::VALUE_NONE, "Indica si cargar o no los dominios", null)
            ->addOption(self::FATHER, 'f', InputOption::VALUE_NONE, "Indica si reasignar el padre a la entidad, por ahora solo funciona con los asociados y asociaciones", null)
            ->addOption(self::DUPLICATE, 'p', InputOption::VALUE_NONE, "Imprimir duplicados", null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '2048M');
        $stopwatch = new Stopwatch();
        /** @var ProgressHelper $progress */
        $progress = $this->getHelperSet()->get('progress');


        $filepath = $input->getArgument(self::FILE_PATH);
        $profile = $input->getArgument(self::PROFILE);
        $delimiter = $input->getArgument(self::DELIMITER);

        $profilepath = dirname(__DIR__) . sprintf("/Resources/config/PerfilCarga/%s.yml", $profile);
        $formpath = dirname(__DIR__) . sprintf("/Resources/config/form/%s.yml", $profile);

        if (!file_exists($filepath)) {
            $output->writeln("<error>El archivo '{$filepath}' no existe!</error>");
            return;
        }

        if (!file_exists($profilepath)) {
            $output->writeln("<error>El perfil de carga '{$profilepath}' no existe!</error>");
            return;
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $ts = $this->getContainer()->get('d_form.transaction');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->elastic = $this->getContainer()->get('fos_elastica.finder.listing.asociado');


        $profileyml = Yaml::parse(file_get_contents($profilepath));
        $this->formConfig = Yaml::parse(file_get_contents($formpath));

        $handler = fopen($filepath, 'r');


        $progress->start($output, shell_exec("wc -l {$filepath} | awk '{ print $1 }'"));
        $progress->setBarCharacter('<comment>=</comment>');

        $row_messages = [];

        //Extract header
        $this->getRow($handler, $delimiter);

        $stopwatch->start(self::SW_PARSE);

        $asesor_id = $input->getOption(self::ASESOR);
        $operador_id = $input->getOption(self::OPERADOR);

        if ($operador_id) {
            $asesor = $em->getRepository(Asesor::CLASS)->createQueryBuilder('a')
                ->where('a.securityUser is NULL')
                ->andWhere('a.operador = :id')
                ->setParameter('id', $operador_id)
                ->getQuery()->getResult()[0];
        } else if ($asesor_id) {
            $asesor = $em->find(Asesor::CLASS, $asesor_id);
        } else {
            $asesor = null;
        }

        $row = null;
        while (false !== ($row = $this->getRow($handler, $delimiter))) {

            $data = [];
            foreach ($profileyml['elements'] as $col_name => $col_idx) {

                if (is_array($col_idx)) {

                    $column_array = $col_idx;
                    $data_array = [];
                    foreach( $column_array as $col_array_idx => $column_array_value ){

                        $idx = $this->columnNumber(trim($column_array_value));
                        $sanize = true;
                        if (!isset($row[$idx])) {

                            throw new \Exception("Column index: [{$idx}]({$column_array_value}) no defined in data\n", print_r($row));
                        } else {
                            $svalue = $sanize ? $this->sanize($row[$idx], $col_name) : $this->sanizeValue($row[$idx]);
                            if(($svalue && strlen($svalue) > 0)){
                                $data_array[] = $svalue;
                            }
                        }
                    }
                    $data[$col_name] = $data_array;
                    //$sanize = (array_key_exists('sanize', $col_idx) ? $col_idx['sanize'] : true);

                } else if (!empty($col_idx)) {

                    $idx = $this->columnNumber(trim($col_idx));
                    $sanize = true;
                    if (!isset($row[$idx])) {

                        throw new \Exception("Column index: [{$idx}]({$col_idx}) no defined in data\n", print_r($row));
                    } else {

                        $data[$col_name] = $sanize ? $this->sanize($row[$idx], $col_name) : $this->sanizeValue($row[$idx]);
                    }
                } else {
                    continue;
                }
            }

            // Convierte a * facilita la carga
            if (!isset($data['nombre_asociacion'])) {
                throw new \Exception("La fila no tiene como relacionarse dentro del sistema\n" . print_r($data));
                continue;
            }

            /* @var Asociacion $asociacion */
            $asociacion = null;
            if(isset($data['nombre_asociacion'])){
                $data['nombre_asociacion'] = mb_strtoupper($data['nombre_asociacion']);
            }
            if($data['siglas_asociacion']){
                $data['siglas_asociacion'] = mb_strtoupper($data['siglas_asociacion']);
            }
            $asociacionid = $data['nombre_asociacion'].$data['siglas_asociacion'].$data['municipio'];
            if(isset($data['centro_poblado'])){
                $asociacionid = $asociacionid . $data['centro_poblado'];
            }

            if (isset($this->cache[$asociacionid])) {
                $asociacion = $this->cache[$asociacionid];
            } else {
                //Ver si existe la asociacion por nombre y ubicacion
                $asociacion = null;
                $asociacion = $em->getRepository('SiiaBundle:Asociacion')
                    ->findOneByNombreLocalizacion(
                        $data['nombre_asociacion'],
                        $data['siglas_asociacion'],
                        $data['departamento'],
                        $data['municipio'],
                        (isset($data['centro_poblado']))?$data['centro_poblado']:null);
                $this->cache[$asociacionid] = $asociacion;
            }

            $this->processDomains($data);

            switch ($profile) {

                case self::ASOCIACION:

                    $data['__type'] = 'asociacion';
                    if (!$asociacion) {
                        $this->row_messages[] = [
                            "Creando",
                            "Asociacion",
                            "{$data['nombre_asociacion']}"
                        ];

                        $asociacion = new Asociacion();
                        if ($asesor) {
                            $this->row_messages[] = [
                                "Asociando a {$asesor}",
                                "Asociacion",
                                "{$data['nombre_asociacion']}"
                            ];
                            $asociacion->addAsesor($asesor);
                        }
                        $asociacion->setData($data);
                    }else{
                        $this->row_messages[] = [
                            "Actualizando",
                            "Asociacion",
                            "{$asociacion->getNombre()}"
                        ];
                        $data = array_merge($data, $asociacion->getData());
                        $asociacion->setData($data);

                        if ($asociacion->getAsesores()->count() == 0 && $asesor) {
                            $this->row_messages[] = [
                                "Asociando a {$asesor}",
                                "Asociacion",
                                "{$asociacion->getNombre()}"
                            ];
                            $asociacion->addAsesor($asesor);
                        }
                    }
                    $ts->persistEntity($asociacion);
                    $em->persist($asociacion); //TODO: resolver bug extraño que causa que el ts no persista al mysql
                    break;
                case self::TECNICO:

                    $data['__type'] = 'asociacion';
                    unset($data['nombre_asociacion']);

                    if ($asociacion) {
                        if(!$asociacion->getDiagnosticoTecnico() || !$asociacion->getDiagnosticoTecnico()[0]){
                            $this->row_messages[] = [
                                "Agregando",
                                "Diagnostico tecnico",
                                "{$asociacion->getNombre()}"
                            ];

                            $entity = new DiagnosticoTecnico();
                            $entity->setData($data);
                            $entity->setAsociacion($asociacion);
                            $ts->persistEntity($entity);
                            $em->persist($entity); //TODO: resolver bug extraño que causa que el ts no persista al mysql
                            $asociacion->addDiagnosticoTecnico($entity);
                            $ts->persistEntity($asociacion);
                            $em->persist($asociacion); //TODO: resolver bug extraño que causa que el ts no persista al mysql
                        }else{
                            /* @var DiagnosticoTecnico $entity*/
                            $entity = $asociacion->getDiagnosticoTecnico()[0];
                            $this->row_messages[] = [
                                "Actualizando diagnostico",
                                "Diagnostico tecnico",
                                "{$asociacion->getNombre()}"
                            ];
                            $data = array_merge($data, $entity->getData());
                            $entity->setData($data);
                            $ts->persistEntity($entity);
                            $em->persist($entity); //TODO: resolver bug extraño que causa que el ts no persista al mysql
                        }
                    }
                    break;
                case self::UNICO:

                    $data['__type'] = 'asociado';

                    if ($asociacion && isset($data['documento_identidad']) && ! empty($data['documento_identidad'])
                        ){

                        $repo = $em->getRepository('SiiaBundle:Asociado');
                        $key_asociado = $this->getKeyAsociado($asociacion, $data);
                        /** @var Asociado $entity */
                        $entity = $this->getAsociado($key_asociado, $data, $input);

                        if(!$entity){

                            $this->row_messages[] = [
                                "Creando",
                                "Asociado y predio",
                                $asociacion->getSigla(). " ". $key_asociado
                            ];
                            $entity = new Asociado();
                            $entity->setAsociacion($asociacion);
                        }else{

                            $this->row_messages[] = [
                                "Actualizando",
                                "Asociado y predio",
                                $entity->getAsociacion()->getSigla(). " ". $key_asociado
                            ];
                            if($input->getOption($this::FATHER) && $entity->getAsociacion()->getId() != $asociacion->getId()){
                                $this->row_messages[] = [
                                    "Actualizando",
                                    "Asociación de asociado",
                                    "OLD: {$entity->getAsociacion()->getSigla()} | NEW: {$asociacion->getSigla()}"
                                ];
                                $entity->setAsociacion($asociacion);
                            }
                            $data = array_merge($data, $entity->getData());
                        }
                        $entity->setData($data);
                        $ts->persistEntity($entity);
                        $em->persist($entity); //TODO: resolver bug extraño que causa que el ts no persista al mysql
                        $this->cache[$key_asociado] = $entity;
                    }else{
                        $detalle = "NO CARGA: ";
                        if(isset($data['siglas_asociacion'])){
                            $detalle = "NO SE ENCONTRÓ LA ASOCIACIÓN ";
                            $detalle = $detalle . $data['siglas_asociacion'];
                        }
                        if(!isset($data['documento_identidad']) || empty($data['documento_identidad'])){
                            $detalle = "| NO SE ENCONTRÓ EL DOCUMENTO DEL ASOCIADO {$data['nombre_asociado']}";
                        }
                        $this->row_messages[] = [
                            "ERROR",
                            "Asociado y predio",
                            $detalle,
                        ];
                    }
                    break;
                case self::PERFIL_SE:

                    $data['__type'] = 'perfil_socioeconomico';
                    if ($asociacion && isset($data['documento_identidad']) && $data['documento_identidad']) {

                        $repo = $em->getRepository('SiiaBundle:Asociado');
                        $key_asociado = $this->getKeyAsociado($asociacion, $data);
                        $asociado = $this->getAsociado($key_asociado, $data, $input);
                        if($asociado){

                            if(!$asociado->getPerfil() || !isset($asociado->getPerfil()[0])){
                                $this->row_messages[] = [
                                    "Creando",
                                    "Perfil",
                                    $key_asociado
                                ];
                                $entity = new PerfilSocioEconomico();
                                $entity->setAsociado($asociado);
                            }else{
                                $this->row_messages[] = [
                                    "Actualizando",
                                    "Perfil",
                                    $key_asociado
                                ];
                                $entity = $asociado->getPerfil()[0];
                                $data = array_merge($data, $entity->getData());
                            }
                            $entity->setData($data);
                            $ts->persistEntity($entity);
                            $em->persist($entity); //TODO: resolver bug extraño que causa que el ts no persista al mysql
                            //$this->cache[$key_asociado] = $entity;
                        }else{
                            $detalle = "NO SE ENCONTRÓ EL ASOCIADO ";
                            $this->row_messages[] = [
                                "ERROR",
                                "Perfil",
                                $detalle . $key_asociado,
                            ];
                        }

                    }else{
                        $detalle = "NO SE ENCONTRÓ LA ASOCIACIÓN ";
                        if(isset($data['siglas_asociacion'])){
                            $detalle = $detalle . $data['siglas_asociacion'];
                        }
                        $this->row_messages[] = [
                            "ERROR",
                            "Perfil",
                            $detalle,
                        ];
                    }
                    break;
            }//TODO: incluir cuadros productivos

            $stopwatch->lap(self::SW_PARSE);
            $progress->advance();
        }

        $progress->finish();
        $event = $stopwatch->stop(self::SW_PARSE);

        $output->writeln("All lines are parsed: <info>TIME: {$event->getDuration()}, MEM: {$event->getMemory()} </info>");

        fclose($handler);

        if($input->getOption(self::DOMAINS)){
            $stopwatch->start(self::SW_DOMAINS);
            $this->finishDomains($output);
            $event = $stopwatch->stop(self::SW_DOMAINS);
        }else{
            //TODO: imprimir dominiosº
        }

        $output->writeln("Domains proccesed: <info>TIME: {$event->getDuration()}, MEM: {$event->getMemory()} </info>");
        $stopwatch->start(self::SW_FLUSH);
        $em->flush();
        $event = $stopwatch->stop(self::SW_FLUSH);
        $output->writeln("Flushed into DB: <info>TIME: {$event->getDuration()}, MEM: {$event->getMemory()} </info>");

        $table = $this->getHelperSet()->get('table');
        $table->setHeaders([
            'Operacion', 'Objeto', 'Detalle'
        ]);
        $table->addRows($this->row_messages);
        $table->render($output);
    }

    /**
     * Get the next csv line in file handler
     * @param $handler
     * @param $delimiter
     * @return array|null
     */
    private function getRow($handler, $delimiter){
        return fgetcsv($handler, 4098, $delimiter);
    }

    private function sanizeValue($v)
    {
        try {
            $v = !mb_check_encoding($v, 'UTF-8') ? mb_convert_encoding($v, 'UTF-8') : $v;
        } catch (\Exception $e) {
            $v = iconv(mb_detect_encoding($v), 'ASCII//TRANSLIT', $v);
        }

        return $v;
    }

    private function sanize($v, $el_name) {

        try {
            $v = !mb_check_encoding($v, 'UTF-8') ? mb_convert_encoding($v, 'UTF-8') : $v;
        } catch (\Exception $e) {
            $v = iconv(mb_detect_encoding($v), 'ASCII//TRANSLIT', $v);
            throw $e;
        }

        if (!isset($this->formConfig['elements'][$el_name])) {
            return $v;
        } else if ($this->formConfig['elements'][$el_name]['class'] == 'input') {

            if (!array_key_exists('type', $this->formConfig['elements'][$el_name])) {
                throw new \Exception("type para {$el_name} no esa definido");
            }

            switch ($this->formConfig['elements'][$el_name]['type']) {
                case 'date':
                    if (empty($v)) {
                        return null;
                    }
                    return date_create_from_format('d/m/Y', $v);
                case 'number':
                    return (float)$v;
                case 'text':
                default:
                    return mb_strtoupper($v);
            }
        } else {
            if (empty($v)) {
                return null;
            }
            return trim($v);
        }
    }

    function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    function columnNumber($col){

        $col = str_pad($col,2,'0',STR_PAD_LEFT);
        $i = ($col{0} == '0') ? 0 : (ord($col{0}) - 64) * 26;
        $i += ord($col{1}) - 64;

        return $i -1;

    }

    private function finishDomains($output)
    {
        foreach ($this->domains as $domain => $values) {
            foreach ($values as $value) {

                $d = $this->dm->getRepository('DFormBundle:Domain')->findDomainL($value);

                if (!$d) {
                    $d = new Domain();
                    $d->setDomain($domain);
                    $this->row_messages[] = [
                        "Creando",
                        "Dominoo",
                        $value
                    ];
                }

                $d->setValue($value);
                $this->dm->persist($d);
                $this->dm->flush();
            }
        }
    }

    private function processDomains(array $data)
    {
        foreach ($this->formConfig['elements'] as $element) {
            if (!isset($element['domain'])) continue;

            $domain = $element['domain'];
            if (!isset($data[$element['name']])) continue;
            $data_domain = $data[$element['name']];
            if(is_array($data_domain)){
                foreach($data_domain as $ddidx => $value){
                    $value = mb_strtoupper($value);
                    $this->addValueToDomain($value, $domain);
                }
            }else{
                $value = mb_strtoupper($data_domain);
                $this->addValueToDomain($value, $domain);
            }
        }
    }

    /**
     * @param $value
     * @param $domain
     */
    private function addValueToDomain($value, $domain)
    {
        if (!empty($value)) {

            if (!isset($this->domains[$domain])) {

                $this->domains[$domain] = [];
            }
            if (!in_array($value, $this->domains[$domain])) {

                $this->domains[$domain][] = $value;
            }
        }
    }

    /**
     * @param $key_asociado
     * @param $data
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @return mixed
     */
    protected function getAsociado($key_asociado, $data, InputInterface $input)
    {
        $asociado = null;
        if (isset($this->cache[$key_asociado])) {

            /* @var Asociado $asociado */
            $asociado = $this->cache[$key_asociado];
            return $asociado;
        } else {

            $results = $this->elastic->find(
                Factory::asociados(
                    $data['documento_identidad'],
                    $data['registro_catastral'],
                    $data['nombre_propiedad']));

            if(count($results) > 0){
                /** @var Asociado $asociado */
                $asociado = $results[0];
                if($input->getOption($this::DUPLICATE) && isset($results[1])){
                    /** @var Asociado $duplicado */
                    foreach($results as $didx => $duplicado){
                        $this->row_messages[] = [
                            "Imprime",
                            "Duplicado",
                            "Asociado-> |id:{$duplicado->getId()} |documento:{$duplicado->getDocumento()} |propiedad:{$duplicado->getNombrePredio()} |asociación:{$duplicado->getAsociacion()->getSigla()}"
                        ];
                    }
                }
            }

            if($asociado){
                $this->cache[$key_asociado] = $asociado;
            }
            return $asociado;
        }
        return null;
    }

    /**
     * @param $asociacion
     * @param $data
     * @return string
     */
    private function getKeyAsociado($asociacion, $data)
    {
        $key_asociado = $asociacion->getId() . " " . $data['documento_identidad'];
        if (isset($data['registro_catastral'])) {
            $key_asociado = $key_asociado . " " . $data['registro_catastral'];
        }
        if (isset($data['nombre_propiedad'])) {
            $key_asociado = $key_asociado . " " . $data['nombre_propiedad'];
        }
        return $key_asociado;
    }
}
