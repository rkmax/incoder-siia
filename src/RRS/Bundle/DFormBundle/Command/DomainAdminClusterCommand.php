<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use JulianReyes\Lib\ParseUtils;
use RRS\Bundle\DFormBundle\Document\Domain;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Helper\TableHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DomainAdminClusterCommand extends ContainerAwareCommand
{
    const update = 'update-data';
    const levenshtein = 'levenshtein';
    const DELETE = 'delete';

    /** @var  DocumentManager */
    private $dm;

    /** @var  TransactionService */
    private $ts;

    /** @var  TwigEngine */
    private $templating;

    private $message;

    protected function configure()
    {
        $this
            ->setName('domain:admin:cluster')
            ->setDescription("Encuentra dominios similares y los agrupa")
            ->addOption(
                self::levenshtein,
                'l',
                InputOption::VALUE_REQUIRED,
                "Define la distancia levenshtein maxima",

                0
            )
            ->addOption(
                self::update,
                null,
                InputOption::VALUE_OPTIONAL,
                "Si se indica actualiza la estructura de datos a el ultimo formulario"
            )
            ->addOption(
                self::DELETE,
                null,
                InputOption::VALUE_NONE,
                "Si se indica borra los dominios repetidos"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $domains = $this->dm->getRepository(Domain::CLASS)->findAll();
        $domains2 = $this->dm->getRepository(Domain::CLASS)->findAll();
        $total = count($domains) * count($domains);

        /** @var ProgressHelper $progress */
        $progress = $this->getHelperSet()->get('progress');
        $progress->setBarCharacter('<comment>=</comment>');
        $progress->setEmptyBarCharacter(' ');
        $progress->setProgressCharacter('|');
        $progress->setRedrawFrequency(500);
        $progress->start($output, $total);

        $results = [];
        //$checked_keys = [];

        foreach ($domains as $keyd => $domain) {
            $shortest = -1;
            $key = "{$domain->getDomain()}:{$domain->getValue()}";

            $results[$key] = [
                'domain' => $domain,
                'closest' => []
            ];

            foreach ($domains2 as $comp_key => $comp_domain) {

                // Solo se ejecuta si el objeto no es el mismo y adicionalmente si pertenecen
                // al mismo dominio
                if ($domain != $comp_domain
                    && $domain->getDomain() == $comp_domain->getDomain()){
                    //&& !isset($checked_keys["{$comp_key}"])) {

                    if($domain->getDomain() == 'genero' && $comp_domain->getDomain() == 'genero'){
                        $hola_mundo = '';
                    }
                    $input_word = ParseUtils::replaceAccents(mb_strtolower($domain->getValue(), 'UTF-8'));
                    $current_word = ParseUtils::replaceAccents(mb_strtolower($comp_domain->getValue(), 'UTF-8'));
                    $lev = levenshtein($input_word, $current_word);

                    if ($lev == 0) {
                        $results[$key]['closest'][] = [
                            'domain' => $comp_domain,
                            'levenshtein' => $lev
                        ];
                        $shortest = 0;
                        //$checked_keys["{$comp_key}"] = 1;

                        //Remover el dominio repetido
                        if($input->getOption(self::DELETE)){
                            unset($domains[$comp_key]);
                            unset($domains2[$comp_key]);
                            //Se retira de la lista de evaluacion de cluster para no ser comparado
                            unset($domains2[$keyd]);
                            $this->dm->remove($comp_domain);
                        }
                    } else if ($lev <= $shortest || $shortest < 0) {
                        $results[$key]['closest'][] = [
                            'domain' => $comp_domain,
                            'levenshtein' => $lev
                        ];
                        $shortest = $lev;
                        //$checked_keys["{$comp_key}"] = 1;
                    }
                }

                $progress->advance();
            }
        }
        $progress->finish();
        $this->dm->flush();

        $this->renderClusterResults($input, $output, $results);
    }

    private function clamp($phrase, $max = 30)
    {
        if (strlen($phrase) > $max) {
            return substr($phrase, 0, $max);
        } else {
            return $phrase;
        }
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->templating = $this->getContainer()->get('templating');
    }

    /**
     * Organiza e imprime resultados
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param $results
     */
    protected function renderClusterResults(InputInterface $input, OutputInterface $output, $results)
    {
        /** @var TableHelper $table */
        $table = $this->getHelperSet()->get('table');
        $table->setHeaders([
            '#', 'id', 'Domain', '(V)alue', '(C)luster', 'Levenshtein'
        ]);

        $table_rows = [];
        $last = "";
        $i = 0;
        foreach ($results as $result) {
            $latest = "";

            foreach ($result['closest'] as $closest) {
                if ($closest['levenshtein'] <= $input->getOption(self::levenshtein)) {
                    if ($latest != $result['domain']->getValue()) {
                        $printme = $this->clamp($result['domain']->getValue());
                        $latest = $result['domain']->getValue();
                    } else {
                        $printme = "";
                    }
                    $table_rows[] = [
                        ++$i,
                        $result['domain']->getId(),
                        $result['domain']->getDomain(),
                        $printme,
                        $this->clamp($closest['domain']->getValue()),
                        $closest['levenshtein']
                    ];
                }
            }
        }
        $table->addRows($table_rows);

        $table->render($output);
    }
}