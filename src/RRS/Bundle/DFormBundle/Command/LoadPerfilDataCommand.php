<?php

namespace RRS\Bundle\DFormBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\Common\Util\Inflector;

use RRS\Bundle\DFormBundle\Document;
use Incoder\Bundle\SiiaBundle\Entity\DForm;


class LoadPerfilDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('data:perfil')
            ->setDescription("Carga datos del perfil")
            ->addArgument('file', InputArgument::REQUIRED, "Ruta archivo CSV", null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('file');

        if (!file_exists($filename)) {
            $output->writeln("<erro>El archivo {$filename} no existe!</erro>");
            return;
        }

        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        $ts = $this->getContainer()->get('d_form.transaction');
        $base = $this->getContainer()->get('siia.asociado.perfil')->getReferenceTransaction();
        $baseTrn = $ts->find($base);

        $handle = fopen($filename, 'r');

        $i = 0;

        // Ignora la primera linea
        fgetcsv($handle, 4096, ";");

        while (($data = fgetcsv($handle, 4096, ";")) !== false) {
            $i++;

            $item = new Document\Transaction($baseTrn);
            $this->parseData($data, $item);

            $dm->persist($item);

            if ($i % 500 === 0) {
                $dm->flush();
            }
        }

        $dm->flush();
    }

    private function parseData($data, $item)
    {
        $result_data = [];

        foreach ($item->getForm()->getElements() as $el) {

            if ($el->getCsv()) {
                switch ($data[$el->getCsv()]) {
                    case 'N/R':
                        $result_data[$el->getName()] = 0;
                        break;
                    default:
                        $result_data[$el->getName()] = $data[$el->getCsv()];
                        break;
                }

            }
        }

        $item->addData($result_data);
    }
}
