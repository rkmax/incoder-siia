<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Document\Form;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class LoadYamlForCommand extends ContainerAwareCommand
{
    const update = 'update-data';
    private $message;

    /** @var  DocumentManager */
    private $dm;

    /** @var  TransactionService */
    private $ts;

    /** @var  array */
    private $form = [];

    protected function configure()
    {
        $this
            ->setName('load:yaml:form')
            ->setDescription("Carga los formulario de dform")
            ->addOption(
                self::update,
                null,
                InputOption::VALUE_NONE,
                "Si se indica actualiza la estructura de datos a el ultimo formulario"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rootDir = $this->getApplication()->getKernel()->getRootDir();
        $baseDir = "/../src/RRS/Bundle/DFormBundle/Resources/config/form";

        $this->message = "<info>{$this->getName()}</info> >>> %s";

        $finder = new Finder();
        $finder->files()->name('*.yml')->in("{$rootDir}{$baseDir}");

        /** @var \SplFileInfo $file */
        foreach ($finder->getIterator() as $file) {
            $hash = sha1_file($file->getRealpath());

            if ($form = $this->getFormByHash($hash)) {
                $output->writeln(sprintf(
                    $this->message, "<comment>Update not required</comment> ({$form->getName()})"
                ));
            } else {
                $form = Form::createFromFile($file->getRealpath());
                $output->writeln(sprintf(
                    $this->message, "<info>Updating</info> ... {$form->getName()}"
                ));

            }
            $this->disablingAllOther($form->getName(), $form->getHash());
            $form->setIsActive(true);
            $this->dm->persist($form);
        }

        $this->dm->flush();

        $progress = $this->getHelperSet()->get('progress');

        if ($input->getOption(self::update)) {
            $output->writeln(sprintf($this->message, "Actualizando datos"));
            $this->updateData($output);
        }
    }

    private function disablingAllOther($name, $hash)
    {
        $this->dm
            ->createQueryBuilder(Form::CLASS)
            ->update()
            ->multiple(true)
            ->field('hash')->notEqual($hash)
            ->field('name')->equals($name)
            ->field('isActive')->set(false)
            ->getQuery()
            ->execute();
    }

    private function getFormByHash($hash)
    {
        $r = $this->dm
            ->getRepository(Form::CLASS)
            ->findBy([ 'hash' => $hash ]);

        if (count($r) > 0) {
            return $r[0];
        }

        return null;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
    }

    private function updateData(OutputInterface $output)
    {
        $transactions = $this->dm->getRepository(SimpleTransaction::CLASS)->findAll();

        foreach ($transactions as $transaction) {
            $form = $this->getForm($transaction->getType());
            if (!$form) {
                throw new \Exception("No se encontro el formulario {$transaction->getType()}");
            }

            $data = $this->updateDataWithForm($transaction->getData(), $form);
            $transaction->setData($data);
            $this->dm->persist($transaction);
        }

        $this->dm->flush();
    }

    private function getForm($name)
    {
        if (count($this->form) != 0) {
            return $this->form[$name];
        }

        $forms = $this->dm->getRepository(Form::CLASS)->findBy([
            'isActive' => true
        ]);

        foreach ($forms as $form) {
            $this->form[$form->getName()] = $form;
        }

        return $this->form[$name];
    }

    private function updateDataWithForm(array $data, Form $form)
    {
        /** @var Element $el */
        foreach($form->getElements() as $el) {
            if (!array_key_exists($el->getName(), $data)) {
                continue;
            }

            $value_data = $data[$el->getName()];

            switch ($el->getClass()) {
                case 'list':
                    if (!is_array($value_data)) {
                        $data[$el->getName()] = [$value_data];
                    } else {
                        $data[$el->getName()] = array_values($value_data);
                    }
                    break;
                case 'select':
                    if (is_array($value_data)) {
                        $data[$el->getName()] = reset($value_data);
                    }
                    break;
            }
        }

        return $data;
    }
}