<?php

namespace RRS\Bundle\DFormBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
* @ODM\EmbeddedDocument
*/
class Error {

    /**
     * @ODM\String
     */
    protected $property;

    /**
     * @ODM\String
     */
    protected $message;

    /**
     * Set property
     *
     * @param string $property
     * @return self
     */
    public function setProperty($property)
    {
        $this->property = $property;
        return $this;
    }

    /**
     * Get property
     *
     * @return string $property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }
}
