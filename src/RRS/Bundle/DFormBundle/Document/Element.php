<?php

namespace RRS\Bundle\DFormBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
/**
 * @ODM\EmbeddedDocument
 * @ExclusionPolicy("all")
*/
class Element {

    /**
     * @ODM\String
     * @Expose
     */
    protected $name;

    /**
     * @ODM\String
     * @Expose
     */
    protected $label;

    /**
     * @ODM\Int
     */
    protected $order;

    /**
     * @ODM\String
     */
    protected $section;

    /**
     * @ODM\String
     */
    protected $group;

    /**
     * @ODM\String
     */
    protected $class;

    /**
     * @ODM\String
     */
    protected $type;

    /**
     * @ODM\EmbedMany(targetDocument="Element")
     */
    protected $columns;

    /**
     * @ODM\String
     */
    protected $domain;

    /**
     * @ODM\Field(type="boolean")
     */
    protected $required;

    /**
     * @ODM\Boolean
     */
    protected $editable;

    /**
     * @ODM\Boolean
     */
    protected $visible;

    /**
     * @ODM\Boolean
     */
    protected $queryable;

    /**
     * @ODM\String
     */
    protected $autocomplete;

    /**
     * @var string
     *
     * @ODM\String
     */
    protected $role;

    /**
     * @ODM\Int
     */
    protected $csv;

    /**
     * @var string
     *
     * @ODM\String
     */
    protected $format;

    /**
     * @ODM\String
     * @var string
     */
    protected $pattern;

    /**
     * @ODM\String
     * @var string
     */
    protected $help;


    public function __construct()
    {
        $this->columns = new ArrayCollection();
        $this->queryable = false;
        $this->visible = true;
        $this->editable = true;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Get label
     *
     * @return string $label
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return self
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Get section
     *
     * @return string $section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set group
     *
     * @param string $group
     * @return self
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * Get group
     *
     * @return string $group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Get class
     *
     * @return string $class
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add column
     *
     * @param Element $column
     */
    public function addColumn(Element $column)
    {
        $this->columns[] = $column;
    }

    /**
     * Remove column
     *
     * @param Element $column
     */
    public function removeColumn(Element $column)
    {
        $this->columns->removeElement($column);
    }

    /**
     * Get columns
     *
     * @return Collection $columns
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return self
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Get domain
     *
     * @return string $domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set required
     *
     * @param boolean $required
     * @return self
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * Get required
     *
     * @return boolean $required
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     * @return self
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
        return $this;
    }

    /**
     * Get editable
     *
     * @return boolean $editable
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return self
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean $visible
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set autocomplete
     *
     * @param string $autocomplete
     * @return self
     */
    public function setAutocomplete($autocomplete)
    {
        $this->autocomplete = $autocomplete;
        return $this;
    }

    /**
     * Get autocomplete
     *
     * @return string $autocomplete
     */
    public function getAutocomplete()
    {
        return $this->autocomplete;
    }

    public function setCsv($csv)
    {
        $this->csv = $csv;
        return $this;
    }

    public function getCsv()
    {
        return $this->csv;
    }


    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @return mixed
     */
    public function getQueryable()
    {
        return $this->queryable;
    }

    /**
     * @param mixed $queryable
     *
     * @return self
     */
    public function setQueryable($queryable)
    {
        $this->queryable = $queryable;
        return $this;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * @param string $help
     */
    public function setHelp($help)
    {
        $this->help = $help;
    }
}
