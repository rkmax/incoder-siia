<?php

namespace RRS\Bundle\DFormBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Util\Inflector;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
* @ODM\Document(collection="form",
 *     repositoryClass="RRS\Bundle\DFormBundle\Repository\FormRepository" )
*/
class Form
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     * @ODM\String
     */
    protected $hash;

    /**
     * @ODM\String
     */
    protected $label;

    /**
     * @ODM\EmbedMany(targetDocument="Element")
     */
    protected $elements;

    /**
     * @var bool
     * @ODM\Boolean
     */
    protected $isActive = false;

    /**
     * @ODM\Collection
     */
    protected $rules = array();

    public function __construct()
    {
        $this->elements = new ArrayCollection();
        $this->rules = array();
    }

    public static function createFromFile($filepath)
    {
        $form = new self;
        $form->setHash(sha1_file($filepath));
        $form->setIsActive(true);

        try {
            $data = Yaml::parse(file_get_contents($filepath));
        } catch (ParseException $e) {
            echo $filepath . PHP_EOL;
            throw $e;
        }

        if (!isset($data['form'])) {
            throw new \Exception("Un formulario debe tener [form] property");
        }

        $form->setName($data['form']['name']);

        if (isset($data['rules'])) {
            $form->setRules($data['rules']);
        }

        if (!isset($data['elements'])) {
            throw new \Exception("Un formulario debe tener elementos [elements] property");
        }

        foreach ($data['elements'] as $elms) {
            $element = new Element();
            foreach ($elms as $prop => $value) {
                call_user_func([$element, "set" . Inflector::classify($prop)], $value);
            }
            $form->addElement($element);
        }

        return $form;
    }

    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return self
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * Get label
     *
     * @return string $label
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add element
     *
     * @param Element $element
     */
    public function addElement(Element $element)
    {
        $this->elements[] = $element;
    }

    /**
     * Remove element
     *
     * @param Element $element
     */
    public function removeElement(Element $element)
    {
        $this->elements->removeElement($element);
    }

    /**
     * Get elements
     *
     * @return ArrayCollection<Element> $elements
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Set rules
     *
     * @param collection $rules
     * @return self
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
        return $this;
    }

    /**
     * Get rules
     *
     * @return collection $rules
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }
}
