<?php

namespace RRS\Bundle\DFormBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ODM\Document(
*     collection="transaction",
*     repositoryClass="RRS\Bundle\DFormBundle\Repository\Transaction"
* )
* @deprecated
*/
class Transaction
{
    /**
     * @ODM\Id
     * @JMS\Exclude
     */
    protected $id;

    /**
     * @ODM\String
     */
    protected $type;

    /**
     * @ODM\ReferenceOne(targetDocument="Form")
     */
    protected $form;

    /**
     * TODO: change name to transaction data
     * @ODM\ReferenceOne(targetDocument="TransactionData")
     */
    protected $data;

    /**
     * @ODM\EmbedMany(targetDocument="Error")
     */
    protected $error;

    /**
     * Inicializa una nueva transacción y relaciona la Transaccion data
     *
     * @param Transaction $old
     */
    public function __construct(Transaction $old = null)
    {
        $this->error = new ArrayCollection();

        if ($old) {
            $this->type = $old->getType();
            $this->form = $old->getForm();
        }
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set form
     *
     * @param Form $form
     *
     * @return self
     */
    public function setForm(Form $form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return Form $form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Add error
     *
     * @param Error $error
     */
    public function addError(Error $error)
    {
        $this->error[] = $error;
    }

    /**
     * Remove error
     *
     * @param Error $error
     */
    public function removeError(Error $error)
    {
        $this->error->removeElement($error);
    }

    /**
     * Get error
     *
     * @return Collection $error
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set data
     *
     * @param TransactionData $data
     *
     * @return self
     */
    public function setData(TransactionData $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return TransactionData $data
     */
    public function getData()
    {
        return $this->data;
    }
    
}
