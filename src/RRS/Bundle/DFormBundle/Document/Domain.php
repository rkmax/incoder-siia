<?php

namespace RRS\Bundle\DFormBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
* @ODM\Document(
*     collection="domain",
*     repositoryClass="RRS\Bundle\DFormBundle\Repository\Domain"
* )
*/
class Domain
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\String
     */
    protected $domain;

    /**
     * @ODM\String
     */
    protected $value;


    /**
     * Get id
     *
     * @return \MongoId $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return self
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Get domain
     *
     * @return string $domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }
}
