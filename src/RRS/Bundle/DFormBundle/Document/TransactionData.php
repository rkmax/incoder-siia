<?php
/**
 *
 * @author  Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * Date: 10/01/14
 * Time: 04:25 PM
 */

namespace RRS\Bundle\DFormBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use JMS\Serializer\Annotation as JMS;

/**
 * Data holder
 *
 * @ODM\Document(collection="transaction_data")
 *
 * @package RRS\Bundle\DFormBundle\Document
 */
class TransactionData
{

    /**
     * @ODM\Id
     * @JMS\Exclude
     */
    protected $id;

    /**
     * @ODM\Raw
     */
    protected $data;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param array $data
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get data
     *
     * @return array $data
     */
    public function getData()
    {
        return $this->data;
    }
}
