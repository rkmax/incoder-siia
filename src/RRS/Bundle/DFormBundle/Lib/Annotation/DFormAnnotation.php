<?php


namespace RRS\Bundle\DFormBundle\Lib\Annotation;

use Doctrine\ORM\Mapping\Annotation;

/**
 * Class DFormAnnotation
 * @Annotation
 */
class DFormAnnotation {

    /**
     * @var string
     */
    private $dformName;

    public function __construct(array $options)
    {
        if (isset($options['value'])) {
            $options['dformName'] = $options['value'];
            unset($options['value']);
        }

        foreach ($options as $propertyName => $value) {
            if (!property_exists($this, $propertyName)) {
                throw new \InvalidArgumentException(
                    sprintf('Property "%s" does not exist', $propertyName)
                );
            }
            $this->$propertyName = $value;
        }
    }

    /**
     * @return string
     */
    public function getDformName()
    {
        return $this->dformName;
    }
} 
