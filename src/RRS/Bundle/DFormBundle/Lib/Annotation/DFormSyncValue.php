<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Lib\Annotation;

use Doctrine\ORM\Mapping\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class RetrieveValue
 * @package JulianReyes\Lib\Annotation\DForm
 * @Annotation
 * @Target("PROPERTY")
 */
class DFormSyncValue {

    private $dformColumn;

    /**
     * @var DataTransformerInterface
     */
    private $dataTrasformerClass;

    public function __construct($options){
        $this->dformColumn = $options['value'];
        if (isset($options['dataTransformer'])) {
            $this->dataTrasformerClass = $options['dataTransformer'];

            if (!class_exists($this->dataTrasformerClass)) {
                throw new \InvalidArgumentException(sprintf(
                    "La clase %s no existe",
                    $this->dataTrasformerClass
                ));
            }
        }
    }

    /**
     * @return mixed
     */
    public function getDformColumn()
    {
        return $this->dformColumn;
    }

    /**
     * @return \Symfony\Component\Form\DataTransformerInterface
     */
    public function getDataTrasformerClass()
    {
        return $this->dataTrasformerClass;
    }
} 
