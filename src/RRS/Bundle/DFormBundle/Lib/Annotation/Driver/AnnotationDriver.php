<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/22/14
 * Time: 11:03 AM
 */

namespace RRS\Bundle\DFormBundle\Lib\Annotation\Driver;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Component\Security\Core\Util\ClassUtils;

class AnnotationDriver implements EventSubscriber {

    private $reader;

    public function __construct(Reader $reader, TransactionService $ts)
    {
        $this->reader = $reader;
        $this->ts = $ts;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof DFormEntity && null !== $entity->getSlug())
        {
            $t = $this->ts->find($entity->getSlug());

            if (!$t) {
                throw new \Exception("No existe transaccion {$entity->getSlug()} " . get_class($entity) . "[{$entity->getId()}]" );
            }

            $entity->setData($this->cleanData((array)$t->getData()));
            $entity->setForm($this->ts->getBaseTransaction($entity));
        }
    }

    private function cleanData($data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value) && isset($value['date'])) {
                $data[$key] = new \DateTime($value['date'], new \DateTimeZone($value['timezone']));
            }
        }

        return (array) $data;
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->prePersist($args);

        if ($entity instanceof DFormEntity) {
            $this->syncValues($entity, $args);
        }
    }

    public function preHydrate(LifecycleEventArgs $args)
    {
        $args;
    }

    public function postHydrate(LifecycleEventArgs $args)
    {
        $args;
    }

    private function syncValues(DFormEntity $entity, LifecycleEventArgs $args, $recompute = true)
    {
        $changed = false;
        $annotationClass = 'RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue';
        $entityClass = ClassUtils::getRealClass($entity);
        $rclass = new \ReflectionClass($entityClass);

        foreach ($rclass->getProperties() as $rprop) {
            $annotation = $this->reader->getPropertyAnnotation($rprop, $annotationClass);

            if ($annotation && null !== ($colname = $annotation->getDformColumn())) {
                if (isset($entity->getData()[$colname]) && null !== ($value = $entity->getData()[$colname])) {
                    $rprop->setAccessible(true);
                    $rprop->setValue($entity, $this->setValue($value, $annotation->getDataTrasformerClass()));
                    $changed = true;
                }
            }
        }

        if ($changed && $recompute) {
            $uow = $args->getEntityManager()->getUnitOfWork();
            $meta = $args->getEntityManager()->getClassMetadata(get_class($entity));
            $uow->recomputeSingleEntityChangeSet($meta, $entity);
        }
    }

    private function setValue($value, $transformerClass)
    {
        if (!$transformerClass) {
            return $value;
        }

        $transformer = new $transformerClass;
        return $transformer->transform($value);
    }


    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'postLoad',
            'preHydrate',
            'postHydrate',
            'postRemove'
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // Si es instancia de DForm
        if ($entity instanceof DFormEntity) {
            $this->ts->persistEntity($entity);
            $this->syncValues($entity, $args, false);
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        // Si es instancia de DForm
        if ($entity instanceof DFormEntity) {
            $this->ts->removeEntity($entity);
        }
    }


}
