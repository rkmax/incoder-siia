<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Lib\Form\Extension;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Util\ClassUtils;
use ReflectionProperty;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormTypeDFormExtension extends AbstractTypeExtension
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $sync = [];

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!$options['dform']) {
            return;
        }

        $builder
            ->add('dform', 'd_form', [
                'data_class' => $options['data_class']
            ])
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function (FormEvent $event) use ($options) {
                    $this->data = $event->getData();
                    $this->syncValues($options['data_class']);
                }
            )
            ->addEventListener(
                FormEvents::SUBMIT,
                function (FormEvent $event) {
                    $form = $event->getForm();
                    $entity = $form->getData();
                    $class = ClassUtils::getRealClass($entity);
                    $rclass = new \ReflectionClass($class);

                    foreach ($this->sync as $key => $value) {
                        $rprop = $rclass->getProperty($key);
                        $rprop->setAccessible(true);
                        $rprop->setValue($entity, $value);
                    }
                }
            );
    }

    private function searchInArray($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            if ($needle === $key) {
                return $value;
            }

            if (is_array($value)) {
                return $this->searchInArray($needle, $value);
            }
        }
    }

    private function syncValues($entityClass)
    {
        $annotationClass = 'RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue';
        $rclass = new \ReflectionClass($entityClass);

        /** @var ReflectionProperty $rprop */
        foreach ($rclass->getProperties() as $rprop) {
            $annotation = $this->reader->getPropertyAnnotation($rprop, $annotationClass);

            if ($annotation && null !== ($colname = $annotation->getDformColumn())) {

                $value = $this->searchInArray($colname, $this->data);
                if ($value) {
                    $this->sync[$rprop->getName()] = $value;
                }
            }
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'dform' => false
        ]);
    }


    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return 'form';
    }
}
