<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace RRS\Bundle\DFormBundle\Lib\Form;


use Doctrine\Common\Collections\Collection;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Component\Form\AbstractType;

abstract class AbstractDFormType extends AbstractType
{
    /**
     * @var TransactionService
     */
    protected $ts;

    public function __construct(TransactionService $ts){
        $this->ts = $ts;
    }

    protected function parseOption(Element $element, Collection $elements = null)
    {
        $sections = [$element->getSection()];

        if (!empty($element->getGroup())) {
            $sections[] = $element->getGroup();
        }

        $sections = implode(" ", array_map(function($section) { return "fieldset-{$section}";}, $sections));

        $label = !$element->getRequired() ? $element->getLabel() : "{$element->getLabel()} (*)";

        $options = [
            'label' => $label,
            'attr' => [
                'class' => "field-{$element->getName()} {$sections}",
                'data-visibility' => $element->getVisible() ? '1': '0'
            ],
            'required' => $element->getRequired()
        ];

        if (!empty($element->getAutocomplete())) {
            $options['attr']['data-autocomplete'] = $element->getAutocomplete();
            $options['attr']['autocomplete'] = 'off';
        }

        if (!empty($element->getRole())) {
            $options['attr']['data-role'] = $element->getRole();
        }

        if (!empty($element->getHelp())) {
            $options['attr']['placeholder'] = $element->getHelp();
        }

        $options['attr']['data-editable'] = $element->getEditable() ? '1': '0';

        switch ($element->getClass()) {
            case 'fieldset':
                $options['elements'] = $elements;
                break;

            case 'list':
            case 'select':
                if (!empty($element->getDomain())) {
                    $options['choices'] = $this->ts->getDomainValues($element->getDomain());
                    $options['empty_value'] = 'Selecciona una opción';
                    $options['empty_data'] = null;
                }
                break;
        }

        switch ($element->getType()) {
            case 'number':
                $options['precision'] = 2;
                $options['attr']['step'] = "0.01";
                break;
            case 'checkbox':
                $options['expanded'] = true;
                $options['multiple'] = true;
                break;
            case 'date':
                $options['years'] = range(1930, date('Y') + 10);
                $options['widget'] = 'single_text';
                $options['attr']['class'] = $options['attr']['class'] . ' datepicker';
                $options['attr']['data-provide'] = "datepicker";
                $options['attr']['data-date-format'] = "dd/mm/yyyy";
                $options['format'] = 'dd/MM/yyyy';
                break;
        }

        return $options;
    }

    protected function parseClass(Element $element)
    {
        switch ($element->getClass()) {
            case 'input':
                return $element->getType();
            case 'list':
            case 'select':
                return 'choice';
            default:
                return $element->getClass();
        }
    }
}
