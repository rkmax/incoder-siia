<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/24/14
 * Time: 10:29 AM
 */

namespace RRS\Bundle\DFormBundle\Lib\Form;


use Symfony\Component\Form\AbstractType;

class DFormSlugType extends AbstractType {

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'd_form_slug';
    }

    public function getParent()
    {
        return 'hidden';
    }
}
