<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/22/14
 * Time: 12:05 PM
 */

namespace RRS\Bundle\DFormBundle\Lib\Form;


use Doctrine\Common\Annotations\Reader;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DFormType extends AbstractDFormType {

    const DFORM_ANOTATION = 'RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation';

    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var array
     */
    private $rules;

    public function __construct(Reader $reader, TransactionService $ts){
        $this->reader = $reader;
        parent::__construct($ts);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $annotation = $this->reader->getClassAnnotation(
            new \ReflectionClass($options['data_class']),
            self::DFORM_ANOTATION
        );

        if (!$annotation) {
            throw new \Exception("La entidad '{$options['data_class']}' debe tener la anotación " .  self::DFORM_ANOTATION);
        }

        // TODO: Logica de actualización de datos para evitar errores como unexpected format

        $form = $this->ts->getForm($annotation->getDformName());

        $elementName = $form->getName();
        $rules = $form->getRules();

        if (count($this->rules) == 0) {
            $this->rules = $rules;
        }

        $filterElements = function(Element $el) use ($elementName) {
            return $el->getSection() == $elementName;
        };

        $elements = $form->getElements()->filter($filterElements);

        /** @var Element $element */
        foreach ($elements as $element) {
            $elementName = $element->getName();
            $filterElements = function(Element $el) use ($elementName) {
                return $el->getSection() == $elementName;
            };

            $builder->add(
                $element->getName(),
                $this->parseClass($element),
                $this->parseOption($element, $form->getElements()->filter($filterElements))
            );
        };
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     *
     * @SuppressWarnings("unused")
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['rules'] = $this->rules;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'inherit_data' => true,
                'virtual' => true,
                'label' => false,
                'allow_add' => true,
            ))
            ->setRequired(array(
                'data_class',
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'd_form';
    }
}
