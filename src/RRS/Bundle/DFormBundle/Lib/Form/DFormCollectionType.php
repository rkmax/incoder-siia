<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/23/14
 * Time: 2:42 PM
 */

namespace RRS\Bundle\DFormBundle\Lib\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DFormCollectionType extends AbstractType {

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'd_form_collection';
    }

    public function getParent()
    {
        return 'collection';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'required'       => false,
                'error_bubbling' => true,
                'allow_add' => true,
                'label' => false,
            ));
    }
}
