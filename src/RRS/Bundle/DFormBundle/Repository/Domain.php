<?php

namespace RRS\Bundle\DFormBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;


class Domain extends DocumentRepository
{
    public function findIn(array $domains)
    {
        return $this
            ->createQueryBuilder()
            ->field('domain')
            ->in($domains)
            ->getQuery()
            ->execute()
        ;
    }

    public function findDomainL($value) {
        return $this
            ->createQueryBuilder()
            ->field('value')->equals(new \MongoRegex("/^$value$/i"))
            ->getQuery()
            ->getSingleResult();
    }

    public function findInDomain($domain, $filter)
    {
        return $this
            ->createQueryBuilder()
            ->field('domain')->equals($domain)
            ->field('value')->equals(new \MongoRegex("/^$filter.*/i"))
            ->getQuery()
            ->execute();
    }
}
