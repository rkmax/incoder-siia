<?php

namespace RRS\Bundle\DFormBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;


class TransactionRepository extends DocumentRepository
{
    public function findOneWithValues(array $values) {
        $qb = $this->createQueryBuilder();

        if (count($values) > 0) {
            foreach ($values as $expr) {
                $field = "data.{$expr['field']}";
                switch ($expr['O']) {
                    case 'OR':
                        $qb->addOr($qb->expr()->field($field)->equals($expr['value']));
                        break;
                    case 'AND':
                        $qb->addAnd($qb->expr()->field($field)->equals($expr['value']));
                        break;
                }
            }

            $data = $qb->getQuery()->getSingleResult();

            if ($data) {
                return $this->createQueryBuilder()
                    ->field('data.$id')->equals(new \MongoId($data->getId()))
                    ->getQuery()->getSingleResult();
            }
        }

        return null;
    }

    /**
     * Find all transaction of certain types
     * @param $types
     * @return array
     */
    public function findAllByTypes($types){
        $qb = $this->createQueryBuilder();
        $field = 'type';
        foreach($types as $key => $type){
            $qb->addOr($qb->expr()->field($field)->equals($type));
        }
        return $qb->getQuery()->execute();
    }
}
