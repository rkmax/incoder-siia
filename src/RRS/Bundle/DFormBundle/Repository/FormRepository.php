<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 5/17/14
 * Time: 4:13 PM
 */

namespace RRS\Bundle\DFormBundle\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;

class FormRepository extends DocumentRepository {

    /**
     * Returns all forms that contains elements with given domain
     * @param $domain
     * @return array
     */
    public function findAllByDomain($domain){
        $qb = $this->createQueryBuilder();
        $qb->field('elements')->elemMatch(
            $qb->expr()->field('domain')->equals($domain)
        );
        return $qb->getQuery()->execute();
    }
} 