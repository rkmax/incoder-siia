<?php

/**
 * @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 */

namespace RRS\Bundle\DFormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * For entities that are injected with a transaction in post persist
 *
 * @MappedSuperclass
 * @ExclusionPolicy("all")
 */
class DFormEntity {

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    protected $slug;

    /**
     * @var array
     * @Expose
     */
    protected $data;

    /**
     * @var array
     */
    protected $form;

    /**
     * @var inst
     * @ORM\Column(name="version", type="integer")
     */
    protected $version = 0;

    /**
     * Set slug
     *
     * @param string $slug
     * @return DFormEntity
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        if ($data != $this->data) {
            $this->data = $data;
        }
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param mixed $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    public function __get($method)
    {
        if (isset($this->getData()[$method])) {
            return $this->getData()[$method];
        }
    }

    public function __set($method, $value)
    {
        $this->data[$method] = $value;
    }

    public function updateVersion()
    {
        $this->version++;
    }
}
