<?php

namespace RRS\Bundle\DFormBundle\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


/**
* @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
*/
class IndicatorsServiceTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->container = static::$kernel->getContainer();
    }

    public function testCount()
    {
        $is = $this->container->get('d_form.indicators');

        $results = $is
            ->count('asociado')
            ->groupBy('genero_asociado')
            ->execute();

        echo var_export($results);
    }
}
