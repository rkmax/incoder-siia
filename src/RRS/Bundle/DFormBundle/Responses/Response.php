<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Response
 *
 * @author chamanx
 */
class Response {
    
    private $uuid;
    private $message;
    private $success;
    
    public function getUuid() {
        return $this->uuid;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getSuccess() {
        return $this->success;
    }

    public function setUuid($uuid) {
        $this->uuid = $uuid;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setSuccess($success) {
        $this->success = $success;
    }


}
