<?php

namespace RRS\Bundle\DFormBundle\Service;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ODM\MongoDB\DocumentManager;
use RRS\Bundle\DFormBundle\Document\Domain;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Document\Form;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use Symfony\Component\Security\Core\Util\ClassUtils;

class TransactionService {

    /**
     * @var DocumentManager $dm
     */
    private $dm;

    private $reader;

    function __construct(DocumentManager $dm, Reader $reader) {
        $this->dm = $dm;
        $this->reader = $reader;
    }

    /**
     *
     * @param string $uuid
     * @return SimpleTransaction
     */
    public function find($uuid)
    {
        $transaction = $this->dm->getRepository(SimpleTransaction::CLASS)->find($uuid);

        return $transaction;
    }

    public function getFormFields($name, $queryable = null)
    {
        $form = $this->getForm($name);

        if (null !== $queryable) {
            $elements = $form->getElements()->filter(
                function (Element $e) use ($queryable) {
                    return $e->getQueryable() == $queryable;
                }
            )->toArray();
        } else {
            $elements = $form->getElements()->toArray();
        }

        $elements = array_map(function (Element $element) use ($name) {
            return [
                'form' => $name,
                'name' => $element->getName(),
                'label' => $element->getLabel(),
                'fullName' => "{$name}.{$element->getName()}"
            ];
        }, $elements);


        return $elements;
    }

    public function getForm($name)
    {
       $form = $this->dm->getRepository(Form::CLASS)
           ->findOneBy([
               'name' => $name,
               'isActive' => true
           ]);

       if (!$form) {
           return null;
       }

       return $form;
    }

    public function queryData($field, $value)
    {
        $results = $this->dm
            ->getRepository(SimpleTransaction::CLASS)
            ->createQueryBuilder()->field($field)->equals($value)
            ->getQuery()->execute();

        $data = [];

        /** @var SimpleTransaction $transaction */
        foreach ($results as $transaction) {
            $data[] = $transaction->getData();
        }

        return $data;
    }

    public function getDomainValues($domainFilter)
    {
        $result = $this->dm
            ->getRepository('DFormBundle:Domain')
            ->createQueryBuilder()->field('domain')->equals($domainFilter)
            ->sort('value')
            ->getQuery()->execute();

        $results = [];

        /** @var Domain $domain */
        foreach ($result as $domain) {
            $results[$domain->getValue()] = $domain->getValue();
        }

        return $results;
    }

    public function persistEntity(DFormEntity $entity)
    {
        $data = $entity->getData();
        $slug = $entity->getSlug();

        if ($slug) {
            $transaction = $this->find($slug);

            if (!$transaction) {
                throw new \Exception("Transaction '{$slug}' not found!");
            }

            $tdata = $transaction->getData();
        } else {
            $form = $this->getBaseTransaction($entity);
            $transaction = new SimpleTransaction();
            $transaction->setType($form->getName());
            $tdata = [];
        }

        $tdata = array_merge($tdata, $data);

        $transaction->setData($tdata);
        $this->dm->persist($transaction);

        $this->dm->flush();
        $newSlug = $transaction->getId();

        $entity->setSlug($newSlug);
    }

    /**
     * remove entity transaction
     * @param DFormEntity $entity
     */
    public function removeEntity(DFormEntity $entity){
        $trn = $this->find($entity->getSlug());
        if($trn){
            $this->dm->remove($trn);
        }
    }

    /**
     * @param DFormEntity|mixed $entity
     * @deprecated El uso de transacciones base ha quedado en desuso
     *
     * @return Form
     */
    public function getBaseTransaction($entity)
    {
        if (is_object($entity)) {
            $rclass = new \ReflectionClass(ClassUtils::getRealClass($entity));
        } else {
            $rclass = new \ReflectionClass($entity);
        }

        $annotationClass = 'RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation';
        $annotation = $this->reader->getClassAnnotation($rclass, $annotationClass);

        return $this->getForm($annotation->getDformName());
    }
}
