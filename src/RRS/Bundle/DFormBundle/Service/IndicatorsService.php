<?php

namespace RRS\Bundle\DFormBundle\Service;

use Doctrine\MongoDB\Collection;
use Doctrine\ODM\MongoDB\DocumentManager;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;

/**
* @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
*/
class IndicatorsService
{
    /**
     * @var DocumentManager $dm
     */
    private $dm;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var Collection
     */
    private $collection;

    function __construct(DocumentManager $dm) {
        $this->dm = $dm;
    }

    /**
     * @param $params
     * @param $slugs
     *
     * @return \Doctrine\MongoDB\ArrayIterator
     */
    public function aggregate($params, $slugs)
    {
        $group = $sort = $match = $unwind = [];

        $slugs = array_map(function ($item) {
            return is_array($item) ? new \MongoId($item['slug']) : new \MongoId($item);
        }, array_values($slugs));

        foreach ($params['fields'] as $field) {
            $name = $field['name'];
            $group[$name] = ['$ifNull' => ["\$data.{$name}", '(blanco)']];
            $sort["_id.${name}"] = 1;
        }

        $match['type'] = $params['object']['value'];

        if (count($params['filters']) > 0) {
            foreach ($params['filters'] as $filter) {
                $name = $filter['name'];
                $comp = $this->mappedComp($filter['comp']['value']);
                $value = $filter['value'];
                $match["data.{$name}"] = empty($comp) ? $value : [$comp => $value];
            }
        }

        if (count($slugs) > 0) {
            $match['_id'] = [
                '$in' => $slugs
            ];
        }

        $this->collection = $this->dm->getDocumentCollection(SimpleTransaction::CLASS);

        /*
         * TODO: Mongo ODM se encuentra inestable, los desarrolladores han cambiado
         *       partes de la API, y esta parte de la aplicacion ha dejado de funcionar
         *       esto es un claro ejemplo de ello.
         */
        if (!method_exists($this->collection, 'aggregate')) {
            $this->collection = $this->collection->getMongoCollection();
        }

        $pipeline = [];

        foreach ($group as $field) {
            $field = $field['$ifNull'][0];
            if ($this->isUnwinded($field)) {
                $pipeline[] = [ '$unwind' => $field ];
            }
        }

        $pipeline[] = [ '$match' => $match ];
        $pipeline[] = [ '$sort' => $sort];
        $pipeline[] = [ '$group' => [ '_id' => $group, 'count' => ['$sum' => 1]] ];

        $aggregate = $this->collection->aggregate($pipeline);

        return ['result' => $aggregate->getCommandResult()['result'], 'total_result' => $this->collection->count($match)];
    }

    /**
     * @param $comp
     *
     * @return string
     * @throws \Exception
     */
    private function mappedComp($comp)
    {
        switch ($comp) {
            case 'E':
                return '';
            case 'NE':
                return '$ne';
            case 'GT':
                return '$gt';
            case 'GTE':
                return '$gte';
            case 'LT':
                return '$lt';
            case 'LTE':
                return '$lte';
            default:
                throw new \Exception("El comparador '{$comp}' no está definido.");
        }
    }

    private function isUnwinded($field)
    {
        $pipeline = [];
        $pipeline[] = [ '$match' => [ ltrim($field, '$') => [ '$ne' => null ]] ];
        $pipeline[] = [ '$limit' => 1 ];

        $result = $this->collection->aggregate($pipeline)->getCommandResult()['result'];

        $isUnwinded = (is_array($result[0]['data'][explode('.', $field)[1]]));

        return $isUnwinded;
    }

    public function getFormFields($object, $field, $slugs = [])
    {
        $data_field = "data.{$field}";
        $group_field = "\$data.{$field}";

        $pipelipe = [
            [
                '$match' => [
                    'type' => $object,
                    $data_field => ['$ne' => null]
                ]
            ],
            [
                '$group' => [
                    '_id' => $group_field
                ]
            ],
        ];

        if (0 < count($slugs)) {


            $slugs = array_map(function ($item) {
                return is_array($item) ? new \MongoId($item['slug']) : new \MongoId($item);
            }, array_values($slugs));

            $pipelipe = array_merge([
                [
                    '$match' => [
                        '_id' => [
                            '$in' => $slugs
                        ]
                    ]
                ]
            ], $pipelipe);
        }

        $aggregate = $this->dm
            ->getDocumentCollection(SimpleTransaction::CLASS)
            ->aggregate($pipelipe)
            ->getCommandResult();

        $values = array_map(function($v) {
            return $v['_id'];
        }, $aggregate['result']);

        return $values;
    }
}
