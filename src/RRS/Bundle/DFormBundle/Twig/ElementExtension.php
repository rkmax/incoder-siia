<?php

namespace RRS\Bundle\DFormBundle\Twig;
use NumberFormatter;
use RRS\Bundle\DFormBundle\Document\Element;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;

/**
* @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
*/
class ElementExtension extends \Twig_Extension
{
    private $section;

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('dform_element', array($this, 'dFormElements')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('dform_echo', array($this, 'dformEcho')),
        );
    }


    public function dformEcho(DFormEntity $entity, $property)
    {
        if (!isset($entity->getData()[$property])) {
            return "";
        }

        $value = $entity->getData()[$property];

        $elements = $entity->getForm()->getElements();
        $dformEl = null;

        foreach ($elements as $element) {
            if ($element->getName() == $property) {
                $dformEl = $element;
                break;
            }
        }

        if ($dformEl) {
            /** @var Element $dformEl */
            switch ($dformEl->getFormat()) {
                case 'money':
                    return "$ " . number_format($value, 2);
                case 'tel':
                    return $this->telefono($value);
                default:
                    return $this->getValue($value);
            }
        }
    }

    private function getValue($value)
    {
        switch (gettype($value)) {
            case 'array':
                if (isset($value['date'])) {
                    $date = new \DateTime($value['date'], new \DateTimeZone($value['timezone']));

                    return $date->format('d/m/Y');
                } else {
                    return implode(', ', $value);
                }
                break;
            case 'object':
                if ($value instanceof \DateTime) {
                    return $value->format('d/m/Y');
                }
                break;
            default:
                return $value;
        }
    }

    public function dFormElements($elements, $section)
    {
        $this->section = $section;
        return array_filter($elements->toArray(), array($this, 'walk'));
    }

    public function walk(Element $element) {
        if ($element->getSection() == $this->section) {
            return true;
        } else return false;
    }

    public function getName()
    {
        return 'element_extension';
    }

    private function telefono($num) {
        $num = preg_replace('/[^0-9]/', '', $num);

        $len = strlen($num);
        if($len == 7)
            $num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
        elseif($len == 10)
            $num = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);

        return $num;
    }
}
