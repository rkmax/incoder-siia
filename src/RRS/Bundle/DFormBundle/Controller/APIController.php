<?php

namespace RRS\Bundle\DFormBundle\Controller;

use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


/**
 * @Route("/api")
 */
class APIController extends Controller
{
    /**
     * @Route("/productos/detail", name="api_productos_detail")
     */
    public function getProductionDetail(Request $request)
    {
        $asociacion = $request->query->get('asociacion', '');

        if (!$asociacion) {
            throw new BadRequestHttpException("No se puede realizar la operacion sin asociacion");
        }

        $productos = $this
            ->get('doctrine.orm.entity_manager')
            ->getRepository('SiiaBundle:Asociacion')
            ->getReporteGeneral($asociacion);

        return new Response($this->get('serializer')->serialize($productos, 'json'));
    }

    /**
     * @param Request $request
     *
     * @Route("/insumos/detail", name="api_insumos_detail")
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     */
    public function getInsumosDetailf(Request $request)
    {
        $asociacion = $request->query->get('asociacion', '');

        if (!$asociacion) {
            throw new BadRequestHttpException("No se puede realizar la operacion sin asociacion");
        }

        $productos = $this
            ->get('doctrine.orm.entity_manager')
            ->getRepository('SiiaBundle:Asociacion')
            ->getInsumos($asociacion);

        return new Response($this->get('serializer')->serialize($productos, 'json'));
    }

    /**
     * @Route("/_/{uuid}", name="dform_api_get_trans", defaults={"_format"="json"})
     * @Method({"GET"})
     */
    public function getTransaction($uuid)
    {
        $transaction = $this
            ->get('doctrine_mongodb')->getManager()
            ->getRepository(SimpleTransaction::CLASS)
            ->findOneById($uuid);

        if (!$transaction) {
            throw $this->createNotFoundException("Object not found");
        }

        $data = $this->get('serializer')->serialize($transaction, 'json');

        return new Response($data);
    }
}
