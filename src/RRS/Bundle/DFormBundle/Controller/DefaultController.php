<?php

namespace RRS\Bundle\DFormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Yaml\Yaml;

/**
 * @Route("/_dev")
 */
class DefaultController extends Controller
{
    const FORM_DATOS = '52937c11e3cb62bb19f5a8e2';
    /**
     * @Route("/client")
     * @Route("/client/{form}", name="form_intro")
     * @Template()
     */
    public function indexAction($form = self::FORM_DATOS)
    {
        return array(
            'form' => $form,
            'form_list' => [
                self::FORM_DATOS => 'Datos basicos',
                // 'caracterizacion_asociacion' => 'Caracterizacion de asociación'
            ]
        );
    }

    /**
     * @Route("/_partial/{template}.{_format}")
     */
    public function partialAction($template, $_format)
    {
        return $this->render("DFormBundle:Partial:{$template}.{$_format}.twig");
    }

    /**
     * @Route("/receiver", name="form_receiver", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function reciverAction(Request $request)
    {
        return new Response("{success: 'OK'}");
    }

    /**
     *
     * @Route("/provider/{form}", name="form_provider", defaults={"_format"="json"})
     */
    public function providerAction($form)
    {
        $path = sprintf('/home/julian/formularios/%s.yml', $form);

        if (!file_exists($path)) {
            throw $this->createNotFoundException("Formulario '$form' no existe");
        }

        $meta = Yaml::parse(file_get_contents($path));

        $meta['domain'] = $meta['domains'];

        $els = [];
        foreach ($meta['elements'] as $element) {
            $element['visible'] = true;
            $els[] = $element;
        }

        $meta['form']['elements'] = $els;
        $meta['form']['rules'] = $meta['rules'];

        unset($meta['domains'], $meta['elements'], $meta['rules']);


        return new Response(json_encode($meta));
    }
}
