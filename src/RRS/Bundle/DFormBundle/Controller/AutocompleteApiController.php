<?php

namespace RRS\Bundle\DFormBundle\Controller;

use RRS\Bundle\DFormBundle\Document\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AutocompleteApiController extends Controller
{
    /**
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/auto/dofa/_/", name="autocomplete_dofa_api", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function dofaAction()
    {
        $type = $this->getRequest()->get('type', '');

        $tipos = [
            'debilidades' => 'Debilidad',
            'oportunidades' => 'Oportunidad',
            'fortalezas' => 'Fortaleza',
            'amenazas' => 'Amenaza'
        ];

        if (!array_key_exists($type, $tipos)) {
            throw new BadRequestHttpException("No se puede completar la solicitud para {$type}");
        }

        $dofaElements = $this->get('doctrine.orm.entity_manager')->getRepository('SiiaBundle:PlanAccion\\' . $tipos[$type])->findAll();

        return new Response($this->get('serializer')->serialize($dofaElements, 'json'));
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/auto/places/_/", name="autocomplete_places_api", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function placesAction(Request $request)
    {
        $query = $request->query->get('query', '');

        if (empty($query)) {
            $results = [];
        } else {
            $results = $this->get('fos_elastica.finder.search_localizacion')->find($query);
        }

        return new Response($this->get('serializer')->serialize($results, 'json'));
    }


    /**
     * @param $group
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/auto/_/{group}", name="autocomplete_api")
     * @Method("GET")
     */
    public function indexAction($group)
    {
        $query = $this->getRequest()->get('query', '');

        if (empty($query)) {
            $response_body = [];
        } else {
            $response_body = [];
            $r = $this
                ->get('doctrine_mongodb')->getManager()
                ->getRepository('DFormBundle:Domain')
                ->findInDomain($group, $query)->toArray();

            /** @var Domain */
            foreach ($r as $v) {
                $response_body[] = $v->getValue();
            }
        }

        return new Response($this->get('serializer')->serialize(array_unique($response_body), 'json'));
    }
}
