<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/19/14
 * Time: 11:58 AM
 */

namespace Incoder\Bundle\SiiaBundle\Twig;


use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class FindExtension extends \Twig_Extension {

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('find', array($this, 'findInCollection')),
        );
    }

    public function findInCollection(Collection $collection, $value, $path)
    {
        $find = null;
        $real_path = array_map(function($i) {
            return "get" . Inflector::camelize($i);
        }, explode('.', $path));

        foreach ($collection as $item) {
            if ($this->getValue($real_path, $item) === $value) {
                $find = $item;
            }
        }

        return $find;
    }

    private function getValue($path, $item)
    {
        $method = $path[0];

        return count($path) == 1 ?
            $item->$method() :
            call_user_func([$this, __FUNCTION__], array_slice($path, 1), $item->$method());
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'find_extension';
    }
}
