<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\TipoProducto;

/**
 * Description of AsociacionRepository
 *
 * @author chamanx
 */
class AsociacionRepository extends AbstractEntityRepository {

    /**
     * Find all elements from an Entity
     *
     * @SuppressWarnings("unused")
     *
     * @param int   $page
     * @param int   $limit
     * @param array $parameters
     *
     * @return array
     */
    public function findAllPaginated($page, $limit, $parameters = []) {
        $query = $this->createQueryBuilder('a')
                ->orderBy('a.nombre');
        $paginator = new Paginator($query);
        return $paginator->getQuery()->getResult();
    }

    public function findAllSlugsByAsesor(array $asesores)
    {
        $qb = $this
            ->createQueryBuilder('a')
            ->select('a.slug')
            ->join('a.asesores', 's');

        if (count($asesores) > 0) {
            $qb->where($qb->expr()->in('s.id', $asesores));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $asociacionId
     * @return array
     */
    public function findAllSlugsByAsociacion($asociacionId)
    {
        $aso = $this->find($asociacionId);
        if($aso){
            return [$aso->getSlug()];
        }
        return null;
    }

    public function findAllByOperador($operador_id)
    {
        $qb = $this
            ->createQueryBuilder('c')
            ->select('c.slug')
            ->join('c.asesores', 's')
            ->join('s.operador', 'o');

        if ($operador_id) {
            $qb
                ->where('o.id = :operador')
                ->setParameter('operador', $operador_id);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $nombre
     * @param $departamento
     * @param string $sigla
     * @param string $municipio
     * @param string $centroPoblado
     * @return Asociacion
     */
    public function findOneByNombreLocalizacion($nombre, $sigla, $departamento, $municipio = '', $centroPoblado = '')
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere("UPPER(a.nombre) like ?1 or UPPER(a.sigla) like ?2")
            ->andWhere("UPPER(a.departamento) like ?5")
            ->setParameter(1, strtoupper("%{$nombre}%"))->setParameter(2, strtoupper("%{$sigla}%"))
            ->setParameter(5, strtoupper("%{$departamento}%"));

        if ($municipio && !$centroPoblado) {
            $qb->andWhere("UPPER(a.municipio) like ?3 ");
            $qb->setParameter(3, strtoupper("%{$municipio}%"));
        }
        if ($municipio && $centroPoblado) {
            $qb->andWhere("UPPER(a.municipio) like ?3 OR UPPER(a.centroPoblado) like ?4 ");
            $qb->setParameter(3, strtoupper("%{$municipio}%"))->setParameter(4, strtoupper("%{$centroPoblado}%"));
        }
        if (!$municipio && $centroPoblado) {
            $qb->andWhere("UPPER(a.centroPoblado) like ?4 ");
            $qb->setParameter(4, strtoupper("%{$centroPoblado}%"));
        }
        $qb->setMaxResults(1);
        try {
            $q = $qb->getQuery();
            return $q->getSingleResult();
        } catch (NoResultException $ex) {
        }
        return null;
    }

    /**
     * @SuppressWarnings("unused")
     *
     * @param $page
     * @param $limit
     * @param $param
     *
     * @return array
     */
    public function findAllPaginatedFilter($page, $limit, $param)
    {
        return $this->createQueryBuilder('a')
                ->where("a.nombre LIKE :key")
                ->orWhere("a.nombre LIKE :key_lw")
                ->setParameter('key', "$param%")
                ->setParameter('key_lw', "{mb_strtolower($param)}%")
                ->getQuery()->getResult();
    }

    public function findAllWithDGEmpresarial(array $asesores_id = [])
    {
        $qb = $this->createQueryBuilder('a')
            ->join('a.diagnosticoEmpresarial', 'dge')
            ->join('a.asesores', 'ass');

        if (count($asesores_id) > 0) {
            $qb->add('where', $qb->expr()->in('ass.id', $asesores_id));
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findAllWithProducts(array $product_ids)
    {
        if (!is_array($product_ids) || count($product_ids) == 0) return [];

        $qb = $this->createQueryBuilder('aso');

        $qb
            ->join('aso.asociado', 'a')
            ->join('a.cuadroPecuario', 'cp')
            ->join('a.cuadroAgricola', 'ca')
            ->join('cp.producto', 'p')
            ->join('ca.producto', 'p2')
            ->add('where', $qb->expr()->orX(
                $qb->expr()->in('p.id', $product_ids),
                $qb->expr()->in('p2.id', $product_ids)
            ));

        return $qb->getQuery()->getResult();
    }

    public function findAllByAsesorQB(array $asesores_id)
    {
        $qb = $this->createQueryBuilder('a')
            ->innerJoin('a.asesores', 's');

        if (count($asesores_id) > 0) {
            $qb->add('where', $qb->expr()->in('s', $asesores_id));
        }

        return $qb;
    }

    /**
     * obtiene las asociaciones filtrado por asesor(es)
     * @param array $asesor_ids
     * @return array asociaciones
     */
    public function findAllByAsesor(array $asesor_ids)
    {
        $qb = $this->createQueryBuilder('a')->distinct(true);

        if (count($asesor_ids) > 0) {
            $qb->join('a.asesores', 'ass')
                ->add('where', $qb->expr()->in('ass.id', $asesor_ids));
        }

        $asociaciones = $qb->getQuery()->getResult();

        return $asociaciones;
    }

    /**
     * @param array $insumo_ids
     *
     * @return array
     * @SuppressWarnings("unused")
     */
    public function findAllWithInsumos(array $insumo_ids)
    {
        $qb = $this->createQueryBuilder('aso');

        $qb
            ->join('aso.asociado', 'a')
            ->join('a.cuadroPecuario', 'cp')
            ->join('a.cuadroAgricola', 'ca');


        return $qb->getQuery()->getResult();
    }

    public function findAllWithProductsInsumos($product_ids, $insumo_ids)
    {
        $qb = $this->createQueryBuilder('aso');
        $product_expr = null;
        $insumo_expr = null;

        if (count($product_ids) == 0 && count($insumo_ids) == 0) {
            return new ArrayCollection();
        }

        $qb
            ->join('aso.asociado', 'a')
            ->leftJoin('a.cuadroPecuario', 'cp')
            ->leftJoin('a.cuadroAgricola', 'ca')
            ->leftJoin('cp.producto', 'p')
            ->leftJoin('ca.producto', 'p2');

        if (is_array($product_ids) && count($product_ids) > 0) {
            $product_expr = $qb->expr()->orX(
                $qb->expr()->in('p.id', $product_ids),
                $qb->expr()->in('p2.id', $product_ids)
            );
        }

        if (is_array($insumo_ids) && count($insumo_ids) > 0) {
            $insumo_expr = $qb->expr()->orX(
                $qb->expr()->in('cp.id', $insumo_ids),
                $qb->expr()->in('ca.id', $insumo_ids)
            );
        }

        if ($insumo_ids && $insumo_expr) {
            $qb->add('where', $qb->expr()->orX($product_expr, $insumo_expr));
        } else {
            $qb->add('where', $product_expr ? $product_expr : $insumo_expr);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $asociacion
     * @param $localizacion
     * @param $producto
     * @param $fechaini
     * @param $fechafin
     * @param null $usuarioId
     * @param null $rol
     * @param null $tipoProducto
     * @return array
     */
    public function getReporteGeneral($asociacion, $localizacion = null, $producto = null, $fechaini = null, $fechafin = null, $usuarioId = null, $rol = null, $tipoProducto = null)
    {
        if($tipoProducto){
            switch ($tipoProducto) {
                case TipoProducto::AGRICOLA:
                    return $this->getProduccionAgricola($asociacion, $localizacion, $producto, $fechaini, $fechafin, $usuarioId, $rol);
                    break;
                case TipoProducto::PECUARIO:
                    return $this->getProduccionPecuaria($asociacion, $localizacion, $producto, $fechaini, $fechafin, $usuarioId, $rol);
                    break;
            }
        }else{
            $resultsa = $this->getProduccionAgricola($asociacion, $localizacion, $producto, $fechaini, $fechafin, $usuarioId, $rol);
            $resultsp = $this->getProduccionPecuaria($asociacion, $localizacion, $producto, $fechaini, $fechafin, $usuarioId, $rol);
            return array_merge($resultsa, $resultsp);
        }

    }

    /**
     * QUERY DE PRODUCCION GENERAL AGRICOLA
     * TODO: agregar dinamicamente grupos
     * @param $tipoCuadro
     * @param $asociacion
     * @param $localizacion
     * @param $producto
     * @param null $usuarioId
     * @param null $rol
     * @param null $fechaini
     * @param null $fechafin
     * @throws \Exception
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQueryBuiderProduccion($tipoCuadro, $asociacion, $localizacion, $producto, $usuarioId = null, $rol = null, $fechaini= null, $fechafin= null)
    {
        $qb = $this->createQueryBuilder('a');
        $nombreProducto = "CONCAT_BS(p.nombreProducto, p.variedadProducto)";
        $i = 0;

        switch ($tipoCuadro) {
            case 'cuadroAgricola':
                $tipo = 'Agrícola';
                $fecha = 'fechaCosecha';
                break;
            case 'cuadroPecuario':
                $tipo = 'Pecuario';
                $fecha = 'fechaActualizacion';
                break;
            default:
                throw new \Exception("El tipo {$tipoCuadro} no está definido");
        }

        $qb
            ->addSelect('CONCAT_BS(CONCAT_BS(a.departamento, a.municipio), a.centroPoblado) as localizacionAsociacion')
            ->addSelect('a.departamento as departamentoAsociacion')
            ->addSelect('a.municipio as municipioAsociacion')
            ->addSelect('a.centroPoblado as centroPobladoAsociacion')
            ->addSelect('a.nombre as nombreAsociacion')
            ->addSelect("{$nombreProducto} as nombreProducto")
            ->addSelect('p.id as idProducto')
            ->addSelect('up.nombre as unidadProduccion')
            ->addSelect('SUM(cp.volumenProduccion) as volumenProducido')
            ->addSelect("'{$tipo}' as tipoProducto")

            ->join('a.asociado', 'aa')
            ->join("aa.{$tipoCuadro}", 'cp')
            ->join('cp.producto', 'p')
            ->join('p.unidadProduccion', 'up')

            ->addGroupBy('localizacionAsociacion','p')
            ;

        if($asociacion){
            $qb ->andWhere("a = ?{$i}")
                ->setParameter($i, $asociacion);
        }

        if($localizacion){
            if($localizacion['departamento']){
                $i+=1;
                $qb ->andWhere("a.departamento like ?{$i}")
                    ->setParameter($i, "{$localizacion['departamento']}%");
            }
            if($localizacion['municipio']){
                $i+=1;
                $qb ->andWhere("a.municipio like ?{$i}")
                    ->setParameter($i, "{$localizacion['municipio']}%");
            }
            if($localizacion['vereda']){
                $i+=1;
                $qb ->andWhere("a.centroPoblado like ?{$i}")
                    ->setParameter($i, "{$localizacion['vereda']}");
            }
        }

        if ($usuarioId && $rol == BaseProfile::ASESOR){
            $i+=1;
            $asociaciones = $this->getAsociacionesDeAsesor($usuarioId);
            $qb->andWhere( $qb->expr()->in('a.id', "?{$i}"))->setParameter($i, $asociaciones);
        }

        if ($usuarioId && $rol == BaseProfile::OPERADOR){
            $i+=1;
            $asociaciones = $this->getAsociacionesDeOperador($usuarioId);
            $qb->andWhere( $qb->expr()->in('a.id', "?{$i}"))->setParameter($i, $asociaciones);
        }

        if ($asociacion && $rol == BaseProfile::ASOCIACION){
            $i+=1;
            $qb->andWhere( $qb->expr()->eq('a.id', "?{$i}"))->setParameter($i, $asociacion);
        }

        if ($producto) {
            $i+=1;
            $qb->andWhere("p.id = ?{$i}")
                ->setParameter($i, $producto);
            $qb->addGroupBy("a");
        }

        if ($fechaini){
            $qb->andWhere("STR_TO_DATE( cp.{$fecha} , '%Y-%m-%d') >= STR_TO_DATE( '{$fechaini}', '%Y-%m-%d')");
        }

        if ($fechafin){
            $qb->andWhere("STR_TO_DATE( cp.{$fecha} , '%Y-%m-%d') <= STR_TO_DATE( '{$fechafin}', '%Y-%m-%d')");
        }

        return $qb;
    }

    /**
     * @return array de departamentos configurados en las asociaciones
     */
    public function findAllDepartamentosDeAsociaciones(){
        $field = 'a.departamento';
        return $this->getListOfField($field)->getQuery()->getResult();
    }

    /**
     *
     * @param null $departamento
     * @return array de municipios configurados en las asociaciones
     */
    public function findAllMunicipiosDeAsociaciones($departamento = null){
        $field = 'a.municipio';
        $qb = $this->getListOfField($field);
        if($departamento){
            $qb->andWhere($qb->expr()->like('a.departamento', '?1'))
            ->setParameter(1, "$departamento%");
        }
        return $qb->getQuery()->getResult();
    }

    /**
     *
     * @param null $departamento
     * @param null $municipio
     * @return array de municipios configurados en las asociaciones
     */
    public function findAllVeredasDeAsociaciones($departamento = null, $municipio = null){
        $field = 'a.centroPoblado';
        $qb = $this->getListOfField($field);
        if($departamento){
            $qb->andWhere($qb->expr()->like('a.departamento', '?1'))
                ->setParameter(1, "$departamento%");
        }
        if($municipio){
            $qb->andWhere($qb->expr()->like('a.municipio', '?2'))
                ->setParameter(2, "$municipio%");
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $asociacion
     * @param $localizacion
     * @param $producto
     * @param $fechaini
     * @param $fechafin
     * @param null $usuarioId
     * @param null $rol
     * @return array
     */
    public function getProduccionAgricola($asociacion, $localizacion, $producto, $fechaini, $fechafin, $usuarioId = null, $rol = null)
    {
        $results = $this
            ->getQueryBuiderProduccion('cuadroAgricola', $asociacion, $localizacion, $producto, $usuarioId, $rol, $fechaini, $fechafin)
            ->getQuery()->getArrayResult();

        return $results;
    }

    /**
     * @SuppressWarnings("unused")
     *
     * @param $asociacion
     * @param $localizacion
     * @param $producto
     * @param $fechaini
     * @param $fechafin
     * @param null $usuarioId
     * @param null $rol
     * @return array
     */
    public function getProduccionPecuaria($asociacion, $localizacion, $producto, $fechaini, $fechafin, $usuarioId = null, $rol = null)
    {
        $results = $this
            ->getQueryBuiderProduccion('cuadroPecuario', $asociacion, $localizacion, $producto, $usuarioId, $rol, $fechaini, $fechafin)
            ->getQuery()->getArrayResult();

        return $results;
    }

    /**
     * @param $operadorId
     * @return array
     */
    public function getAsociacionesDeOperador($operadorId){
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.asesores', 'ass');
        $qb->join('ass.operador','op');
        $qb->andWhere("op.id = ?1")->setParameter(1, $operadorId);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $asesorId
     * @return array
     */
    public function getAsociacionesDeAsesor($asesorId){
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.asesores', 'ass');
        $qb->andWhere("ass.id = ?1")->setParameter(1, $asesorId);
        return $qb->getQuery()->getResult();
    }

    public function getInsumosByCuadroProductivo($tipo, $asociacion)
    {
        $a = $this->getEntityManager()->getRepository('SiiaBundle:' . $tipo)
            ->createQueryBuilder('c')
            ->select('c.id')
            ->join('c.asociado', 's')
            ->join('s.asociacion', 'a')
            ->where('a.id = :asociacion')
            ->setParameter('asociacion', $asociacion)
            ->getQuery()->getArrayResult();

        $a = array_map(function($i) {
            return $i['id'];
        }, $a);

        if (count($a) == 0) {
            return [];
        }

        $qb = $this
            ->getEntityManager()
            ->getRepository('SiiaBundle:Insumo')
            ->createQueryBuilder('i')
            ->select('i.frecuencia', 'i.nombre', 'i.marca')
            ->orderBy('i.frecuencia')
            ->groupBy('i.frecuencia', 'i.nombre', 'i.marca');
        $qb
            ->join('i.cuadroProductivo', 'c')
            ->add('where', $qb->expr()->in('c.id', $a));

        return $qb->getQuery()->getArrayResult();
    }

    public function getInsumos($asociacion)
    {
        $a = $this
            ->getInsumosByCuadroProductivo('CuadroAgricola', $asociacion);
        $b = $this
            ->getInsumosByCuadroProductivo('CuadroPecuario', $asociacion);

        return array_merge($a, $b);
    }

    /**
     * @param $field
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getListOfField($field)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()->from('SiiaBundle:Asociacion', 'a');
        $qb->addSelect($field)
            ->addGroupBy($field)
            ->addOrderBy($field)
            ->andWhere($qb->expr()->isNotNull($field))
            ->andWhere($qb->expr()->not($qb->expr()->eq($qb->expr()->trim($field), "''")));
        return $qb;
    }
}

