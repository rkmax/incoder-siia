<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Repository\PlanAccion;

use Incoder\Bundle\SiiaBundle\Repository\AbstractEntityRepository;

class DebilidadRepository extends AbstractEntityRepository {

    public function findByAsociacion($id)
    {
        $dql = $this->getEntityManager()
            ->createQueryBuilder()
            ->from('SiiaBundle:PlanAccion\PlanAccionAsociacion', 'pam')
            ->select('d.id')
            ->leftJoin('pam.elementos', 'pamel')
            ->leftJoin('pamel.nivel', 'n')
            ->leftJoin('n.accion', 'na')
            ->leftJoin('na.actividades', 'ac')
            ->leftJoin('ac.debilidades', 'd')
            ->where('pam.asociacion = :asociacion')
            ->andWhere('na.nivel = n')
            ->orderBy('d.id')
            ->getDQL()
        ;

        $qb = $this->createQueryBuilder('debilidad');

        $qb
            ->where($qb->expr()->in('debilidad.id', $dql))
            ->select('partial debilidad.{id, descripcion}')
            ->orderBy('debilidad.descripcion')
            ->addOrderBy('debilidad.id')
            ->setParameter('asociacion', $id);

        return $qb->getQuery()->execute();
    }
}