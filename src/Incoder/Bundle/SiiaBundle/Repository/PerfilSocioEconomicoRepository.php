<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Repository;


use Incoder\Bundle\SiiaBundle\Entity\Asociacion;

class PerfilSocioEconomicoRepository extends AbstractEntityRepository
{
    public function findAllSlugsByAsesor(array $asesores)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p.slug')
            ->join('p.asociado', 'd')
            ->join('d.asociacion', 'a')
            ->join('a.asesores', 's');

        if (count($asesores) > 0) {
            $qb->where($qb->expr()->in('s.id', $asesores));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $asociacionId
     * @return array
     */
    public function findAllSlugsByAsociacion($asociacionId)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p.slug')
            ->join('p.asociado', 'd')
            ->join('d.asociacion', 'a');

        if ($asociacionId) {
            $qb->where($qb->expr()->in('a.id', $asociacionId));
        }

        return $qb->getQuery()->getResult();
    }
} 