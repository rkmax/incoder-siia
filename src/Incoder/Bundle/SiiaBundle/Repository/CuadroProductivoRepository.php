<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/28/14
 * Time: 9:37 PM
 */

namespace Incoder\Bundle\SiiaBundle\Repository;

/**
 * Class CuadroPecuarioRepository
 * @package Incoder\Bundle\SiiaBundle\Repository
 */
class CuadroProductivoRepository extends AbstractEntityRepository {
    /**
     * @param $departamento
     * @param $asociacion
     * @param $municipio
     * @param $fechaini
     * @param $fechafin
     * @return array
     */
    public function getReporteGeneral($departamento, $asociacion, $municipio, $fechaini, $fechafin)
    {
        $qb = $this->createQueryBuilder('cp');

        if($departamento){
            $qb->andWhere("cp.departamento like '%?1%'")
                ->setParameter(1, $departamento);
        }

        if($asociacion){
            $qb->andWhere("cp.asociacion like '%?1%'")
                ->setParameter(1, $departamento);
        }


        $paginator = new Paginator($qb);


        return $paginator;
    }
} 