<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Repository;

/**
 * Description of DiagnosticoTecnicoRepository
 *
 * @author chamanx
 */
class DiagnosticoTecnicoRepository extends AbstractEntityRepository
{
    public function findAllByOperador($operador_id)
    {
        $qb = $this
            ->createQueryBuilder('d')
            ->select('d.slug')
            ->join('d.asociacion', 'c')
            ->join('c.asesores', 's');
        if ($operador_id) {
            $qb
                ->join('s.operador', 'o')
                ->where('o.id = :operador')
                ->setParameter('operador', $operador_id);
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findAllSlugsByAsesor(array $asesores)
    {
        return $this->findSlugsAllByAsesor($asesores);
    }

    public function findSlugsAllByAsesor(array $asesores)
    {
        $qb = $this
            ->createQueryBuilder('d')
            ->select('d.slug')
            ->join('d.asociacion', 'a')
            ->join('a.asesores', 's');

        if (count($asesores) > 0) {
            $qb->where($qb->expr()->in('s.id', $asesores));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $asociacionId
     * @return array
     */
    public function findAllSlugsByAsociacion($asociacionId)
    {
        $qb = $this
            ->createQueryBuilder('d')
            ->select('d.slug')
            ->join('d.asociacion', 'a');

        if ($asociacionId) {
            $qb->where($qb->expr()->in('a.id', $asociacionId));
        }
        return $qb->getQuery()->getResult();
    }

}
