<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 3/28/14
 * Time: 7:38 AM
 */

namespace Incoder\Bundle\SiiaBundle\Repository;


use Doctrine\ORM\QueryBuilder;

class ProductoRepository extends AbstractEntityRepository{

    /**
     * Override
     * @param $page
     * @param $limit
     * @param array $params
     * @return array
     */
    public function findAllPaginated($page, $limit, $params = []) {
        /* @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('a')
            //->setFirstResult(0)
            //->setMaxResults($limit)
            ->orderBy('a.nombreProducto');

        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $param) {
                $qb->andWhere("a.$key = ?1")
                    ->setParameter(1, $param);
            }
        }

        return $qb->getQuery()->getResult();
    }
} 