<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Repository;

/**
 * Description of DiagnosticoRepository
 *
 * @author German Rosales
 */
class DiagnosticoProductivoRepository extends AbstractEntityRepository {
    
    public function getLastByAsociacion($asociacionId)
    {
        $qb = $this->createQueryBuilder('dgp')->
                where('dgp.id = ?1')->
                orderBy('dgp.date desc')->
                setParameter(1, $asociacionId)->setMaxResults(1);
        $lastDiagnostico = $qb->getQuery()->getSingleResult();
        return $lastDiagnostico;
    }
}
