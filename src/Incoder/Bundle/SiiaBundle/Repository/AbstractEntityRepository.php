<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Description of AbstractEntityRepository
 *
 * @author chamanx
 */
class AbstractEntityRepository extends EntityRepository {

    /**
     * Find all elements from an Entity
     *
     * @param int   $page
     * @param int   $limit
     * @param array $parameters
     *
     * @return array
     */
    public function findAllPaginated($page, $limit, $parameters = []) {
        $qb = $this->createQueryBuilder('a');

        if (is_array($parameters) && count($parameters) > 0) {
            foreach ($parameters as $key => $param) {
                $qb->andWhere("a.$key = ?1")
                        ->setParameter(1, $param);
            }
        }
        return $qb->getQuery()->getResult();
    }

}
