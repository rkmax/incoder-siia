;
(function () {
    $(function () {

        $('.add-link').click(function (e) {
            e.preventDefault();

            var $collection = $(this).parents('[data-prototype]'),
                prototye = $collection.data('prototype');


            if(typeof $collection.data('index') === 'undefined'){
                $collection.data('index', $collection.find('tr td:first-child input, tr td:first-child select').size());
            }

            var index = $collection.data('index'),
                newRow = prototye.replace(/__name__/g, index);

            $collection.find('tr:last-child').before(newRow);
            $collection.data('index', index + 1);
        });

        $(document).on('click', '.remove-link', function () {
            var $this = $(this);
            $this.parent().parent().remove();
        });

    });
})();
