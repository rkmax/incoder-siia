/**
 * Created by julian reyes on 2/05/14.
 */
;(function() {
    "use strict";

    angular.module('generator', [])
        .config(['$compileProvider', function($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|file):|data:image\/|data:application\//);
        }])
        .filter('unsafe', function($sce) {
            return function(val) {
                return $sce.trustAsHtml(val);
            };
        })
        .directive('jreSortable', function() {
            return {
                scope: {
                    jreSortable: '='
                },
                link: function (scope, el) {
                    el.sortable({
                        cursor: 'pointer',
                        start: scope.startSort,
                        update: scope.endSort
                    })
                },
                controller: function ($scope) {
                    $scope.startSort = function(ev, ui) {
                        ui.item.data('start', ui.item.index());
                    };
                    $scope.endSort = function (ev, ui) {
                        var start = ui.item.data('start'),
                            end = ui.item.index();

                        $scope.jreSortable.splice(end, 0, $scope.jreSortable.splice(start, 1)[0]);
                        $scope.$apply();
                    };
                }
            };
        })
        .directive('jreDraggable', function() {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    element.addClass('enable-drag');
                    element.draggable({
                        revert: true,
                        helper: 'clone',
                        appendTo: 'body'
                    });
                },
                controller: function ($scope) {}
            };
        })
        .directive('jreDroppable', function() {
            return {
                restrict: 'A',
                scope: {
                    jreDroppable: '=',
                    maxItems: '@'
                },
                link: function(scope, el) {

                    var existsIn = function (o, col) {
                        var i;
                        for (i = 0; i < col.length; i++) {
                            var _col = _.omit(col[i], '$$hashKey', 'comp', 'value', 'free');
                            if (JSON.stringify(_col) === JSON.stringify(o)) {
                                return true;
                            }
                        }

                        return false;
                    };

                    el.droppable({
                        drop: function (event, ui) {
                            var item = JSON.parse(JSON.stringify(angular.element(ui.draggable[0]).scope().item)),
                                max = scope.maxItems || 2;
                            if (scope.jreDroppable.length < max) {
                                if (!existsIn(item, scope.jreDroppable)) {
                                    scope.jreDroppable.push(item);
                                    scope.$apply();
                                }
                            }
                        }
                    });
                },
                controller: function ($scope) {}
            };
        })
        .controller('reporting', ['$scope', '$http', function($scope, $http) {
            $scope.selected_object = null;
            $scope.selected_chart = null;
            $scope.selected_fields = [];
            $scope.selected_filters = [];
            $scope.selected_options = {
                ranged: false,
                ranged_groups: 5
            };

            $scope.data_url = null;
            $scope.values_url = null;
            $scope.results_url = null;
            $scope.image_base64_url = null;
            $scope.excel_base64_url = null;

            $scope.available_objects = [
                { label: 'Caracterización asociación', value: 'caracterizacion_asociacion' },
                { label: 'Perfil socioeconómico', value: 'perfil_socioeconomico' },
                { label: 'Diagnóstico técnico', value: 'diagnostico_tecnico' }
            ];
            $scope.available_fields = [];
            $scope.available_comps = [
                { value: "E", label: "= (Igual)" },
                { value: "NE", label: "<> (Diferente)" },
                { value: "GT", label: ">  (Mayor que)" },
                { value: "GTE", label: ">= (Mayor o igual) " },
                { value: "LT", label: "< (Menor que)" },
                { value: "LTE", label: "<= (Menor o igual)" }
            ];
            $scope.available_charts = [
                { value: 'bar', label: 'Barras' },
                { value: 'bar_h', label: 'Barras horizontales' },
                { value: 'pie', label: 'Pastel' }
            ];
            $scope.available_fields = {};
            $scope.original_data = [];

            $scope.getValuesFor = function (fullName) {
                if (!(fullName in $scope.available_fields)) {
                    $scope.available_fields[fullName] = [];

                    $http({
                        method: 'GET',
                        url: $scope.values_url,
                        params: {
                            query: fullName
                        }
                    })
                        .success(function(data) {
                            $scope.available_fields[fullName] = _.uniq(_.flatten(data));
                        });
                }

                return $scope.available_fields[fullName];
            };

            $scope.removeSelected = function (item) {
                removeItem('selected_fields', item);
            };
            $scope.removeFilter = function (item) {
                removeItem('selected_filters', item);
            };

            var generateExcel = function() {
                var indicatorsWorkbook = ExcelBuilder.createWorkbook();
                var indicatorsSheet = indicatorsWorkbook.createWorksheet({name: 'Data'});

                indicatorsSheet.setData($scope.original_data);

                indicatorsWorkbook.addWorksheet(indicatorsSheet);

                var data = ExcelBuilder.createFile(indicatorsWorkbook);
                $scope.excel_base64_url = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + data;
            };

            $scope.generateChart = function() {
                var params = {
                    object: $scope.selected_object,
                    fields: $scope.selected_fields,
                    filters: $scope.selected_filters,
                    chart: $scope.selected_chart,
                    options: $scope.selected_options
                };

                $scope.image_base64_url = null;

                function is_numeric(mixed_var) {
                    //  discuss at: http://phpjs.org/functions/is_numeric/

                    var whitespace =
                        " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
                    return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
                        1)) && mixed_var !== '' && !isNaN(mixed_var);
                }

                var getTotalized = function(data) {
                    var  i, j, item = [];

                    item[0] = "Total";

                    for (i = 0; i < data.length; i++) {
                        for (j = 1; j < data[i].length; j++) {
                            if (is_numeric(data[i][j])) {
                                if (typeof item[j] === 'undefined') {
                                    item[j] = 0;
                                }
                                item[j] += data[i][j];
                            }
                        }
                    }

                    data.push(item);

                    return data;
                }

                $http({
                    method: 'POST',
                    url: $scope.results_url,
                    data: params
                })
                    .success(function(result) {

                        if (result.data.length == 0) {
                            alert("No se encontrado resultados");
                            return;
                        }

                        var data = google.visualization.arrayToDataTable(result.data),
                            chart_div = $('#chart-result')[0],
                            chart,
                            table = new google.visualization.Table($('#chart-table')[0]);

                        $scope.original_data = result.data;

                        switch ($scope.selected_chart.value) {
                            case 'bar':
                                chart = new google.visualization.ColumnChart(chart_div);
                                break;
                            case 'bar_h':
                                chart = new google.visualization.BarChart(chart_div);
                                break;
                            case 'pie':
                                chart = new google.visualization.PieChart(chart_div);
                                break;
                        }

                        chart.draw(data, $.extend({
                            animation: {
                                duration: 5
                            }
                        }, result.options));
                        table.draw(google.visualization.arrayToDataTable(getTotalized(result.data)));

                        $scope.image_base64_url = chart.getImageURI();
                        generateExcel();
                    });
            };

            $scope.$watch('selected_object', function(newVal, oldVal) {
                if (newVal != '' && newVal != oldVal && typeof newVal !== 'undefined' && newVal != null) {
                    getFieldList(newVal);
                } else {
                    $scope.available_fields = [];
                }
            });

            var getFieldList = function(val) {
                $http({
                    method: 'GET',
                    url: $scope.data_url,
                    params: {object: val.value}
                })
                    .success(function(data) {
                        $scope.available_fields = data;
                    });
            };

            var removeItem = function (collection, item) {
                $scope[collection] = _.filter($scope[collection], function(field) {
                    return JSON.stringify(field) !== JSON.stringify(item);
                });
            };
        }]);
})();