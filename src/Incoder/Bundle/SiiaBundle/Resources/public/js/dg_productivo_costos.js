/**
 * Created by julian reyes escrigas on 2/10/14.
 */

;
(function () {
    $(function () {
        var $collection = $('[data-prototype]'),
            $selector = $('.mes-selector'),
            prototye = $collection.data('prototype'),
            disableValue = function (value) {
                $selector.find('option[value="' + value + '"]').attr('disabled', 'disabled');
                $selector.prop('selectedIndex', 0);
            },
            enableValue = function (value) {
                $selector.find('option[value="' + value + '"]').attr('disabled', null);
            };

        $collection.find('tr td:first-child input[type="text"]').each(function (k, v) {
            disableValue($(v).val());
        });

        $collection.data('index', $collection.find('tr td:first-child input[type="text"]').size());

        $('.add-link').click(function (e) {
            e.preventDefault();

            var index = $collection.data('index'),
                newRow = prototye.replace(/__name__/g, index),
                value = $selector.val();

            if (!value) return;

            $collection.find('tr:last-child').before(newRow);
            $collection.find('tr:last-child').prev().find('input').eq(0).val(value);
            $collection.data('index', index + 1);
            disableValue(value);
        });

        $collection.on('click', '.remove-link', function () {
            var $this = $(this),
                value = $this.parent().parent().find('td:first-child input[type="text"]').val();

            enableValue(value);
            $this.parent().parent().remove();
        });
    });
})();
