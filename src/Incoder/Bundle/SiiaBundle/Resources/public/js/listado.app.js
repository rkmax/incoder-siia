/**
 * Created by julian reyes on 22/05/14.
 */

;(function() {
    "use strict";

    angular.module('listado', [])
        .controller('caracterizacion', ['$scope', '$http', function($scope, $http) {
            $scope.searchByList = [];
            $scope.searchBy = null;
            $scope.searchUrl = null;
            $scope.searchQuery = null;
            $scope.actionsUrl = null;
            $scope.previousBy = null;
            $scope.updateSearchBy = function (item) {

                if (!item) {
                    return;
                }

                if (_.isObject(item)) {
                    $scope.searchBy = item;
                } else {
                    $scope.previousBy = item;
                    $scope.searchBy = _.find($scope.searchByList, function(i) {
                        return i.id == item;
                    });
                }
            };
            $scope.searchNow = function() {
                if ($scope.searchBy == null) {
                    return;
                }

                var query = {};
                query[$scope.searchBy.id] =  $scope.searchQuery;
                location.replace($scope.searchUrl + '?' + $.param(query));
            };
            $scope.clearSearch = function() {
                location.replace($scope.searchUrl);
            };

            $scope.$watch('actionsUrl', function(newVal, oldVal) {
                $http
                    .get($scope.actionsUrl).success(function(data){
                        $scope.searchByList = data;
                        $scope.updateSearchBy($scope.previousBy);
                    });
            });

        }])
    ;
})();
