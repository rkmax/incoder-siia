/**
 * Created by julian on 3/29/14.
 */
;(function() {
    "use strict";

    var TMPL = {
            options: Handlebars.compile($('#tmpl-options').html())
        },
        $target = $('#form_fields'),
        $choice = $('#object')
    ;

    $(function() {
        $choice.on('change', function() {
            $target.empty();
            
            if ($choice.val() == "") return;

            $.getJSON($choice.data().url, {'object': $choice.val()}, function (results) {
                $target.append(TMPL.options({acciones: results}));
            });
        })
    });
})();
