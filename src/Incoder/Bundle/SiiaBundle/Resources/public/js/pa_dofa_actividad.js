/**
 * Created by julian on 3/3/14.
 */

;(function() {
    var addElement = function() {
        var $collection = $(this).parents('.collection'),
            $list = $collection.find('.list'),
            prototype = $list.attr('data-prototype'),
            limit = $collection.data().limit,
            index,
            $append;

        if (typeof limit !== 'undefined' && $list.find('li').size() >= limit) {
            alert("Solo puede agregar " + limit + " elementos de este tipo");
            return;
        }

        if (typeof $collection.data('index') === 'undefined') {
            $collection.data('index', $list.find('li').size());
        }

        index = $collection.data('index');
        $append = $(prototype.replace(/__name__/g, index));
        $append.find('[data-autocomplete]').each(function(k, v) {
            SIIACEnableAutocomplete(v);
        });

        $list.append($append);


        $collection.data('index', index + 1);

        $collection.find('input:not(.form-control)').addClass('form-control');

    };

    var removeElement = function(e) {
        e.preventDefault();
        var $el = $(this).parents('li');

        $el.remove();
    };

    var selectFormat = function(option) {
        if (!option.id) return option.text;

        return "<p>" + option.text + "</p>";
    };

    $(function() {
        $(document).find('.collection').each(function(k, v) {
            var $button = $('<a></a>', {'text': '+', 'href': '#'}),
                $label = $(v).find('label');
            $button.on('click', addElement);

            $label.text($label.text() + " ");
            $(v).find('label').append($button);
        });
        $(document).on('click', '.remove', removeElement);
        $('select[name*="accion"]').select2({
            formatResult: selectFormat,
            formatSelection: selectFormat,
            escapeMarkup: function(m) { return m; }
        });
        $('[name="dofa_actividad[actividad]"],[name="dofa_actividad[componente]"]').select2();
        $(document).find('.select2-container').removeClass('form-control').css({
            'width': '100%',
            'height': '34px'
        });
    });
})();
