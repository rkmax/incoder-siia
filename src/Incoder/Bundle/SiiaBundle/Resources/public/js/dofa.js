/**
 * Created by julian on 3/6/14.
 */

;(function() {

    "use strict";


    var TMPL = {
        options: Handlebars.compile($('#tmpl-options').html()),
        list: Handlebars.compile($('#tmpl-list').html()),
        componentes: Handlebars.compile($('#tmpl-componentes').html())
    };

    var updateTitle = function() {
        var $panel = $(this).parents('.panel'),
            title = $(this).val();
        $panel.find('.panel-title > a:first-child').text(title || "Nueva Actividad");
    };

    var getComponente = function() {
        var $el = $(this),
            $targetEl = $el.parents('.panel').find('[name*="[accion]"]'),
            $targetDiv = $('.componente'),
            targetElSelected = $targetEl.val(),
            url = URL_COMP,
            setoptions = function(data) {
                $targetDiv.children().remove();
                $targetEl.children().remove();
                $targetEl.append(TMPL.options(data));
                $targetDiv.append(TMPL.list(data));
                $targetEl.val(targetElSelected);
            };

        if ($el.val() === "" || $el.val() === null) {
            setoptions({acciones: [{id: '', description: 'Seleccione un componente'}]});
            cleanLateral();
            return;
        }

        $.getJSON(url.replace('__ID__', $el.val()), setoptions);
    };

    var trimData = function (data) {
        var o = {};
        $.each(data, function (k, v) {
            var m = k.match(/^(model)(\w+)$/i);
            if (m) {
                o[m[2].toLowerCase()] = v;
            }
        });

        return o;
    }

    var addActividad = function(e) {
        e.preventDefault();
        var $collection = $('.panel-group'),
            $el,
            index;

        if (typeof $collection.data('index') === 'undefined') {
            $collection.data('index', $collection.find('.panel').size());
        }

        index = $collection.data('index');
        $collection.data('index', index + 1);

        $el = $($collection.data().prototype.replace(/__name__/g, index));
        $el.find('[name*="[componente]"]').append(TMPL.componentes());

        $collection.append($el);

        App.setClasses();
    };

    var removeActividad = function(e) {
        e.preventDefault();
        var $el = $(this).parents('.panel');

        if ($('.panel-group').find('.in').size() == 0) {
            cleanLateral();
        }

        $el.remove();
    };

    var addElement = function(type, data) {
        var $list = $('.in [id*="' + type + '"]'),
            index,
            $result,
            exist = false;

        if (typeof $list.data('index') === 'undefined') {
            $list.data('index', $list.find('li').size());
        }

        index = $list.data('index');

        if (index > 0) {
            $.each($('.in [name*="[description]"]'), function(k, v) {
                if ($(v).val() === data.description) {
                    exist = true;
                }
            });
        }

        if (exist) {
            throw 'Ya existe el elemento, no se hace nada';
        }

        $list.data('index', index + 1);

        $result = $($list.data().prototype.replace(/__name__/g, index));
        $result.find('input[type="text"]').prop('readonly', 'readonly');
        $result.find('input[type="text"]').css({'border': '0'});
        $result.find('input[type="text"]').css({'border': '0', width: '95%'});

        $.each(data, function(k, value) {
            $result.find('[name*="[' + k + ']"]').val(value);
        });

        $list.append($result);
    };

    var removeElement = function(e) {
        e.preventDefault();
        var $el = $(this).parents('li');

        $el.remove();
    };

    var clickElement = function(e) {
        e.preventDefault();
        var $el = $(this),
            operation = $el.attr('href').match(/(\w+)\/(add|remove)$/);

        if (!operation) {
            return;
        }

        switch (operation[2]) {
            case 'add':
                addElement(operation[1], trimData($el.data()));
                break;
            default:
                throw 'Operacion no soportada';
                break;
        }
    };

    var cleanLateral = function() {
        var $comp = $('.componente');
        $comp.children().remove();
        $comp.append($('<p></p>', {'html': $comp.data().original}));
    };

    $(function() {
        $(document.body).tooltip({
            selector: '[data-tooltip]'
        });
        $(document.body).on('keyup','[name*="[descripcion]"]', updateTitle);
        $(document.body).on('change', '[name*="[componente]"]', getComponente);
        $(document.body).on('click', '[data-model]', clickElement);
        $(document.body).on('click', '.remove', removeElement)
        ;
        // Elimina o carga los componentes en la barra lateral
        $(document.body).on('hidden.bs.collapse', '.panel-collapse', cleanLateral);
        $(document.body).on('shown.bs.collapse', '.panel-collapse', function() {
            var $field = $(this).find('[name*="[componente]"]');
            if ($field.val() !== "") {
                $field.trigger('change');
            }
        });

        $(document.body).on('click', '.new-activity', addActividad);
        $(document.body).on('click', '.remove-activity', removeActividad);

        $('.save-dofa').on('click', function() {
            $('form').submit();
        })

        $('.list').find('input[type="text"]').prop('readonly', 'readonly');
        $('.list').find('input[type="text"]').removeClass('form-control');
        $('.list').find('input[type="text"]').css({'border': '0', width: '95%'});


    });
})();
