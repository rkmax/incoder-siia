/**
 * Created by julian on 3/29/14.
 */
;(function() {
    "use strict";

    var $debilidad_selector = $('#debilidades-selector'),
        $actividad_selector = $('input[name*="actividad"]'),
        $asociacion = $('input[name*="asociacion"]'),
        TMPL_debilidades = Handlebars.compile($('#tmpl-debilidades').html()),
        TPML_actividad = Handlebars.compile($('#tmpl-actividad').html()),
        s_results = {},
        TPML_actividad_sel = Handlebars.compile($('#tmpl-actividad-select').html()),
        updateStyles = function () {

            $(document).find('.select2-container').removeClass('form-control').css({
                'width': '100%',
                'height': '34px'
            });
        },
        obtainActividades = function () {

            if ($debilidad_selector.val() == "") return;

            $.getJSON(
                $debilidad_selector.data().activityUrl,
                {'debilidad': $debilidad_selector.val(), asociacion: $asociacion.val()},
                function(results) {
                    s_results = {};
                    $.map(results, function(val) {
                        val['text'] = val.id;
                        s_results[val.id] = val;
                    });

                    $actividad_selector.select2({
                        width: $('.select2-container').width(),
                        data: results,
                        formatResult: function (actividad) {
                            return TPML_actividad(actividad);
                        },
                        formatSelection: function (actividad) {
                            return TPML_actividad_sel(actividad);
                        },
                        escapeMarkup: function (m) { return m; },
                        matcher: function(term, id) {
                            var text = s_results[id].actividad.descripcion;

                            return text.toUpperCase().indexOf(term.toUpperCase())>=0;
                        }
                    });
                }
            );
        },
        optainDebilidades = function() {
            $debilidad_selector.empty();
            $.getJSON(
                $debilidad_selector.data().url,
                {asociacion: $asociacion.val()},
                function(results) {
                    $debilidad_selector.html(TMPL_debilidades({results: results}));
                    $debilidad_selector.select2();

                    updateStyles();
                }
            )
        };


    $(function() {
        optainDebilidades();
        $actividad_selector.select2({
            placeholder: 'Seleccione actividad',
            data: []
        });

        $debilidad_selector.on('change', obtainActividades);
        //$actividad_selector.on('change', function() {
        //    $('input[name*="actividad"]').val($actividad_selector.val());
        //});
    });
})();
