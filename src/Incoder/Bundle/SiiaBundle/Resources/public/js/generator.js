/**
 * Contiene la logica de negocio asociada a la vista del generador de informes
 *
 * @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 */


;(function(window) {
    "use strict";

    var series = {
            x: {
                lbl: "",
                value: null
            },
            y: {
                lbl: "",
                value: null
            }
        },
        helpers = {},
        filter = [],
        config = {
            colors: [],
            colors_index: 0,
            chartToShow: 'bar',
            interval: null,
            result_div: 'chart-result',
            table_div: 'chart-table',
            chart: '#options_chart',
            chart_details: "#chart-details",
            action: '#generate-btn',
            path: '#results_path',
            debug: false,
            field_list: '#form_fields',
            btn_set: {
                x: '#btn_set_serie_x',
                y: '#btn_set_serie_y'
            },
            field: {
                x: {
                    lbl: '#bar_x_lbl',
                    value: '#bar_x'
                },
                y: {
                    lbl: '#bar_y_lbl',
                    value: '#bar_y'
                }
            }
        };

    /**
     * Fija los valores de una serie, segun la opcion marcada
     *
     * @param serieName
     */
    helpers.setSerie = function setSerie(serieName) {
        var value = $(config.field_list).val();

        if (null === value) {
            throw "No se ha seleccionado un campo";
        }

        var label = $(config.field_list).find('option:selected').text();

        $.each(config.field, function(k, v) {
            if (k === serieName) return;
            if ($(v.value).val() === value) {
                $(v.value).val(null);
                $(v.lbl).val('');
            }
        });

        $(config.field[serieName].lbl).val(label);
        $(config.field[serieName].value).val(value);

        $(config.field_list).val(null);
    };

    helpers.setFilter = function setFilter(e)
    {
        e.preventDefault();

        var $button = $(this),
            label = $(config.field_list).find('option:selected').text(),
            value = $(config.field_list).val(),
            $parent = $button.parents('.form-group');

        if (null === value) {
            throw "No se ha seleccionado un campo";
        }

        $(config.field_list).val(null);

        $parent.find('[readonly]').val(label);
        $parent.find('[type="hidden"]').val(value);
    }

    helpers.clearFilter = function clearFilter(e)
    {
        e.preventDefault();

        var $button = $(this),
            $parent = $button.parents('.form-group');

        $parent.find('input, select').val(null);
    }

    /**
     * Devuelve los valores listos para la consulta
     *
     * @returns {{params: {x: {lbl: string, value: null}, y: {lbl: string, value: null}}, chart: (*|jQuery)}}
     */
    helpers.getSeries = function getSeries() {
        series = {};

        $.each(config.field, function(k, v) {
            series[k] = {
                value: $(v.value).val(),
                label: $(v.lbl).val()
            };
        });


        var filter = {
            field: $('[name="filter[0][field]"]').val(),
            comp: $('[name="filter[0][comp]"]').val(),
            value: $('[name="filter[0][value]"]').val()
        };

        config.chartToShow = $(config.chart).val();

        if (null === filter.value || "" === filter.value) {
            return {params: series, chart: config.chartToShow, object: $('[name="form[object]"]').val()};
        } else {
            return {params: series, filters: filter, chart: config.chartToShow, object: $('[name="form[object]"]').val()};
        }
    };


    /**
     * Solicita los datos al servidor
     */
    helpers.requestData = function requestData() {

        $.getJSON($(config.path).val(), helpers.getSeries(), function(result) {
            
            var data = new google.visualization.arrayToDataTable(result.data),
                chart,
                table = new google.visualization.Table(document.getElementById(config.table_div));

            switch (config.chartToShow) {
                case 'bar':
                    chart = new google.visualization.ColumnChart(document.getElementById(config.result_div));
                    break;
                case 'pie':
                    chart = new google.visualization.PieChart(document.getElementById(config.result_div));
                    break;

            }
            
            chart.draw(data, $.extend({
                animation: {
                    duration: 5
                }
            }, result.options));

            table.draw(data);
        });
    }

    $(function() {
        // Enlaza los botones
        $.each(config.btn_set, function(k, v) {
            if (config.debug) console.debug('[' + v +'] OnClick Event handler');
            $(v).click(function() {
                helpers.setSerie(k);
            });
        });

        $(config.action).click(helpers.requestData);

        // Enlaza botones filtros
        $('fieldset [data-role="add-filter"]').on('click', helpers.setFilter);
        $('fieldset [data-role="clear-filter"]').on('click', helpers.clearFilter);
    })
})(window);
