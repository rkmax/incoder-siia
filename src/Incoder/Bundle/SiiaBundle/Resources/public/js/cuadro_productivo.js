/**
 * Created by chamanx on 3/2/14.
 * Actualizado por Julian Reyes 24/Mar/2014
 */
;
(function () {
    $(function () {

        var BASE = (ENV === 'dev' ? '/app_dev.php' : ''),
            URL_API = BASE + '/productivo/productos/__ID__/show.json',
            setSpan = function() {
                $('[name*="volumen_"], [name*="valor_"]').each(function(k, v) {
                    var $label = $(v).parents('.form-group').find('label'),
                        text = $label.text();

                    $label.empty();
                    $label.append($('<span>', {text: text}));
                    $label.append($('<span>', {'class': 'units'}));
                });


                var $label_p = $('label[for="' + $('select[name*="producto"]').attr('id') + '"]'),
                    text_p = $label_p.text();

                $label_p.empty();
                $label_p
                    .append($('<span></span>', {'text': text_p}))
                    .append($('<span></span>', {'class': 'cultivo', 'style': 'text-transform: capitalize;'}));

            },
            processProduct = function(product) {
                $('.units').each(function(k, v) {
                    var $unit = $(v);

                    if ($unit.parents('label[for*="produccion"], label[for*="producido"]').size() > 0) {
                        $unit.text( " (" + product.unidadProduccion.nombre + ")");
                    }

                    if ($unit.parents('label[for*="vendido"]').size() > 0) {
                        $unit.text( " (" + product.unidadVenta.nombre + ")");
                    }

                    if ($unit.parents('label[for*="valor"]').size() > 0) {
                        $unit.text( " (" + product.unidadVenta.nombre + ")");
                    }

                    if (typeof product.tipoCultivo != 'undefined') {
                        $('.cultivo').text( " " + product.tipoCultivo);

                        if ($('input[name*="tipo_cultivo"]').size() > 0) {
                            $('input[name*="tipo_cultivo"]').val(product.tipoCultivo);
                            $('input[name*="tipo_cultivo"]').change();
                        }
                    }
                });
            },
            $producto = $('select[name*="producto"]'),
            updateUnits = function() {
                var val = $producto.val();

                if (typeof val !== 'undefined' && val !== "" && val != null) {
                    $('.units, .cultivo').empty();
                    $.getJSON(URL_API.replace(/__ID__/, val), processProduct);
                }
            };

        $('.add-link').click(function (e) {
            e.preventDefault();

            var $collection = $(this).parents('[data-prototype]'),
                prototye = $collection.data('prototype');


            if(typeof $collection.data('index') === 'undefined'){
                $collection.data('index', $collection.find('tr td:first-child input, tr td:first-child select').size());
            }

            var index = $collection.data('index'),
                newRow = prototye.replace(/__name__/g, index);

            $collection.find('tr:last-child').before(newRow);
            $collection.data('index', index + 1);
        });

        $(document).on('click', '.remove-link', function () {
            var $this = $(this);
            $this.parent().parent().remove();
        });

        $producto.on('change', updateUnits);

        $producto.select2();

        $(document).find('.select2-container').removeClass('form-control').css({
            'width': '100%',
            'height': '34px'
        });

        setSpan();
        updateUnits();

    });
})();
