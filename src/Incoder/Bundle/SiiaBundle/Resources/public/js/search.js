/**
 * Created by julian on 4/4/14.
 */

;(function() {
    "use strict";

    var isRunning = function ($link) {
        if (typeof $link.data().running === 'undefined') {
            $link.data('running', true);
        } else {
            return $link.data().running;
        }

        return false;
    };

    var getProductsDetails = function (e) {

        e.preventDefault();

        var $link = $(this),
            template = Handlebars.compile($('#tpml-productos').html()),
            $target = $link.siblings('.products');

        if (isRunning($link)) {
            return;
        }

        $target.empty();

        $.getJSON($link.data().url, function(results) {
            $link.data().running = false;
            $link.remove();
            $target.append(template({
                productos: results,
                search_url: SEARCH_URL
            }));
        });
    };

    var getInsumoDetauls = function(e) {
        e.preventDefault();

        var $link = $(this),
            template = Handlebars.compile($('#tpml-insumos').html()),
            $target = $link.siblings('.insumos');

        if (isRunning($link)) {
            return;
        }

        $target.empty();

        $.getJSON($link.data().url, function(results) {
            $link.remove();
            $target.append(template({insumos: results}));
        });
    };

    $(function() {
        $('.show-products').on('click', getProductsDetails);
        $('.show-insumos').on('click', getInsumoDetauls);
    });
})();
