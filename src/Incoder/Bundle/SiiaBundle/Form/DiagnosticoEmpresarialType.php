<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Incoder\Bundle\SiiaBundle\Form\DiagnosticoEmpresarialRespuestaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiagnosticoEmpresarialType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaDiagnostico', 'date', array(
                'attr' => [
                    'data-provide' => 'datepicker',
                    'data-date-format' => 'dd/mm/yyyy',
                    'class' => 'datepicker'
                ],
                'format' => 'dd/MM/yyyy',
                'widget' => 'single_text',
                'label' => 'Fecha diagnóstico'
            ))
            ->add('asociacion', 'entity_hidden', array(
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\Asociacion'
            ))
            ->add('respuesta', 'collection', array(
                'type' => new DiagnosticoEmpresarialRespuestaType(),
                'allow_add' => true,
                'label' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial'
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_asociacion_diagnosticoempresarial';
    }
}
