<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Doctrine\ORM\EntityRepository;
use Incoder\Bundle\SiiaBundle\Repository\AsociacionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AsesorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellidos')
            ->add('documentoIdentidad', null , [
                'required' => true,
                'label' => 'Documento de identidad'
            ])

            ->add('email', 'email', [
                'label' => 'Correo electrónico',
                'required' => true,
            ])

            ->add('user', 'text', [
                'label' => 'Nombre usuario',
                'required' => true,
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])

            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Las contraseñas deben coincidir.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => $options['password_required'],
                'first_options'  => array('label' => 'Contraseña'),
                'second_options' => array('label' => 'Repetir contraseña'),
            ))
            ->add('asociaciones', null, [
                'query_builder' => function (AsociacionRepository $repository) use ($options) {
                        return $repository->findAllByAsesorQB($options['asesores']);
                    },
                'attr' => [
                    'style' => 'width: 450px; height:200px;'
                ]
            ])
            ->add('operador', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\Operador'
            ])
                
            //TODO: asociacion list
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\Asesor',
            'password_required' => true
        ))->setRequired([
            'asesores'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_asesor';
    }
}
