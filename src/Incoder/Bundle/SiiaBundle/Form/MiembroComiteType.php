<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MiembroComiteType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cargo', 'choice', [
                'choices' => [
                    'Presidente' => 'Presidente',
                    'Vicepresidente' => 'Vicepresidente',
                    'Tesorero' => 'Tesorero',
                    'Secretario' => 'Secretario',
                    'Fiscal' => 'Fiscal',
                    'Vocal' => 'Vocal',
                ],
                'label' => 'Cargo',
            ])
            ->add('nivel_educativo', 'choice', [
                'choices' => [
                    'Preescolar' => 'Preescolar',
                    'Básica primaria' => 'Básica primaria',
                    'Básica secundaria' => 'Básica secundaria',
                    'Media académica' => 'Media académica',
                    'Universitaria' => 'Universitaria',
                    'Media académica' => 'Media académica',
                    'Ninguno' => 'Ninguno',
                    'Técnico/tecnológico' => 'Técnico/tecnológico',
                ],
                'label' => 'Nivel educativo',
            ])
            ->add('nombre', 'text')
            ->add('documento', 'text')
            ->add('email', 'email', [ 'required' => false ])
            ->add('telefono', 'text')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\MiembroComite'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'miembro_comite';
    }
}
