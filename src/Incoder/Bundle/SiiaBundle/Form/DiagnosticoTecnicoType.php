<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiagnosticoTecnicoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dform', 'd_form', array(
                'data_class' => $options['data_class']
            ))
            ->add('planos', 'collection',
                    array(
                        'type' => new PlanoType(), 
                        'allow_add' => true, 
                        'by_reference' => false, 
                        'allow_delete' => true,
                        'label' => 'Planos'
                    )
            )
            ->add('caudales', 'collection',
                    array(
                        'type' => new CaudalEstiajeType(),
                        'allow_add' => true, 
                        'by_reference' => false, 
                        'allow_delete' => true,
                        'label' => 'Caudales periodo de estiaje'
                    )
            )
            ->add('miembros', 'collection',
                    array(
                        'type' => new MiembroComiteType(),
                        'allow_add' => true, 
                        'by_reference' => false, 
                        'allow_delete' => true,
                        'label' => 'Miembros de junta directiva'
                    )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_diagnosticotecnico';
    }
}
