<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InsumoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('marca')
            ->add('frecuencia', 'choice', array(
                'choices'   => array(
                    'Diaria' => 'Diaria',
                    'Semanal' => 'Semanal',
                    'Mensual' => 'Mensual',
                    'Quincenal' => 'Quincenal',
                    'Trimestral' => 'Trimestral',
                    'Cuatrimestral' => 'Cuatrimestral',
                    'Semestral' => 'Semestral',
                    'Anual' => 'Anual'),
                'required'  => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\Insumo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dg_productivo_insumo';
    }
}
