<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Range;

class ProduccionType extends AbstractType
{
    /**
     * @var Translator
     */
    private $translator;

    /*public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }*/

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dform', 'd_form', array(
                'data_class' => $options['data_class']
            ))
            ->add('costoProducto', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\CostoProducto'
            ])
            ->add('mes', null, [
                'read_only' => true
            ])
            ->add('anio', 'date', [
                'widget' => 'choice',
                'format' => 'yyyyMMdd',
                'years' => range(date('Y')-20, date('Y')+1),
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\Produccion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_produccion';
    }
}
