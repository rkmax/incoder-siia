<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/18/14
 * Time: 7:20 PM
 */

namespace Incoder\Bundle\SiiaBundle\Form\PlanAccion;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlanAccionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('afirmacion', 'entity', array(
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion',
                'empty_value' => !$options['id'] ? 'Seleccione afirmación' : false,
                'query_builder' => function(EntityRepository $er) use ($options) {
                    if (!$options['id']) {
                        return $er->findAllWithoutOptions();
                    } else {
                        return $er->createQueryBuilder('a')
                           ->where('a.id = :id')
                           ->setParameter('id', $options['id']);
                    }
                }
            ))
            ->add('opciones', 'collection', array(
                'type' => new OpcionPorAfirmacionType(),
                'allow_add' => true
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccion',
            'id' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_planaccion';
    }
}
