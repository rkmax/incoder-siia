<?php

namespace Incoder\Bundle\SiiaBundle\Form\PlanAccion;


use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DebilidadType extends AbstractDOFAElementType
{

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Debilidad',
            'error_bubbling' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_planaccion_debilidad';
    }
}
