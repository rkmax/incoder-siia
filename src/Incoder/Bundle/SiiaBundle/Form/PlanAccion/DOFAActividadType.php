<?php

namespace Incoder\Bundle\SiiaBundle\Form\PlanAccion;

use Doctrine\ORM\EntityManager;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Componente;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionElemento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DOFAActividadType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @SuppressWarnings("unused")
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'hidden')
            ->add('componente')
            ->add('accion', null ,[
                'property' => 'uniqueName'
            ])
            ->add('actividad')
            ->add('debilidades', 'collection', [
                'type' => new DebilidadType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('oportunidades', 'collection', [
                'type' => new OportunidadType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('fortalezas', 'collection', [
                'type' => new FortalezaType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('amenazas', 'collection', [
                'type' => new AmenazaType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dofa_actividad';
    }
}
