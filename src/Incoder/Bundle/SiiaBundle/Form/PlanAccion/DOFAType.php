<?php

namespace Incoder\Bundle\SiiaBundle\Form\PlanAccion;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DOFAType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @SuppressWarnings("unused")
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('asociacion', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\Asociacion'
            ])
            ->add('actividad', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad',
            ])
            ->add('isComplete', null, [
                'label' => 'Completada'
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFA'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_planaccion_dofa';
    }
}
