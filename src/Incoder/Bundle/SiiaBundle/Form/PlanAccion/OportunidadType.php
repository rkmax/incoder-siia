<?php

namespace Incoder\Bundle\SiiaBundle\Form\PlanAccion;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OportunidadType extends AbstractDOFAElementType
{
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Oportunidad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_planaccion_oportunidad';
    }
}
