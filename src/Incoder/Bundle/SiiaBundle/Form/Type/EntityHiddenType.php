<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/21/14
 * Time: 1:39 PM
 */

namespace Incoder\Bundle\SiiaBundle\Form\Type;


use Doctrine\Common\Persistence\ObjectManager;
use Incoder\Bundle\SiiaBundle\Form\DataTransformer\EntityToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EntityHiddenType extends AbstractType {
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToIdTransformer($this->objectManager, $options['class']);
        $builder->addModelTransformer($transformer);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'class' => null,
            'invalid_message' => 'The entity does not exist.',
        ))->setRequired([
                'class'
            ]);
    }

    public function getParent()
    {
        return 'hidden';
    }

    public function getName()
    {
        return 'entity_hidden';
    }
} 
