<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/28/14
 * Time: 11:05 PM
 */

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CuadroPecuarioType
 * @package Incoder\Bundle\SiiaBundle\Form
 */
class CuadroPecuarioType extends AbstractType{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('asociado', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\Asociado'
            ])
            //->add('producto')
            ->add('dform', 'd_form', array(
                'data_class' => $options['data_class']
            ))
            ->add('insumos', 'collection', [
                'type' => new InsumoType(),
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dg_productivo_cuadro_pecuario';
    }
} 
