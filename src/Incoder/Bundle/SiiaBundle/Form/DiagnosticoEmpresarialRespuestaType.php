<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiagnosticoEmpresarialRespuestaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('diagnostico', 'entity_hidden', array(
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial'
            ))
            ->add('afirmacion', 'entity_hidden', array(
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion'
            ))
            ->add('opcion')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_asociacion_diagnosticoempresarialrespuesta';
    }
}
