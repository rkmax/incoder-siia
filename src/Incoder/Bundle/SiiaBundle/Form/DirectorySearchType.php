<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Form;

/**
 * Description of DirectorySearchType
 *
 * @author chamanx
 */
class DirectorySearchType extends \Symfony\Component\Form\AbstractType{
    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nombre', 'text', ['required' => FALSE])
            ->add('tipoProducto', 'text', ['required' => FALSE])
            //->add('variedad', 'text', ['required' => FALSE])
            ->add('catidad', 'number', ['required' => FALSE])
            ->add('areaSembrada', 'number', ['required' => FALSE])
            //->add('fechaProduccion', 'date', ['required' => FALSE])
            ->add('vereda', 'text', ['required' => FALSE])
            ->add('municipio', 'text', ['required' => FALSE])
            ->add('departamento', 'text', ['required' => FALSE])
            ->add('send', 'submit');
    }

    public function getName() {
        return "incoder_bundle_siiabundle_asociacion_search_filters";
    }
    
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
        $resolver->setDefaults([ 'data_class' => 'Incoder\Bundle\SiiaBundle\Lib\AsociacionSearchFilters']);
    }


}
