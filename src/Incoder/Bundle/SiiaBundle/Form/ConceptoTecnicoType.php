<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 11/03/14
 * Time: 10:01 AM
 */

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConceptoTecnicoType extends AbstractType{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('diagnostico', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico'
            ])*/
            ->add('concepto')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\ConceptoTecnico'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dg_tecnico_concepto';
    }
}