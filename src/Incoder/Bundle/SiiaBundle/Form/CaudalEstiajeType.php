<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/18/14
 * Time: 5:57 PM
 */

namespace Incoder\Bundle\SiiaBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CaudalEstiajeType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'date', [
                'label' => 'Fecha',
                'attr' => [],
                'input' => 'datetime',
                'widget' => 'single_text'
            ])
            ->add('caudal', 'number', [
                'label' => 'Caudal en periodo de estiaje (L/s)',
                'required' => true,
                'attr' => []
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\CaudalEstiaje'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return "caudal_estiaje";
    }
}