<?php

namespace Incoder\Bundle\SiiaBundle\Form\Productivo;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UnidadProduccionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('abreviatura')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\Productivo\UnidadProduccion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_productivo_unidadproduccion';
    }
}
