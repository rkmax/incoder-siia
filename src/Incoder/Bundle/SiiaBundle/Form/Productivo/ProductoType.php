<?php

namespace Incoder\Bundle\SiiaBundle\Form\Productivo;

use Incoder\Bundle\SiiaBundle\Entity\Productivo\Producto;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoAgricola;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreProducto')
            ->add('variedadProducto')
            ->add('unidadProduccion')
            ->add('unidadVenta')
            ->add('tipo', 'choice', [
                'empty_value' => 'Selecione tipo',
                'choices' => [
                    Producto::TAGRICOLA => 'Agrícola',
                    Producto::TPECUARIO => 'Pecuario'
                ]
            ])
            ->add('tipoCultivo', 'choice', [
                'empty_value' => '-- Seleccione tipo cultivo --',
                'required' => false,
                'choices' => [
                    ProductoAgricola::TPERENNE => 'Perenne',
                    ProductoAgricola::TSEMIPEN => 'Semiperenne',
                    ProductoAgricola::TTRANSIT => 'Transitorio'
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\Productivo\Producto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'incoder_bundle_siiabundle_productivo_producto';
    }
}
