<?php

namespace Incoder\Bundle\SiiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlanoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', 'integer', [
                'label' => 'Número',
                'required' => true,
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('contenido', 'text', [
                'label' => 'Contenido',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            /*->add('diagnostico_tecnico', 'entity_hidden', [
                'class' => 'Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico'
            ])*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Incoder\Bundle\SiiaBundle\Entity\Plano'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'planos';
    }
}
