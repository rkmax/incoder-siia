<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Lib\JSAdapters;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Class GoogleChartJSAdapter
 * @package Incoder\Bundle\SiiaBundle\Lib\JSAdapters
 *
 * @ExclusionPolicy("all")
 */
class GoogleChartJSAdapter {

    const blanco = '(en blanco)';

    /**
     * @var array
     */
    private $original_results = [];

    /**
     * @var array
     */
    private $query = [];

    /**
     * @var array
     * @Expose
     */
    private $options = [];

    /**
     * @var array
     * @Expose
     */
    private $data = [];

    /**
     * @var string
     */
    private $titleRow = "";

    private $titleRowSum = "";


    public function __construct($results, array $query)
    {
        $this->original_results = $results;
        $this->query = $query;

        $this->processResultsData();
    }

    private function processSingleAxis()
    {
        $data = [];

        // Obtenemos todos los posibles valores del eje e inicializamos
        foreach ($this->original_results['result'] as $r) {
            $x = reset($r['_id']);
            $s = $r['count'];

            $data[] = [$x, $s];
        }

        $this->processResultOptions();
        $this->data = array_merge([[$this->titleRow, $this->titleRowSum]], $data);
    }

    private function processDoubleAxis()
    {
        $data = [];
        $rows = [];
        $cols = [];

        // Obtenemos todos los posibles valores de los ejes
        foreach ($this->original_results['result'] as $r) {
            $x = reset($r['_id']);
            $y = next($r['_id']);

            $rows[$x] = 0;
            $cols[$y] = 0;
        }

        // Organizamos la tabla
        foreach (array_keys($rows) as $row) {
            foreach (array_keys($cols) as $col) {
                $data[$row][$col] = 0;
            }
        }

        // Populamos la tabla
        foreach ($this->original_results['result'] as $r) {
            $x = reset($r['_id']);
            $y = next($r['_id']);
            $s = $r['count'];

            $data[$x][$y] = $s;
        }

        $this->processResultOptions();

        // Limpiamos la data
        $clean_data = [];
        $title_row = [];
        foreach ($data as $key_row => $row) {
            $title_row = array_keys($row);
            $clean_data[] = array_merge([$key_row], array_values($row));
        }

        $this->titleRow = array_merge([$this->titleRow], $title_row);

        $this->data = array_merge([$this->titleRow], $clean_data);
    }



    public function processResultsData()
    {
        if (count($this->original_results['result']) == 0) {
            $this->data = [];
        } else {
            list($item) = $this->original_results['result'];

            if (count($item['_id']) > 1) {
                $this->processDoubleAxis();
            } else {
                $this->processSingleAxis();
            }
        }

        $this->processExtraOptions();

        return $this->data;
    }

    /**
     * Transpone una matriz
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     *
     * @param $arr
     *
     * @return array
     */
    private function flipDiagonally($arr) {
        $out = array();
        foreach ($arr as $key => $subarr) {
            foreach ($subarr as $subkey => $subvalue) {
                $out[$subkey][$key] = $subvalue;
            }
        }
        return $out;
    }

    public function processResultOptions()
    {
        if (count($this->options) > 0)
        {
            return $this->options;
        }

        switch ($this->query['object']['value']) {
            case 'perfil_socioeconomico':
                $title = "Asociados por %s";
                $this->titleRowSum = "Asociados";
                break;
            case 'caracterizacion_asociacion':
                $title = "Asociaciones por %s";
                $this->titleRowSum = "Asociaciones";
                break;
            case 'diagnostico_tecnico':
                $title = "Asociaciones por %s";
                $this->titleRowSum = "Asociaciones";
                break;
            default:
                throw new \Exception("Objeto {$this->query['object']} no definido");
        }

        switch (count($this->query['fields'])) {
            case 0:
                $label = "";
                break;
            case 1:
                $label = $this->query['fields'][0]['label'];
                $title = sprintf($title, $label);
                $this->titleRow = $label;
                break;
            case 2:
                $label = $this->query['fields'][0]['label'];
                $title = sprintf($title, $label);
                $this->titleRow = $label;
                $title .= sprintf(" y %s", $this->query['fields'][1]['label']);
                break;
            default:
                throw new \Exception("Bad number of fields");
        }

        $this->options = ['title' => $title];

        $chart = $this->query['chart']['value'];

        switch ($chart) {
            case 'bar_h':
                $extra = [
                    'animation' => [
                        'easing' => 'linear',
                        'duration' => '100'
                    ],
                    'hAxis' => [
                        'title' => 'N° ' . $this->titleRowSum,
                        'gridlines' => ['count' => -1]
                        //'ticks' => [0, 2, 4],
                    ],
                    'vAxis' => [
                        'title' => $label,
                        'showTextEvery' => true
                    ]
                ];
                break;
            case 'bar':
                $extra = [
                    'animation' => [
                        'easing' => 'linear',
                        'duration' => '100'
                    ],
                    'vAxis' => [
                        'title' => 'N° ' . $this->titleRowSum,
                        'gridlines' => ['count' => -1]
                        //'ticks' => [0, 2, 4],
                    ],
                    'hAxis' => [
                        'title' => $label,
                        'showTextEvery' => true
                    ]
                ];
                break;
            case 'pie':
                $extra = [
                    'is3D' => true
                ];
                break;
            default:
                throw new \Exception("Chart type {$chart} isn't supported!");
        }

        $this->options = array_merge($this->options, $extra);

        return $this->options;
    }

    public function getResult()
    {
        return [
            'data' => $this->processResultsData(),
            'options' => $this->processResultOptions()
        ];
    }

    private function processExtraOptions()
    {
        if (!isset($this->query['options'])) {
            return;
        }

        if ($this->query['options']['ranged']) {
            $this->proccesRangedData();
        }
    }

    private function proccesRangedData()
    {

        $groups = isset($this->query['options']['ranged_groups']) ?
            $this->query['options']['ranged_groups']: 5 ;
        $new_data = [];
        $numeric_data = [];
        $min = 0;
        $max = -1;

        // Obtiene los valores numericos, el maximo y minimo valor
        foreach ($this->data as $row_data) {
            if (is_numeric($row_data[0])) {
                $numeric_data[] = $row_data[0];
                $max = max($max, $row_data[0]);
            } else {
                // Los valores no numericos quedan en el mismo orden
                $new_data[] = $row_data;
            }
        }

        // Organizamos los valores en orden
        sort($numeric_data, SORT_NUMERIC);

        $step_size = ceil($max / $groups);
        $steps = [];

        // Crea los rangos de valores
        while($min < $max) {
            $step_max = min($min + $step_size + 1, $max);
            $key = "{$min} - {$step_max}";
            $steps[$key] = [];

            // Organiza los valores actuales en los grupos
            foreach($numeric_data as $row_numeric) {
                if ($row_numeric > $min && $row_numeric <= $step_max) {
                    $steps[$key][] = $row_numeric;
                }
            }

            // Totaliza los valores
            $total = 0;
            foreach ($this->data as $row_data) {
                $label = $row_data[0];
                $value = $row_data[1];
                if (in_array($label, $steps[$key])) {
                    $total += $value;
                }
            }

            $new_data[] = [$key, $total];

            $min += ($step_size + 1);
        }

        $this->data = $new_data;
    }
}
