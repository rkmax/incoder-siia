<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Lib;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of AsociacionSearchFiltersType
 *
 * @author chamanx
 */
class AsociacionSearchFilters {
    
    /**
     * 
     * @var string 
     */
    private $nombre;
    
    /**
     * 
     * @var string 
     */
    private $tipoProducto;
    /**
     *
     * @var string 
     */
    private $variedad;
    /**
     *
     * @var int 
     */
    private $catidad;
    /**
     *
     * @var int 
     */
    private $areaSembrada;
    /**
     * @Assert\Type("\Date")
     * @var date 
     */
    private $fechaProduccion;
    /**
     *
     * @var string 
     */
    private $vereda;
    /**
     *
     * @var string 
     */
    private $municipio;
    /**
     *
     * @var string 
     */
    private $departamento;
    
    public function getTipoProducto() {
        return $this->tipoProducto;
    }

    public function getVariedad() {
        return $this->variedad;
    }

    public function getCatidad() {
        return $this->catidad;
    }

    public function getAreaSembrada() {
        return $this->areaSembrada;
    }

    public function getFechaProduccion() {
        return $this->fechaProduccion;
    }

    public function getVereda() {
        return $this->vereda;
    }

    public function getMunicipio() {
        return $this->municipio;
    }

    public function getDepartamento() {
        return $this->departamento;
    }

    public function setTipoProducto($tipoProducto) {
        $this->tipoProducto = $tipoProducto;
    }

    public function setVariedad($variedad) {
        $this->variedad = $variedad;
    }

    public function setCatidad($catidad) {
        $this->catidad = $catidad;
    }

    public function setAreaSembrada($areaSembrada) {
        $this->areaSembrada = $areaSembrada;
    }

    public function setFechaProduccion(date $fechaProduccion) {
        $this->fechaProduccion = $fechaProduccion;
    }

    public function setVereda($vereda) {
        $this->vereda = $vereda;
    }

    public function setMunicipio($municipio) {
        $this->municipio = $municipio;
    }

    public function setDepartamento($departamento) {
        $this->departamento = $departamento;
    }
    
    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }




    
}
