<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\DSL\Caracterizacion;


use Elastica\Query\Bool;
use Elastica\Query\FuzzyLikeThis;
use Elastica\Query\Match;
use Elastica\Query\MultiMatch;
use Elastica\Query\Nested;

class Factory extends Nested {

    public static function operador($query)
    {
        $multi = new MultiMatch();
        $multi->setFields([
            'asesores.operador.id',
            'asesores.operador.nombre',
            'asesores.operador.documentoIdentidad',
        ]);
        $multi->setQuery($query);

        $nested = new Nested();
        $nested->setPath('asesores.operador');
        $nested->setQuery($multi);

        return $nested;
    }

    public static function asesor($asesor)
    {
        $query = new Match();
        $query->setField('asesores.id', $asesor);

        $nested = new Nested();

        $nested->setPath('asesores');
        $nested->setQuery($query);

        return $nested;
    }

    public static function asesorName($query)
    {
        $nested = new Nested();
        $nested->setPath('asesores');

        $bool = new Bool();

        $nombre = new Match();
        $nombre->setField('asesores.nombre', $query);
        $bool->addShould($nombre);

        $apellido = new Match();
        $apellido->setField('asesores.apellidos', $query);
        $bool->addShould($apellido);

        $documento = new Match();
        $documento->setField('asesores.documentoIdentidad', $query);
        $bool->addShould($documento);

        $nested->setQuery($bool);

        return $nested;
    }

    public static function localizacionAsociacion($query)
    {
        $bool = new Bool();

        $queryMunicipio = new Match();
        $queryMunicipio->setField('municipio', $query);
        //$queryMunicipio->setFieldBoost('municipio', 5);
        $bool->addShould($queryMunicipio);

        $queryDepartamento = new Match();
        $queryDepartamento->setField('departamento', $query);
        $bool->addShould($queryDepartamento);

        $queryCP = new Match();
        $queryCP->setField('centroPoblado', $query);
        //$queryCP->setFieldBoost('centroPoblado', 10);
        $bool->addShould($queryCP);

        return $bool;
    }

    /**
     * Query de elastica para encontrar asociados por registro documento, registro catastral o nombre propiedad
     * @param $documento
     * @param null $registroCatastral
     * @param null $nombrePropiedad
     * @return Bool
     * @throws \Exception
     */
    public static function asociados($documento, $registroCatastral = null, $nombrePropiedad = null){
        $propiedad = new Bool();

        if(empty($documento)){
            throw new \Exception("ERROR: documento es nulo");
        }

        if (!empty($registroCatastral)) {
            $queryRegistro = new Match();
            $queryRegistro->setField('registroCatastral', $registroCatastral);
            $propiedad->addShould($queryRegistro);
        }

        if (!empty($nombrePropiedad)) {
            $queryNombreProp = new Match();
            $queryNombreProp->setField('nombrePredio', $nombrePropiedad);
            $propiedad->addShould($queryNombreProp);
        }

        $queryDocumento = new Match();
        $queryDocumento->setField('documento', $documento);

        $bool = new Bool();
        if(!empty($registroCatastral) && !empty($nombrePropiedad)){
            $bool->addMust($propiedad);
        }
        $bool->addMust($queryDocumento);

        return $bool;
    }


    public static function localizacion($departamento, $municipio, $centroPoblado = null)
    {

        $departamento = trim($departamento);
        $municipio = trim($municipio);
        $centroPoblado = trim($centroPoblado);

        $bool = new Bool();

        $queryMunicipio = new FuzzyLikeThis();
        $queryMunicipio->addFields(['nombreMunicipio']);
        $queryMunicipio->setLikeText($municipio);
        $bool->addMust($queryMunicipio);


        $queryDepartamento = new FuzzyLikeThis();
        $queryDepartamento->addFields(['nombreDepartamento']);
        $queryDepartamento->setLikeText($departamento);
        $bool->addMust($queryDepartamento);

        if ($centroPoblado && !empty($centroPoblado)) {
            $queryCP = new FuzzyLikeThis();
            $queryCP->addFields(['nombreCentroPoblado']);
            $queryCP->setLikeText($centroPoblado);
            $bool->addMust($queryCP);
        }

        return $bool;
    }

    public static function nombreAsocacion($nombre)
    {

        $query = new FuzzyLikeThis();
        $query->addFields(['nombre', 'sigla']);
        $query->setLikeText($nombre);

        return $query;
    }
}