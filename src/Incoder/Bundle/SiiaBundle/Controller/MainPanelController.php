<?php

namespace Incoder\Bundle\SiiaBundle\Controller;

use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use JulianReyes\Controller\ControllerNavegationInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/admin")
 */
class MainPanelController extends Controller implements ControllerNavegationInterface
{

    /**
     * @Route("", name="siia_panel")
     * @Method({"GET"})
     * @Template()
     */
    public function panelAction()
    {
        if ($this->getUser()->hasRole(BaseProfile::ASOCIACION)) {
            $asociacion = $this->getUser()->getAsociacion()->getAsociacion()->getId();
            return $this->redirect($this->generateUrl('asociacion_caracterizacion_show', [
                'id' => $asociacion
            ]));
        }

        return [
            'leftnav' => $this->getLeftNav(),
        ];
    }

    public function getLeftFormNav()
    {
        // TODO: Implement getLeftFormNav() method.
    }

    public function getLeftNav()
    {
        $security = $this->get('security.context');

        // Si el usuario no esta logueado de ninguna manera
        if (!$security->isGranted('IS_AUTHENTICATED_REMEMBERED') ){
            return [];
        }

        $options = [
            [
                'icon' => 'fa-key',
                'title' => 'Cambiar contraseña',
                'route' => 'fos_user_change_password'
            ],
            [
                'icon' => 'fa-sign-out',
                'title' => 'Cerrar sesion',
                'route' => 'fos_user_security_logout'
            ]
        ];

        if ($security->isGranted(BaseProfile::IMPERSONATING)) {
            $options[] = [
                'icon' => 'fa-eye-slash',
                'title' => 'Salir personificacion',
                'route' => 'siia_panel',
                'route_args' => [
                    '_switch_user' => '_exit'
                ]
            ];
        }

        return $options;
    }

    public function getLeftShowNav()
    {
        // TODO: Implement getLeftShowNav() method.
    }
}
