<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Productivo;

use JulianReyes\Controller\AbstractEntityController;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;

/**
 * Class UnidadVentaController
 * @package Incoder\Bundle\SiiaBundle\Controller\Productivo
 * @EController("productivo.unidad_venta")
 * @Route("/productivo/unidad/venta")
 */
class UnidadVentaController extends AbstractEntityController {

    /**
     * Lists all Productivo\UnidadVenta entities.
     *
     * @Route("/", name="p_unidad_venta")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        return parent::indexAction($request, $params, 1, 10);
    }

    /**
     * Creates a new Productivo\UnidadVenta entity.
     *
     * @Route("/", name="p_unidad_venta_create")
     * @Method("POST")
     * @Template("SiiaBundle:Productivo/UnidadVenta:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Displays a form to create a new Productivo\UnidadVenta entity.
     *
     * @Route("/new", name="p_unidad_venta_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing Productivo\UnidadVenta entity.
     *
     * @Route("/{id}/edit", name="p_unidad_venta_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Edits an existing Productivo\UnidadVenta entity.
     *
     * @Route("/{id}", name="p_unidad_venta_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Productivo/UnidadVenta:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute()
            ]
        ];
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'productivo.unidad_venta.new',
                'route' => $this->getBaseRoute() . '_new',
                'icon' => 'fa-plus',
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'productivo_main'
            ]
        ];
    }
} 
