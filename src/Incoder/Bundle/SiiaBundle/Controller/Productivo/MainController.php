<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Productivo;

use JulianReyes\Controller\ControllerNavegationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class MainController
 * @package Incoder\Bundle\SiiaBundle\Controller\Productivo
 * @Route("/productivo")
 */
class MainController extends Controller implements ControllerNavegationInterface
{
    /**
     * @Route("/", name="productivo_main")
     * @Method("GET")s
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        return [
            'leftnav' => $this->getLeftNav()
        ];
    }

    public function getLeftFormNav()
    {
        // TODO: Implement getLeftFormNav() method.
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'productivo.menu.unidad_venta',
                'icon' => 'fa-chevron-right',
                'route' => 'p_unidad_venta'
            ],
            [
                'title' => 'productivo.menu.unidad_produccion',
                'icon' => 'fa-chevron-right',
                'route' => 'p_unidad_produccion'
            ],
            [
                'title' => 'productivo.menu.producto',
                'icon' => 'fa-chevron-right',
                'route' => 'p_producto'
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'siia_panel'
            ]
        ];
    }

    public function getLeftShowNav()
    {
        // TODO: Implement getLeftShowNav() method.
    }
}
