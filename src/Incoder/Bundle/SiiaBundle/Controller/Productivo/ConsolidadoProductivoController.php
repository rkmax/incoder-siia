<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Productivo;


use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\Asociado;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Entity\CuadroAgricola;
use Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario;
use Incoder\Bundle\SiiaBundle\Repository\ProductoRepository;
use Incoder\Bundle\SiiaBundle\Service\ConsolidadoProductivoService;
use JulianReyes\Controller\ControllerPropertyHolder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ConsolidadoProductivoController
 * @package Incoder\Bundle\SiiaBundle\Controller\Productivo
 * @Route("/admin/consolidados")
 */
class ConsolidadoProductivoController extends Controller
{
    /**
     * @param Asociacion $asociacion
     *
     * @return array
     *
     * @Route("/asociacion/{id}", name="consolidado_asociacion")
     * @Method("GET")
     * @Template()
     */
    public function asociacionAction(Asociacion $asociacion)
    {
        $total = [];

        // Agricola
        /** @var Asociado $asociado */
        foreach ($asociacion->getAsociado() as $asociado)
        {
            $total = $this->consolidate($asociado, $total);
        }

        return ['items' => $total];
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociado $asociado
     *
     * @return array
     *
     * @Route("/asociado/{id}", name="consolidado_asociado")
     * @Method("GET")
     * @Template("SiiaBundle:Productivo/ConsolidadoProductivo:asociacion.html.twig")
     */
    public function asociadoAction(Asociado $asociado)
    {
        $total = [];
        $total = $this->consolidate($asociado, $total);

        return ['items' => $total];
    }

    private function consolidate(Asociado $asociado, array $total)
    {
        /** @var CuadroAgricola $cp */
        foreach ($asociado->getCuadroAgricola() as $cp)
        {
            $total = $this->consolidateProducto($cp, $total);
        }

        /** @var CuadroPecuario $cp */
        foreach ($asociado->getCuadroPecuario() as $cp)
        {
            $total = $this->consolidateProducto($cp, $total);
        }

        return $total;
    }

    private function consolidateProducto($cp, $total)
    {
        if (!$cp->getProducto()) {
            return $total;
        }

        if (!array_key_exists($cp->getProducto()->getId(), $total))
        {
            $total[$cp->getProducto()->getId()] = [
                'producto' => "{$cp->getProducto()}",
                'unidad_produccion' => "{$cp->getProducto()->getUnidadProduccion()}",
                'volumen_produccion' => 0,
                'unidad_venta' => "{$cp->getProducto()->getUnidadVenta()}",
                'volumen_venta' => 0,
            ];
        }

        $total[$cp->getProducto()->getId()]['volumen_produccion'] += $cp->getVolumenProduccion();
        $total[$cp->getProducto()->getId()]['volumen_venta'] += $cp->getVolumenVendido();

        return $total;
    }

    /**
     * Obtener los datos para el consolidado productivo general
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @internal \DateTime $fechaini
     * @internal \DateTime $fechafin
     * @internal param string $departamento
     * @internal param string $asociacion
     * @internal param string $municipio
     * @ParamConverter("fechaini", options={"format": "Y-m-d"})
     * @ParamConverter("fechafin", options={"format": "Y-m-d"})
     * @return array
     *
     * @Route("/", name="consolidado_general")
     * @Method("GET")
     * @Template()
     */
    public function reporteGeneralAction(Request $request) {

        $asociacion = $request->query->get('asociacion');
        $producto = $request->query->get('producto');
        $fechaini = $request->query->get('fechaini');
        $fechafin = $request->query->get('fechafin');
        $tipoProducto = $request->query->get('tipop');
        $usuarioId = null;
        $rol= null;

        $localizacion = [
            'departamento' => $request->query->get('departamento'),
            'municipio' => $request->query->get('municipio'),
            'vereda' => $request->query->get('vereda'),
        ];

        if (!$this->getUser()->hasRole(BaseProfile::ADMIN) && !$this->getUser()->hasRole(BaseProfile::SUPERVISOR)) {
            if($this->getUser()->hasRole(BaseProfile::ASESOR)){
                $usuarioId = $this->getUser()->getAsesor()->getId();
                $rol = BaseProfile::ASESOR;
            }
            if($this->getUser()->hasRole(BaseProfile::OPERADOR)){
                $usuarioId = $this->getUser()->getOperador()->getId();
                $rol = BaseProfile::OPERADOR;
            }
            if($this->getUser()->hasRole(BaseProfile::ASOCIACION)){
                $rol = BaseProfile::ASOCIACION;
            }
        }
        $reporteViews = $this->getCaracterizacionRepo()->getReporteGeneral(
            $asociacion,
            $localizacion,
            $producto,
            $fechaini,
            $fechafin,
            $usuarioId,
            $rol,
            $tipoProducto);

        $asociaciones = $this->getAsociaciones();
        $productos = $this->getProductoRepo()->findAll();
        $departamentos = $this->getCaracterizacionRepo()->findAllDepartamentosDeAsociaciones();
        $municipios = $this->getCaracterizacionRepo()->findAllMunicipiosDeAsociaciones($localizacion['departamento']);
        $veredas = $this->getCaracterizacionRepo()->findAllVeredasDeAsociaciones(
            $localizacion['departamento'],
            $localizacion['municipio']);

        return array(
            'leftnav' => $this->getLeftNav(),
            'views' => $reporteViews,
            'asociaciones' => $asociaciones,
            'selAsociacion' => $asociacion,
            'selLocalizacion' => $localizacion,
            'selProducto' => $producto,
            'productos' => $productos,
            'selTipoProducto' => $tipoProducto,
            'selfechaini' => $fechaini,
            'selfechafin' => $fechafin,
            'veredas' => $veredas,
            'municipios' => $municipios,
            'departamentos' => $departamentos,
            'selDepartamento' => $localizacion['departamento'],
            'selMunicipio' => $localizacion['municipio'],
            'selVereda' => $localizacion['vereda']
        );
    }

    /**
     * @return ConsolidadoProductivoService
     */
    public function getService(){
        return $this->get('siia.consolidado.productivo');
    }

    public function getLeftNav()
    {
        $menu = [];
        if (!$this->getUser()->hasRole(BaseProfile::ASOCIACION)) {
            $menu = [
                [
                    'title' => 'general.menu.back',
                    'icon' => 'fa-chevron-left',
                    'route' => 'siia_panel'
                ],
            ];
        }else{
            $menu = [
                [
                    'title' => 'general.menu.back',
                    'icon' => 'fa-chevron-left',
                    'route' => 'asociacion_caracterizacion_show',
                    'route_args' => [
                        'id' => $this->getUser()->getAsociacion()->getAsociacion()->getId()
                    ]
                ],
            ];
        }
        return $menu;
    }

    /**
     * @return array|\Incoder\Bundle\SiiaBundle\Entity\Asociacion[]
     */
    private function getAsociaciones()
    {
        $caracterizacionService = $this->get('siia.caracterizacion');
        $configuracion = new ControllerPropertyHolder(
            null,
            null,
            'Incoder\Bundle\SiiaBundle\Form\AsociacionType',
            'Incoder\Bundle\SiiaBundle\Entity\Asociacion',
            'siia.caracterizacion',
            null,
            null);
        $caracterizacionService->setConfiguracion($configuracion);
        if($this->getUser()->hasRole(BaseProfile::ADMIN) || $this->getUser()->hasRole(BaseProfile::SUPERVISOR)){
            return $caracterizacionService->findAll();
        }
        if($this->getUser()->hasRole(BaseProfile::OPERADOR)){
            return $this->getCaracterizacionRepo()->getAsociacionesDeOperador($this->getUser()->getOperador()->getId());
        }
        if($this->getUser()->hasRole(BaseProfile::ASESOR)){
            return $this->getCaracterizacionRepo()->getAsociacionesDeAsesor($this->getUser()->getAsesor()->getId());
        }
        if($this->getUser()->hasRole(BaseProfile::ASOCIACION)){
            return [
                $this->getCaracterizacionRepo()->find($this->getUser()->getAsociacion()->getAsociacion()->getId())
            ];

        }
        return [];
    }

    /**
     *
     * @return \Incoder\Bundle\SiiaBundle\Repository\AsociacionRepository
     */
    public function getCaracterizacionRepo() {
        return $this->getDoctrine()->getManager()
            ->getRepository('SiiaBundle:Asociacion');
    }

    /**
     *
     * @return ProductoRepository
     */
    public function getProductoRepo() {
        return $this->getDoctrine()->getManager()
            ->getRepository('SiiaBundle:Productivo\AbstractProducto');
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getAsociacionProfileRepo() {
        return $this->getDoctrine()->getManager()
            ->getRepository('SiiaBundle:AsociacionProfile');
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\SecurityUser
     */
    public function getUser() {
        return parent::getUser();
    }

} 
