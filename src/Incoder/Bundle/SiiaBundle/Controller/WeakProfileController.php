<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Controller;

use JulianReyes\Controller\AbstractWeakEntityController;

/**
 * Description of WeakProfileController
 *
 * @author chamanx
 */
abstract class WeakProfileController extends AbstractWeakEntityController implements IProfileController {
    
     /**
     * 
     * @return \Incoder\Bundle\SiiaBundle\Service\ProfileService
     */
    public function getProfileService() {
        return $this->get('siia.profile');
    }

    /**
     * Override
     */
    protected function postValidationCreate() {
        $this->getProfileService()->
                createUser($this->entity, $this->getRoleName());
        $this->getService()->persist($this->entity);
        return parent::postValidationCreate();
    }

    /**
     * Override
     */
    protected function postValidationUpdate() {
        //TODO: como hacer todo esto de manera transaccional
        $this->getProfileService()->updateUser($this->entity);
        $this->getService()->persist($this->entity, true);
        return parent::postValidationUpdate();
    }

    /**
     * Devuelve el rol para este perfil
     *
     * @return string
     */
    public function getRoleName() {
        return $this->getConfig()->rol_name;
    }
}
