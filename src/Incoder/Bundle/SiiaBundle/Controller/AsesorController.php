<?php

namespace Incoder\Bundle\SiiaBundle\Controller;

use JulianReyes\Controller\ArrayAdapter;
use JulianReyes\Controller\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Form\AsesorType;
use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Asesor controller.
 * se considera asesor como entidad debil de asociacion
 * ademas se reciben filtros por operador desde la sesion
 * @Route("/admin/asesor")
 * @package Incoder\Bundle\SiiaBundle\Controller
 * @EController("asesor")
 */
class AsesorController extends WeakProfileController {

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\SecurityUser 
     */
    public function getUser() {
        return parent::getUser();
    }

    /**
     * obtener el operador de alguna manera de la sesion
     *  
     * @return \Incoder\Bundle\SiiaBundle\Entity\Operador
     */
    protected function getCurrentOperador() {
        $user = $this->getUser();
        if ($user) {
            $operador = $this->getService()->getOneOperadorByUser($user->getId());
            return $operador;
        }
        return null;
    }

    /**
     * obtiene el id de operador en sesion si existe, sino retorna nulo
     * @return int
     */
    protected function getCurrentOperadorId() {
        $operador = $this->getCurrentOperador();
        $operadorId = null;
        if ($operador) {
            $operadorId = $operador->getId();
        }
        return $operadorId;
    }

    /**
     *
     * @Route("/", name="asesor")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        $currentOperadorId = $this->getCurrentOperadorId();
        if(!$currentOperadorId){
            throw new AccessDeniedHttpException(
                "Su usuario no es operador"
            );
        }
        return parent::indexAction($request, $params, 1, 10, $currentOperadorId);
    }


    /**
     * Persiste en la base de datos, los datos del formulario
     *
     * @param Request $request
     *
     * @param null $asociado
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/", name="asesor_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asesor:new.html.twig")
     */
    public function createAction(Request $request, $asociado = null) {
        return parent::createAction($request, $this->getCurrentOperadorId());
    }

    /**
     * Muestra el formularios vacio
     *
     *
     * @return array
     *
     * @Route("/new", name="asesor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociado = null) {
        return parent::newAction($this->getCurrentOperadorId());
    }

    /**
     * Muestra el formulario con los datos obtenidos de la base de datos
     *
     * @param int  $id
     *
     * @return array
     *
     * @Route("/{id}/edit", name="asesor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociado = null) {
        return parent::editAction($id, $this->getCurrentOperadorId());
    }

    /**
     * Persiste los cambios en la base de datos con los datos obtenidos
     * del formulario
     *
     * @param Request $request
     * @param int     $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}", name="asesor_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asesor:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociado = null) {
        return parent::updateAction($request, $id, $this->getCurrentOperadorId());
    }

    public function getLeftFormNav() {
        $menu[] = [
            'title' => 'general.menu.back',
            'icon' => 'fa-chevron-left',
            'route' => 'asesor'
        ];
        return $menu;
    }

    public function getLeftNav() {
        return [
            [
                'title' => 'asesor.new',
                'icon' => 'fa-plus',
                'route' => $this->getBaseRoute() . '_new'
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'siia_panel'
            ]
        ];
    }

}
