<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Profile;


use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\AsociacionProfile;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Form\AsociacionProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AsociacionProfileController
 * @package Incoder\Bundle\SiiaBundle\Controller\Profile
 * @Route("/profiles/asociacion")
 */
class AsociacionProfileController extends Controller
{

    /**
     * @param Asociacion $asociacion
     *
     * @return array
     *
     * @Route("/{id}/new", name="profile_asociacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Asociacion $asociacion)
    {
        if ($asociacion->getPerfil()) {
            $this->get('session')->set('notice', "La asociacion ya tiene creado ");
            return $this->redirect($this->generateUrl(
                'asociacion_caracterizacion_show',
                [
                    'id' => $asociacion->getId()
                ]
            ));
        }

        $entity = new AsociacionProfile();
        $entity->setAsociacion($asociacion);

        $default_options = array(
            'action' => $this->generateUrl($this->getBaseRoute() . '_create'),
            'method' => 'POST'
        );

        $form = $this->createForm(new AsociacionProfileType(), $entity, $default_options);
        $this->addButton($form, 'Guardar');

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/", name="profile_asociacion_create")
     * @Method("POST")
     * @Template("@Siia/Profile/AsociacionProfile/new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AsociacionProfile();
        $form = $this->createForm(new AsociacionProfileType(), $entity);
        $this->addButton($form, 'Guardar');

        $form->submit($request);

        if ($form->isValid()) {
            $this->get('siia.profile')->createUser($entity, BaseProfile::ASOCIACION);

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl(
                'asociacion_caracterizacion_show',
                [ 'id' => $entity->getAsociacion()->getId() ]
            ));
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Asociacion $asociacion
     *
     * @return array
     *
     * @Route("/{id}/edit", name="profile_asociacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Asociacion $asociacion)
    {
        $entity = $asociacion->getPerfil();

        $entity->setEmail($entity->getSecurityUser()->getEmail());

        $default_options = array(
            'action' => $this->generateUrl($this->getBaseRoute() . '_update', ['id' => $asociacion->getId()]),
            'method' => 'PUT',
            'password_required' => false
        );

        $form = $this->createForm(new AsociacionProfileType(), $entity, $default_options);
        $this->addButton($form, 'Actualizar');

        return [
            'edit_form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociacion
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @Route("/{id}", name="profile_asociacion_update")
     * @Method("PUT")
     * @Template("@Siia/Profile/AsociacionProfile/edit.html.twig")
     */
    public function updateAction(Request $request, Asociacion $asociacion)
    {
        $entity = $asociacion->getPerfil();

        if (!$entity) {
            throw $this->createNotFoundException("El usuario SIIAT no existe para '{$asociacion}'");
        }

        $form = $this->createForm(new AsociacionProfileType(), $entity);
        $this->addButton($form, 'Actualizar');
        $form->submit($request);

        if ($form->isValid()) {
            $this->get('siia.profile')->updateUser($entity);
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl(
                'asociacion_caracterizacion_show',
                [ 'id' => $entity->getAsociacion()->getId() ]
            ));
        }

        return [
            'edit_form' => $form->createView()
        ];
    }

    private function getBaseRoute()
    {
        return 'profile_asociacion';
    }

    protected function addButton(Form $form, $label) {
        $form->add('submit', 'submit', array('label' => $label, 'attr' => [
            'class' => 'btn btn-primary'
        ]));
    }

}