<?php

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;

use JulianReyes\Controller\AbstractEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * PlanAccion\Afirmacion controller.
 *
 * @Route("/afirmacion")
 * @EController("plan.afirmacion")
 */
class AfirmacionController extends AbstractEntityController
{
    /**
     *
     * @Route("/", name="plan-accion_afirmacion")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        return parent::indexAction($request, $params, 1, 10);
    }

    /**
     * Creates a new PlanAccion\Afirmacion entity.
     *
     * @Route("/", name="plan-accion_afirmacion_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion\Afirmacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Displays a form to create a new PlanAccion\Afirmacion entity.
     *
     * @Route("/new", name="plan-accion_afirmacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing PlanAccion\Afirmacion entity.
     *
     * @Route("/{id}/edit", name="plan-accion_afirmacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Edits an existing PlanAccion\Afirmacion entity.
     *
     * @Route("/{id}", name="plan-accion_afirmacion_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion\Afirmacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'plan.afirmacion.new',
                'icon' => 'fa-plus',
                'route' => 'plan-accion_afirmacion_new'
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion'
            ]
        ];
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion_afirmacion'
            ]
        ];
    }
}
