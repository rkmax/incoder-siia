<?php

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;

use JulianReyes\Controller\AbstractEntityController;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;

/**
 * PlanAccion\Accion controller.
 *
 * @Route("/accion")
 * @EController("plan.accion")
 */
class AccionController extends AbstractEntityController
{
    /**
     * @Route("/", name="plan-accion_accion")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        return parent::indexAction($request, $params, 1, 10);
    }

    /**
     * Creates a new PlanAccion\Accion entity.
     *
     * @Route("/", name="plan-accion_accion_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion\Accion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Displays a form to create a new PlanAccion\Accion entity.
     *
     * @Route("/new", name="plan-accion_accion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing PlanAccion\Accion entity.
     *
     * @Route("/{id}/edit", name="plan-accion_accion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Edits an existing PlanAccion\Accion entity.
     *
     * @Route("/{id}", name="plan-accion_accion_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion\Accion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }
    /**
     * Deletes a PlanAccion\Accion entity.
     *
     * @Route("/{id}", name="plan-accion_accion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        return parent::deleteAction($request, $id);
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute()
            ]
        ];
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'plan.accion.new',
                'route' => $this->getBaseRoute() . '_new',
                'icon' => 'fa-plus',
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion'
            ]
        ];
    }
}
