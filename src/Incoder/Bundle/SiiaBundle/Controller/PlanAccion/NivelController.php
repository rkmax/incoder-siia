<?php

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use JulianReyes\Controller\AbstractEntityController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * PlanAccion\Nivel controller.
 *
 * @Route("/nivel")
 * @EController("plan.nivel")
 */
class NivelController extends AbstractEntityController
{
    /**
     * Lists all PlanAccion\Nivel entities.
     *
     * @Route("/", name="plan-accion_nivel")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        return parent::indexAction($request, $params, 1, 10);
    }

    /**
     * Creates a new PlanAccion\Nivel entity.
     *
     * @Route("/", name="plan-accion_nivel_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion\Nivel:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Displays a form to create a new PlanAccion\Nivel entity.
     *
     * @Route("/new", name="plan-accion_nivel_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing PlanAccion\Nivel entity.
     *
     * @Route("/{id}/edit", name="plan-accion_nivel_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Edits an existing PlanAccion\Nivel entity.
     *
     * @Route("/{id}", name="plan-accion_nivel_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion\Nivel:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }
    /**
     * Deletes a PlanAccion\Nivel entity.
     *
     * @Route("/{id}", name="plan-accion_nivel_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        return parent::deleteAction($request, $id);
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion_nivel'
            ]
        ];
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'plan.nivel.new_level',
                'route' => 'plan-accion_nivel_new',
                'icon' => 'fa-plus',
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion'
            ]
        ];
    }
}
