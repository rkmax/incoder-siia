<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;

use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFA;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad;
use JMS\Serializer\SerializationContext;
use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Controller\ArrayAdapter;
use JulianReyes\Controller\Pagerfanta;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class DOFAController
 * @package Incoder\Bundle\SiiaBundle\Controller\PlanAccion
 * @Route("/dofa")
 * @EController("plan.dofa.main")
 */
class DOFAController extends AbstractWeakEntityController
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @Route("/debilidades", name="dofa_debilidades_asociacion", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function obtainDebilidadesAction(Request $request)
    {
        $asociacion = $request->get('asociacion', '');

        if (!$asociacion) {
            throw new BadRequestHttpException("El parametro 'asociacion' es necesario");
        }

        $debilidades = $this
            ->getDoctrine()
            ->getRepository('SiiaBundle:PlanAccion\Debilidad')
            ->findByAsociacion($asociacion);

        $content = $this->get('serializer')->serialize($debilidades, 'json');

        return new Response($content);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     * @return mixed
     *
     * @Route("/actividades", name="dofa_optain_actividades", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function obtainActividadesAction(Request $request)
    {
        $debilidad = $request->get('debilidad', '');
        $asociacion = $request->get('asociacion', '');

        if (!$debilidad) {
            throw new BadRequestHttpException("El parametro 'debilidad' es necesario");
        }

        if (!$asociacion) {
            throw new BadRequestHttpException("El parametro 'asociacion' es necesario");
        }

        $debilidad = $this
            ->get('doctrine.orm.entity_manager')
            ->getRepository('SiiaBundle:PlanAccion\Debilidad')
            ->find($debilidad);

        $dofas = $this
            ->get('doctrine.orm.entity_manager')
            ->getRepository('SiiaBundle:PlanAccion\DOFA')
            ->createQueryBuilder('d')
            ->where('d.asociacion = :asociacion_id')
            ->setParameter('asociacion_id', $asociacion)
            ->getQuery()->getResult();

        // Devuelve las actividades relacionadas a la debilidad
        // que no hayan sido agregadas antes
        $actividades = $debilidad->getActividades()->filter(
            function(DOFAActividad $actividad) use ($dofas) {
                /** @var DOFA $dofa */
                foreach ($dofas as $dofa) {
                    if ($dofa->getActividad() == $actividad) {
                        return false;
                    }
                }

                if (!$actividad->getAccion()) {
                    return false;
                }

                return true;
            }
        );

        $context = SerializationContext::create()->enableMaxDepthChecks();

        $actividades = array_values($actividades->toArray());

        return new Response($this->get('serializer')->serialize($actividades, 'json', $context));
    }

    /**
     *
     * @Route("/{asociacion}/", name="dofa_principal")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $asociacion = null)
    {
        return parent::indexAction($request, [], 1, 10, $asociacion);
    }

    /**
     * @param Request $request
     * @param null    $asociacion
     *
     * @return array|mixed
     *
     * @Route("/{asociacion}/", name="dofa_principal_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion/DOFA:new.html.twig")
     */
    public function createAction(Request $request, $asociacion = null)
    {
        return parent::createAction($request, $asociacion);
    }


    /**
     * @param null $asociacion
     *
     * @return array|mixed
     *
     * @Route("/{asociacion}/new", name="dofa_principal_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociacion = null)
    {
        return parent::newAction($asociacion);
    }

    /**
     * @param int  $id
     * @param null $asociacion
     *
     * @return array
     * @Route("/{id}/edit", name="dofa_principal_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociacion = null)
    {
        return parent::editAction($id, $asociacion);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @param null    $asociado
     *
     * @return mixed
     * @Route("/{id}", name="dofa_principal_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion/DOFA:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociado = null)
    {
        return parent::updateAction($request, $id, $asociado);
    }


    /**
     * @param $id
     *
     * @return array
     *
     * @Route("/{id}/show", name="dofa_principal_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        return parent::showAction($id);
    }


    protected function returnAfterUpdate($entity = null, $params = [])
    {
        $params['asociacion'] = $this->getRelatedEntity()->getId();
        return parent::returnAfterUpdate($entity, $params);
    }

    protected function returnAfterCreate($entity = null, $params = [])
    {
        $params['asociacion'] = $this->getRelatedEntity()->getId();

        return parent::returnAfterCreate($entity, $params);
    }


    /**
     * @return array
     */
    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute(),
                'route_args' => [
                    'asociacion' => $this->getRelatedEntity()->getId()
                ]
            ]
        ];
    }

    public function getLeftShowNav()
    {
        if (count($this->getEntity()->getPerfil()) > 0) {
            $suffix = 'edit';
            $route_args = [
                'id' => $this->getEntity()->getPerfil()->last()->getId()
            ];
            $icon = 'fa-pencil';
        } else {
            $suffix = 'new';
            $route_args = [
                'asociado' => $this->getEntity()->getId()
            ];
            $icon = 'fa-plus';
        }
        return array_merge(
            [
                [
                    'title' => "asociado.resumen.${suffix}_perfil",
                    'icon' => $icon,
                    'route' => "perfil_socioe_${suffix}",
                    'route_args' => $route_args
                ],
                [
                    'title' => 'asociado.cuadro.agricola.list',
                    'icon' => 'fa-plus',
                    'route' => 'cuadro_agricola_list',
                    'route_args' => ['asociado' => $this->getEntity()->getId()]
                ],
                [
                    'title' => 'asociado.cuadro.pecuario.list',
                    'icon' => 'fa-plus',
                    'route' => 'cuadro_pecuario_list',
                    'route_args' => ['asociado' => $this->getEntity()->getId()]
                ]
            ],parent::getLeftShowNav());
    }
    /**
     * @return array
     */
    public function getLeftNav()
    {
        $menu = [];
        if(!$this->getUser()->hasRole(BaseProfile::SUPERVISOR)){
            $menu[] = [
                'title' => 'plan.dofa.new',
                'icon' => 'fa-plus',
                'route' => $this->getBaseRoute() . '_new',
                'route_args' => [
                    'asociacion' => $this->getRelatedEntity()->getId()
                ]
            ];
        }
        $menu[] = [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan_accion_ver'
        ];
        return $menu;
    }
}
