<?php

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;


use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\OpcionPorAfirmacion;
use JulianReyes\Controller\AbstractEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MainController
 * @EController("plan.main")
 */
class MainController extends AbstractEntityController
{
    /**
     * @Route("/", name="plan-accion")
     * @Method({"GET"})
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        $results = parent::indexAction($request, $params, 1, 20);
        $results['opciones'] = $this->getDoctrine()->getManager()
            ->getRepository('SiiaBundle:PlanAccion\Opcion')->findAll();

        return $results;
    }

    /**
     * @Route("/", name="plan-accion_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion\Main:new.html.twig")
     *
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * @Route("/new", name="plan-accion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing PlanAccion\Nivel entity.
     *
     * @Route("/{id}/edit", name="plan-accion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Edits an existing PlanAccion\Nivel entity.
     *
     * @Route("/{id}", name="plan-accion_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion\Main:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'plan.menu.admin_aspects',
                'route' => $this->getBaseRoute() . '_aspecto'
            ],
            [
                'title' => 'plan.menu.admin_level',
                'route' => $this->getBaseRoute() . '_nivel'
            ],
            [
                'title' => 'plan.menu.admin_options',
                'route' => $this->getBaseRoute() . '_opcion'
            ],
            [
                'title' => 'plan.menu.a_afirmacion',
                'route' => $this->getBaseRoute() . '_afirmacion'
            ],
            [
                'title' => 'plan.menu.a_componente',
                'route' => $this->getBaseRoute() . '_componente'
            ],
            [
                'title' => 'plan.menu.a_accion',
                'route' => $this->getBaseRoute() . '_accion'
            ],
            [
                'title' => 'plan.menu.a_diagnostico',
                'route' => $this->getBaseRoute() . '_diagnostico'
            ],
            [
                'title' => 'plan.dofa.actividad.gestion',
                'route' => 'plan-accion_dofa_actividad'
            ],
            [
                'title' => 'plan.plan.new',
                'route' => $this->getBaseRoute() . '_new',
                'icon' => 'fa-plus'
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'siia_panel'
            ]
        ];
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute()
            ]
        ];
    }
}
