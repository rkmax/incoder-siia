<?php

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JulianReyes\Controller\AbstractEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;

/**
 * PlanAccion\Aspecto controller.
 *
 * @Route("/aspectos")
 * @EController("plan.aspecto")
 */
class AspectoController extends AbstractEntityController
{

    /**
     * Lists all PlanAccion\Aspecto entities.
     *
     * @Route("/", name="plan-accion_aspecto")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        return parent::indexAction($request, $params, 1, 10);
    }

    /**
     * Creates a new PlanAccion\Aspecto entity.
     *
     * @Route("/", name="plan-accion_aspecto_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion\Aspecto:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Displays a form to create a new PlanAccion\Aspecto entity.
     *
     * @Route("/new", name="plan-accion_aspecto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing PlanAccion\Aspecto entity.
     *
     * @Route("/{id}/edit", name="plan-accion_aspecto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Edits an existing PlanAccion\Aspecto entity.
     *
     * @Route("/{id}", name="plan-accion_aspecto_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion\Aspecto:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'plan.aspect.new_aspect',
                'route' => 'plan-accion_aspecto_new',
                'icon' => 'fa-plus',
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion'
            ]
        ];
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion_aspecto'
            ]
        ];
    }
}
