<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/21/14
 * Time: 4:33 PM
 */

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;


use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionElemento;
use JulianReyes\Controller\ControllerNavegationInterface;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PlanAccionController
 * @Route("/ver")
 */
class PlanAccionController extends Controller implements ControllerNavegationInterface {

    /**
     * @Route("/", name="plan_accion_ver")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $page = 1;
        $limit = 20;
        $entities = $this->getList($page, $limit);
        $adapter = new ArrayAdapter($entities);
        $pagerfanta = new Pagerfanta($adapter);
        //Obtiene el parametro page del request y si no viene toma el por defecto
        $page = ($request->query->get('page'))? $request->query->get('page') : $page;
        $pagerfanta->setCurrentPage($page);
        $pagerfanta->setMaxPerPage($limit);

        return [
            'entities' => $pagerfanta,
            'leftnav' => $this->getLeftNav()
        ];
    }

    private function genPaAccion($id)
    {
        $force = (boolean) $this->getRequest()->get('force', false);
        $pas = $this->get('siia.plan_accion');

        $plan = $pas->generateAndSave($id, $force);

        $niveles = [];
        foreach ($pas->getNiveles() as $n) {
            $niveles[$n->getName()] = $n->getTo();
        }

        /** @var PlanAccionElemento $elemento */
        $data = [];
        foreach ($plan->getElementos() as $elemento) {

            $item = [
                "column" => "data",
                "porcentaje" => round($elemento->getCalificacion(), 0),
                "aspecto" => "{$elemento->getAspecto()}"
            ];

            foreach ($niveles as $nivel => $nivel_value) {
                $item[$nivel] = $nivel_value;
            }

            $data[] = $item;
        }

        $options = [
            "type" => "radar",
            "radius" => "35%",
            "dataProvider" => $data,
            "valueAxes" => [
                [
                    "axisTitleOffset" => 20,
                    "minimum" => 0,
                    "maximum" => 100,
                    "axisAlpha" => 0.15
                ]
            ],
            "startDuration" => 0,
            "graphs" => [
                [
                    "balloonText" => "[[value]]% [[category]]",
                    "bullet" => "round",
                    "valueField" => "porcentaje",
                    "fillAlphas" => 0.15,
                    "title" => "Evaluacion"
                ]
            ],
            "legend" => [
                "labelText" => "[[title]]",
                "maxColumns" => 1,
            ],
            "categoryField" => "aspecto",
            "creditsPosition" => "bottom-right"
        ];

        foreach (array_keys($niveles) as $nivel) {
            $options["graphs"][] = ["valueField" => $nivel, "title" => $nivel];
        }

        return [
            'plan' => $plan,
            'options' => $options,
            'entity' => $plan->getAsociacion()
        ];
    }

    /**
     * @Route("/{id}", name="plan_accion_generate")
     * @Method("GET")
     * @Template()
     */
    public function generateAction($id)
    {
        $data = $this->genPaAccion($id);

        return array_merge($data, [
            'leftnav' => $this->getLeftFormNav(),
        ]);
    }

    /**
     * @Route("/{id}/min", name="plan_accion_generate_min")
     * @Method("GET")
     * @Template("SiiaBundle:PlanAccion/PlanAccion:result.html.twig")
     */
    public function generateMinimalAction($id)
    {
        $data = $this->genPaAccion($id);

        return $data;
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute()
            ]
        ];
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'siia_panel'
            ]
        ];
    }

    public function getBaseRoute()
    {
        return 'plan_accion_ver';
    }

    public function getLeftShowNav()
    {
        // TODO: Implement getLeftShowNav() method.
    }

    private function getList($page, $limit, $params = array())
    {
        $asesores_id = [];
        $repository = $this
            ->get('doctrine.orm.entity_manager')
            ->getRepository('SiiaBundle:Asociacion');

        if ($this->getUser()->hasRole(BaseProfile::ADMIN) || $this->getUser()->hasRole(BaseProfile::SUPERVISOR)) {
            return $repository->findAllWithDGEmpresarial();
        } else if ($this->getUser()->hasRole(BaseProfile::OPERADOR)) {
            if (isset($params['asesor']) && !empty($params['asesor'])) {
                $asesores_id[] = $params['asesor'];
            } else {
                /** @var Asesor $asesor */
                foreach ($this->getUser()->getOperador()->getAsesor() as $asesor) {
                    $asesores_id[] = $asesor->getId();
                }
            }
        } else if ($this->getUser()->hasRole(BaseProfile::ASESOR)) {
            $asesores_id[] = $this->getUser()->getAsesor()->getId();
        }

        return $repository->findAllWithDGEmpresarial($asesores_id);
    }
}
