<?php

namespace Incoder\Bundle\SiiaBundle\Controller\PlanAccion;

use JulianReyes\Controller\AbstractEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * PlanAccion\OpcionPorAfirmacion controller.
 *
 * @Route("/opcion-por-afirmacion")
 * @EController("plan.opcion_afirmacion")
 */
class OpcionPorAfirmacionController extends AbstractEntityController
{
    /**
     * Lists all PlanAccion\OpcionPorAfirmacion entities.
     *
     * @Route("/", name="plan-accion_plan")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $params = [])
    {
        return parent::indexAction($request, $params, 1, 10);
    }

    /**
     * Creates a new PlanAccion\OpcionPorAfirmacion entity.
     *
     * @Route("/", name="plan-accion_plan_create")
     * @Method("POST")
     * @Template("SiiaBundle:PlanAccion\OpcionPorAfirmacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * Displays a form to create a new PlanAccion\OpcionPorAfirmacion entity.
     *
     * @Route("/new", name="plan-accion_plan_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Displays a form to edit an existing PlanAccion\OpcionPorAfirmacion entity.
     *
     * @Route("/{id}/edit", name="plan-accion_plan_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }


    /**
     * Edits an existing PlanAccion\OpcionPorAfirmacion entity.
     *
     * @Route("/{id}", name="plan-accion_plan_update")
     * @Method("PUT")
     * @Template("SiiaBundle:PlanAccion\OpcionPorAfirmacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute()
            ]
        ];
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'plan.plan.new',
                'route' => $this->getBaseRoute() . '_new',
                'icon' => 'fa-plus',
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'plan-accion'
            ]
        ];
    }
}
