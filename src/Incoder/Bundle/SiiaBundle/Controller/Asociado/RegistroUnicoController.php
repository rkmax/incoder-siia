<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociado;


use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Controller\ArrayAdapter;
use JulianReyes\Controller\Pagerfanta;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegistroUnicoController
 * @package Incoder\Bundle\SiiaBundle\Controller\Asociado
 * @EController("registro_unico")
 */
class RegistroUnicoController extends AbstractWeakEntityController
{
    /**
     *
     * @Route("/{asociacion}/", name="registro_unico")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $asociacion = null)
    {
        return parent::indexAction($request, [], 1, 10, $asociacion);
    }

    /**
     * @param Request $request
     * @param null    $asociacion
     *
     * @return array|mixed
     *
     * @Route("/{asociacion}/", name="registro_unico_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asociado/RegistroUnico:new.html.twig")
     */
    public function createAction(Request $request, $asociacion = null)
    {
        return parent::createAction($request, $asociacion);
    }


    /**
     * @param null $asociacion
     *
     * @return array|mixed
     *
     * @Route("/{asociacion}/new", name="registro_unico_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociacion = null)
    {
        return parent::newAction($asociacion);
    }

    /**
     * @param int  $id
     * @param null $asociacion
     *
     * @return array
     * @Route("/{id}/edit", name="registro_unico_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociacion = null)
    {
        return parent::editAction($id, $asociacion);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @param null    $asociado
     *
     * @return mixed
     * @Route("/{id}", name="registro_unico_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asociado\RegistroUnico:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociado = null)
    {
        return parent::updateAction($request, $id, $asociado);
    }


    /**
     * @param $id
     *
     * @return array
     *
     * @Route("/{id}/show", name="registro_unico_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        return parent::showAction($id);
    }


    protected function returnAfterUpdate($entity = null, $params = [])
    {
        $params['asociacion'] = $this->getRelatedEntity()->getId();
        return parent::returnAfterUpdate($entity, $params);
    }

    protected function returnAfterCreate($entity = null, $params = [])
    {
        $params['asociacion'] = $this->getRelatedEntity()->getId();

        return parent::returnAfterCreate($entity, $params);
    }


    /**
     * @return array
     */
    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => $this->getBaseRoute(),
                'route_args' => [
                    'asociacion' => $this->getRelatedEntity()->getId()
                ]
            ]
        ];
    }

    public function getLeftShowNav()
    {
        if (count($this->getEntity()->getPerfil()) > 0) {
            $suffix = 'edit';
            $route_args = [
                'id' => $this->getEntity()->getPerfil()->last()->getId()
            ];
            $icon = 'fa-pencil';
        } else {
            $suffix = 'new';
            $route_args = [
                'asociado' => $this->getEntity()->getId()
            ];
            $icon = 'fa-plus';
        }
        return array_merge(
        [
            [
                'title' => 'asociado.registro_unico.edit',
                'icon' => 'fa-pencil',
                'route' => $this->getBaseRoute() . '_edit',
                'route_args' => [
                    'id' => $this->getEntity()->getId()
                ]
            ],
            [
                'title' => "asociado.resumen.${suffix}_perfil",
                'icon' => $icon,
                'route' => "perfil_socioe_${suffix}",
                'route_args' => $route_args
            ],
            [
                'title' => 'asociado.cuadro.agricola.list',
                'route' => 'cuadro_agricola_list',
                'route_args' => ['asociado' => $this->getEntity()->getId()]
            ],
            [
                'title' => 'asociado.cuadro.pecuario.list',
                'route' => 'cuadro_pecuario_list',
                'route_args' => ['asociado' => $this->getEntity()->getId()]
            ],
        ],parent::getLeftShowNav());
    }
    /**
     * @return array
     */
    public function getLeftNav()
    {
        return [
            [
                'title' => 'asociado.registro_unico.new',
                'icon' => 'fa-plus',
                'route' => $this->getBaseRoute() . '_new',
                'route_args' => [
                    'asociacion' => $this->getRelatedEntity()->getId()
                ]
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'asociacion_caracterizacion_show',
                'route_args' => [
                    'id' => $this->getRelatedEntity()->getId()
                ]
            ]
        ];
    }
}
