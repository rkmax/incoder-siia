<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 3/1/14
 * Time: 2:23 PM
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociado;

use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Controller\ArrayAdapter;
use JulianReyes\Controller\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CuadroPecuarioController
 * @package Incoder\Bundle\SiiaBundle\Controller\Asociado
 * @EController("cuadro_pecuario")
 * @Route("/cuadro_pecuario")
 */
class CuadroPecuarioController extends AbstractWeakEntityController {

    /**
     *
     * @Route("/{asociado}", name="cuadro_pecuario_list")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $asociado = null)
    {
        return parent::indexAction($request, [], 1, 10, $asociado);
    }

    /**
     * @param Request $request
     * @param null    $asociado
     *
     * @return array|mixed
     *
     * @Route("/{asociado}", name="cuadro_pecuario_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asociado/CuadroPecuario:new.html.twig")
     */
    public function createAction(Request $request, $asociado = null)
    {
        return parent::createAction($request, $asociado);
    }

    /**
     * @param null $asociado
     *
     * @return array|mixed
     *
     * @Route("/{asociado}/new", name="cuadro_pecuario_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociado = null)
    {
        return parent::newAction($asociado);
    }


    /**
     * @param int  $id
     * @param null $asociado
     *
     * @return array
     *
     * @Route("/{id}/edit", name="cuadro_pecuario_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociado = null)
    {
        return parent::editAction($id, $asociado);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @param null    $asociado
     *
     * @return mixed
     *
     * @Route("/{id}", name="cuadro_pecuario_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asociado/CuadroPecuario:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociado = null)
    {
        return parent::updateAction($request, $id, $asociado);
    }

    protected function returnAfterCreate($entity = null, $params = [])
    {
        $params['asociado'] = $this->getRelatedEntity()->getId();

        return $this->redirect($this->generateUrl('cuadro_pecuario_list', $params));
    }

    protected function returnAfterUpdate($entity = null, $params = [])
    {
        return $this->returnAfterCreate($entity, $params);
    }


    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'cuadro_pecuario_list',
                'route_args' => [
                    'asociado' => $this->getRelatedEntity()->getId()
                ]
            ]
        ];
    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'asociado.cuadro.pecuario.new',
                'icon' => 'fa-plus',
                'route' => 'cuadro_pecuario_new',
                'route_args' => ['asociado' => $this->getRelatedEntity()->getId()]
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'registro_unico_show',
                'route_args' => [
                    'id' => $this->getRelatedEntity()->getId()
                ]
            ]];
    }
} 