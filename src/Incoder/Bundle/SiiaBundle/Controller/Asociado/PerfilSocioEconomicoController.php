<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociado;


use Incoder\Bundle\SiiaBundle\Entity\PerfilSocioEconomico;
use JulianReyes\Controller\AbstractWeakEntityController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PerfilSocioEconomicoController
 * @package Incoder\Bundle\SiiaBundle\Controller\Asociado
 * @EController("perfil_socio_economico")
 * @Route("/perfil")
 */
class PerfilSocioEconomicoController extends AbstractWeakEntityController {


    /**
     * @param Request $request
     * @param null    $asociado
     *
     * @return array|mixed
     *
     * @Route("/{asociado}", name="perfil_socioe_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asociado/PerfilSocioEconomico:new.html.twig")
     */
    public function createAction(Request $request, $asociado = null)
    {
        return parent::createAction($request, $asociado);
    }

    /**
     * @param null $asociado
     *
     * @return array|mixed
     *
     * @Route("/{asociado}/new", name="perfil_socioe_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociado = null)
    {
        return parent::newAction($asociado);
    }


    /**
     * @param int  $id
     * @param null $asociado
     *
     * @return array
     *
     * @Route("/{id}/edit", name="perfil_socioe_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociado = null)
    {
        return parent::editAction($id, $asociado);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @param null    $asociado
     *
     * @return mixed
     *
     * @Route("/{id}", name="perfil_socioe_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asociado/PerfilSocioEconomico:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociado = null)
    {
        return parent::updateAction($request, $id, $asociado);
    }

    /**
     * @param PerfilSocioEconomico $entity
     * @param array $params
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function returnAfterCreate($entity = null, $params = [])
    {
        $params['id'] = $entity->getAsociado()->getId();

        return $this->redirect($this->generateUrl('registro_unico_show', $params));
    }

    protected function returnAfterUpdate($entity = null, $params = [])
    {
        return $this->returnAfterCreate($entity, $params);
    }


    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'registro_unico_show',
                'route_args' => [
                    'id' => $this->getRelatedEntity()->getId()
                ]
            ]
        ];
    }

    public function getLeftNav()
    {
        // TODO: Implement getLeftNav() method.
    }
}
