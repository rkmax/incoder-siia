<?php

namespace Incoder\Bundle\SiiaBundle\Controller;

use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\Insumo;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\AbstractProducto;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\Producto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Incoder\Bundle\SiiaBundle\Form\DirectorySearchType;
use Incoder\Bundle\SiiaBundle\Lib\AsociacionSearchFilters;

/**
 * @Route("/directorio")
 */
class DirectorioPublicoController extends Controller {

    const TAB_SEARCH = 'search';
    const TAB_DIRECTORY = 'directory';



    /**
     * @Route("/contenido", name="siiac_contenido")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function contenidoAction()
    {
        return [];
    }

    /**
     *
     * @return \Incoder\Bundle\SiiaBundle\Repository\AsociacionRepository
     */
    public function getCaracterizacionRepo() {
        return $this->getDoctrine()->getManager()
                        ->getRepository('SiiaBundle:Asociacion');
    }

    /**
     * @Route("/{page}/{limit}", name="siia_directorio", requirements={"page" = "\d+", "limit" = "\d+"})
     * @Method({"GET"})
     * @Template()
     */
    public function indexAction($page = 1, $limit = 20) {
        $results = $this->getCaracterizacionRepo()->findAllPaginated($page, $limit);

        return [
            'query' => '',
            'tab' => self::TAB_SEARCH,
            'results' => $results,
            'search_results' => [],
            'search_form' => $this->getSearchForm()->createView()
        ];
    }

    /**
     *
     * @param null $filters
     *
     * @return \Symfony\Component\Form\Form
     */
    public function getSearchForm($filters = null) {
        if(!$filters)
            $filters = new AsociacionSearchFilters();
        return $this->createForm(
            new DirectorySearchType(),
            $filters,
            [   'action' => $this->generateUrl('siia_directorio_search'),
                'method' => 'POST']);
    }

    /**
     * @Route("/search", name="siia_directorio_search")
     * @Method({"GET", "POST"})
     * @Template("SiiaBundle:DirectorioPublico:search.html.twig")
     */
    public function searchAction(Request $request) {

        $requestQuery = $request->query->get('query', '');

        $results = [];

        if (!empty($requestQuery)) {
            $q_results = $this->get('fos_elastica.finder.search')->find($requestQuery, 500);
            $q_ids = [];
            $qi_ids = [];

            foreach ($q_results as $e) {
                if ($e instanceof AbstractProducto) {
                    $q_ids[] = intval($e->getId());
                }

                if ($e instanceof Insumo) {
                    $qi_ids[] = intval($e->getCuadroProductivo()->getId());
                }

                if ($e instanceof Asociacion) {
                    $results[] = $e;
                }
            }
            $more = $this->getCaracterizacionRepo()->findAllWithProductsInsumos($q_ids, $qi_ids);

            if (!is_array($more)) {
                $more = $more->toArray();
            }

            $results = array_merge($results, $more);
        } else {
            $results = [];
        }

        return [
            'query' => $request->query->get('query', ''),
            'tab' => self::TAB_SEARCH,
            'results' => [],
            'search_results' => $results,
        ];
    }

    /**
     * @Route("/directory", name="siia_directorio_list")
     * @Route("/directory/{page}", name="siia_directorio_list", requirements={"page" = "\d+"})
     * @Route("/directory/{page}/{limit}", name="siia_directorio_list", requirements={"page" = "\d+", "limit" = "\d+"})
     * @Method({"GET"})
     * @Template("SiiaBundle:DirectorioPublico:index.html.twig")
     */
    public function directoryAction($page = 1, $limit = 20) {
        $filter = $this->getRequest()->query->get('filter');
        $results = $this->getCaracterizacionRepo()->findAllPaginatedFilter($page, $limit, $filter);

        return [
            'query' => '',
            'tab' => self::TAB_DIRECTORY,
            'results' => $results,
            'search_results' => []
        ];
    }

}
