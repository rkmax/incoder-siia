<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 11/03/14
 * Time: 10:19 AM
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;


use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Controller\ArrayAdapter;
use JulianReyes\Controller\Pagerfanta;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 *
 * @package Incoder\Bundle\SiiaBundle\Controller
 *
 * @Route("/diagnostico/tecnico/concepto")
 * @author German Rosales
 * @EController("diagnostico_tecnico_concepto")
 */
class ConceptoTecnicoController extends AbstractWeakEntityController{

    /**
     *
     * @Route("/{diagnostico}", name="concepto_tecnico")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $diagnostico = null)
    {
        return parent::indexAction($request, [], 1, 10, $diagnostico);
    }

    /**
     * @param Request $request
     * @param null    $diagnostico
     *
     * @return array|mixed
     *
     * @Route("/{diagnostico}", name="concepto_tecnico_create")
     * @Method("POST")
     * @Template("SiiaBundle:ConceptoTecnico:new.html.twig")
     */
    public function createAction(Request $request, $diagnostico = null)
    {
        return parent::createAction($request, $diagnostico);
    }

    /**
     * @param null $diagnostico
     *
     * @return array|mixed
     *
     * @Route("/{diagnostico}/new", name="concepto_tecnico_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($diagnostico = null)
    {
        return parent::newAction($diagnostico);
    }


    /**
     * @param int  $id
     * @param null $diagnostico
     *
     * @return array
     *
     * @Route("/{id}/edit", name="concepto_tecnico_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $diagnostico = null)
    {
        return parent::editAction($id, $diagnostico);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @param null    $diagnostico
     *
     * @return mixed
     *
     * @Route("/{id}", name="concepto_tecnico_update")
     * @Method("PUT")
     * @Template("SiiaBundle:ConceptoTecnico:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $diagnostico = null)
    {
        return parent::updateAction($request, $id, $diagnostico);
    }

    protected function returnAfterCreate($entity = null, $params = array()) {
        return $this->redirect(
            $this->generateUrl(
                'asociacion_caracterizacion_show', ['id' => $this->getRelatedEntity()->getAsociacion()->getId()]));
    }

    protected function returnAfterUpdate($entity = null, $params = []) {
        return $this->redirect(
            $this->generateUrl(
                'asociacion_caracterizacion_show', ['id' => $this->getRelatedEntity()->getAsociacion()->getId()]));
    }


    public function getLeftFormNav() {
        $menu = [];
        $menu[] = [
            'title' => 'general.menu.back',
            'icon' => 'fa-chevron-left',
            'route' => 'asociacion_caracterizacion_show',
            'route_args' => ['id' => $this->getRelatedEntity()->getAsociacion()->getId()]
        ];
        return $menu;
    }

    public function getLeftNav()
    {

        return [];
    }
} 