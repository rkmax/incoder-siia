<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/20/14
 * Time: 11:14 AM
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;

use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta;
use JulianReyes\Controller\AbstractEntityController;
use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Controller\ArrayAdapter;
use JulianReyes\Controller\Pagerfanta;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DiagnosticoEmpresarialController
 *
 * @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * @Route("/diagnostico/empresarial")
 * @EController("diagnostico_empresarial")
 */
class DiagnosticoEmpresarialController extends AbstractWeakEntityController {

    /**
     * Lists
     *
     * @Route("/{asociacion}", name="diagnostico_empresarial")
     * @Method("GET")
     * @Template()
     */
    function listAction(Request $request, $asociacion = null)
    {
        return parent::indexAction($request, [], 1, 10, $asociacion);
    }


    /**
     * Creates a new PlanAccion\Accion entity.
     *
     * @Route("/{asociacion}", name="diagnostico_empresarial_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asociacion\DiagnosticoEmpresarial:new.html.twig")
     */
    public function createAction(Request $request, $asociacion = null)
    {
        return parent::createAction($request, $asociacion);
    }

    /**
     * Displays a form to create a new PlanAccion\Accion entity.
     *
     * @Route("/{asociacion}/new", name="diagnostico_empresarial_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociacion = null)
    {
        return parent::newAction($asociacion);
    }

    /**
     * Displays a form to edit an existing PlanAccion\Accion entity.
     *
     * @Route("/{asociacion}/{id}/edit", name="diagnostico_empresarial_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociacion = null)
    {
        return parent::editAction($id, $asociacion);
    }

    /**
     * Edits an existing PlanAccion\Accion entity.
     *
     * @Route("/{asociacion}/{id}", name="diagnostico_empresarial_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asociacion\DiagnosticoEmpresarial:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociacion = null)
    {
        return parent::updateAction($request, $id, $asociacion);
    }
    /**
     * Deletes a PlanAccion\Accion entity.
     *
     * @Route("/{asociacion}/{id}", name="diagnostico_empresarial_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        return parent::deleteAction($request, $id);
    }

    public function getLeftFormNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'asociacion_caracterizacion_show',
                'route_args' => ['id' => $this->getRelatedEntity()->getId()]
            ]
        ];
    }

    public function getLeftNav()
    {
        // TODO: Implement getLeftNav() method.
    }

    /**
     * @param DiagnosticoEmpresarial $entity
     * @param array $params
     * @return RedirectResponse
     */
    protected function returnAfterCreate($entity = null, $params = [])
    {
        $params['id'] = $entity->getAsociacion()->getId();
        return $this->redirect($this->generateUrl('plan_accion_generate', $params));
    }

    protected function returnAfterUpdate($entity = null, $params = [])
    {
        $params['id'] = $entity->getAsociacion()->getId();
        $params['force'] = true;
        return $this->redirect($this->generateUrl('plan_accion_generate', $params));
    }
}
