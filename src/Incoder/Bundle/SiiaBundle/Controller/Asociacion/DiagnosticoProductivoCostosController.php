<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;

use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * 
 * @package Incoder\Bundle\SiiaBundle\Controller
 *
 * @Route("/diagnostico/productivo/costos")
 * @author German Rosales
 * @EController("diagnostico_productivo_costos")
 */
class DiagnosticoProductivoCostosController extends AbstractWeakEntityController {

    /**
     * Persiste en la base de datos, los datos del formulario
     *
     * @param Request  $request
     * @param int|null $diagnosticoProductivo
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{diagnosticoProductivo}/", name="diagnostico_productivo_costo_create")
     * @Method("POST")
     * @Template("SiiaBundle:DiagnosticoProductivo:Costos:new.html.twig")
     */
    public function createAction(Request $request, $diagnosticoProductivo = null) {
        return parent::createAction($request, $diagnosticoProductivo);
    }

    /**
     * Muestra el formularios vacio
     *
     * @param int|null $diagnosticoProductivo
     *
     * @return array
     *
     * @Route("/{diagnosticoProductivo}/new", name="diagnostico_productivo_costo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($diagnosticoProductivo = null) {
        return parent::newAction($diagnosticoProductivo);
    }

    /**
     * Muestra el formulario con los datos obtenidos de la base de datos
     *
     * @param int $id
     * @param int $diagnosticoProductivo
     *
     * @return array
     *
     * @Route("/{diagnosticoProductivo}/{id}/edit", name="diagnostico_productivo_costo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $diagnosticoProductivo = null) {
        $this->entity = $this->getService()->find($id);
        return parent::editAction($id, $diagnosticoProductivo);
    }

    /**
     * Persiste los cambios en la base de datos con los datos obtenidos
     * del formulario
     *
     * @param Request $request
     * @param int     $id
     * @param int     $diagnosticoProductivo
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{diagnosticoProductivo}/{id}", name="diagnostico_productivo_costo_update")
     * @Method("PUT")
     * @Template("SiiaBundle:DiagnosticoProductivo:Costos:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $diagnosticoProductivo = null) {
        return parent::updateAction($request, $id, $diagnosticoProductivo);
    }

    public function getLeftFormNav() {
        $menu = [];
        $menu[] = [
            'title' => 'general.menu.back',
            'icon' => 'fa-chevron-left',
            'route' => 'diagnostico_productivo_costo',
            'route_args' => ['diagnosticoProductivo' => $this->getRelatedEntity()->getId()]
        ];
        return $menu;
    }

    public function getLeftNav() {
        return [
            [
                'title' => 'asociacion.dg_productivo.costo.new',
                'icon' => 'fa-plus',
                'route' => 'diagnostico_productivo_costo_new',
                'route_args' => ['diagnosticoProductivo' => $this->getRelatedEntity()->getId()]
            ],
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'asociacion_caracterizacion_show',
                'route_args' => ['id' => $this->getRelatedEntity()->getAsociacion()->getId()]
            ],
        ];
    }

    protected function returnAfterCreate($entity = null, $params = array()) {
        return $this->redirect($this->generateUrl(
            'diagnostico_productivo_costo',
            ['diagnosticoProductivo' => $this->getRelatedEntity()->getId()]
        ));
    }

    protected function returnAfterUpdate($entity = null, $params = []) {
        return $this->redirect($this->generateUrl(
            'diagnostico_productivo_costo', ['diagnosticoProductivo' => $this->getRelatedEntity()->getId()]
        ));
    }

}
