<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;

use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * package Incoder\Bundle\SiiaBundle\Controller
 *
 * @Route("/diagnostico/productivo/produccion")
 * @author German Rosales
 * @EController("diagnostico_productivo_produccion")
 */
class DiagnosticoProductivoProduccionController extends AbstractWeakEntityController {

    /**
     * Persiste en la base de datos, los datos del formulario
     *
     * @param Request $request
     * @param int     $producto
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{producto}/", name="diagnostico_productivo_produccion_create")
     * @Method("POST")
     * @Template("SiiaBundle:DiagnosticoProductivo:Produccion:new.html.twig")
     */
    public function createAction(Request $request, $producto = null) {
        return parent::createAction($request, $producto);
    }

    /**
     * Muestra el formularios vacio
     *
     * @param int $producto
     *
     * @return array
     *
     * @Route("/{producto}/new", name="diagnostico_productivo_produccion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($producto = null) {
        return parent::newAction($producto);
    }

    /**
     * Muestra el formulario con los datos obtenidos de la base de datos
     *
     * @param string $id
     * @param int   $producto
     *
     * @return array
     *
     * @Route("/{producto}/{id}/edit", name="diagnostico_productivo_produccion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $producto = null) {
        return parent::editAction($id, $producto);
    }

    /**
     * Persiste los cambios en la base de datos con los datos obtenidos
     * del formulario
     *
     * @param Request $request
     * @param int     $id
     * @param int     $producto
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{producto}/{id}", name="diagnostico_productivo_produccion_update")
     * @Method("PUT")
     * @Template("SiiaBundle:DiagnosticoProductivo:Produccion:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $producto = null) {
        return parent::updateAction($request, $id, $producto);
    }

    public function getLeftFormNav() {
        $menu = [];
        $menu[] = [
            'title' => 'general.menu.back',
            'icon' => 'fa-chevron-left',
            'route' => 'diagnostico_productivo_produccion',
            'route_args' => ['producto' => $this->getRelatedEntity()->getId()]
        ];
        return $menu;
    }

    public function getLeftNav() {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'asociacion_caracterizacion_show',
                'route_args' => ['id' => $this->getRelatedEntity()->getDiagnosticoProductivo()->getAsociacion()->getId()]
            ],
            [
                'title' => 'asociacion.resumen.dg_productivo_produccion_new',
                'icon'  => 'fa-plus',
                'route' => 'diagnostico_productivo_produccion_new',
                'route_args' => ['producto' => $this->getRelatedEntity()->getId()]
            ]
        ];
    }
    
    protected function returnAfterCreate($entity = null, $params = array()) {
        return $this->redirect(
                        $this->generateUrl(
                                'diagnostico_productivo_produccion', ['producto' => $this->getRelatedEntity()->getId()]));
    }

    protected function returnAfterUpdate($entity = null, $params = []) {
        return $this->redirect(
                        $this->generateUrl(
                                'diagnostico_productivo_produccion', ['producto' => $this->getRelatedEntity()->getId()]));
    }
}
