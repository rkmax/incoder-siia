<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;

use JulianReyes\Controller\AbstractWeakEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * 
 * @package Incoder\Bundle\SiiaBundle\Controller
 *
 * @Route("/diagnostico/tecnico")
 * @author German Rosales
 * @EController("diagnostico_tecnico")
 */
class DiagnosticoTecnicoController extends AbstractWeakEntityController {

    /**
     * Persiste en la base de datos, los datos del formulario
     *
     * @param Request  $request
     * @param int|null $asociacion
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{asociacion}/", name="diagnostico_tecnico_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asociacion/DiagnosticoTecnico:new.html.twig")
     */
    public function createAction(Request $request, $asociacion = null) {
        return parent::createAction($request, $asociacion);
    }

    /**
     * Muestra el formularios vacio
     *
     * @param int|null $asociacion
     *
     * @return array
     *
     * @Route("/{asociacion}/new", name="diagnostico_tecnico_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($asociacion = null) {
        return parent::newAction($asociacion);
    }

    /**
     * Muestra el formulario con los datos obtenidos de la base de datos
     *
     * @param int $id
     * @param int $asociacion
     *
     * @return array
     *
     * @Route("/{asociacion}/{id}/edit", name="diagnostico_tecnico_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $asociacion = null) {
        $this->entity = $this->getService()->find($id);
        return parent::editAction($id, $asociacion);
    }

    /**
     * Persiste los cambios en la base de datos con los datos obtenidos
     * del formulario
     *
     * @param Request $request
     * @param string  $id
     *
     * @param null    $asociacion
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{asociacion}/{id}", name="diagnostico_tecnico_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asociacion/DiagnosticoTecnico:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $asociacion = null) {
        return parent::updateAction($request, $id, $asociacion);
    }

    public function getLeftFormNav() {
        $menu = [];
        $menu[] = [
            'title' => 'general.menu.back',
            'icon' => 'fa-chevron-left',
            'route' => 'asociacion_caracterizacion_show',
            'route_args' => ['id' => $this->getRelatedEntity()->getId()]
        ];
        return $menu;
    }

    public function getLeftNav() {
        return [];
    }
    
    protected function returnAfterCreate($entity = null, $params = array()) {
        return $this->redirect(
                        $this->generateUrl(
                                'asociacion_caracterizacion_show', ['id' => $this->getRelatedEntity()->getId()]));
    }

    protected function returnAfterUpdate($entity = null, $params = []) {
        return $this->redirect(
                        $this->generateUrl(
                                'asociacion_caracterizacion_show', ['id' => $this->getRelatedEntity()->getId()]));
    }
}
