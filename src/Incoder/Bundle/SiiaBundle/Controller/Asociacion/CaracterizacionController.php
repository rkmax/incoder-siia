<?php

/**
 * @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;

use Elastica\Query\Bool;
use Elastica\Query\HasChild;
use Elastica\Query\Match;
use Elastica\Query\MatchAll;
use Elastica\Query\MultiMatch;
use Elastica\Query\Nested;
use Elastica\Query\QueryString;
use Elastica\Query\Term;
use Elastica\Query;
use FOS\ElasticaBundle\Paginator\FantaPaginatorAdapter;
use Incoder\Bundle\SiiaBundle\DSL\Caracterizacion\Factory;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico;
use Incoder\Bundle\SiiaBundle\Entity\Operador;
use JulianReyes\Controller\AbstractEntityController;
use JulianReyes\Lib\Annotation\EntityControllerAnnotation as EController;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 *
 * @package Incoder\Bundle\SiiaBundle\Controller
 *
 * @Route("/caracterizacion")
 * @EController("caracterizacion")
 */
class CaracterizacionController extends AbstractEntityController
{
    /**
     *
     * Route("/", name="asociacion_caracterizacion")
     * Method("GET")
     * Template()
     *
     * @SuppressWarnings("unused")
     */
    function listAction(Request $request, $params = [])
    {
        $asesor = $request->get('asesor', '');

        $ar = $this->get('doctrine.orm.entity_manager')->getRepository('SiiaBundle:Asesor');

        if ($this->getUser()->hasRole(BaseProfile::ADMIN) || $this->getUser()->hasRole(BaseProfile::SUPERVISOR)) {
            $asesores = $ar->findAll();
        } else if ($this->getUser()->hasRole(BaseProfile::OPERADOR)) {
            $asesores = $ar->findAllByOperador($this->getUser()->getOperador()->getId());
        } else {
            $asesores = [];
        }

        $result =  parent::indexAction($request, ['asesor' => $asesor]);

        $result['asesores'] = $asesores;
        $result['asesor'] = $asesor;

        return $result;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @Route("/", name="asociacion_caracterizacion")
     * @Template("@Siia/Asociacion/Caracterizacion/list_rev2.html.twig")
     * @Method("GET")
     */
    public function listRev2Action(Request $request)
    {
        $mainQuery = new Bool();
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);
        $query = null;
        $searchBy = null;

        $filtradoPorOperador = function (Operador $operador) use ($mainQuery) {

            $mainAsesor = new Bool();
            foreach ($operador->getAsesor() as $asesor) {

                $mainAsesor->addShould(Factory::asesor($asesor->getId()));
            }
            $mainQuery->addMust($mainAsesor);
        };

        // Restriccion por defecto segun el rol de la persona
        if ($this->getUser()->hasRole(BaseProfile::ADMIN)
            || $this->getUser()->hasRole(BaseProfile::SUPERVISOR)
            || $this->getUser()->hasRole(BaseProfile::SUPER_ADMIN)) {

            $mainQuery->addShould(new MatchAll());
        } else if ($this->getUser()->hasRole(BaseProfile::OPERADOR)) {

            $filtradoPorOperador($this->getUser()->getOperador());
        } else if ($this->getUser()->hasRole(BaseProfile::ASESOR)) {

            $asesor = $this->getUser()->getAsesor();
            $mainQuery->addMust(Factory::asesor($asesor->getId()));
        }

        if (null !== ($nombre = $request->query->get('nombre'))) {

            $searchBy = 'nombre';
            $query = $nombre;
            $mainQuery->addMust(Factory::nombreAsocacion($nombre));
        }

        // Busqueda por operador
        if (null !== ($operador = $request->query->get('operador'))
            && ($this->getUser()->hasRole(BaseProfile::ADMIN)
                || $this->getUser()->hasRole(BaseProfile::SUPERVISOR))) {

            $query = $operador;
            $searchBy = 'operador';
            $mainQuery->addMust(Factory::operador($query));
        }

        // Busqueda por asesor
        if (null !== ($asesor = $request->query->get('asesor'))
            && ($this->getUser()->hasRole(BaseProfile::OPERADOR)
                || $this->getUser()->hasRole(BaseProfile::SUPERVISOR))) {

            $mainQuery->addMust(Factory::asesorName($asesor));
            $query = $asesor;
            $searchBy = 'asesor';
        }

        // Busqueda por Ubicacion
        if (null !== ($localizacion = $request->query->get('localizacion'))) {

            $mainQuery->addMust(Factory::localizacionAsociacion($localizacion));
            $query = $localizacion;
            $searchBy = 'localizacion';
        }

        // Busqueda por producto
        if (null !== ($producto = $request->query->get('producto'))) {

            $query = $producto;
            $searchBy = 'producto';
            $mainProducto = new Bool();
            foreach (['cuadroAgricola', 'cuadroPecuario'] as $cuadro) {

                $matchProducto = new MultiMatch();
                $matchProducto->setFields([
                    "asociado.{$cuadro}.producto.nombreProducto",
                    "asociado.{$cuadro}.producto.variedadProducto"
                ]);
                $matchProducto->setQuery($producto);

                $nestedProducto = new Nested();
                $nestedProducto->setPath("asociado.{$cuadro}.producto");
                $nestedProducto->setQuery($matchProducto);

                $mainProducto->addShould($nestedProducto);
            }

            $mainQuery->addMust($mainProducto);
        }

        $q = Query::create($mainQuery);

        $sorts = $request->query->get('sorts');
        if($sorts){
            foreach($sorts as $sorti => $sortv){
                $q->setSort([$sorti => ['order' => $sortv]]);
            }
        }

        // Obtener resultados
        $result = $this->get('fos_elastica.finder.listing.asociacion')->findPaginated($q);
        $result->setCurrentPage($page);
        $result->setMaxPerPage($limit);

        return [
            'page' => $page,
            'limit' => $limit,
            'query' => "'{$query}'",
            'searchBy' => "'{$searchBy}'",
            'leftnav' => $this->getLeftNav(),
            'entities' => $result,
            'sorts' => $sorts,
        ];
    }

    /**
     * @return Response
     *
     * @Route("/action", name="asociacion_caracterizacion_actions", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function listActions()
    {
        $actions = [
            ['id' => 'nombre', 'label' => 'Nombre asociación'],
            ['id' => 'producto', 'label' => 'Nombre producto'],
            ['id' => 'localizacion', 'label' => 'Ubicacion asociación'],
        ];

        if ($this->getUser()->hasRole(BaseProfile::OPERADOR) || $this->getUser()->hasRole(BaseProfile::SUPERVISOR)) {
            $actions[] = ['id' => 'asesor', 'label' => 'Nombre asesor'];
        }
        if ($this->getUser()->hasRole(BaseProfile::ADMIN) || $this->getUser()->hasRole(BaseProfile::SUPERVISOR)) {
            $actions[] = ['id' => 'operador', 'label' => 'Nombre operador'];
        }

        return new Response(json_encode($actions));
    }

    /**
     * Persiste en la base de datos, los datos del formulario
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/", name="asociacion_caracterizacion_create")
     * @Method("POST")
     * @Template("SiiaBundle:Asociacion/Caracterizacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * @param $id
     *
     * @return array
     *
     * @Route("/{id}/show", name="asociacion_caracterizacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        return parent::showAction($id);
    }


    /**
     * Muestra el formularios vacio
     *
     * @return array
     *
     * @Route("/new", name="asociacion_caracterizacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        return parent::newAction();
    }

    /**
     * Muestra el formulario con los datos obtenidos de la base de datos
     *
     * @param string $id
     *
     * @internal param string $dformType
     * @return array
     *
     * @Route("/{id}/edit", name="asociacion_caracterizacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        return parent::editAction($id);
    }

    /**
     * Persiste los cambios en la base de datos con los datos obtenidos
     * del formulario
     *
     * @param Request $request
     * @param string $id
     *
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}", name="asociacion_caracterizacion_update")
     * @Method("PUT")
     * @Template("SiiaBundle:Asociacion/Caracterizacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        return parent::updateAction($request, $id);
    }

    /**
     *
     * @param $asociacion
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return array
     *
     * @Route("/list/{asociacion}", name="asociacion_caracterizacion_usuarios")
     * @Method("GET")
     * @Template()
     */
    public function registroUnicoAction($asociacion)
    {
        $asociacion = $this->getService()->find($asociacion);

        if (!$asociacion) {
            throw $this->createNotFoundException("La asociacion indicada no existe");
        }
        $this->entity = $asociacion;

        return [
            'leftnav' => $this->getLeftFormNav(),
            'entities' => $asociacion->getAsociado(),
        ];
    }

    public function getLeftFormNav()
    {
        if (!$this->getUser()->hasRole(BaseProfile::ASOCIACION)) {
            return [
                [
                    'title' => 'general.menu.back',
                    'icon' => 'fa-chevron-left',
                    'route' => $this->getBaseRoute()
                ]
            ];
        } else {
            return [
                [
                    'title' => 'general.menu.back',
                    'icon' => 'fa-chevron-left',
                    'route' => $this->getBaseRoute() .'_show',
                    'route_args' => ['id' => $this->getEntity()->getId() ]
                ]
            ];
        }

    }

    public function getLeftNav()
    {
        $menu = [];
        if(!$this->getUser()->hasRole(BaseProfile::SUPERVISOR)){

            $menu[] = [
                'title' => 'asociacion.menu.caracterizacion.new',
                'icon' => 'fa-plus',
                'route' => $this->getBaseRoute() . '_new',
            ];
        };

        $menu[] = [
            'title' => 'general.menu.back',
            'icon' => 'fa-chevron-left',
            'route' => 'siia_panel'
        ];

        return $menu;
    }

    public function getLeftShowNav()
    {
        $menu = [
            [
                'title' => 'asociacion.resumen.listado_registro',
                'icon' => 'fa-cogs',
                'route' => $this->getBaseRoute() . '_usuarios',
                'route_args' => [
                    'asociacion' => $this->getEntity()->getId()
                ],
            ],
        ];

        if(!$this->getUser()->hasRole(BaseProfile::SUPERVISOR)){
            $menu[] = [
                    'title' => 'asociacion.resumen.registro_unico',
                    'route' => 'registro_unico',
                    'route_args' => ['asociacion' => $this->getEntity()->getId()],
            ];
            $menu[] = [
                'title' => 'asociacion.menu.caracterizacion.edit',
                'icon' => 'fa-pencil',
                'route' => $this->getBaseRoute() . '_edit',
                'route_args' => [
                    'id' => $this->getEntity()->getId()
                ]
            ];
        }

        if (!$this->getUser()->hasRole(BaseProfile::ASOCIACION)) {
            // Define que ruta y mensaje usar
            if (count($this->getEntity()->getDiagnosticoEmpresarial()) == 0) {

                if(!$this->getUser()->hasRole(BaseProfile::SUPERVISOR)){
                    $menu[] = [
                        'title' => 'asociacion.resumen.dg_empresarial_new',
                        'icon' => 'fa-plus',
                        'route' => 'diagnostico_empresarial_new',
                        'route_args' => ['asociacion' => $this->getEntity()->getId()]
                    ];
                }
            } else {
                if(!$this->getUser()->hasRole(BaseProfile::SUPERVISOR)){
                    $menu[] = [
                        'title' => 'asociacion.resumen.dg_empresarial_edit',
                        'icon' => 'fa-pencil',
                        'route' => 'diagnostico_empresarial_edit',
                        'route_args' => [
                            'asociacion' => $this->getEntity()->getId(),
                            'id' => $this->getEntity()->getDiagnosticoEmpresarial()->last()->getId()
                        ],
                    ];
                }


                $menu[] = [
                    'title' => 'asociacion.plan.generar_plan',
                    'icon' => 'fa-cogs',
                    'route' => 'plan_accion_generate',
                    'route_args' => ['id' => $this->getEntity()->getId()]
                ];
                $menu[] = [
                    'title' => 'ver DOFA',
                    'icon' => 'fa-th-large',
                    'route' => 'dofa_principal',
                    'route_args' => [
                        'asociacion' => $this->getEntity()->getId(),
                    ]
                ];
            }

            if(!$this->getUser()->hasRole(BaseProfile::SUPERVISOR)){
                if (count($this->getEntity()->getDiagnosticoTecnico()) == 0) {
                    $menu[] = [
                        'title' => 'asociacion.resumen.dg_tecnico_new',
                        'icon' => 'fa-plus',
                        'route' => 'diagnostico_tecnico_new',
                        'route_args' => ['asociacion' => $this->getEntity()->getId()]
                    ];
                } else {
                    $dgTecnicoId = $this->getEntity()->getDiagnosticoTecnico()->last()->getId();
                    $menu[] = [
                        'title' => 'asociacion.resumen.dg_tecnico_edit',
                        'icon' => 'fa-pencil',
                        'route' => 'diagnostico_tecnico_edit',
                        'route_args' => [
                            'asociacion' => $this->getEntity()->getId(),
                            'id' => $dgTecnicoId,
                        ],
                    ];
                }
                // Editar, actualizar usuario de asociacion
                if ($this->getEntity()->getPerfil()) {
                    $action = '_edit';
                    $title = 'Editar usuario SIIAT';
                    $icon = 'fa-pencil-square';
                } else {
                    $action = '_new';
                    $title = 'Crear usuario SIIAT';
                    $icon = 'fa-user';
                }
                $menu[] = [
                    'title' => $title,
                    'icon' => $icon,
                    'route' => 'profile_asociacion' . $action,
                    'route_args' => [
                        'id' => $this->getEntity()->getId()
                    ]
                ];
            }
        }

        if ($this->getUser()->hasRole(BaseProfile::ADMIN)) {
            if (count($this->getEntity()->getDiagnosticoTecnico()) > 0 ) {
                /** @var DiagnosticoTecnico $dgt */
                $dgt = $this->getEntity()->getDiagnosticoTecnico()->last();
                if(count($dgt->getConceptos()) == 0){
                    $dgTecnicoId = $dgt->getId();
                    $menu[] = [
                        'title' => 'asociacion.dg_tecnico.concepto.new',
                        'icon' => 'fa-plus',
                        'route' => 'concepto_tecnico_new',
                        'route_args' => [
                            'diagnostico' => $dgTecnicoId,
                        ]
                    ];
                } else {
                    $conceptoId = $this->getEntity()->getDiagnosticoTecnico()->last()->getConceptos()->last()->getId();
                    $menu[] = [
                        'title' => 'asociacion.dg_tecnico.concepto.edit',
                        'icon' => 'fa-pencil',
                        'route' => 'concepto_tecnico_edit',
                        'route_args' => [
                            'id' => $conceptoId,
                        ]
                    ];
                }

            }
        }

        $menu[] = [
            'title' => 'asociacion.consolidado.productivo.label',
            'route' => 'consolidado_general',
            'route_args' => ['asociacion' => $this->getEntity()->getId()],
        ];

        if ($this->getUser()->hasRole(BaseProfile::ASOCIACION)) {
            $menu[] = [
                'title' => 'Indicadores',
                'route' => 'siia_indicadores_generador'
            ];
            $menu[] = [
                'icon' => 'fa-key',
                'title' => 'Cambiar contraseña',
                'route' => 'fos_user_change_password'
            ];
            $menu[] = [
                'icon' => 'fa-sign-out',
                'title' => 'Cerrar sesion',
                'route' => 'fos_user_security_logout'
            ];
        } else {
            $menu[] = [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'asociacion_caracterizacion'
            ];
        }

        return $menu;
    }


}
