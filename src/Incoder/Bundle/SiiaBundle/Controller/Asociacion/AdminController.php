<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Controller\Asociacion;


use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Incoder\Bundle\SiiaBundle\Entity\Admin\MoveAsociacion;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Form\Admin\MoveAsociacionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

/**
 * Class AdminController
 * @package Incoder\Bundle\SiiaBundle\Controller\Asociacion
 *
 * @Route("/admin", service="siia.controllers.asociacion_admin")
 */
class AdminController
{

    /**
     * @var EntityRepository
     */
    private $asesorRepository;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var Router
     */
    private $router;

    function __construct(EntityRepository $asesoreRepository, FormFactory $factory, Router $router)
    {
        $this->asesorRepository = $asesoreRepository;
        $this->formFactory = $factory;
        $this->router = $router;
    }


    /**
     * @param Asociacion $asociacion
     *
     * @return array
     *
     * @Route("/{id}/move", name="siia_admin_move_asociacion")
     * @Method("GET")
     * @Template()
     */
    public function moveAction(Asociacion $asociacion)
    {
        $entity = new MoveAsociacion();
        $entity->setAsociacion($asociacion);
        $form = $this->buildForm($entity);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
            'leftnav' => [
                [
                    'title' => 'general.menu.back',
                    'icon' => 'fa-chevron-left',
                    'route' => 'asociacion_caracterizacion'
                ]
            ]
        ];
    }

    /**
     * @param Request $request
     *
     * @return array|RedirectResponse
     *
     * @Route("/move", name="siia_admin_move_asociacion_post")
     * @Method("POST")
     * @Template("@Siia/Asociacion/Admin/move.html.twig")
     */
    public function doMoveAction(Request $request)
    {
        $entity = new MoveAsociacion();
        $form = $this->buildForm($entity);

        $form->submit($request);

        if ($form->isValid()) {
            $asociacion = $entity->getAsociacion();
            $nuevoAsesor = $entity->getAsesor();

            /** @var Asesor $asesor */
            foreach ($asociacion->getAsesores() as $asesor) {
                $asesor->removeAsociacion($asociacion);
                $this->asesorRepository->persist($asesor);
            }

            $asociacion->getAsesores()->clear();

            $asociacion->addAsesor($nuevoAsesor);
            $this->asesorRepository->persist($asociacion);

            $this->asesorRepository->flush();

            return new RedirectResponse($this->router->generate('asociacion_caracterizacion'));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView()
        ];
    }

    private function buildForm($entity = null)
    {
        $form = $this->formFactory->create(new MoveAsociacionType(), $entity, [
            'action' => $this->router->generate('siia_admin_move_asociacion_post'),
            'method' => 'POST'
        ]);

        $form
            ->add('save', 'submit', [
                'label' => 'Mover',
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);

        return $form;
    }
}