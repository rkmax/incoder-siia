<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Controller;

/**
 *
 * @author chamanx
 */
interface IProfileController {
    
    function getProfileService();
    
    function getRoleName();
}
