<?php

namespace Incoder\Bundle\SiiaBundle\Controller;

use JulianReyes\Controller\AbstractEntityController;
use FOS\UserBundle\Doctrine\GroupManager;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;

abstract class ProfileController extends AbstractEntityController implements IProfileController {

    /**
     * 
     * @return \Incoder\Bundle\SiiaBundle\Service\ProfileService
     */
    public function getProfileService() {
        return $this->get('siia.profile');
    }

    /**
     * Override
     */
    protected function postValidationCreate() {
        $this->getProfileService()->
                createUser($this->entity, $this->getRoleName());
        $this->getService()->persist($this->entity);
        return parent::postValidationCreate();
    }

    /**
     * Override
     */
    protected function postValidationUpdate() {
        //TODO: como hacer todo esto de manera transaccional
        $this->getProfileService()->updateUser($this->entity);
        $this->getService()->persist($this->entity, true);
        return parent::postValidationUpdate();
    }

    /**
     * Devuelve el rol para este perfil
     *
     * @return string
     */
    public function getRoleName() {
        return $this->getConfig()->rol_name;
    }

}
