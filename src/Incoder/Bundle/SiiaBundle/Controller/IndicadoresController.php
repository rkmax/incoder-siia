<?php

namespace Incoder\Bundle\SiiaBundle\Controller;


use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\AsociacionProfile;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico;
use Incoder\Bundle\SiiaBundle\Entity\PerfilSocioEconomico;
use Incoder\Bundle\SiiaBundle\Lib\JSAdapters\GoogleChartJSAdapter;
use JulianReyes\Controller\ControllerNavegationInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/admin/indicadores")
 */
class IndicadoresController extends Controller implements ControllerNavegationInterface
{
    /**
     * @return array
     *
     * @Route("/r", name="siia_indicadores_generador")
     * @Method("GET")
     * @Template("@Siia/Indicadores/generator_rev2.html.twig")
     */
    public function generatorAction()
    {
        return [
            'leftnav' => $this->getLeftNav(),
            'fields' => [],
        ];
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     *
     * @return array
     *
     * @Route("/o", name="siia_indicadores_objeto", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function generatorObjectAction(Request $request)
    {
        $type = $request->get('object', '');

        switch ($type) {
            case 'diagnostico_tecnico':
            case 'perfil_socioeconomico':
            case 'caracterizacion_asociacion':
                $fields = $this->get('d_form.transaction')->getFormFields($type, true);
                break;
            default:
                throw new BadRequestHttpException("No se puede realizar la operacion " . $type);
        }

        return new Response($this->get('serializer')->serialize($fields, 'json'));
    }

    /**
     * @Route("/v", name="siia_indicadores_value", defaults={"_format"="json"})
     * @Method("GET")
     */
    public function getAllValuesAction(Request $request)
    {
        $query = $request->get('query', '');

        list($object, $field) = explode('.', $query);

        $slugs = $this->getSlugsByRol($object);

        $result = $this->get('d_form.indicators')->getFormFields($object, $field, $slugs);

        return new Response($this->get('serializer')->serialize($result, 'json'));
    }

    /**
     * @Route("/results", name="siia_indicadores_results", defaults={"_format"="json"})
     * @Method({"POST"})
     */
    public function generatorResultAction(Request $request)
    {
        $fields = [];

        $params = json_decode($request->getContent(), true);

        /* TODO: El uso del operador $in en mongo esta limitado a un array de 10.000 elementos
         *       encontrar una manera de diferente de filtrar según rol
         */
        $slugs = $this->getSlugsByRol($params['object']['value']);

        $results = $this->get('d_form.indicators')->aggregate($params, $slugs);

        $results = new GoogleChartJSAdapter($results, $params);

        return new Response($this->get('serializer')->serialize($results, 'json'));
    }

    public function getLeftFormNav()
    {

    }

    public function getLeftNav()
    {
        return [
            [
                'title' => 'general.menu.back',
                'icon' => 'fa-chevron-left',
                'route' => 'siia_panel'
            ],
        ];
    }

    public function getBaseRoute()
    {

    }

    public function getLeftShowNav()
    {

    }

    private function getSlugsByRol($object)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $asesores = [];

        if (!$this->getUser()->hasRole(BaseProfile::ADMIN) && !$this->getUser()->hasRole(BaseProfile::SUPERVISOR) ) {
            if ($this->getUser()->hasRole(BaseProfile::OPERADOR)) {
                $operador = $this->getUser()->getOperador()->getId();
                $asesores = $em->getRepository(Asesor::CLASS)->getAllAsesorByOperador($operador);
            } else if ($this->getUser()->hasRole(BaseProfile::ASESOR)) {
                $asesores[] = [
                    'id' => $this->getUser()->getAsesor()->getId()
                ];
            }
        }

        foreach ($asesores as $key => $value) {
            $asesores[$key] = $value['id'];
        }

        switch ($object) {
            case 'perfil_socioeconomico':
                $r = $em->getRepository(PerfilSocioEconomico::CLASS);
                break;
            case 'caracterizacion_asociacion':
                $r = $em->getRepository(Asociacion::CLASS);
                break;
            case 'diagnostico_tecnico':
                $r = $em->getRepository(DiagnosticoTecnico::CLASS);
                break;
            default:
                return [];
        }
        if($this->getUser()->hasRole(BaseProfile::ASOCIACION)){
            /* @var AsociacionProfile $usuarioAsociacion */
            $usuarioAsociacion = $this->getUser()->getAsociacion();
            $slugs = $r->findAllSlugsByAsociacion($usuarioAsociacion->getAsociacion()->getId());
        }else{
            $slugs = $r->findAllSlugsByAsesor($asesores);
        }

        return $slugs;
    }
}
