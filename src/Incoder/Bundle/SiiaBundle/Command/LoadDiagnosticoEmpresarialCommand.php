<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Opcion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\TableHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDiagnosticoEmpresarialCommand extends ContainerAwareCommand
{
    const file = 'file';
    const delimiter = 'delimiter';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransformedFinder
     */
    private $finder;

    /**
     * @var array
     */
    private $rows = [];

    /**
     * @var array
     */
    private $opciones = [];

    /** @var  Afirmacion[] */
    private $afirmaciones = [];

    protected function configure()
    {
        $this
            ->setName('load:diagnostico:empresarial')
            ->setDescription("Carga diagnostico empresariales")
            ->addArgument(self::file, InputArgument::REQUIRED, "Ruta del archivo a cargar")
            ->addArgument(self::delimiter, InputArgument::OPTIONAL, "Delimitador de columnas", ",")
            ;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->finder = $this->getContainer()->get('fos_elastica.finder.search.asociacion');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument(self::file);

        $output->write("Leyendo archivo {$file}.. ");

        if (!file_exists($file)) {
            $output->writeln("<error>No existe</error>");
            return;
        }

        if (false === ($handler = fopen($file, 'r'))) {
            $output->writeln("<error>No se pudo abrir</error>");
            return;
        }

        $output->writeln("<info>OK</info>");

        // Extrae el encabezado
        fgetcsv($handler, 4096, $input->getArgument(self::delimiter));

        while (false !== ($row = fgetcsv($handler, 4096, $input->getArgument(self::delimiter)))) {
            $this->processRow($row, $output);
        }

        fclose($handler);

        $this->em->flush();

        /** @var TableHelper $table */
        $table = $this->getHelperSet()->get('table');
        $table->setHeaders([
            'Asociacion', 'Encontrada', 'Tiene DG Empresarial', 'Cargado'
        ]);
        $table->addRows($this->rows);
        $table->render($output);
    }

    private function processRow($row, OutputInterface $output)
    {
        $yes = "<info>[YES]</info>";
        $no = "<error>[NO ]</error>";

        $asociacion = $this->finder->find($row[0]);
        $table_row = [$row[0]];

        if (count($asociacion) == 0) {
            // No encontrada
            $table_row[] = $no;
            // Sin DG Empresarial
            $table_row[] = $no;

            // Cargado
            $table_row[] = $no;
            return;
        }

        /** @var Asociacion $asociacion */
        $asociacion = $asociacion[0];
        // Encontrada
        $table_row[] = $yes;

        // Tiene DG Empresarial?
        if ($asociacion->getDiagnosticoEmpresarial()->count() == 0) {
            $table_row[] = $no;
            $table_row[] = $yes;

            $dg = new DiagnosticoEmpresarial();
            $dg->setAsociacion($asociacion);
            $dg->setFechaDiagnostico(new \DateTime());

            for ($i = 1; $i < count($row); $i++) {
                $r = new DiagnosticoEmpresarialRespuesta();
                $r->setDiagnostico($dg);
                $r->setOpcion($this->getOptionValue($row[1]));
                $r->setAfirmacion($this->getAfirmacion($i));
                $this->em->persist($r);
            }
            $this->em->persist($dg);

        } else {
            $table_row[] = $yes;
            $table_row[] = $no;
        }

        $this->rows[] = $table_row;
    }

    private function getOptionValue($valor)
    {
        if (!isset($this->opciones[$valor])) {
            $opcion = $this->em->getRepository(Opcion::CLASS)->createQueryBuilder('o')
                ->where('o.name LIKE :OPTION')
                ->setParameter('OPTION', "%{$valor}%")
                ->getQuery()->getOneOrNullResult();

            if (!$opcion) {
                throw new \Exception("No fue encontrada la opcion '{$valor}'");
            }

            $this->opciones[$valor] = $opcion;
        }

        return $this->opciones[$valor];
    }

    private function getAfirmacion($i)
    {
        if (count($this->afirmaciones) == 0) {
            $this->afirmaciones = $this->em->getRepository(Afirmacion::CLASS)->findAll();
        }

        return $this->afirmaciones[$i- 1];
    }

}