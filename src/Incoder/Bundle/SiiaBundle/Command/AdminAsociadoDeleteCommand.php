<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Incoder\Bundle\SiiaBundle\Entity\Asociado;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * @property EntityManager em
 * @property TransformedFinder elastica
 * @property \Symfony\Component\Console\Output\OutputInterface $output
 * @property \Symfony\Component\Console\Input\InputInterface input
 */
class AdminAsociadoDeleteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName("admin:asociado:delete")
            ->addArgument("name", InputArgument::REQUIRED, "Dato del asociado")
        ;
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->elastica = $this->getContainer()->get('fos_elastica.finder.listing.asociado');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();

        $this->input = $input;
        $this->output = $output;

        /** @var QuestionHelper $dialog */
        $dialog = $this->getHelperSet()->get('question');

        $asociados = $this->searchAsociado($input->getArgument("name"));

        $asociado_ids = array_map(function (Asociado $a) { return $a->getId();}, $asociados);

        if (count($asociados)) {
            $question = new Question("Indique el ID del que desee eliminar: ");
            $question->setAutocompleterValues($asociado_ids);
            $id = $dialog->ask($input, $output, $question);

            if ($id) {
                $this->deleteAsociado($id, $dialog);
            }
        }
    }

    public function searchAsociado($name)
    {
        $asociados = $this->elastica->find($name);

        /** @var Table $table */
        $table = $this->getHelperSet()->get('table');

        $table->setHeaders([
            "id", "Nombre", "Documento"
        ]);

        $rows = array_map(function (Asociado $asociado) {
            return [
                $asociado->getId(),
                "{$asociado}",
                $asociado->getDocumento()
            ];
        }, $asociados);

        $table->addRows($rows);
        $table->render($this->output);

        return $asociados;
    }

    private function deleteAsociado($id, QuestionHelper $helper)
    {
        $asociado = $this->em->getRepository(Asociado::CLASS)->find($id);

        if ($asociado) {
            $message = sprintf(
                "Desea eliminar a [<info>%s</info>] <comment>%s</comment> ? ",
                $asociado->getId(),
                $asociado
            );

            $question = new ChoiceQuestion($message, array("Yes", "No"), "No");

            if ("Yes" == $helper->ask($this->input, $this->output, $question)) {
                $this->em->remove($asociado);
                $this->em->flush();
            }
        }
    }


}