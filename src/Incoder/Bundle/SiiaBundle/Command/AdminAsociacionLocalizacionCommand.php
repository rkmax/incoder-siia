<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 4/21/14
 * Time: 10:23 PM
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Incoder\Bundle\SiiaBundle\DSL\Caracterizacion\Factory;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico;
use Incoder\Bundle\SiiaBundle\Entity\Localizacion;
use Incoder\Bundle\SiiaBundle\Entity\PerfilSocioEconomico;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AdminAsociacionLocalizacionCommand extends ContainerAwareCommand
{

    const ID = 'id';
    const PERFIL = 'perfil';
    const DIAGNOSTICO = 'dg-tecnico';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransactionService
     */
    private $ts;

    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var TransformedFinder
     */
    private $loc;

    /**
     * @var OutputInterface
     */
    private $output;

    protected function configure()
    {
        $this
            ->setName('admin:asociacion:localizacion')
            ->setDescription("Actualiza Departamento, Municipio, Centro poblado segun la localizacion de la asociacion")
            ->addArgument(self::ID, InputArgument::OPTIONAL, "El id de la asociacion a afectar")
            ->addOption(self::DIAGNOSTICO, 'd', InputOption::VALUE_NONE, "Si desea ejecutar el comando sobre diagnósticos tecnicos", null)
            ->addOption(self::PERFIL, 'p', InputOption::VALUE_NONE, "Si desea ejecutar el comando sobre perfiles socioeconomicos", null);
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->loc = $this->getContainer()->get('fos_elastica.finder.search_localizacion.localizacion');
    }

    /**
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();
        $this->output = $output;

        $id = $input->getArgument(self::ID);

        if ($input->getOption(self::DIAGNOSTICO)) {
            $output->writeln("Actualizando " . DiagnosticoTecnico::CLASS);
            $this->dgtecnico($id);
        } else if ($input->getOption(self::PERFIL)) {
            $output->writeln("Actualizando " . PerfilSocioEconomico::CLASS);
            $this->perfil($id);
        } else {
            $output->writeln("Actualizando " . Asociacion::CLASS);
            $this->caracterizacion($id);
        }

        $this->em->flush();
    }

    private function caracterizacion($id)
    {
        $this->updateLocation(Asociacion::CLASS, 'localizacion_asociacion', $id);
    }

    private function perfil($id)
    {
        $this->updateLocation(PerfilSocioEconomico::CLASS, 'ciudad_natal', $id);
    }

    private function dgtecnico($id)
    {
        $this->updateLocation(DiagnosticoTecnico::CLASS, 'localizacion_distrito', $id);
    }

    private function updateLocation($entityClass, $properyLocation, $id = null)
    {
        $r = $this->em->getRepository($entityClass);

        $entities = (!$id ? $r->findAll() : $r->findBy(['id' => $id]));

        foreach( $entities as $entity ) {

            if ($entityClass == Asociacion::CLASS) {
                $this->updateLocationCaracterizacion($entity);
            } else {
                $localizacion = $entity->$properyLocation;

                if (!$localizacion) {
                    $this->output->writeln(
                        "{$entityClass}({$entity->getId()}) - <error>No tiene localizacion </error>"
                    );
                    continue;
                }

                /** @var Localizacion $oloc */
                $results = $this->loc->find(substr($localizacion, 0, 255));
                if (count($results) > 0) {
                    $oloc = $results[0];
                    $entity->departamento = $oloc->getNombreDepartamento();
                    $entity->municipio = $oloc->getNombreMunicipio();
                    $entity->centro_poblado = $oloc->getNombreCentroPoblado();
                    $this->ts->persistEntity($entity);
                } else {
                    $this->output->writeln(
                        "{$entityClass}({$entity->getId()}) - <error>No se encuentra localizacion </error>"
                    );
                }
            }

        }
    }

    private function updateLocationCaracterizacion(Asociacion $asociacion)
    {
        $municipio = $asociacion->getMunicipio();
        $departame = $asociacion->getDepartamento();
        $centroPob = $asociacion->getCentroPoblado();
        $localizac = $asociacion->localizacion_asociacion;

        if ($municipio && $departame) {
            $query = Factory::localizacion($departame, $municipio, $centroPob);
            $localizacion = $this->loc->find($query, 1);

            if (count($localizacion) == 0) {
                // Muestra que se ignoro esta asociacion
                return;
            } else {
                /** @var Localizacion $localizacion */
                $localizacion = $localizacion[0];
                $data = [
                    'departamento' => $localizacion->getNombreDepartamento(),
                    'municipio' => $localizacion->getNombreMunicipio(),
                    'centro_poblado' => $localizacion->getNombreCentroPoblado()
                ];
                $asociacion->setData(array_merge($asociacion->getData(), $data));
                $this->ts->persistEntity($asociacion);
                $this->em->persist($asociacion);
            }
        }
    }
}