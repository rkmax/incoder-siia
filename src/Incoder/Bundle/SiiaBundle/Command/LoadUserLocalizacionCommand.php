<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ORM\EntityManager;
use Elastica\Exception\ResponseException;
use Elastica\Query\Bool;
use Elastica\Query\Match;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Incoder\Bundle\SiiaBundle\DSL\Caracterizacion\Factory;
use Incoder\Bundle\SiiaBundle\Entity\Localizacion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Exception\ContextErrorException;

class LoadUserLocalizacionCommand extends ContainerAwareCommand
{
    const file = 'file';
    const delimiter = 'delimiter';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransformedFinder
     */
    private $lc;

    private $fields = [
        'CodigoDepartamento' => null,
        'NombreDepartamento' => 0,
        'CodigoMunicipio' => null,
        'NombreMunicipio' => 1,
        'CodigoCentroPoblado' => null,
        'NombreCentroPoblado' => 2,
        'CodigoTipo' => null,
        'NombreTipo' => null,
    ];

    protected function configure()
    {
        $this
            ->setName("load:user:localizacion")
            ->setDescription("Carga las localizaciones de los archivos enviando por el usuario")
            ->addArgument(self::file, InputArgument::REQUIRED, "Ruta del archivo a cargar")
            ->addOption(self::delimiter, 'd', InputOption::VALUE_REQUIRED, "Delimitador del archivo", "\t")
        ;
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->lc = $this->getContainer()->get('fos_elastica.finder.search_localizacion.localizacion');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();
        $file = $input->getArgument(self::file);
        $delimiter = $input->getOption(self::delimiter);

        if (!$file || !file_exists($file)) {
            $output->writeln("<error>Archivo {$file} no existe</error>");
            return;
        }

        $output->writeln("<info>Cargando</info> localizaciones desde {$file}");

        $handle = fopen($file, 'r');

        // Header
        fgetcsv($handle, 4096, $delimiter);

        while (false !== ($row = fgetcsv($handle, 4096, $delimiter))) {
            $this->processRow($row, $output);
        }
        fclose($handle);

        $this->em->flush();
    }

    private function processRow($row, OutputInterface $output)
    {
        $data = [];

        foreach ($this->fields as $field => $index) {
            if (is_null($index)) {
                continue;
            }
            try {
                $data[$field] = $row[$index];
            } catch (ContextErrorException $e) {
                $output->writeln("<error>Data</error> " . json_encode($data));
                throw $e;
            }

        }

        $municipio = $data['NombreMunicipio'];
        $departamento = $data['NombreDepartamento'];
        $centroPoblado = $data['NombreCentroPoblado'];

        $query = Factory::localizacion(
            $departamento,
            $municipio,
            $centroPoblado
        );

        $location = $this->lc->find($query, 1);
        $search_loc = implode(", ", $data);

        try {
            $location = $this->lc->find($query, 1);
        } catch (ResponseException $e) {
            $output->writeln("<error>Data</error> {$search_loc}");
            throw $e;
        }

        // No existe se crea
        if (count($location) == 0) {
            $query = Factory::localizacion(
                $departamento,
                $municipio
            );

            // Cualquier ubicacion sirve
            $anyLocation = $this->lc->find($query, 1);

            if (count($anyLocation) == 0) {
                $output->writeln("<error>No se encontro municipio</error> {$search_loc}");
            } else {
                $anyLocation = reset($anyLocation);

                $newLoc = $this->creafromAny($anyLocation, $data['NombreCentroPoblado']);
                $this->em->persist($newLoc);
                $output->writeln("<info>Creada</info> {$newLoc}");
            }
        } else {
            $location = reset($location);
            $output->writeln("<comment>Ya existe</comment> {$search_loc} -> <info>{$location->getId()}</info> {$location->getLongDescripcion()}");
        }
    }

    public function creafromAny(Localizacion $any, $nombreCentroPoblado)
    {
        $localizacion = new Localizacion();
        $rclass = new \ReflectionClass($localizacion);

        foreach ($rclass->getProperties() as $prop) {
            // No queremos copiar el id
            if ($prop == 'id') {
                continue;
            }

            $prop->setAccessible(true);
            $prop->setValue($localizacion, $prop->getValue($any));
        }

        $localizacion->setCodigoTipo('CP');
        $localizacion->setNombreTipo("Centro Poblado no categorizado");
        $localizacion->setNombreCentroPoblado($nombreCentroPoblado);
        // Codigo temporal sirve para identificar los NO DANE
        $localizacion->setCodigoCentroPoblado($any->getCodigoMunicipio() . "XXX");

        return $localizacion;
    }
}