<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ORM\EntityManager;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Componente;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadMatrizDofaCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    private $dofa = array();

    private $componente = array();

    protected function configure()
    {
        $this
            ->setName('matriz:load')
            ->setDescription("Carga los datos de la dofa")
            ->addArgument('filepath', InputArgument::REQUIRED, "Ruta del archivo a cargar")
            ->addOption('header', null, InputOption::VALUE_NONE, "Definie si el archivo tiene")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager');

        $filepath = $input->getArgument('filepath');
        $hasHeader = $input->getOption('header');

        ini_set('auto_detect_line_endings', true);

        if (!file_exists($filepath)) {
            $output->writeln("<error>El archivo '{$filepath}' no exise</error>");
            return;
        }

        $handle = fopen($filepath, "r");

        // Extract the header
        if ($hasHeader) {
            fgetcsv($handle, 2048, ',', '"');
        }

        while ($data = fgetcsv($handle, 2048, ',', '"')) {
            $componente = $data[0];
            $debilidad = $data[1];
            $actividad = $data[2];
            $fortaleza = explode("\n", $data[3]);
            $amenaza = explode("\n", $data[4]);
            $oportunidad = explode("\n", $data[5]);

            $d = $this->getDOFA($debilidad, 'Debilidad');
            $s = $this->getDOFA($actividad, 'Actividad');
            $c = $this->getComponente($componente);

            $aa = new DOFAActividad();
            $aa->setComponente($c);
            $aa->setActividad($s);
            $aa->addDebilidad($d);

            foreach ($fortaleza as $fortaleza_) {
                if ($fortaleza_) $aa->addFortaleza($this->getDOFA($fortaleza_, 'Fortaleza'));
            }

            foreach ($amenaza as $amenaza_) {
                if ($amenaza_) $aa->addAmenaza($this->getDOFA($amenaza_, 'Amenaza'));
            }

            foreach ($oportunidad as $oportunidad_) {
                if ($oportunidad_) $aa->addOportunidad($this->getDOFA($oportunidad_, 'Oportunidad'));
            }

            $this->em->persist($aa);
        }

        $output->writeln("Guardando...");

        $this->em->flush();
    }

    /**
     * @param $descripcion
     * @param $tipo
     *
     * @return mixed|DOFAActividad
     */
    private function getDOFA($descripcion, $tipo)
    {
        $descripcion = ltrim(trim($descripcion), "- ");

        if (empty($descripcion)) {
            return null;
        }

        $key = sha1(strtolower("{$tipo}{$descripcion}"));

        if (array_key_exists($key, $this->dofa)) {
            return $this->dofa[$key];
        }

        $property = 'descripcion';

        $dofa = $this->em->getRepository('SiiaBundle:PlanAccion\\' . $tipo)->findOneBy([
            $property => $descripcion
        ]);

        $class = 'Incoder\Bundle\SiiaBundle\Entity\PlanAccion\\' . $tipo;
        $method = "set" . ucfirst($property);

        if (!$dofa) {
            $dofa = new $class();
            $dofa->$method($descripcion);
            $this->em->persist($dofa);
        }

        $this->dofa[$key] = $dofa;

        return $this->dofa[$key];
    }

    private function getComponente($nombre)
    {
        if (array_key_exists($nombre, $this->componente)) {
            return $this->componente[$nombre];
        }

        $componente = $this->em->getRepository('SiiaBundle:PlanAccion\Componente')->findOneBy([
            'nombre' => $nombre
        ]);

        if (!$componente) {
            $componente = new Componente();
            $componente->setNombre($nombre);

            $this->em->persist($componente);
        }

        $this->componente[$nombre] = $componente;

        return $componente;
    }
} 
