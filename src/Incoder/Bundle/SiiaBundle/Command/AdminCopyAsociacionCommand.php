<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Incoder\Bundle\SiiaBundle\Entity\Asociado;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdminCopyAsociacionCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransactionService
     */
    private $ts;

    /**
     * @var DocumentManager
     */
    private $dm;

    protected function configure()
    {
        $this
            ->setName('admin:copy:asociacion')
            ->setDescription("Copia datos de la asociacion a otras entidades");
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * @SuppressWarnings("unused")
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();
        $name = "   <info>admin</info>";

        $asociaciones = $this->em->getRepository('SiiaBundle:Asociacion')->findAll();

        foreach ($asociaciones as $asociacion) {
            $data = [
                'nombre_asociacion' => $asociacion->nombre_asociacion,
                'siglas_asociacion' => $asociacion->siglas_asociacion,
                'localizacion_asociacion' => $asociacion->localizacion_asociacion
            ];

            /** @var DiagnosticoTecnico $dt */
            foreach ($asociacion->getDiagnosticoTecnico() as $dt) {
                $dt->setData(array_merge($dt->getData(), $data));
                $this->ts->persistEntity($dt);
            }

            /** @var Asociado $a */
            foreach ($asociacion->getAsociado() as $a) {

                if ($p = $a->getPerfil()->first()) {
                    $p->setData(array_merge($p->getData(), $data));
                    $this->ts->persistEntity($p);
                }
            }
        }

        $this->em->flush();
    }
}