<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 4/21/14
 * Time: 10:23 PM
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdminAsociacionSiglasCommand extends ContainerAwareCommand
{

    const ASO_ID = 'id';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransactionService
     */
    private $ts;

    /**
     * @var DocumentManager
     */
    private $dm;

    protected function configure()
    {
        $this
            ->setName('admin:asociacion:siglas')
            ->setDescription("Comando diseñado para uso en una ocación ya que los datos del nombre corto de las asociaciones se encuentra en el campo nombre en los datos de producción actuales y se necesita en el campo sigla")
            ->addArgument(self::ASO_ID, InputArgument::OPTIONAL, "El id de la asociacion a afectar");
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();
        $name = "   <info>admin</info>";

        $id = $input->getArgument(self::ASO_ID);
        $r = $this->em->getRepository('SiiaBundle:Asociacion');

        $asociaciones = (!$id ? $r->findAll() : $r->findBy(['id' => $id]));

        foreach( $asociaciones as $asociacion ){
            $siglas = $asociacion->siglas_asociacion;
            if(!$siglas || empty($siglas)){
                $matches = null;
                $nombre = $asociacion->getNombre();
                if (str_word_count($nombre, 0) == 1) {
                    $siglas = mb_strtoupper($nombre, 'UTF-8');
                } else if (preg_match('/[A-Z\-]+/', $nombre, $matches)) {

                    if (strlen($matches[0]) > 3) {
                        $siglas = $matches[0];
                    } else {
                        $siglas = $nombre;
                    }
                }
                $asociacion->siglas_asociacion = $siglas;
                $asociacion->updateVersion();
                $this->em->persist($asociacion);
                $output->writeln("{$name}: <info>Asociación {$asociacion->getSigla()}({$asociacion->getId()}) afectada</info>");
            }
        }
        $this->em->flush();
        $output->writeln("{$name}: <info>Completado</info>");

        return;

    }
} 