<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ORM\PersistentCollection;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AsignAsociacionCommand extends ContainerAwareCommand
{
    const ASESOR = 'arg_asesor';

    protected function configure()
    {
        $this
            ->setName('assign:asociacion')
            ->setDescription("Asignacion las asociaciones a algun asesor")
            ->addArgument(self::ASESOR, InputArgument::REQUIRED, "Nombre del asesor", null)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     *
     * @SuppressWarnings("unused")
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asesorName = $input->getArgument(self::ASESOR);

        if (!$asesorName) {
            $output->writeln("<error>El nombre del asesor no puede estar vacio</error>");
            return;
        }

        $em = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager');

        $asesor = $em->getRepository('SiiaBundle:Asesor')->findOneBy(['nombre' => $asesorName]);

        if (!$asesor) {
            $output->writeln("<error>El asesor '{$asesorName}' no pudo ser encontrado</error>");
            return;
        }

        /** @var PersistentCollection $asociaciones */
        $asociaciones = $em->getRepository('SiiaBundle:Asociacion')->findAll();

        /** @var Asociacion $asociacion */
        foreach ($asociaciones as $asociacion) {
            $asesor->addAsociacion($asociacion);
        }

        $em->persist($asesor);
        $em->flush();

        $totalAsociaciones = count($asociaciones);

        $output->writeln("Se relacion el asesor '{$asesorName}' a {$totalAsociaciones}");
    }
} 
