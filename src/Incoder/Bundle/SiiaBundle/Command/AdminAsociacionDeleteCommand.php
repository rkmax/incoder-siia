<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AdminAsociacionDeleteCommand extends ContainerAwareCommand
{
    const ASO_ID = 'asociacion_id';
    const NOMBRE = 'nombre';
    const DEPARTAMENTO = 'departamento';
    const MUNICIPIO = 'municipio';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransactionService
     */
    private $ts;

    /**
     * @var DocumentManager
     */
    private $dm;

    protected function configure()
    {
        $this
            ->setName('admin:asociacion:delete')
            ->setDescription("Elimina una asociacion")
            ->addArgument(self::ASO_ID, InputArgument::OPTIONAL, "El id de la asociacion a eliminar")
            ->addOption(self::NOMBRE, '-nm', InputOption::VALUE_OPTIONAL, 'Nombre o sigla de la asociación')
            ->addOption(self::DEPARTAMENTO, '-d', InputOption::VALUE_OPTIONAL, 'Departamento donde opera la asociación')
            ->addOption(self::MUNICIPIO, '-m', InputOption::VALUE_OPTIONAL, 'Municipio donde opera la asociación');
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * @SuppressWarnings("unused")
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();
        $name = "   <info>admin</info>";

        //Obteniendo argumentos
        $id = $input->getArgument(self::ASO_ID);
        $nombre = $input->getOption(self::NOMBRE);
        $departamento = $input->getOption(self::DEPARTAMENTO);
        $municipio = $input->getOption(self::MUNICIPIO);

        $asociacion = null;
        if($id){ //busca la asociacion por id
            $asociacion = $this->em->getRepository('SiiaBundle:Asociacion')->find($id);
        }
        if($nombre&&($departamento||$municipio) && !$asociacion){ //Busca la asociacion por nombre y localizacion
            $asociacion = $this->em->getRepository('SiiaBundle:Asociacion')
                ->findOneByNombreLocalizacion($nombre, $nombre, $departamento, $municipio);
        }

        //Sino encontramos la asociacion terminamos comando
        if (!$asociacion) {
            $output->writeln("{$name}: <error>La asociacion indicada ({$id}) no existe</error>");
            return;
        }

        //delete in em so he cascade takes effect
        $this->em->remove($asociacion);
        $this->em->flush();

        $output->writeln("asociacion borrada");
    }
} 
