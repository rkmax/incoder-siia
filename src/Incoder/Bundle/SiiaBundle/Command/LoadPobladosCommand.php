<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ORM\EntityManager;
use Incoder\Bundle\SiiaBundle\Entity\Localizacion;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadPobladosCommand extends ContainerAwareCommand
{
    const NORMAL = 'NORMAL';
    const UPPER = 'UPPER';
    const LOWER = 'LOWER';
    const CAPITALIZE = 'CPIZE';
    const CAPITALIZE_W = 'CPIZEW';
    /**
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('load:poblados')
            ->setDescription("Carga los centros poblados segun el formato")
            ->addArgument('filepath', InputArgument::REQUIRED, "Ruta del archivo a cargar")
            ->addOption('header', null, InputOption::VALUE_NONE, "Definie si el archivo tiene")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager');

        $filepath = $input->getArgument('filepath');
        $hasHeader = $input->getOption('header');

        ini_set('auto_detect_line_endings', true);

        if (!file_exists($filepath)) {
            $output->writeln("<error>El archivo '{$filepath}' no exise</error>");
            return;
        }

        $handle = fopen($filepath, "r");

        // Extract the header
        if ($hasHeader) {
            fgetcsv($handle, 2048, ',', '"');
        }

        $fields = [
            'CodigoDepartamento' => 0,
            'NombreDepartamento' => 3,
            'CodigoMunicipio' => 1,
            'NombreMunicipio' => 4,
            'CodigoCentroPoblado' => 2,
            'NombreCentroPoblado' => 5,
            'CodigoTipo' => 6,
            'NombreTipo' => 7,
        ];

        while ($data = fgetcsv($handle, 2048, ',', '"')) {
            $localizacion = new Localizacion();
            foreach ($fields as $field => $index) {
                $setter = "set{$field}";
                call_user_func([$localizacion, $setter], $this->getValue($data, $index));
            }
            $this->em->persist($localizacion);
        }

        $output->writeln("Guardando...");

        $this->em->flush();
    }

    private function getValue($data, $index, $mode = self::NORMAL, $encoding = "UTF-8")
    {
        if (!array_key_exists($index, $data)) {
            throw new \Exception("El indice {$index} no existe en data");
        }

        $value = trim($data[$index]);

        switch ($mode) {
            case self::CAPITALIZE:
                return $this->mb_ucfirst($value, $encoding);
            case self::LOWER:
                return mb_strtolower($value, $encoding);
            case self::UPPER:
                return mb_strtoupper($value, $encoding);
            case self::CAPITALIZE_W:
                return mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
            case self::NORMAL:
            default:
                return $value;
        }
    }

    function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);

        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }
} 
