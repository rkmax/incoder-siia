<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 5/29/14
 * Time: 8:38 AM
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdminSyncAsociadoCommand extends ContainerAwareCommand{
    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Reader
     */
    private $reader;

    protected function configure()
    {
        $this
            ->setName('admin:sync:asociado')
            ->setDescription("Actualiza los asociados en mysql con la data de mongo. Actualmente solo lo hace para el registro catastral")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     *
     * @SuppressWarnings("unused")
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* SETUP */
        $this->em = $this
            ->getContainer()
            ->get('doctrine.orm.entity_manager');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->reader = $this->getContainer()->get('annotation_reader');
        /* FINISH SETUP */

        $asociados = $this->em->getRepository('SiiaBundle:Asociado')->findAll();

        foreach($asociados as $asociado){
            $output->writeln(">>>Procesando asociado: <info> Nombre:{$asociado->getNombre()}  Documento:{$asociado->getDocumento()} Asociacion: {$asociado->getAsociacion()->getSigla()}</info>");
            try{
                if($this->syncData($this->reader, $asociado, $output)){
                    $this->em->persist($asociado);
                    $output->writeln(">>>Asociado procesado: <info> Nombre:{$asociado->getNombre()}  Documento:{$asociado->getDocumento()} Asociacion: {$asociado->getAsociacion()->getSigla()}</info>");
                }
            }catch (Exception $e){
                $output->writeln("Error al procesar asociado <error> {$asociado->getNombre()} MESG: {$e->getMessage()}</error>");
            }
        }

        $this->em->flush();
        $output->writeln('END');
    }

    /**
     * @param Reader $reader
     * @param DFormEntity $entity
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return bool whether Something change or not in the entity
     */
    protected function syncData(Reader $reader, $entity, OutputInterface $output){
        $changed = false;
        $annotationClass = 'RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue';
        $entityClass = ClassUtils::getRealClass($entity);
        $rclass = new \ReflectionClass($entityClass);

        foreach ($rclass->getProperties() as $rprop) {
            $annotation = $reader->getPropertyAnnotation($rprop, $annotationClass);
            if ($annotation && null !== ($colname = $annotation->getDformColumn())) {
                if (isset($entity->getData()[$colname]) && null !== ($value = $entity->getData()[$colname])) {
                    $rprop->setAccessible(true);
                    //$oldValue = $rprop->getValue();
                    $rprop->setValue($entity, $this->setValue($value, $annotation->getDataTrasformerClass()));
                    //$newValue = $rprop->getValue();
                    $changed = true;
                    //$output->writeln("OldValue: {$oldValue} Changed value: {$newValue}");
                }
            }
        }

        if ($changed) {
            $uow = $this->em->getUnitOfWork();
            $meta = $this->em->getClassMetadata(get_class($entity));
            $uow->recomputeSingleEntityChangeSet($meta, $entity);
        }

        return $changed;
    }

    private function setValue($value, $transformerClass)
    {
        if (!$transformerClass) {
            return $value;
        }

        $transformer = new $transformerClass;
        return $transformer->transform($value);
    }
}