<?php
    /**
     * The MIT License (MIT)
     * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy
     * of this software and associated documentation files (the “Software”), to deal
     * in the Software without restriction, including without limitation the rights
     * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
     * copies of the Software, and to permit persons to whom the Software is
     * furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in
     * all copies or substantial portions of the Software.
     *
     * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
     * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
     * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
     * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
     * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
     * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
     * THE SOFTWARE.
     */

    namespace Incoder\Bundle\SiiaBundle\Command;



    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\NonUniqueResultException;
    use Incoder\Bundle\SiiaBundle\Entity\Asociado;
    use Incoder\Bundle\SiiaBundle\Entity\CuadroAgricola;
    use Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario;
    use Incoder\Bundle\SiiaBundle\Entity\Productivo\AbstractProducto;
    use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoAgricola;
    use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoPecuario;
    use Incoder\Bundle\SiiaBundle\Entity\Productivo\UnidadProduccion;
    use Incoder\Bundle\SiiaBundle\Entity\Productivo\UnidadVenta;
    use RRS\Bundle\DFormBundle\Service\TransactionService;
    use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
    use Symfony\Component\Console\Input\InputArgument;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Output\OutputInterface;

    class LoadGarbageCommand extends ContainerAwareCommand
    {
        private $unidadesVenta = array();
        private $unidadesProduccion = array();
        private $productos = array();


        /**
         * @var TransactionService
         */
        private $ts;

        /**
         * @var EntityManager
         */
        private $em;

        private $asociadosCount = 0;

        /**
         * @var ArrayCollection
         */
        private $asociados;

        const AGRICOLA = 'AGRÍCOLA';
        const PECUARIO = 'PECUARIO';

        protected function configure()
        {
            $this
                ->setName('garbage:load')
                ->setDescription("Garbage")
                ->addArgument('filepath', InputArgument::REQUIRED, "Ruta del archivo a cargar")
                ->addOption('header', null, InputOption::VALUE_NONE, "Definie si el archivo tiene")
            ;
        }

        /**
         * @param InputInterface  $input
         * @param OutputInterface $output
         *
         * @throws \Exception
         * @return int|null|void
         *
         * @SuppressWarnings("unused")
         */
        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $this->em = $em = $this
                ->getContainer()
                ->get('doctrine.orm.entity_manager');

            $this->ts = $ts = $this->getContainer()->get('d_form.transaction');

            $filepath = $input->getArgument('filepath');
            $hasHeader = $input->getOption('header');

            if (!file_exists($filepath)) {
                $output->writeln("<error>El archivo '{$filepath}' no exise</error>");
                return;
            }

            $handle = fopen($filepath, "r");

            // Extract the header
            if ($hasHeader) {
                fgetcsv($handle, 2048, ',', '"');
            }

            while ($data = fgetcsv($handle, 2048, ',', '"')) {
                $tipo = $data[48];
                $nombre = $data[9];
                $variedad = $data[10];
                $undProduccion = $data[17];
                $undVenta = $data[19];
                $cantidadProduccion = $data[18];
                $cantidadVenta = $data[20];
                $nombreAsociado = $data[0];
                $apellidos = $data[1];
                $documento = $data[2];
                $asociacion = $data[3];

                $asociado = $this->getAsociado($documento, $nombreAsociado, $apellidos, $asociacion);

                if (!$asociado) {
                    continue;
                }

                list($producto, $cantidadProduccion, $cantidadVenta) = $this->getProducto($nombre, $variedad, $undProduccion, $undVenta, $tipo, $cantidadProduccion, $cantidadVenta);

                if ($tipo == self::AGRICOLA) {
                    $slug = $ts->getForm('diagnostico_productivo_cuadro_agricola')->getId();
                    $cuadro = new CuadroAgricola();
                    $cuadro->setSlug($slug);
                } else if ($tipo == self::PECUARIO) {
                    $slug = $ts->getForm('diagnostico_productivo_cuadro_pecuario')->getId();
                    $cuadro = new CuadroPecuario();
                    $cuadro->setSlug($slug);
                } else {
                    throw new \Exception("No está definido el tipo '{$tipo}'");
                }

                $cuadro->setAsociado($asociado);
                $cuadro->setProducto($producto);
                $cuadro->setVolumenProduccion($cantidadProduccion);
                $cuadro->setVolumenVendido($cantidadVenta);

                $em->persist($cuadro);
            }

            fclose($handle);

            $em->flush();
        }

        /**
         * @param $nombre
         * @param $variedad
         * @param $unidadProduccion
         * @param $unidadVenta
         * @param $tipo
         * @param $cantidadProduccion
         * @param $cantidadVenta
         *
         * @throws \Exception
         * @return mixed
         */
        private function getProducto($nombre, $variedad, $unidadProduccion, $unidadVenta, $tipo, $cantidadProduccion, $cantidadVenta)
        {
            $em = $this
                ->getContainer()
                ->get('doctrine.orm.entity_manager');

            $key = "{$tipo}{$nombre}{$variedad}";

            switch ($unidadProduccion) {
                case 'CAJA':
                    switch ($nombre) {
                        case 'MORA':
                            $cantidadProduccion *= 7.5;
                            $unidadProduccion = 'KILO';
                            break;
                        case 'AGUACATE':
                            $unidadProduccion = 'UNIDAD';
                            break;
                        case 'BREVAS':
                            $unidadProduccion = 'KILO';
                            break;
                        case 'CAFÉ':
                            $cantidadProduccion *= 120;
                            $unidadProduccion = 'KILO';
                            break;
                        case 'GRANADILLA':
                            $cantidadProduccion *= 30;
                            $unidadProduccion = 'UNIDAD';
                            break;
                        case 'PLATANO':
                            $cantidadProduccion *= 12.5;
                            $unidadProduccion = 'KILO';
                            break;
                    }
                    break;
                case 'UNIDAD':
                    if ($nombre = 'PASTO') {
                        $cantidadProduccion *= 1000;
                        $unidadProduccion = 'KILO';
                    }
                    break;
                case 'CANASTILLA':
                    if ($nombre == 'CAFÉ') {
                        $cantidadProduccion *= 120;
                        $unidadProduccion = 'KILO';
                    } else {
                        $cantidadProduccion *= 30;
                        $unidadProduccion = 'KILO';
                    }
                    break;
                case 'GUACAL':
                    $cantidadProduccion *= 18;
                    $unidadProduccion = 'UNIDAD';
                    break;
                case 'HOJA':
                    $unidadProduccion = 'KILO';
                    break;
                case 'RACIMO':
                    $cantidadProduccion *= 12.5;
                    $unidadProduccion = 'KILO';
                    break;
                case 'LIBRA':
                    $cantidadProduccion *= 0.5;
                    $unidadProduccion = 'KILO';
                    break;
                case 'ARROBA':
                    $cantidadProduccion *= 12.5;
                    $unidadProduccion = 'KILO';
                    break;
                case 'BULTO':
                    $cantidadProduccion *= 50;
                    $unidadProduccion = 'KILO';
                    break;
                case 'CARGA':
                    $cantidadProduccion *= 120;
                    $unidadProduccion = 'KILO';
                    break;
                case 'CANASTILLAS':
                    $cantidadProduccion *= 30;
                    $unidadProduccion = 'KILO';
                    break;
                case 'DOCENA':
                    $cantidadProduccion *= 12;
                    $unidadProduccion = 'UNIDAD';
                    break;
                case 'TALLO':
                    $unidadProduccion = 'UNIDAD';
                    break;
                case 'TONELADA':
                    $cantidadProduccion *= 1000;
                    $unidadProduccion = 'KILO';
                    break;
                case '':
                    throw new \Exception("La unidad no puede ser vacio en {$nombre}");

            }

            $cantidadVenta = $cantidadProduccion;
            $unidadVenta = $unidadProduccion;

            if (array_key_exists($key, $this->productos)) {
                return [$this->productos[$key], $cantidadProduccion, $cantidadVenta];
            }

            if ($tipo == self::AGRICOLA) {
                $r = $em->getRepository('SiiaBundle:Productivo\ProductoAgricola');
            } else {
                $r = $em->getRepository('SiiaBundle:Productivo\ProductoPecuario');
            }

            /** @var AbstractProducto $producto */

            $producto = $r
                ->createQueryBuilder('p')
                ->where('p.nombreProducto = :nombre')
                ->andWhere('p.variedadProducto = :variedad')
                ->setParameter('nombre', $nombre)
                ->setParameter('variedad', $variedad)
                ->getQuery()->getOneOrNullResult();

            if (!$producto) {
                $producto = $tipo == self::AGRICOLA ? new ProductoAgricola() : new ProductoPecuario();
                $producto->setNombreProducto(trim($nombre));
                $producto->setVariedadProducto(trim($variedad));
            }

            $producto->setUnidadProduccion($this->getUnidadProduccion($unidadProduccion));
            $producto->setUnidadVenta($this->getUnidadVenta($unidadVenta));

            $em->persist($producto);

            $this->productos[$key] = $producto;

            return [$producto, $cantidadProduccion, $cantidadVenta];
        }

        /**
         * @param $nombre
         *
         * @return UnidadVenta
         */
        private function getUnidadVenta($nombre)
        {
            $em = $this
                ->getContainer()
                ->get('doctrine.orm.entity_manager');

            if (array_key_exists($nombre, $this->unidadesVenta)) {
                return $this->unidadesVenta[$nombre];
            }

            $unidadVenta = $em
                ->getRepository('SiiaBundle:Productivo\UnidadVenta')
                ->findOneBy(['nombre' => $nombre]);

            if (!$unidadVenta) {
                $unidadVenta = new UnidadVenta();
                $unidadVenta->setNombre($nombre);
                $em->persist($unidadVenta);
            }

            $this->unidadesVenta[$nombre] = $unidadVenta;

            return $unidadVenta;
        }

        /**
         * @param $nombre
         *
         * @return UnidadProduccion
         */
        private function getUnidadProduccion($nombre)
        {
            $em = $this
                ->getContainer()
                ->get('doctrine.orm.entity_manager');

            if (array_key_exists($nombre, $this->unidadesProduccion)) {
                return $this->unidadesProduccion[$nombre];
            }

            $unidadProduccion = $em
                ->getRepository('SiiaBundle:Productivo\UnidadProduccion')
                ->findOneBy(['nombre' => $nombre]);

            if (!$unidadProduccion) {
                $unidadProduccion = new UnidadProduccion();
                $unidadProduccion->setNombre($nombre);
                $em->persist($unidadProduccion);
            }

            $this->unidadesProduccion[$nombre] = $unidadProduccion;

            return $unidadProduccion;
        }

        private function getAsociado($documento, $nombre, $apellidos, $asociacion)
        {
            if (empty($documento) || $documento == '') return null;

            $asociado = $this->getContainer()->get('doctrine.orm.entity_manager')
                ->getRepository('SiiaBundle:Asociado')
                ->createQueryBuilder('a')
                ->where('a.documento = :doc')
                ->andWhere('a.documento != :nulo')
                ->setParameter('doc', $documento)
                ->setParameter('nulo', null)
                ->getQuery()->getResult();

            $asociacion = $this->getAsociacion($asociacion);

            if (!$asociacion) {
                return null;
            }

            if (!$asociado) {
                $slug = $this->ts->getForm('registro_unico')->getId();
                $asociado = new Asociado();
                $asociado->setSlug($slug);
                $asociado->setNombre($nombre);
                $asociado->setApellidos($apellidos);
                $asociado->setAsociacion($asociacion);

                $this->em->persist($asociado);
            }

            return $asociado;
        }

        private function getAsociacion($nombre)
        {
            try {
                $asociacion = $this->getContainer()->get('doctrine.orm.entity_manager')
                    ->getRepository('SiiaBundle:Asociacion')
                    ->findOneBy([
                        'nombre' => $nombre
                    ]);

                if (!$asociacion) {
                    return null;
                }

                return $asociacion;
            } catch (NonUniqueResultException $e) {
                throw new \Exception("Imposible obtener una unica asociacion para '{$nombre}'");
            }
        }

    }
