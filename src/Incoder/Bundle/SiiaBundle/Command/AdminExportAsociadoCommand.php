<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 7/15/14
 * Time: 9:55 AM
 */

namespace Incoder\Bundle\SiiaBundle\Command;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Incoder\Bundle\SiiaBundle\DSL\Caracterizacion\Factory;
use Incoder\Bundle\SiiaBundle\Entity\Asociado;
use Incoder\Bundle\SiiaBundle\Entity\CuadroAgricola;
use Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario;
use RRS\Bundle\DFormBundle\Document\SimpleTransaction;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class AdminExportAsociadoCommand extends ContainerAwareCommand{

    /**
     * @var DocumentManager
     */
    private $dm;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TransactionService
     */
    private $ts;

    /**
     * @var TransformedFinder
     */
    private $elastica;

    protected function configure()
    {
        $this
            ->setName('admin:export:asociado')
            ->setDescription("Exporta un asociado con toda la información cargada")
            //->addArgument("user", InputArgument::REQUIRED, "user")
            //->addArgument("password", InputArgument::REQUIRED, "Pass")
            //->addArgument("DB", InputArgument::REQUIRED, "DB")
            ->addArgument("file", InputArgument::REQUIRED, "File")
            ->addArgument("data", InputArgument::REQUIRED, "Dato del asociado");
    }

    private function setUp()
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->ts = $this->getContainer()->get('d_form.transaction');
        $this->dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $this->elastica = $this->getContainer()->get('fos_elastica.finder.listing.asociado');
    }

    /**
     * @SuppressWarnings("unused")
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setUp();
        $asociados = $this->elastica->find(Factory::asociados($input->getArgument("data")));
        $content = "";
        /* @var Asociado $asociado */
        foreach($asociados as $asociado){
            $slugs = [];
            $dumps = [];
            //slugs y dumps del asociado
            $slugs[] = $asociado->getSlug();
            $dumps[$asociado->getSlug()] = "asociado --where=\"slug='{$asociado->getSlug()}'\"";
            if($asociado->getPerfil() != null && isset($asociado->getPerfil()[0])){
                $slugs[] = $asociado->getPerfil()[0]->getSlug();
                $dumps[$asociado->getPerfil()[0]->getSlug()] = "perfil_socioeconomico --where=\"slug='{$asociado->getPerfil()[0]->getSlug()}'\"";
            }
            //slugs y dumps de cuadros productivos
            $cuadros = $asociado->getCuadroAgricola();
            $cuadros = array_merge($cuadros->toArray(), $asociado->getCuadroPecuario()->toArray());
            foreach($cuadros as $cuadro){
                $slugs[] = $cuadro->getSlug();
                $dumps[$cuadro->getSlug()] = "cuadro_productivo --where=\"slug='{$cuadro->getSlug()}'\"";
                if($cuadro instanceof CuadroPecuario){
                    $dumps[$cuadro->getId()] = "cuadro_pecuario --where=\"id='{$cuadro->getId()}'\"";
                }
                if($cuadro instanceof CuadroAgricola){
                    $dumps[$cuadro->getId()] = "cuadro_agricola --where=\"id='{$cuadro->getId()}'\"";
                }
            }

            foreach($slugs as $slugkey => $slug){
                $content .= $this->getJsonFromSlug($slug). PHP_EOL;
            }

            //dump what's interesting
            foreach($dumps as $dumpkey => $dump){
                $exec_line = "mysqldump ". "-u{$this->em->getConnection()->getUsername()} ".
                    "-p{$this->em->getConnection()->getPassword()} ".
                    "{$this->em->getConnection()->getDatabase()} --no-create-info ". $dump ;
                    //" > ". $input->getArgument("file"). $dumpkey. " ". PHP_EOL;
                $content .= $exec_line. PHP_EOL;
                $content .= shell_exec($exec_line). PHP_EOL;
            }
        }

        $fs = new Filesystem();
        try {

            $fs->dumpFile( $input->getArgument("file"), $content);
        } catch (IOExceptionInterface $e) {

        }

    }

    protected function getJsonFromSlug($slug){
        $documentos = $this->dm->getDocumentCollection(
            SimpleTransaction::CLASS)->find(
                ['_id' => new \MongoId($slug)]);

        foreach($documentos as $value){
            return json_encode($value) ;
        }
        return '';
    }


} 