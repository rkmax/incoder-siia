<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\Doctrine;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\CostoProducto;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoProductivo;
use Incoder\Bundle\SiiaBundle\Form\AsociacionType;
use JulianReyes\Lib\ParseUtils;
use JulianReyes\Service\EntityService;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAsociacion implements FixtureInterface, ContainerAwareInterface {

    private $container;

    /**
     * @var EntityService $es
     */
    private $es;


    /**
     * @var TransactionService
     */
    private $ts;

    private $om;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        $this->om = $manager;
        $config = [
            'form' => 'Incoder\Bundle\SiiaBundle\Form\AsociacionType',
            'entity' => 'Incoder\Bundle\SiiaBundle\Entity\Asociacion',
            'base_route' => 'asociacion_caracterizacion'
        ];


        $this->ts = $ts = $this->container->get('d_form.transaction');
        $this->es = $es = $this->container->get('entity.service');
        $es->setConfiguracion(ParseUtils::array2Object($config));
        $this->faker = $faker = $this->container->get('davidbadura_faker.faker');

        try {
            $slug = $ts->getForm('caracterizacion_asociacion')->getId();
        } catch (\Exception $e) {
            echo 'en ' . __METHOD__ . PHP_EOL;
            throw $e;
        }


        $nombres = ['ASODESCANSO', 'ASOGRANO'];

        for ($i = 0; $i < 2; $i++) {
            $data = [
                'dform' => [
                    'slug' => $slug,
                    'data' => [
                        'nombre_asociacion' => $nombres[$i],
                        'municipio' => $faker->municipio,
                        'departamento' => $faker->departamento,
                        'telefono_contacto' => $faker->phoneNumber
                    ]
                ]
            ];
            list($asociacion, $form) = $es->create($data, true);
            if ($form->isValid()) {
                $this->createProductivo($asociacion);
            }
        }
        $this->om->flush();
    }

    private function createProductivo($asociacion)
    {
        try {
            $slug = $this->ts->getForm('diagnostico_productivo')->getId();
        } catch (\Exception $e) {
            echo 'en ' . __METHOD__ . PHP_EOL;
            throw $e;
        }
        $productivo = new DiagnosticoProductivo();
        $productivo->setSlug($slug);
        $productivo->setAsociacion($asociacion);

        $this->addProductos($productivo);
        $this->om->persist($productivo);
    }

    private function addProductos(DiagnosticoProductivo $productivo)
    {
        $slug = $this->ts->getForm('diagnostico_productivo_costos_producto')->getId();
        for ($i = 0; $i < rand(5, 12); $i++)
        {
            $pro = new CostoProducto();
            $pro->setSlug($slug);
            $pro->setData([
                'nombre_producto' => $this->faker->nombreProducto
            ]);
            $pro->setDiagnosticoProductivo($productivo);
            $productivo->addCostoProductos($pro);
            $this->om->persist($pro);
        }
    }
}
