<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/18/14
 * Time: 2:37 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;


use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use Doctrine\ORM\Mapping as ORM;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;

/**
 * Toma del caudal de estiaje
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dp_tecnico_caudales")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoTecnicoRepository")
 * DForm("diagnostico_tecnico_caudal_estiaje")
 */
class CaudalEstiaje
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     * @ORM\Column(name="caudal", type="decimal")
     * SyncValue("caudal_estiaje")
     */
    private $caudal;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha", type="date")
     * SyncValue("fecha_toma")
     */
    private $fecha;

    /**
     * @var DiagnosticoTecnico
     *
     * @ORM\ManyToOne(targetEntity="DiagnosticoTecnico", inversedBy="caudales")
     * @ORM\JoinColumn(name="dg_tecnico_id", referencedColumnName="id", nullable=false)
     */
    private $diagnosticoTecnico;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getCaudal()
    {
        return $this->caudal;
    }

    /**
     * @param float $caudal
     */
    public function setCaudal($caudal)
    {
        $this->caudal = $caudal;
    }

    /**
     * @param \DateTime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico
     */
    public function getDiagnosticoTecnico()
    {
        return $this->diagnosticoTecnico;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico $diagnosticoTecnico
     */
    public function setDiagnosticoTecnico($diagnosticoTecnico)
    {
        $this->diagnosticoTecnico = $diagnosticoTecnico;
    }

} 
