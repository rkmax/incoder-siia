<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;

/**
 * Planos del distrito
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dp_tecnico_plano")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoTecnicoRepository")
 * DForm("diagnostico_tecnico_plano")
 */
class Plano {
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    public function getId() {
        return $this->id;
    }
    
    /**
     * 
     * @var integer
     * @ORM\Column(name="numero", type="integer")
     * SyncValue("numero_plano")
     */
    private $numero;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="string", length=2044)
     * SyncValue("contenido")
     */
    private $contenido;
    
    /**
     * @var DiagnosticoTecnico
     *
     * @ORM\ManyToOne(targetEntity="DiagnosticoTecnico", inversedBy="planos")
     * @ORM\JoinColumn(name="dg_tecnico_id", referencedColumnName="id", nullable=false)
     */
    private $diagnosticoTecnico;
    
    public function getNumero() {
        return $this->numero;
    }

    public function getContenido() {
        return $this->contenido;
    }

    public function getDiagnosticoTecnico() {
        return $this->diagnosticoTecnico;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setContenido($contenido) {
        $this->contenido = $contenido;
    }

    public function setDiagnosticoTecnico(DiagnosticoTecnico $diagnosticoTecnico) {
        $this->diagnosticoTecnico = $diagnosticoTecnico;
    }

}
