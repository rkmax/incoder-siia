<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use FOS\UserBundle\Model\GroupInterface;

/**
 * @ORM\Entity
 * @ORM\Table("security_user")
 */
class SecurityUser extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="SecurityGroup")
     * @ORM\JoinTable(name="security_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\OneToOne(targetEntity="Asesor", mappedBy="securityUser")
     */
    protected $asesor;

    /**
     * @ORM\OneToOne(targetEntity="Operador", mappedBy="securityUser")
     */
    protected $operador;

    /**
     * @ORM\OneToOne(targetEntity="AsociacionProfile", mappedBy="securityUser")
     */
    protected $asociacion;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();

        parent::__construct();
    }

    /**
     * Add groups
     *
     * @param \FOS\UserBundle\Model\GroupInterface $groups
     *
     * @return SecurityUser
     */
    public function addGroup(GroupInterface $groups) {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups
     *
     * @param GroupInterface $groups
     *
     * @return $this|void
     */
    public function removeGroup(GroupInterface $groups) {
        $this->groups->removeElement($groups);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups() {
        return $this->groups;
    }


    /**
     * Set asesor
     *
     * @param Asesor $asesor
     *
     * @return SecurityUser
     */
    public function setAsesor(Asesor $asesor = null)
    {
        $this->asesor = $asesor;

        return $this;
    }

    /**
     * Get asesor
     *
     * @return Asesor
     */
    public function getAsesor()
    {
        return $this->asesor;
    }

    /**
     * Set operador
     *
     * @param Operador $operador
     *
     * @return SecurityUser
     */
    public function setOperador(Operador $operador = null)
    {
        $this->operador = $operador;

        return $this;
    }

    /**
     * Get operador
     *
     * @return Operador
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
     * @return AsociacionProfile
     */
    public function getAsociacion()
    {
        return $this->asociacion;
    }

    /**
     * @param AsociacionProfile $asociacion
     */
    public function setAsociacion($asociacion)
    {
        $this->asociacion = $asociacion;
    }
}