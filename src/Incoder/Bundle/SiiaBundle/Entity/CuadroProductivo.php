<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/28/14
 * Time: 11:58 AM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\AbstractProducto;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CuadroProductivo
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Entity
 * @ORM\Table("cuadro_productivo")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="tipo", type="string")
 * @ORM\DiscriminatorMap({"cuadro_agricola" = "CuadroAgricola", "cuadro_pecuario" = "CuadroPecuario"})
 */
abstract class CuadroProductivo extends DFormEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Insumo", cascade={"persist", "remove"}, mappedBy="cuadroProductivo")
     */
    protected $insumos;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $insumos
     */
    public function setInsumos($insumos)
    {
        $this->insumos = $insumos;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInsumos()
    {
        return $this->insumos;
    }

    /**
     * @param Insumo $insumo
     */
    public function addInsumo(Insumo $insumo){
        if(!$this->insumos){
            $this->insumos = new ArrayCollection();
        }
        $insumo->setCuadroProductivo($this);
        $this->insumos[] = $insumo;
    }

    /**
     * @param Insumo $insumo
     */
    public function removeInsumo(Insumo $insumo) {
        $this->insumos->removeElement($insumo);
    }

    public function getVolumenProduccion()
    {
        return $this->volumenProduccion;
    }

    public function getVolumenVendido()
    {
        return $this->volumenVendido;
    }

} 
