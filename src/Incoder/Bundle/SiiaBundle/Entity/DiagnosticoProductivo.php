<?php

/**
 *
 * @author  Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * Date: 10/01/14
 * Time: 05:37 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Class DiagnosticoProductivo
 *
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dg_productivo")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoProductivoRepository")
 * @DForm("diagnostico_productivo")
 */
class DiagnosticoProductivo extends DFormEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Asociacion
     * @ORM\ManyToOne(targetEntity="Asociacion", cascade={"remove"}, inversedBy="diagnosticoProductivo")
     * @ORM\JoinColumn(name="asociacion_id", referencedColumnName="id", nullable=false)
     */
    private $asociacion;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="CostoProducto", mappedBy="diagnosticoProductivo")
     */
    private $costoProductos;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set asociacion
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociacion
     *
     * @return DiagnosticoProductivo
     */
    public function setAsociacion(Asociacion $asociacion) {
        $this->asociacion = $asociacion;

        return $this;
    }

    /**
     * Get asociacion
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociacion
     */
    public function getAsociacion() {
        return $this->asociacion;
    }
    
    public function getCostoProductos() {
        return $this->costoProductos;
    }

    public function addCostoProductos(CostoProducto $costoProducto) {
        $this->costoProductos[] = $costoProducto;
    }
    
    public function removeCostoProductos(CostoProducto $costoProducto) {
        $this->costoProductos->removeElement($costoProducto);
    }
    
    public function __construct() {
        $this->costoProductos = new ArrayCollection();
    }

}
