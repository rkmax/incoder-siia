<?php

    namespace Incoder\Bundle\SiiaBundle\Entity\Productivo;

    use Doctrine\Common\Util\ClassUtils;
    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Bridge\Doctrine\Validator\Constraints as Cons;

    /**
     * Producto
     * @ORM\Entity
     */
    class Producto extends AbstractProducto
    {
        /**
         * @var string
         */
        private $tipo;

        /**
         * @var string
         */
        private $tipoCultivo;

        public function createProductoEspecializado($producto = null)
        {
            switch ($this->getTipo()) {
                case self::TAGRICOLA:
                    if (!$producto) $producto = new ProductoAgricola();
                    $producto->setTipoCultivo($this->getTipoCultivo());
                    break;
                case self::TPECUARIO:
                    if (!$producto) $producto = new ProductoPecuario();
                    break;
            }

            if (!$producto) {
                throw new \Exception("EL producto es nulo, no se puede especializar");
            }

            // Copia las propiedades comunes
            self::copyProperties($this, $producto);

            return $producto;
        }

        public function toArray()
        {
            $o = [];
            $rclass = new \ReflectionClass($this);

            foreach ($rclass->getProperties() as $prop) {
                $p = $prop->getName();
                $getter = "get" . ucfirst($p);
                $prop->setAccessible(true);
                $o[$p] = $this->$getter();
            }

            return $o;
        }

        public static function createFromProductoEspecializado($producto)
        {
            $p = new self();

            self::copyProperties($producto, $p);

            switch (ClassUtils::getClass($producto)) {
                case ProductoAgricola::CLASS:
                    /** @var ProductoAgricola $producto */
                    $p->setTipo(self::TAGRICOLA);
                    $p->setTipoCultivo($producto->getTipoCultivo());
                    break;
                case ProductoPecuario::CLASS:
                    $p->setTipo(self::TPECUARIO);
                    break;
            }

            return $p;
        }

        /**
         * @param string $tipo
         *
         * @return Producto
         */
        public function setTipo($tipo)
        {
            $this->tipo = $tipo;

            return $this;
        }

        /**
         * @return string
         */
        public function getTipo()
        {
            return $this->tipo;
        }

        /**
         * @return string
         */
        public function getTipoCultivo()
        {
            return $this->tipoCultivo;
        }

        /**
         * @param string $tipoCultivo
         *
         * @return Producto
         */
        public function setTipoCultivo($tipoCultivo)
        {
            $this->tipoCultivo = $tipoCultivo;

            return $this;
        }
    }
