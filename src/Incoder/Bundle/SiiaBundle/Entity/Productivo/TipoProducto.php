<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 4/24/14
 * Time: 6:24 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity\Productivo;


abstract class TipoProducto
{
    const PECUARIO = 'pecuario';
    const AGRICOLA = 'agricola';
}