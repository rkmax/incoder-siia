<?php

namespace Incoder\Bundle\SiiaBundle\Entity\Productivo;

use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractProducto
 *
 * @ORM\Table("p_producto")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\ProductoRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="tipo", type="string")
 * @ORM\DiscriminatorMap({"agricola" = "ProductoAgricola", "pecuario" = "ProductoPecuario", "producto" = "Producto"})
 */
abstract class AbstractProducto
{
    const TAGRICOLA = 'agricola';
    const TPECUARIO = 'pecuario';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_producto", type="string", length=255)
     */
    protected $nombreProducto;

    /**
     * @var string
     *
     * @ORM\Column(name="variedad_producto", type="string", length=255, nullable=true   )
     */
    protected $variedadProducto;

    /**
     * @var UnidadProduccion
     *
     * @ORM\ManyToOne(targetEntity="UnidadProduccion")
     * @ORM\JoinColumn(name="unidad_produccion_id", referencedColumnName="id")
     */
    protected $unidadProduccion;

    /**
     * @var UnidadVenta
     *
     * @ORM\ManyToOne(targetEntity="UnidadVenta")
     * @ORM\JoinColumn(name="unidad_venta_id", referencedColumnName="id")
     */
    protected $unidadVenta;

    public function __toString()
    {
        return trim("{$this->getNombreProducto()} {$this->getVariedadProducto()}");
    }



    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreProducto
     *
     * @param string $nombreProducto
     *
     * @return AbstractProducto
     */
    public function setNombreProducto($nombreProducto)
    {
        $this->nombreProducto = $nombreProducto;

        return $this;
    }

    /**
     * Get nombreProducto
     *
     * @return string
     */
    public function getNombreProducto()
    {
        return $this->nombreProducto;
    }

    /**
     * Set unidadProduccion
     *
     * @param string $unidadProduccion
     *
     * @return AbstractProducto
     */
    public function setUnidadProduccion($unidadProduccion)
    {
        $this->unidadProduccion = $unidadProduccion;

        return $this;
    }

    /**
     * Get unidadProduccion
     *
     * @return string
     */
    public function getUnidadProduccion()
    {
        return $this->unidadProduccion;
    }

    /**
     * Set unidadVenta
     *
     * @param string $unidadVenta
     *
     * @return AbstractProducto
     */
    public function setUnidadVenta($unidadVenta)
    {
        $this->unidadVenta = $unidadVenta;

        return $this;
    }

    /**
     * Get unidadVenta
     *
     * @return string
     */
    public function getUnidadVenta()
    {
        return $this->unidadVenta;
    }

    /**
     * Set variedadProducto
     *
     * @param string $variedadProducto
     *
     * @return AbstractProducto
     */
    public function setVariedadProducto($variedadProducto)
    {
        $this->variedadProducto = $variedadProducto;

        return $this;
    }

    /**
     * Get variedadProducto
     *
     * @return string
     */
    public function getVariedadProducto()
    {
        return $this->variedadProducto;
    }

    public static function copyProperties($from, $to)
    {
        // Copia las propiedades comunes
        $rclass_to = new \ReflectionClass($to);
        $rclass_from = new \ReflectionClass($from);
        foreach ($rclass_to->getProperties() as $prop_to) {

            // La propiedad debe existir en ambos
            if (!(property_exists($from, $prop_to->getName()) && property_exists($to, $prop_to->getName()))) {
                continue;
            }
            $prop_from = $rclass_from->getProperty($prop_to->getName());

            $prop_to->setAccessible(true);
            $prop_from->setAccessible(true);

            $prop_to->setValue($to, $prop_from->getValue($from));
        }
    }
}
