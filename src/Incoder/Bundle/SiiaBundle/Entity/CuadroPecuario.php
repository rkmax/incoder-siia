<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/27/14
 * Time: 9:15 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoPecuario;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Registro pecuario de asociado
 *
 * @ORM\Table("cuadro_pecuario")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\CuadroPecuarioRepository")
 * @DForm("diagnostico_productivo_cuadro_pecuario")
 */
class CuadroPecuario extends CuadroProductivo {

    /**
     *
     * @var Asociado
     * @ORM\ManyToOne(targetEntity="Asociado", cascade={"remove"}, inversedBy="cuadroPecuario")
     * @ORM\JoinColumn(name="asociado_id", referencedColumnName="id", nullable=false)
     */
    protected $asociado;

    /**
     *
     * @var string
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoPecuario")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     * @NotNull(message="Debe seleccionar un producto")
     */
    protected $producto;

    /**
     *
     * @var string
     * @ORM\Column(name="especie_animal", type="string", length=255, nullable=true)
     * @SyncValue("especie_animal")
     */
    private $especieAnimal;

    /**
     *
     * @var string
     * @ORM\Column(name="linea_animal", type="string", length=255, nullable=true)
     * @SyncValue("linea_animal")
     */
    private $lineaAnimal;

    /**
     *
     * @var string
     * @ORM\Column(name="raza_animal", type="string", length=255, nullable=true)
     * @SyncValue("raza_animal")
     */
    private $razaAnimal;

    /**
     *
     * @var int
     * @ORM\Column(name="cantidad_animal", type="integer", nullable=true)
     * @SyncValue("cantidad_animal")
     */
    private $cantidad;

    /**
     * @var float
     * @ORM\Column(name="volumen_producido", type="decimal", scale=2, nullable=true)
     * @SyncValue("volumen_producido")
     */
    private $volumenProduccion;

    /**
     * @var float
     * @ORM\Column(name="volumen_vendido", type="decimal", scale=2, nullable=true)
     * @SyncValue("volumen_vendido")
     */
    private $volumenVendido;

    /**
     * @var string
     * @ORM\Column(name="fecha_actualizacion", type="string", length=20, nullable=true)
     */
    private $fechaActualizacion;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param string $especieAnimal
     */
    public function setEspecieAnimal($especieAnimal)
    {
        $this->especieAnimal = $especieAnimal;
    }

    /**
     * @return string
     */
    public function getEspecieAnimal()
    {
        return $this->especieAnimal;
    }

    /**
     * @param string $lineaAnimal
     */
    public function setLineaAnimal($lineaAnimal)
    {
        $this->lineaAnimal = $lineaAnimal;
    }

    /**
     * @return string
     */
    public function getLineaAnimal()
    {
        return $this->lineaAnimal;
    }

    /**
     * @param string $razaAnimal
     */
    public function setRazaAnimal($razaAnimal)
    {
        $this->razaAnimal = $razaAnimal;
    }

    /**
     * @return string
     */
    public function getRazaAnimal()
    {
        return $this->razaAnimal;
    }

    /**
     * @param float $volumenProduccion
     */
    public function setVolumenProduccion($volumenProduccion)
    {
        $this->volumenProduccion = $volumenProduccion;
    }

    /**
     * @return float
     */
    public function getVolumenProduccion()
    {
        return $this->volumenProduccion;
    }

    /**
     * @param float $volumenVendido
     */
    public function setVolumenVendido($volumenVendido)
    {
        $this->volumenVendido = $volumenVendido;
    }

    /**
     * @return float
     */
    public function getVolumenVendido()
    {
        return $this->volumenVendido;
    }



    /**
     * @return ProductoPecuario
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * @param ProductoPecuario $producto
     */
    public function setProducto(ProductoPecuario $producto)
    {
        $this->producto = $producto;
    }



    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociado $asociado
     */
    public function setAsociado($asociado)
    {
        $this->asociado = $asociado;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociado
     */
    public function getAsociado()
    {
        return $this->asociado;
    }

    /**
     * @param string $fechaActualizacion
     */
    public function setFechaActualizacion($fechaActualizacion)
    {
        $this->fechaActualizacion = $fechaActualizacion;
    }

    /**
     * @return string
     */
    public function getFechaActualizacion()
    {
        return $this->fechaActualizacion;
    }

} 
