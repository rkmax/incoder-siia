<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;

/**
 * Miembro del comite de la asociacion
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dp_tecnico_miembro")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoTecnicoRepository")
 * DForm("diagnostico_tecnico_miembros_junta")
 */
class MiembroComite
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    public function getId() {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=255)
     * SyncValue("cargo_integrante")
     */
    private $cargo;

    /**
     * @var string
     *
     * @ORM\Column(name="nivel_educativo", type="string", length=255, nullable=true)
     * SyncValue("nivel_educativo_integrante")
     */
    private $nivel_educativo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * SyncValue("nombre_integrante")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="documento", type="string", length=50, nullable=true)
     * SyncValue("documento_integrante")
     */
    private $documento;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * SyncValue("e_mail_integrante")
     */
    private $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=50, nullable=true)
     * SyncValue("telefono_integrante")
     */
    private $telefono;
    
     /**
     * @var DiagnosticoTecnico
     *
     * @ORM\ManyToOne(targetEntity="DiagnosticoTecnico", inversedBy="miembros")
     * @ORM\JoinColumn(name="dg_tecnico_id", referencedColumnName="id", nullable=false)
     */
    private $diagnosticoTecnico;
    
    public function getNombre() {
        return $this->nombre;
    }

    public function getDocumento() {
        return $this->documento;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function getDiagnosticoTecnico() {
        return $this->diagnosticoTecnico;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setDocumento($documento) {
        $this->documento = $documento;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function setDiagnosticoTecnico(DiagnosticoTecnico $diagnosticoTecnico) {
        $this->diagnosticoTecnico = $diagnosticoTecnico;
    }

    /**
     * @param string $cargo
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    /**
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * @param string $nivel_educativo
     */
    public function setNivelEducativo($nivel_educativo)
    {
        $this->nivel_educativo = $nivel_educativo;
    }

    /**
     * @return string
     */
    public function getNivelEducativo()
    {
        return $this->nivel_educativo;
    }

    
}
