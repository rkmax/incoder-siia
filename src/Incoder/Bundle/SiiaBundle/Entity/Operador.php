<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Operador
 *
 * @ORM\Table("operador")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\OperadorRepository")
 */
class Operador extends BaseProfile {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="documento_identificacion", type="string", length=255)
     */
    private $documentoIdentificacion;

    /**
     * @ORM\OneToMany(targetEntity="Asesor", mappedBy="operador", cascade={"persist"})
     */
    private $asesor;

    /**
     * @var SecurityUser
     * @ORM\OneToOne(targetEntity="SecurityUser", inversedBy="operador")
     * @ORM\JoinColumn(name="security_user_id", referencedColumnName="id")
     */
    protected $securityUser;

    /**
     * Set securityUser
     *
     * @param SecurityUser $securityUser
     * @return BaseProfile
     */
    public function setSecurityUser(SecurityUser $securityUser = null)
    {
        $this->securityUser = $securityUser;

        return $this;
    }

    /**
     * Get securityUser
     *
     * @return SecurityUser
     */
    public function getSecurityUser()
    {
        return $this->securityUser;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->asesor = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Operador
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set documentoIdentificacion
     *
     * @param string $documentoIdentificacion
     * @return Operador
     */
    public function setDocumentoIdentificacion($documentoIdentificacion) {
        $this->documentoIdentificacion = $documentoIdentificacion;

        return $this;
    }

    /**
     * Get documentoIdentificacion
     *
     * @return string
     */
    public function getDocumentoIdentificacion() {
        return $this->documentoIdentificacion;
    }

    /**
     * Add asesor
     *
     * @param Asesor $asesor
     *
     * @return Operador
     */
    public function addAsesor(Asesor $asesor) {
        $this->asesor[] = $asesor;

        $asesor->setOperador($this);

        return $this;
    }

    /**
     * Remove asesor
     *
     * @param Asesor $asesor
     */
    public function removeAsesor(Asesor $asesor) {
        $this->asesor->removeElement($asesor);
    }

    /**
     * Get asesor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsesor() {
        return $this->asesor;
    }

    public function __toString() {
        return "" . $this->getNombre();
    }

}
