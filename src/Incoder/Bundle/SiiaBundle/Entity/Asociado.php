<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Asociado
 *
 * @ORM\Table("asociado")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\AsociadoRepository")
 * @DForm("registro_unico")
 */
class Asociado extends DFormEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var Asociacion
     * @ORM\ManyToOne(targetEntity="Asociacion", inversedBy="asociado")
     * @ORM\JoinColumn(name="asociacion_id", referencedColumnName="id", nullable=false)
     */
    private $asociacion;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="documento", nullable=false)
     * @SyncValue("documento_identidad")
     * @NotBlank(message="El documento no puede ser blanco o nulo")
     */
    private $documento;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="apellidos", nullable=true)
     * @SyncValue("apellidos_asociado")
     * @NotBlank(message="Debe tener apellidos")
     */
    private $apellidos;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="nombre", nullable=false)
     * @SyncValue("nombre_asociado")
     * @NotBlank(message="Debe tener un nombre")
     */
    private $nombre;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="departamento", nullable=true)
     * @SyncValue("departamento_propiedad")
     */
    private $departamento;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="municipio", nullable=true)
     * @SyncValue("municipio_propiedad")
     */
    private $municipio;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="centro_poblado", nullable=true)
     * @SyncValue("centro_poblado_propiedad")
     */
    private $centroPoblado;

    /**
     * @var string
     * @ORM\Column(type="string", length=1025, name="nombre_propiedad", nullable=true)
     * @SyncValue("nombre_propiedad")
     */
    private $nombrePredio;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, name="registro_catastral", nullable=true)
     * @SyncValue("registro_catastral")
     */
    private $registroCatastral;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="CuadroPecuario", mappedBy="asociado", cascade={"remove"})
     */
    private $cuadroPecuario;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="CuadroAgricola", mappedBy="asociado", cascade={"remove"})
     */
    private $cuadroAgricola;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="PerfilSocioEconomico", mappedBy="asociado", cascade={"remove"})
     */
    private $perfil;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    function __construct()
    {
    }

    public function __toString() {
        return trim("{$this->nombre} {$this->apellidos}");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set asociacion
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociacion
     *
     * @return Asociado
     */
    public function setAsociacion(Asociacion $asociacion)
    {
        $this->asociacion = $asociacion;

        return $this;
    }

    /**
     * Get asociacion
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociacion
     */
    public function getAsociacion()
    {
        return $this->asociacion;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $perfil
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;
    }

    /**
     * Add cuadroPecuario
     *
     * @param CuadroPecuario $cuadroPecuario
     *
     * @return Asociado
     */
    public function addCuadroPecuario(CuadroPecuario $cuadroPecuario)
    {
        $this->cuadroPecuario[] = $cuadroPecuario;

        return $this;
    }

    /**
     * Remove cuadroPecuario
     *
     * @param CuadroPecuario $cuadroPecuario
     */
    public function removeCuadroPecuario(CuadroPecuario $cuadroPecuario)
    {
        $this->cuadroPecuario->removeElement($cuadroPecuario);
    }

    /**
     * Get cuadroPecuario
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCuadroPecuario()
    {
        return $this->cuadroPecuario;
    }

    /**
     * Add cuadroAgricola
     *
     * @param CuadroAgricola $cuadroAgricola
     *
     * @return Asociado
     */
    public function addCuadroAgricola(CuadroAgricola $cuadroAgricola)
    {
        $this->cuadroAgricola[] = $cuadroAgricola;

        return $this;
    }

    /**
     * Remove cuadroAgricola
     *
     * @param CuadroAgricola $cuadroAgricola
     */
    public function removeCuadroAgricola(CuadroAgricola $cuadroAgricola)
    {
        $this->cuadroAgricola->removeElement($cuadroAgricola);
    }

    /**
     * Get cuadroAgricola
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCuadroAgricola()
    {
        return $this->cuadroAgricola;
    }

    /**
     * Add perfil
     *
     * @param PerfilSocioEconomico $perfil
     *
     * @return Asociado
     */
    public function addPerfil(PerfilSocioEconomico $perfil)
    {
        $this->perfil[] = $perfil;

        return $this;
    }

    /**
     * Remove perfil
     *
     * @param PerfilSocioEconomico $perfil
     */
    public function removePerfil(PerfilSocioEconomico $perfil)
    {
        $this->perfil->removeElement($perfil);
    }

    /**
     * @return string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @param string $documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    /**
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param string $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getProductosProducidos()
    {
        $productos = [];

        foreach ($this->getCuadroAgricola() as $cuadroAgricola) {
            $nombre = "{$cuadroAgricola->getProducto()}";
            $productos[$nombre] = $nombre;
        }

        foreach ($this->getCuadroPecuario() as $cuadroPecuario) {
            $nombre = "{$cuadroPecuario->getProducto()}";
            $productos[$nombre] = $nombre;
        }

        return array_values($productos);
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param string $centroPoblado
     */
    public function setCentroPoblado($centroPoblado)
    {
        $this->centroPoblado = $centroPoblado;
    }

    /**
     * @return string
     */
    public function getCentroPoblado()
    {
        return $this->centroPoblado;
    }

    /**
     * @param string $departamento
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return string
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param string $municipio
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
    }

    /**
     * @return string
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * @param string $nombrePredio
     */
    public function setNombrePredio($nombrePredio)
    {
        $this->nombrePredio = $nombrePredio;
    }

    /**
     * @return string
     */
    public function getNombrePredio()
    {
        return $this->nombrePredio;
    }

    /**
     * @param string $registroCatastral
     */
    public function setRegistroCatastral($registroCatastral)
    {
        $this->registroCatastral = $registroCatastral;
    }

    /**
     * @return string
     */
    public function getRegistroCatastral()
    {
        return $this->registroCatastral;
    }
}
