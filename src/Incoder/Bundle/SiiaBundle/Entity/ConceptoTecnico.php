<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 11/03/14
 * Time: 09:37 AM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;

/**
 * Conceptos tecnicos del incoder
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("concepto_tecnico")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\ConceptoTecnicoRepository")
 */
class ConceptoTecnico {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId() {
        return $this->id;
    }

    /**
     * @var DiagnosticoTecnico
     *
     * @ORM\ManyToOne(targetEntity="DiagnosticoTecnico", inversedBy="conceptos")
     * @ORM\JoinColumn(name="dg_tecnico_id", referencedColumnName="id", nullable=false)
     */
    private $diagnosticoTecnico;

    /**
     *
     * @var string
     * @ORM\Column(name="concepto", type="string", length=2000, nullable=false)
     *
     */
    private $concepto;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @param string $concepto
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;
    }

    /**
     * @return string
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico $diagnosticoTecnico
     */
    public function setDiagnosticoTecnico($diagnosticoTecnico)
    {
        $this->diagnosticoTecnico = $diagnosticoTecnico;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico
     */
    public function getDiagnosticoTecnico()
    {
        return $this->diagnosticoTecnico;
    }

    /**
     * @param \DateTime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }



} 