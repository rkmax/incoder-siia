<?php

/**
 * Created by PhpStorm.
 * @author  Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * Date: 10/01/14
 * Time: 05:48 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class DiagnosticoTecnico
 *
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dg_tecnico")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoTecnicoRepository")
 * @DForm("diagnostico_tecnico")
 */
class DiagnosticoTecnico extends DFormEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Asociacion
     * @ORM\ManyToOne(targetEntity="Asociacion", cascade={"remove"}, inversedBy="diagnosticoTecnico")
     * @ORM\JoinColumn(name="asociacion_id", referencedColumnName="id", nullable=false)
     */
    private $asociacion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Plano", mappedBy="diagnosticoTecnico", cascade={"persist", "remove"})
     */
    private $planos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="MiembroComite", mappedBy="diagnosticoTecnico", cascade={"persist", "remove"})
     */
    private $miembros;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="CaudalEstiaje", mappedBy="diagnosticoTecnico", cascade={"persist", "remove"})
     */
    private $caudales;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ConceptoTecnico", mappedBy="diagnosticoTecnico", cascade={"persist", "remove"})
     */
    private $conceptos;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set asociacion
     *
     * @param Asociacion $asociacion
     *
     * @return DiagnosticoTecnico
     */
    public function setAsociacion(Asociacion $asociacion) {
        $this->asociacion = $asociacion;

        return $this;
    }

    /**
     * Get asociacion
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociacion
     */
    public function getAsociacion() {
        return $this->asociacion;
    }

    public function getPlanos() {
        return $this->planos;
    }

    public function getMiembros() {
        return $this->miembros;
    }

    public function getCaudales() {
        return $this->caudales;
    }

    public function setPlanos(\Doctrine\Common\Collections\Collection $planos) {
        foreach ($planos as $plano) {
            $plano->setDiagnosticoTecnico($this);
            $this->planos->add($plano);
        }
    }

    public function setMiembros(\Doctrine\Common\Collections\Collection $miembros) {
        foreach ($miembros as $miembro) {
            $miembro->setDiagnosticoTecnico($this);
            $this->miembros->add($miembro);
        }
    }

    public function setCaudales(\Doctrine\Common\Collections\Collection $caudales) {
        foreach ($caudales as $caudal) {
            $caudal->setDiagnosticoTecnico($this);
            $this->caudales->add($caudal);
        }
        $this->caudales = $caudales;
    }

    /**
     * Add plano
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Plano $plano
     *
     * @return Asociacion
     */
    public function addPlano(Plano $plano) {
        $plano->setDiagnosticoTecnico($this);
        $this->planos[] = $plano;

        return $this;
    }

    /**
     * Remove plano
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Plano $plano
     */
    public function removePlano(Plano $plano) {
        $this->planos->removeElement($plano);
    }

    /**
     * Add caudal
     *
     * @param CaudalEstiaje $caudal
     *
     * @return Asociacion
     */
    public function addCaudal(CaudalEstiaje $caudal) {
        $caudal->setDiagnosticoTecnico($this);
        $this->caudales[] = $caudal;

        return $this;
    }

    /**
     * Remove caudal
     *
     * @param CaudalEstiaje $caudal
     */
    public function removeCaudal(CaudalEstiaje $caudal) {
        $this->caudales->removeElement($caudal);
    }

    /**
     * Add miembro
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Miembro $miembro
     *
     * @return Asociacion
     */
    public function addMiembro(MiembroComite $miembro) {
        $miembro->setDiagnosticoTecnico($this);
        $this->miembros[] = $miembro;

        return $this;
    }

    /**
     * Remove miembro
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\MiembroComite $miembro
     */
    public function removeMiembro(MiembroComite $miembro) {
        $this->miembros->removeElement($miembro);
    }

    public function __construct() {
        $this->caudales = new ArrayCollection();
        $this->miembros = new ArrayCollection();
        $this->planos = new ArrayCollection();
        $this->conceptos = new ArrayCollection();
    }

    /**
     * Add caudales
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\CaudalEstiaje $caudales
     * @return DiagnosticoTecnico
     */
    public function addCaudale(\Incoder\Bundle\SiiaBundle\Entity\CaudalEstiaje $caudales)
    {
        $this->caudales[] = $caudales;

        return $this;
    }

    /**
     * Remove caudales
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\CaudalEstiaje $caudales
     */
    public function removeCaudale(\Incoder\Bundle\SiiaBundle\Entity\CaudalEstiaje $caudales)
    {
        $this->caudales->removeElement($caudales);
    }

    /**
     * Add conceptos
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\ConceptoTecnico $conceptos
     * @return DiagnosticoTecnico
     */
    public function addConcepto(\Incoder\Bundle\SiiaBundle\Entity\ConceptoTecnico $conceptos)
    {
        $this->conceptos[] = $conceptos;

        return $this;
    }

    /**
     * Remove conceptos
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\ConceptoTecnico $conceptos
     */
    public function removeConcepto(\Incoder\Bundle\SiiaBundle\Entity\ConceptoTecnico $conceptos)
    {
        $this->conceptos->removeElement($conceptos);
    }

    /**
     * Get conceptos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConceptos()
    {
        return $this->conceptos;
    }

    public function setConceptos($conceptos)
    {
        $this->conceptos = $conceptos;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
