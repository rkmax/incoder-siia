<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/28/14
 * Time: 1:09 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Insumo
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("insumo")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\InsumoRepository")
 * DForm("diagnostico_productivo_insumo")
 */
class Insumo {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(name="nombre_insumo", type="string", length=255, nullable=true)
     * SyncValue("nombre_insumo")
     */
    private $nombre;

    /**
     *
     * @var string
     * @ORM\Column(name="marca_insumo", type="string", length=255, nullable=true)
     * SyncValue("marca_insumo")
     */
    private $marca;

    /**
     *
     * @var string
     * @ORM\Column(name="frecuencia_uso_insumo", type="string", length=255, nullable=true)
     * SyncValue("frecuencia_uso_insumo")
     */
    private $frecuencia;

    /**
     *
     * @var CuadroProductivo
     * @ORM\ManyToOne(targetEntity="CuadroProductivo", cascade={"persist"}, inversedBy="insumos")
     * @ORM\JoinColumn(name="cuadro_id", referencedColumnName="id", nullable=false)
     */
    protected $cuadroProductivo;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $frecuencia
     */
    public function setFrecuencia($frecuencia)
    {
        $this->frecuencia = $frecuencia;
    }

    /**
     * @return string
     */
    public function getFrecuencia()
    {
        return $this->frecuencia;
    }

    /**
     * @param string $marca
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    }

    /**
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\CuadroProductivo $cuadroProductivo
     */
    public function setCuadroProductivo($cuadroProductivo)
    {
        $this->cuadroProductivo = $cuadroProductivo;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\CuadroProductivo
     */
    public function getCuadroProductivo()
    {
        return $this->cuadroProductivo;
    }

}
