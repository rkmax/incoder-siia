<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * DiagnosticoEmpresarial
 *
 * @ORM\Table("dg_empresarial")
 * @ORM\Entity
 */
class DiagnosticoEmpresarial
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_diagnostico", type="datetime")
     */
    private $fechaDiagnostico;

    /**
     * @var Asociacion
     * @ORM\ManyToOne(targetEntity="Asociacion", cascade={"remove"}, inversedBy="diagnosticoEmpresarial")
     * @ORM\JoinColumn(name="asociacion_id", referencedColumnName="id", nullable=false)
     */
    private $asociacion;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="DiagnosticoEmpresarialRespuesta", mappedBy="diagnostico", cascade={"remove"})
     */
    private $respuesta;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociacion
     */
    public function getAsociacion()
    {
        return $this->asociacion;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociacion
     */
    public function setAsociacion(Asociacion $asociacion)
    {
        $this->asociacion = $asociacion;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->respuesta = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add respuesta
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta $respuesta
     *
*@return DiagnosticoEmpresarial
     */
    public function addRespuesta(DiagnosticoEmpresarialRespuesta $respuesta)
    {
        $this->respuesta[] = $respuesta;
    
        return $this;
    }

    /**
     * Remove respuesta
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta $respuesta
     */
    public function removeRespuesta(DiagnosticoEmpresarialRespuesta $respuesta)
    {
        $this->respuesta->removeElement($respuesta);
    }

    /**
     * Get respuesta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set fechaDiagnostico
     *
     * @param \DateTime $fechaDiagnostico
     * @return DiagnosticoEmpresarial
     */
    public function setFechaDiagnostico($fechaDiagnostico)
    {
        $this->fechaDiagnostico = $fechaDiagnostico;
    
        return $this;
    }

    /**
     * Get fechaDiagnostico
     *
     * @return \DateTime 
     */
    public function getFechaDiagnostico()
    {
        return $this->fechaDiagnostico;
    }
}
