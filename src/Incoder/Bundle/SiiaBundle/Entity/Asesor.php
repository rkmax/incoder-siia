<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * Asesor
 *
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("asesor")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\AsesorRepository")
 */
class Asesor extends BaseProfile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="documento_identidad", type="string", length=50)
     */
    private $documentoIdentidad;


    /**
     * @ORM\ManyToOne(targetEntity="Operador", inversedBy="asesor")
     * @ORM\JoinColumn(name="operador_id", referencedColumnName="id")
     */
    private $operador;
    
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ManyToMany(targetEntity="Asociacion", inversedBy="asesores")
     * @JoinTable(name="asesores_asociaciones")
     **/
    private $asociaciones;

    /**
     * @var SecurityUser
     * @ORM\OneToOne(targetEntity="SecurityUser", inversedBy="asesor")
     * @ORM\JoinColumn(name="security_user_id", referencedColumnName="id")
     */
    protected $securityUser;

    /**
     * Set securityUser
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\SecurityUser $securityUser
     * @return BaseProfile
     */
    public function setSecurityUser(\Incoder\Bundle\SiiaBundle\Entity\SecurityUser $securityUser = null)
    {
        $this->securityUser = $securityUser;

        return $this;
    }

    /**
     * Get securityUser
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\SecurityUser
     */
    public function getSecurityUser()
    {
        return $this->securityUser;
    }
    
    
    public function __construct() {
        $this->asociaciones = new ArrayCollection();
    }

    public function __toString() {
        return trim("{$this->getNombre()} {$this->getApellidos()}");
    }
    
    /**
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAsociaciones() {
        return $this->asociaciones;
    }
    
    /**
     * 
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociacion
     */
    public function addAsociacion(Asociacion $asociacion) {
        if (!$this->asociaciones->contains($asociacion)) {
            $this->asociaciones[] = $asociacion;
        }
    }
    
    /**
     * 
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociacion
     */
    public function removeAsociacion(Asociacion $asociacion) {
        if ($this->asociaciones->contains($asociacion)) {
            $this->asociaciones->removeElement($asociacion);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Asesor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Asesor
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    
        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set documentoIdentidad
     *
     * @param string $documentoIdentidad
     * @return Asesor
     */
    public function setDocumentoIdentidad($documentoIdentidad)
    {
        $this->documentoIdentidad = $documentoIdentidad;
    
        return $this;
    }

    /**
     * Get documentoIdentidad
     *
     * @return string 
     */
    public function getDocumentoIdentidad()
    {
        return $this->documentoIdentidad;
    }

    /**
     * Set operador
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Operador $operador
     * @return Asesor
     */
    public function setOperador(\Incoder\Bundle\SiiaBundle\Entity\Operador $operador = null)
    {
        $this->operador = $operador;
    
        return $this;
    }

    /**
     * Get operador
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\Operador 
     */
    public function getOperador()
    {
        return $this->operador;
    }

    /**
     * Add asociaciones
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociaciones
     * @return Asesor
     */
    public function addAsociacione(\Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociaciones)
    {
        $this->asociaciones[] = $asociaciones;
    
        return $this;
    }

    /**
     * Remove asociaciones
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociaciones
     */
    public function removeAsociacione(\Incoder\Bundle\SiiaBundle\Entity\Asociacion $asociaciones)
    {
        $this->asociaciones->removeElement($asociaciones);
    }
}
