<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Localizacion
 *
 * @ORM\Table("localizacion")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\LocalizacionRepository")
 */
class Localizacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_departamento", type="string", length=2)
     */
    private $codigoDepartamento;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_municipio", type="string", length=5)
     */
    private $codigoMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_centro_poblado", type="string", length=8)
     */
    private $codigoCentroPoblado;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_departamento", type="string", length=255)
     */
    private $nombreDepartamento;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_municipio", type="string", length=255)
     */
    private $nombreMunicipio;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_centroPoblado", type="string", length=255)
     */
    private $nombreCentroPoblado;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_tipo", type="string", length=5)
     */
    private $codigoTipo;

    /**
     * @var string
     * @ORM\Column(name="nombre_tipo", type="string", length=255)
     */
    private $nombreTipo;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoDepartamento
     *
     * @param string $codigoDepartamento
     * @return Localizacion
     */
    public function setCodigoDepartamento($codigoDepartamento)
    {
        $this->codigoDepartamento = $codigoDepartamento;

        return $this;
    }

    /**
     * Get codigoDepartamento
     *
     * @return string
     */
    public function getCodigoDepartamento()
    {
        return $this->codigoDepartamento;
    }

    /**
     * Set codigoMunicipio
     *
     * @param string $codigoMunicipio
     * @return Localizacion
     */
    public function setCodigoMunicipio($codigoMunicipio)
    {
        $this->codigoMunicipio = $codigoMunicipio;

        return $this;
    }

    /**
     * Get codigoMunicipio
     *
     * @return string
     */
    public function getCodigoMunicipio()
    {
        return $this->codigoMunicipio;
    }

    /**
     * Set codigoCentroPoblado
     *
     * @param string $codigoCentroPoblado
     * @return Localizacion
     */
    public function setCodigoCentroPoblado($codigoCentroPoblado)
    {
        $this->codigoCentroPoblado = $codigoCentroPoblado;

        return $this;
    }

    /**
     * Get codigoCentroPoblado
     *
     * @return string
     */
    public function getCodigoCentroPoblado()
    {
        return $this->codigoCentroPoblado;
    }

    /**
     * Set nombreDepartamento
     *
     * @param string $nombreDepartamento
     * @return Localizacion
     */
    public function setNombreDepartamento($nombreDepartamento)
    {
        $this->nombreDepartamento = $nombreDepartamento;

        return $this;
    }

    /**
     * Get nombreDepartamento
     *
     * @return string
     */
    public function getNombreDepartamento()
    {
        return $this->nombreDepartamento;
    }

    /**
     * Set nombreMunicipio
     *
     * @param string $nombreMunicipio
     * @return Localizacion
     */
    public function setNombreMunicipio($nombreMunicipio)
    {
        $this->nombreMunicipio = $nombreMunicipio;

        return $this;
    }

    /**
     * Get nombreMunicipio
     *
     * @return string
     */
    public function getNombreMunicipio()
    {
        return $this->nombreMunicipio;
    }

    /**
     * Set nombreCentroPoblado
     *
     * @param string $nombreCentroPoblado
     * @return Localizacion
     */
    public function setNombreCentroPoblado($nombreCentroPoblado)
    {
        $this->nombreCentroPoblado = $nombreCentroPoblado;

        return $this;
    }

    /**
     * Get nombreCentroPoblado
     *
     * @return string
     */
    public function getNombreCentroPoblado()
    {
        return $this->nombreCentroPoblado;
    }

    /**
     * Set codigoTipo
     *
     * @param string $codigoTipo
     * @return Localizacion
     */
    public function setCodigoTipo($codigoTipo)
    {
        $this->codigoTipo = $codigoTipo;

        return $this;
    }

    /**
     * Get codigoTipo
     *
     * @return string
     */
    public function getCodigoTipo()
    {
        return $this->codigoTipo;
    }

    /**
     * Set nombreTipo
     *
     * @param string $nombreTipo
     * @return Localizacion
     */
    public function setNombreTipo($nombreTipo)
    {
        $this->nombreTipo = $nombreTipo;

        return $this;
    }

    /**
     * Get nombreTipo
     *
     * @return string
     */
    public function getNombreTipo()
    {
        return $this->nombreTipo;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("descripcion")
     *
     * @return string
     */
    public function getDescripcion()
    {
        return "{$this->getNombreCentroPoblado()} - {$this->getNombreDepartamento()}";
    }

    public function getLongDescripcion()
    {
        return "{$this->getNombreDepartamento()}, {$this->getNombreMunicipio()}, {$this->getNombreCentroPoblado()}";
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }
}
