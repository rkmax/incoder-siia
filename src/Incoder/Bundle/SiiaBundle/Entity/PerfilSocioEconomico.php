<?php

    /**
     *
     * @author  Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
     * Date: 10/01/14
     * Time: 05:37 PM
     */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class PerfilSocioEconomico
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("perfil_socioeconomico")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\PerfilSocioEconomicoRepository")
 * @DForm("perfil_socioeconomico")
 */
class PerfilSocioEconomico extends DFormEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Asociado
     * @ORM\ManyToOne(targetEntity="Asociado", cascade={"remove"}, inversedBy="perfil")
     * @ORM\JoinColumn(name="asociado_id", referencedColumnName="id", nullable=false)
     */
    private $asociado;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociacion
     */
    public function getAsociado()
    {
        return $this->asociado;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociado $asociado
     */
    public function setAsociado(Asociado $asociado)
    {
        $this->asociado = $asociado;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
