<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/27/14
 * Time: 9:15 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoAgricola;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Registro agricola de asociado
 *
 * @ORM\Table("cuadro_agricola")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\CuadroAgricolaRepository")
 * @DForm("diagnostico_productivo_cuadro_agricola")
 */
class CuadroAgricola extends CuadroProductivo {



    /**
     *
     * @var Asociado
     * @ORM\ManyToOne(targetEntity="Asociado", cascade={"remove"}, inversedBy="cuadroAgricola")
     * @ORM\JoinColumn(name="asociado_id", referencedColumnName="id", nullable=false)
     */
    protected $asociado;

    /**
     *
     * @var string
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoAgricola")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     * @NotNull(message="Debe seleccionar un producto")
     */
    protected $producto;

    /**
     *
     * @var \DateTime
     * @ORM\Column(name="fecha_siembra", type="date", nullable=true)
     * @SyncValue("fecha_siembra")
     */
    private $fechaSiembra;

    /**
     *
     * @var \DateTime
     * @ORM\Column(name="fecha_cosecha", type="date", nullable=true)
     * @SyncValue("fecha_cosecha")
     */
    private $fechaCosecha;

    /**
     *
     * @var string
     * @ORM\Column(name="tipo_cultivo", type="string", length=255, nullable=true)
     * @SyncValue("tipo_cultivo")
     */
    private $tipoCultivo;

    /**
     * @var float
     * @ORM\Column(name="area_sembrada", type="decimal", scale=2, nullable=true)
     * @SyncValue("area_sembrada")
     */
    private $areaSembrada;

    /**
     *
     * @var string
     * @ORM\Column(name="unidad_area_cultivada", type="string", length=255, nullable=true)
     * @SyncValue("unidad_area_cultivada")
     */
    private $unidadAreaCultivada;

    /**
     * @var float
     * @ORM\Column(name="volumen_produccion", type="decimal", scale=2, nullable=true)
     * @SyncValue("volumen_produccion")
     */
    private $volumenProduccion;

    /**
     * @var float
     * @ORM\Column(name="volumen_vendido", type="decimal", scale=2, nullable=true)
     * @SyncValue("volumen_vendido")
     */
    private $volumenVendido;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param float $areaSembrada
     */
    public function setAreaSembrada($areaSembrada)
    {
        $this->areaSembrada = $areaSembrada;
    }

    /**
     * @return float
     */
    public function getAreaSembrada()
    {
        return $this->areaSembrada;
    }

    /**
     * @param \DateTime $fechaCosecha
     */
    public function setFechaCosecha($fechaCosecha)
    {
        $this->fechaCosecha = $fechaCosecha;
    }

    /**
     * @return \DateTime
     */
    public function getFechaCosecha()
    {
        return $this->fechaCosecha;
    }

    /**
     * @param \DateTime $fechaSiembra
     */
    public function setFechaSiembra($fechaSiembra)
    {
        $this->fechaSiembra = $fechaSiembra;
    }

    /**
     * @return \DateTime
     */
    public function getFechaSiembra()
    {
        return $this->fechaSiembra;
    }

    /**
     * @param string $tipoCultivo
     */
    public function setTipoCultivo($tipoCultivo)
    {
        $this->tipoCultivo = $tipoCultivo;
    }

    /**
     * @return string
     */
    public function getTipoCultivo()
    {
        return $this->tipoCultivo;
    }

    /**
     * @param string $unidadAreaCultivada
     */
    public function setUnidadAreaCultivada($unidadAreaCultivada)
    {
        $this->unidadAreaCultivada = $unidadAreaCultivada;
    }

    /**
     * @return string
     */
    public function getUnidadAreaCultivada()
    {
        return $this->unidadAreaCultivada;
    }

    /**
     * @param float $volumenProduccion
     */
    public function setVolumenProduccion($volumenProduccion)
    {
        $this->volumenProduccion = $volumenProduccion;
    }

    /**
     * @return float
     */
    public function getVolumenProduccion()
    {
        return $this->volumenProduccion;
    }

    /**
     * @param float $volumenVendido
     */
    public function setVolumenVendido($volumenVendido)
    {
        $this->volumenVendido = $volumenVendido;
    }

    /**
     * @return float
     */
    public function getVolumenVendido()
    {
        return $this->volumenVendido;
    }



    /**
     * @return ProductoAgricola
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * @param ProductoAgricola $producto
     */
    public function setProducto(ProductoAgricola $producto)
    {
        $this->producto = $producto;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\Asociado $asociado
     */
    public function setAsociado($asociado)
    {
        $this->asociado = $asociado;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\Asociado
     */
    public function getAsociado()
    {
        return $this->asociado;
    }
} 
