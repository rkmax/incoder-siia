<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\MappedSuperclass
*/
class BaseProfile
{

    const ADMIN = 'ROLE_ADMIN';
    const OPERADOR = 'ROLE_OPERADOR';
    const ASESOR = 'ROLE_ASESOR';
    const ASOCIACION = 'ROLE_ASOCIACION';
    const SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const IMPERSONATING = 'ROLE_PREVIOUS_ADMIN';
    const SUPERVISOR = 'ROLE_SUPERVISOR';
    
    private $email;
    
    private $user;
    
    private $password;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}
