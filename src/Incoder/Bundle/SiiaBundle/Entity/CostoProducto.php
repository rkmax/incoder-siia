<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;

/**
 * CostoProducto
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dp_costo_producto")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoProductivoCostosRepository")
 * @DForm("diagnostico_productivo_costos_producto")
 */
class CostoProducto extends DFormEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="area_cultivada", type="decimal", precision=17, scale=4, nullable=true)
     * @SyncValue("area_cultivada")
     */
    private $areaCultivada;

    /**
     * @var float
     *
     * @ORM\Column(name="costos_produccion", type="decimal", precision=19, scale=2, nullable=true)
     * @SyncValue("costos_produccion")
     */
    private $costosProduccion;

    /**
     * @var boolean
     *  TODO: DForm\SyncValue("")
     */
    private $tienePlanAgricola;

    /**
     * @var boolean
     * TODO: ORM\Column(name="tiene_plan_riesgo", type="decimal", nullable=true)
     * TODO: DForm\SyncValue("")
     */
    private $tienePlanRiesgo;

    /**
     * @var integer
     * @ORM\Column(name="vida_util_cultivo", type="string", nullable=true)
     * @SyncValue("vida_util_cultivo")
     */
    private $vidaUtilCultivo;

    /**
     * @var integer
     * @ORM\Column(name="periodo_productivo", type="integer", nullable=true)
     * @SyncValue("periodo_productivo")
     */
    private $periodoProductivo;

    /**
     * 
     * @var string
     *
     * @ORM\Column(name="nombre_producto", type="string")
     * @SyncValue("nombre_producto")
     */
    private $producto;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="variedad_producto", type="string", nullable=true)
     * @SyncValue("variedad_producto")
     */
    private $variedad;
    
    /**
     * @var DiagnosticoProductivo
     *
     * @ORM\ManyToOne(targetEntity="DiagnosticoProductivo", cascade={"remove"}, inversedBy="costoProductos")
     * @ORM\JoinColumn(name="dg_productivo_id", referencedColumnName="id", nullable=false)
     */
    private $diagnosticoProductivo;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Produccion", mappedBy="costoProducto", cascade={"ALL"})
     */
    private $mesesProductivos;
    
    public function __construct() {
        $this->mesesProductivos = new ArrayCollection();
    }

    public function __toString() {
        return ucfirst(mb_strtolower($this->producto));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set diagnosticoProductivo
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoProductivo $diagnosticoProductivo
     *
     * @return Asociacion
     */
    public function setDiagnosticoProductivo(DiagnosticoProductivo $diagnosticoProductivo = null)
    {
        $this->diagnosticoProductivo = $diagnosticoProductivo;

        return $this;
    }

    /**
     * Get diagnosticoProductivo
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoProductivo
     */
    public function getDiagnosticoProductivo()
    {
        return $this->diagnosticoProductivo;
    }

    /**
     * Set areaCultivada
     *
     * @param float $areaCultivada
     * @return CostoProducto
     */
    public function setAreaCultivada($areaCultivada)
    {
        $this->areaCultivada = $areaCultivada;
    
        return $this;
    }

    /**
     * Get areaCultivada
     *
     * @return float 
     */
    public function getAreaCultivada()
    {
        return $this->areaCultivada;
    }

    /**
     * Set costosProduccion
     *
     * @param float $costosProduccion
     * @return CostoProducto
     */
    public function setCostosProduccion($costosProduccion)
    {
        $this->costosProduccion = $costosProduccion;
    
        return $this;
    }

    /**
     * Get costosProduccion
     *
     * @return float 
     */
    public function getCostosProduccion()
    {
        return $this->costosProduccion;
    }

    /**
     * Set tienePlanAgricola
     *
     * @param boolean $tienePlanAgricola
     * @return CostoProducto
     */
    public function setTienePlanAgricola($tienePlanAgricola)
    {
        $this->tienePlanAgricola = $tienePlanAgricola;
    
        return $this;
    }

    /**
     * Get tienePlanAgricola
     *
     * @return boolean 
     */
    public function getTienePlanAgricola()
    {
        return $this->tienePlanAgricola;
    }

    /**
     * Set tienePlanRiesgo
     *
     * @param boolean $tienePlanRiesgo
     * @return CostoProducto
     */
    public function setTienePlanRiesgo($tienePlanRiesgo)
    {
        $this->tienePlanRiesgo = $tienePlanRiesgo;
    
        return $this;
    }

    /**
     * Get tienePlanRiesgo
     *
     * @return boolean 
     */
    public function getTienePlanRiesgo()
    {
        return $this->tienePlanRiesgo;
    }

    /**
     * Set vidaUtilCultivo
     *
     * @param integer $vidaUtilCultivo
     * @return CostoProducto
     */
    public function setVidaUtilCultivo($vidaUtilCultivo)
    {
        $this->vidaUtilCultivo = $vidaUtilCultivo;
    
        return $this;
    }

    /**
     * Get vidaUtilCultivo
     *
     * @return integer 
     */
    public function getVidaUtilCultivo()
    {
        return $this->vidaUtilCultivo;
    }

    /**
     * Set periodoProductivo
     *
     * @param integer $periodoProductivo
     * @return CostoProducto
     */
    public function setPeriodoProductivo($periodoProductivo)
    {
        $this->periodoProductivo = $periodoProductivo;
    
        return $this;
    }

    /**
     * Get periodoProductivo
     *
     * @return integer 
     */
    public function getPeriodoProductivo()
    {
        return $this->periodoProductivo;
    }
    
    /**
     * 
     * @return string
     */
    public function getProducto() {
        return $this->producto;
    }
    
    /**
     * 
     * @param string $producto
     */
    public function setProducto($producto) {
        $this->producto = $producto;
    }

    /**
     * @param string $variedad
     */
    public function setVariedad($variedad)
    {
        $this->variedad = $variedad;
    }

    /**
     * @return string
     */
    public function getVariedad()
    {
        return $this->variedad;
    }
    
    public function getMesesProductivos() {
        return $this->mesesProductivos;
    }

    public function addMesesProductivos(Produccion $mesProductivo) {
        $this->mesesProductivos[] = $mesProductivo;
    }

    public function addMesesProductivo(Produccion $mesProductivo) {
        if (!$this->mesesProductivos->contains($mesProductivo)) {
            $mesProductivo->setCostoProducto($this);
            $this->mesesProductivos->add($mesProductivo);
        }
    }

    public function removeMesesProductivo(Produccion $mesProductivo) {
        $this->mesesProductivos->removeElement($mesProductivo);
    }
    
    public function removeMesesProductivos(Produccion $mesProductivo) {
        $this->mesesProductivos->removeElement($mesProductivo);
    }
}
