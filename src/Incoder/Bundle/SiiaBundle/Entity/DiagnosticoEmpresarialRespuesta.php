<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/20/14
 * Time: 9:03 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Opcion;

/**
 * DiagnosticoEmpresarial
 *
 * @ORM\Table("dg_empresarial_respuesta")
 * @ORM\Entity
 */
class DiagnosticoEmpresarialRespuesta {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DiagnosticoEmpresarial
     * @ORM\ManyToOne(targetEntity="DiagnosticoEmpresarial", inversedBy="respuesta")
     * @ORM\JoinColumn(name="dg_empresarial_id", referencedColumnName="id", nullable=false)
     */
    private $diagnostico;

    /**
     * @var Afirmacion
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion", inversedBy="opcion")
     * @ORM\JoinColumn(name="afirmacion_id", referencedColumnName="id", nullable=false)
     */
    private $afirmacion;

    /**
     * @var Opcion
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Opcion")
     * @ORM\JoinColumn(name="opcion_id", referencedColumnName="id", nullable=false)
     */
    private $opcion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set diagnostico
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial $diagnostico
     *
*@return DiagnosticoEmpresarialRespuesta
     */
    public function setDiagnostico(DiagnosticoEmpresarial $diagnostico)
    {
        $this->diagnostico = $diagnostico;
    
        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }

    /**
     * Set afirmacion
     *
     * @param Afirmacion $afirmacion
     * @return DiagnosticoEmpresarialRespuesta
     */
    public function setAfirmacion(Afirmacion $afirmacion)
    {
        $this->afirmacion = $afirmacion;
    
        return $this;
    }

    /**
     * Get afirmacion
     *
     * @return Afirmacion
     */
    public function getAfirmacion()
    {
        return $this->afirmacion;
    }

    /**
     * Set opcion
     *
     * @param Opcion $opcion
     * @return DiagnosticoEmpresarialRespuesta
     */
    public function setOpcion(Opcion $opcion)
    {
        $this->opcion = $opcion;
    
        return $this;
    }

    /**
     * Get opcion
     *
     * @return Opcion
     */
    public function getOpcion()
    {
        return $this->opcion;
    }

    /**
     *
     * @return mixed
     */
    public function getValorRespuesta()
    {
        $valor = 0;

        foreach ($this->getAfirmacion()->getOpcion() as $opcion) {
            if ($opcion->getOpcion()->getId() === $this->opcion->getId()) {
                $valor = $opcion->getValor();
            }
        }

        return $valor;
    }
}
