<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class Amenaza
 * @package Incoder\Bundle\SiiaBundle\Entity\PlanAccion
 * @ORM\Entity
 */
class Amenaza extends DOFAElement {

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="DOFAActividad", inversedBy="amenazas")
     * @ORM\JoinTable(name="dofa_actividad_amenaza")
     * @Exclude
     */
    private $actividades;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actividades = new ArrayCollection();
    }

    /**
     * Add actividades
     *
     * @param DOFAActividad $actividades
     *
     * @return Oportunidad
     */
    public function addActividade(DOFAActividad $actividades)
    {
        if (!$this->actividades->contains($actividades)) {
            $this->actividades[] = $actividades;
        }

        return $this;
    }

    /**
     * Remove actividades
     *
     * @param DOFAActividad $actividades
     */
    public function removeActividade(DOFAActividad $actividades)
    {
        $this->actividades->removeElement($actividades);
    }

    /**
     * Get actividades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActividades()
    {
        return $this->actividades;
    }
}
