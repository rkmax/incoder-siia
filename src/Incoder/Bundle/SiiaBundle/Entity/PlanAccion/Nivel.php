<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/16/14
 * Time: 1:15 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Accion;

/**
 * Class Nivel
 *
 * @ORM\Table("pa_nivel")
 * @ORM\Entity
 */
class Nivel {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_level", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="from_percentaje", type="integer")
     */
    private $from;


    /**
     * @var int
     *
     * @ORM\Column(name="to_percentaje", type="integer")
     */
    private $to;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Accion", mappedBy="nivel")
     */
    private $accion;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Diagnostico", mappedBy="nivel")
     */
    private $diagnostico;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set from
     *
     * @param integer $from
     * @return Nivel
     */
    public function setFrom($from)
    {
        $this->from = $from;
    
        return $this;
    }

    /**
     * Get from
     *
     * @return integer 
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param integer $to
     * @return Nivel
     */
    public function setTo($to)
    {
        $this->to = $to;
    
        return $this;
    }

    /**
     * Get to
     *
     * @return integer 
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Nivel
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->getName();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accion = new ArrayCollection();
        $this->diagnostico = new ArrayCollection();
    }

    /**
     * Add accion
     *
     * @param Accion $accion
     * @return Nivel
     */
    public function addAccion(Accion $accion)
    {
        $this->accion[] = $accion;

        return $this;
    }

    /**
     * Remove accion
     *
     * @param Accion $accion
     */
    public function removeAccion(Accion $accion)
    {
        $this->accion->removeElement($accion);
    }

    /**
     * Get accion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Add accion
     *
     * @param Diagnostico $diagnostico
     * @return Nivel
     */
    public function addDiagnostico(Diagnostico $diagnostico)
    {
        $this->diagnostico[] = $diagnostico;

        return $this;
    }

    /**
     * Remove accion
     *
     * @param Diagnostico $diagnostico
     */
    public function removeDiagnostico(Diagnostico $diagnostico)
    {
        $this->diagnostico->removeElement($diagnostico);
    }

    /**
     * Get accion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }
}
