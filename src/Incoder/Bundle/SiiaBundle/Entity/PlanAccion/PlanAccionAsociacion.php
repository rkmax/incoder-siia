<?php

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;

/**
 * PlanAccionAsociacion
 *
 * @ORM\Table("pa_asociacion_main")
 * @ORM\Entity
 */
class PlanAccionAsociacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Asociacion
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\Asociacion", inversedBy="planAcciones")
     * @ORM\JoinColumn(name="asociacion_id", referencedColumnName="id", nullable=false)
     */
    private $asociacion;

    /**
     * @var PlanAccionElemento
     * @ORM\OneToMany(targetEntity="Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionElemento", mappedBy="planAccion", cascade={"remove"})
     */
    private $elementos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->elementos = new ArrayCollection();
    }

    /**
     * Add elementos
     *
     * @param PlanAccionElemento $elemento
     *
     * @return PlanAccionAsociacion
     */
    public function addElemento(PlanAccionElemento $elemento)
    {
        if (!$this->elementos->contains($elemento)) {
            $this->elementos[] = $elemento;
            $elemento->setPlanAccion($this);
        }
    
        return $this;
    }

    /**
     * Remove elementos
     *
     * @param PlanAccionElemento $elementos
     */
    public function removeElemento(PlanAccionElemento $elementos)
    {
        $this->elementos->removeElement($elementos);
    }

    /**
     * Get elementos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getElementos()
    {
        return $this->elementos;
    }

    /**
     * @return Asociacion
     */
    public function getAsociacion()
    {
        return $this->asociacion;
    }

    /**
     * @param Asociacion $asociacion
     *
     * @return $this
     */
    public function setAsociacion(Asociacion $asociacion)
    {
        $this->asociacion = $asociacion;

        return $this;
    }
}
