<?php

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * OpcionPorAfirmacion
 *
 * @ORM\Table("pa_opcion_por_afirmacion")
 * @ORM\Entity
 * @UniqueEntity(
 *  fields={"afirmacion", "opcion"},
 *  errorPath="opcion",
 *  message="La opción ya se encuentra registrada en el sistema"
 * )
 */
class OpcionPorAfirmacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="valor", type="integer")
     */
    private $valor;

    /**
     * @var Afirmacion
     * @ORM\ManyToOne(targetEntity="Afirmacion", cascade={"remove"}, inversedBy="opcion")
     * @ORM\JoinColumn(name="afirmacion_id", referencedColumnName="id", nullable=false)
     */
    private $afirmacion;

    /**
     * @var Opcion
     * @ORM\ManyToOne(targetEntity="Opcion", cascade={"remove"})
     * @ORM\JoinColumn(name="opcion_id", referencedColumnName="id", nullable=false)
     */
    private $opcion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     * @return OpcionPorAfirmacion
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return integer 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set afirmacion
     *
     * @param Afirmacion $afirmacion
     * @return OpcionPorAfirmacion
     */
    public function setAfirmacion(Afirmacion $afirmacion)
    {
        $this->afirmacion = $afirmacion;
    
        return $this;
    }

    /**
     * Get afirmacion
     *
     * @return Afirmacion
     */
    public function getAfirmacion()
    {
        return $this->afirmacion;
    }

    /**
     * Set opcion
     *
     * @param Opcion $opcion
     * @return OpcionPorAfirmacion
     */
    public function setOpcion(Opcion $opcion)
    {
        $this->opcion = $opcion;
    
        return $this;
    }

    /**
     * Get opcion
     *
     * @return Opcion
     */
    public function getOpcion()
    {
        return $this->opcion;
    }

    public function __toString()
    {
        return "{$this->valor}";
    }
}