<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\MaxDepth;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Exclude;


/**
 * Class DOFA
 * @package Incoder\Bundle\SiiaBundle\Entity\PlanAccion
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\PlanAccion\DOFAActividadRepository")
 * @ORM\Table("dofa_actividad")
 */
class DOFAActividad {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Componente
     * @ORM\ManyToOne(targetEntity="Componente", fetch="EAGER")
     * @ORM\JoinColumn(name="componente_id", referencedColumnName="id")
     */
    private $componente;

    /**
     * @var Actividad
     * @ORM\ManyToOne(targetEntity="Actividad")
     * @ORM\JoinColumn(name="dofa_s_actividad_id", referencedColumnName="id")
     */
    private $actividad;

    /**
     * @var Accion
     * @ORM\ManyToOne(targetEntity="Accion", inversedBy="actividades")
     * @ORM\JoinColumn(name="accion_id", referencedColumnName="id")
     * @MaxDepth(depth=3)
     */
    private $accion;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Debilidad", mappedBy="actividades", cascade={"persist"})
     * @Exclude
     */
    private $debilidades;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Oportunidad", mappedBy="actividades", cascade={"persist"})
     * @Exclude
     */
    private $oportunidades;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Fortaleza", mappedBy="actividades", cascade={"persist"})
     * @Exclude
     */
    private $fortalezas;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Amenaza", mappedBy="actividades", cascade={"persist"})
     * @Exclude
     */
    private $amenazas;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->debilidades = new ArrayCollection();
        $this->oportunidades = new ArrayCollection();
        $this->fortalezas = new ArrayCollection();
        $this->amenazas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set componente
     *
     * @param Componente $componente
     *
     * @return DOFAActividad
     */
    public function setComponente(Componente $componente = null)
    {
        $this->componente = $componente;

        return $this;
    }

    /**
     * Get componente
     *
     * @return Componente
     */
    public function getComponente()
    {
        return $this->componente;
    }

    /**
     * Set accion
     *
     * @param Accion $accion
     *
     * @return DOFAActividad
     */
    public function setAccion(Accion $accion = null)
    {
        $this->accion = $accion;

        return $this;
    }

    /**
     * Get accion
     *
     * @return Accion
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Add debilidad
     *
     * @param Debilidad $debilidad
     *
     * @return Componente
     */
    public function addDebilidad(Debilidad $debilidad)
    {
        if (!$this->debilidades->contains($debilidad)) {
            $this->debilidades[] = $debilidad;
            $debilidad->addActividade($this);
        }

        return $this;
    }

    /**
     * Remove debilidad
     *
     * @param Debilidad $debilidad
     */
    public function removeDebilidad(Debilidad $debilidad)
    {
        if ($this->debilidades->contains($debilidad)) {
            $this->debilidades->removeElement($debilidad);
            $debilidad->removeActividade($this);
        }
    }

    /**
     * Get debilidad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDebilidad()
    {
        return $this->debilidades;
    }

    /**
     * Add oportunidad
     *
     * @param Oportunidad $oportunidad
     *
     * @return Componente
     */
    public function addOportunidad(Oportunidad $oportunidad)
    {
        if (!$this->oportunidades->contains($oportunidad)) {
            $this->oportunidades[] = $oportunidad;
            $oportunidad->addActividade($this);
        }

        return $this;
    }

    /**
     * Remove oportunidad
     *
     * @param Oportunidad $oportunidad
     */
    public function removeOportunidad(Oportunidad $oportunidad)
    {
        if ($this->oportunidades->contains($oportunidad)) {
            $this->oportunidades->removeElement($oportunidad);
            $oportunidad->removeActividade($this);
        }
    }

    /**
     * Get oportunidad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOportunidad()
    {
        return $this->oportunidades;
    }

    /**
     * Get fortaleza
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFortaleza()
    {
        return $this->fortalezas;
    }

    public function addFortaleza(Fortaleza $fortaleza)
    {
        if (!$this->fortalezas->contains($fortaleza)) {
            $this->fortalezas[] = $fortaleza;
            $fortaleza->addActividade($this);
        }
    }

    public function removeFortaleza(Fortaleza $fortaleza) {
        if ($this->fortalezas->contains($fortaleza)) {
            $this->fortalezas->removeElement($fortaleza);
            $fortaleza->removeActividade($this);
        }
    }

    /**
     * Add amenaza
     *
     * @param Amenaza $amenaza
     *
     * @return Componente
     */
    public function addAmenaza(Amenaza $amenaza)
    {
        if (!$this->amenazas->contains($amenaza)) {
            $this->amenazas[] = $amenaza;
            $amenaza->addActividade($this);
        }

        return $this;
    }

    /**
     * Remove amenaza
     *
     * @param Amenaza $amenaza
     */
    public function removeAmenaza(Amenaza $amenaza)
    {
        if ($this->amenazas->contains($amenaza)) {
            $this->amenazas->removeElement($amenaza);
            $amenaza->addActividade($this);
        }
    }

    /**
     * Get amenaza
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAmenaza()
    {
        return $this->amenazas;
    }

    /**
     * Add debilidades
     *
     * @param Debilidad $debilidades
     *
     * @return Componente
     */
    public function addDebilidade(Debilidad $debilidades)
    {
        $this->debilidades[] = $debilidades;

        return $this;
    }

    /**
     * Remove debilidades
     *
     * @param Debilidad $debilidades
     */
    public function removeDebilidade(Debilidad $debilidades)
    {
        $this->debilidades->removeElement($debilidades);
    }

    /**
     * Get debilidades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDebilidades()
    {
        return $this->debilidades;
    }

    /**
     * Add oportunidades
     *
     * @param Oportunidad $oportunidades
     *
     * @return Componente
     */
    public function addOportunidade(Oportunidad $oportunidades)
    {
        $this->oportunidades[] = $oportunidades;

        return $this;
    }

    /**
     * Remove oportunidades
     *
     * @param Oportunidad $oportunidades
     */
    public function removeOportunidade(Oportunidad $oportunidades)
    {
        $this->oportunidades->removeElement($oportunidades);
    }

    /**
     * Get oportunidades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOportunidades()
    {
        return $this->oportunidades;
    }

    /**
     * Get fortalezas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFortalezas()
    {
        return $this->fortalezas;
    }

    public function setFortalezas($fortalezas)
    {
        $this->fortalezas = $fortalezas;
    }

    /**
     * Get amenazas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAmenazas()
    {
        return $this->amenazas;
    }

    public function setAmenazas($amenazas)
    {
        $this->amenazas = $amenazas;
    }

    public function getJoined($property, $glue = ",")
    {
        if (!property_exists($this, $property)) {
            throw new \Exception("La propiedad $property no existe");
        }

        $toString = [];

        /** @var DOFAElement $e */
        foreach($this->$property as $e) {
            $toString[] = $e->getDescription();
        }

        return implode($glue, $toString);
    }

    public function setOportunidades($oportunidades)
    {
        $this->oportunidades = $oportunidades;
    }

    public function setDebilidades($debilidades)
    {
        $this->debilidades = $debilidades;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function checkIfHas($property, $objectId)
    {
        /** @var DOFAElement $object */
        foreach ($this->$property as $object) {
            if ($object->getId() == $objectId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set actividad
     *
     * @param Actividad $actividad
     *
     * @return DOFAActividad
     */
    public function setActividad(Actividad $actividad = null)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return Actividad
     */
    public function getActividad()
    {
        return $this->actividad;
    }
}
