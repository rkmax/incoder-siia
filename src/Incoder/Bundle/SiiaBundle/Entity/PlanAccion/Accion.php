<?php

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Accion
 *
 * @ORM\Table("pa_accion")
 * @ORM\Entity("Incoder\Bundle\SiiaBundle\Repository\AccionRepository")
 */
class Accion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotNull()
     */
    private $description;

    /**
     * @var Aspecto
     * @ORM\ManyToOne(targetEntity="Aspecto", cascade={"remove"})
     * @ORM\JoinColumn(name="aspecto_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $aspecto;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="DOFAActividad", mappedBy="accion")
     */
    private $actividades;


    /**
     * @var Nivel
     *
     * @ORM\ManyToOne(targetEntity="Nivel", cascade={"remove"}, inversedBy="accion")
     * @ORM\JoinColumn(name="nivel_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $nivel;

    public function getUniqueName()
    {
        return "<b>{$this->aspecto}</b> - <b>{$this->getNivel()}</b>, {$this->getDescription()}";
    }

    public function __toString()
    {
        return "{$this->getDescription()}";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Accion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set aspecto
     *
     * @param Aspecto $aspecto
     * @return Accion
     */
    public function setAspecto(Aspecto $aspecto)
    {
        $this->aspecto = $aspecto;

        return $this;
    }

    /**
     * Get aspecto
     *
     * @return Aspecto
     */
    public function getAspecto()
    {
        return $this->aspecto;
    }

    /**
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param string $nivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actividades = new ArrayCollection();
    }

    /**
     * Add actividades
     *
     * @param DOFAActividad $actividades
     *
     * @return Accion
     */
    public function addActividade(DOFAActividad $actividades)
    {
        $this->actividades[] = $actividades;

        return $this;
    }

    /**
     * Remove actividades
     *
     * @param DOFAActividad $actividades
     */
    public function removeActividade(DOFAActividad $actividades)
    {
        $this->actividades->removeElement($actividades);
    }

    /**
     * Get actividades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActividades()
    {
        return $this->actividades;
    }
}
