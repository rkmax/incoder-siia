<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as Unique;

/**
 * Accion
 *
 * @ORM\Table(name = "pa_diagnostico",
 *      uniqueConstraints={@UniqueConstraint(name="aspecto_nivel",columns={"aspecto_id", "nivel_id"})}
 * )
 * @Unique(
 *      fields={"aspecto", "nivel"},
 *      errorPath="aspecto_id",
 *      message="Este nivel ya fue agregado al aspecto"
 * )
 * @ORM\Entity
 */
class Diagnostico {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var Aspecto
     * @ORM\ManyToOne(targetEntity="Aspecto", cascade={"remove"})
     * @ORM\JoinColumn(name="aspecto_id", referencedColumnName="id", nullable=false)
     */
    private $aspecto;


    /**
     * @var Nivel
     *
     * @ORM\ManyToOne(targetEntity="Nivel", cascade={"remove"}, inversedBy="diagnostico")
     * @ORM\JoinColumn(name="nivel_id", referencedColumnName="id", nullable=false)
     */
    private $nivel;

    public function __toString()
    {
        return "{$this->getDescription()}";
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Accion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set aspecto
     *
     * @param Aspecto $aspecto
     * @return Accion
     */
    public function setAspecto(Aspecto $aspecto)
    {
        $this->aspecto = $aspecto;

        return $this;
    }

    /**
     * Get aspecto
     *
     * @return Aspecto
     */
    public function getAspecto()
    {
        return $this->aspecto;
    }

    /**
     * @return string
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @param string $nivel
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    }
} 
