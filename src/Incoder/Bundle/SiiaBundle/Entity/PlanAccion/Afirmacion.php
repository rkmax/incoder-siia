<?php

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\OpcionPorAfirmacion;

/**
 * Afirmacion
 *
 * @ORM\Table("pa_afirmacion")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\AfirmacionRepository")
 */
class Afirmacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var Aspecto
     * @ORM\ManyToOne(targetEntity="Aspecto", cascade={"remove"})
     * @ORM\JoinColumn(name="aspecto_id", referencedColumnName="id", nullable=false)
     */
    private $aspecto;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="OpcionPorAfirmacion", mappedBy="afirmacion")
     */
    private $opcion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Afirmacion
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Afirmacion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set aspecto
     *
     * @param Aspecto $aspecto
     * @return Afirmacion
     */
    public function setAspecto(Aspecto $aspecto)
    {
        $this->aspecto = $aspecto;
    
        return $this;
    }

    /**
     * Get aspecto
     *
     * @return Aspecto
     */
    public function getAspecto()
    {
        return $this->aspecto;
    }

    public function __toString()
    {
        return "({$this->getAspecto()}) {$this->getDescription()}";
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->opcion = new ArrayCollection();
    }
    
    /**
     * Add opcion
     *
     * @param OpcionPorAfirmacion $opcion
     * @return Afirmacion
     */
    public function addOpcion(OpcionPorAfirmacion $opcion)
    {
        $this->opcion[] = $opcion;
    
        return $this;
    }

    /**
     * Remove opcion
     *
     * @param OpcionPorAfirmacion $opcion
     */
    public function removeOpcion(OpcionPorAfirmacion $opcion)
    {
        $this->opcion->removeElement($opcion);
    }

    /**
     * Get opcion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOpcion()
    {
        return $this->opcion;
    }

    public function getMaxPuntaje()
    {
        $max = 0;

        foreach ($this->getOpcion() as $opcion) {
            $max = max($max, $opcion->getValor());
        }

        return $max;
    }
}
