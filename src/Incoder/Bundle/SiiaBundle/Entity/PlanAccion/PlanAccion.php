<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/18/14
 * Time: 7:18 PM
 */

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;


use Doctrine\Common\Collections\ArrayCollection;

class PlanAccion {

    /**
     * @var Afirmacion
     */
    private $afirmacion;

    private $opciones;

    public function __construct()
    {
        $this->opciones = new ArrayCollection();
    }

    public function getId()
    {
        return $this->getAfirmacion() ? $this->getAfirmacion()->getId() : null;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $opciones
     */
    public function setOpciones($opciones)
    {
        $this->opciones = $opciones;
    }

    /**
     * @return mixed
     */
    public function getOpciones()
    {
        return $this->opciones;
    }

    /**
     * @return \Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion
     */
    public function getAfirmacion()
    {
        return $this->afirmacion;
    }

    /**
     * @param \Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Afirmacion $afirmacion
     */
    public function setAfirmacion(Afirmacion $afirmacion)
    {
        $this->afirmacion = $afirmacion;
    }
} 
