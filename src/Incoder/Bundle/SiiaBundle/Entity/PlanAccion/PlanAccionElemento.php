<?php

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PlanAccionElemento
 *
 * @ORM\Table("pa_asociacion_elemento")
 * @ORM\Entity
 */
class PlanAccionElemento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Aspecto
     * @ORM\ManyToOne(targetEntity="Aspecto")
     * @ORM\JoinColumn(name="aspecto_id", referencedColumnName="id")
     */
    private $aspecto;

    /**
     * @var PlanAccionAsociacion
     * @ORM\ManyToOne(targetEntity="PlanAccionAsociacion", inversedBy="elementos", cascade={"persist"})
     * @ORM\JoinColumn(name="main_id", referencedColumnName="id")
     */
    private $planAccion;

    /**
     * @var Nivel
     * @ORM\ManyToOne(targetEntity="Nivel")
     * @ORM\JoinColumn(name="nivel_id", referencedColumnName="id")
     */
    private $nivel;

    /**
     * @var float
     * @ORM\Column(name="calificacion", type="decimal", scale=2)
     */
    private $calificacion;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->acciones = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set planAccion
     *
     * @param PlanAccionAsociacion $planAccion
     *
     * @return PlanAccionElemento
     */
    public function setPlanAccion(PlanAccionAsociacion $planAccion = null)
    {
        $this->planAccion = $planAccion;
    
        return $this;
    }

    /**
     * Get planAccion
     *
     * @return PlanAccionAsociacion
     */
    public function getPlanAccion()
    {
        return $this->planAccion;
    }

    public function getComponentes()
    {
        $componentes = [];

        /** @var Accion $accion */
        foreach ($this->nivel->getAccion() as $accion) {
            if ($accion->getComponente()) {
                $componentes[] = $accion->getComponente();
            }
        }

        return $componentes;
    }

    public function getJoinedDiagnosticos($glue = ',')
    {
        if ($this->nivel->getDiagnostico()->count() == 0) {
            return "No hay diagnosticos";
        }

        $aspecto = $this->aspecto;

        $diagnosticos = array_map(function (Diagnostico $diagnostico) {
            return $diagnostico->getDescription();
        }, $this->nivel->getDiagnostico()->filter(function(Diagnostico $diagnostico) use ($aspecto) {
            return $diagnostico->getAspecto() == $aspecto;
        })->toArray());

        return implode($glue, $diagnosticos);
    }

    public function getAcciones()
    {
        $acciones = new ArrayCollection();

        /** @var Accion $accion */
        foreach ($this->nivel->getAccion() as $accion) {
            if ($accion->getAspecto() == $this->aspecto) {
                $acciones->add($accion);
            }
        }

        return $acciones;
    }

    public function getJoinedAcciones($glue = ', ')
    {
        if ($this->getAcciones()->count() == 0) {
            return "No hay acciones";
        }

        $acciones = array_map(function (Accion $accion) {
            return $accion->getDescription();
        }, $this->getAcciones()->toArray());

        return implode($glue, $acciones);
    }

    /**
     * Set nivel
     *
     * @param Nivel $nivel
     *
     * @return PlanAccionElemento
     */
    public function setNivel(Nivel $nivel = null)
    {
        $this->nivel = $nivel;
    
        return $this;
    }

    /**
     * Get nivel
     *
     * @return Nivel
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * @return Aspecto
     */
    public function getAspecto()
    {
        return $this->aspecto;
    }

    /**
     * @param Aspecto $aspecto
     *
     * @return $this
     */
    public function setAspecto($aspecto)
    {
        $this->aspecto = $aspecto;

        return $this;
    }

    /**
     * @return float
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * @param float $calificacion
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    }
}
