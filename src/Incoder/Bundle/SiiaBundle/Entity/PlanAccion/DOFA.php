<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Entity\PlanAccion;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Gedmo\Mapping\Annotation\Timestampable;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class DOFA
 * @package Incoder\Bundle\SiiaBundle\Entity\PlanAccion
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\PlanAccion\DOFARepository")
 * @ORM\Table(
 *      name="dofa",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="idx_unica_actividad", columns={"asociacion_id", "actividad_id"})}
 * )
 * @ORM\HasLifecycleCallbacks()
 */
class DOFA {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Asociacion
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\Asociacion", inversedBy="dofa")
     */
    private $asociacion;

    /**
     * @var DOFAActividad
     * @ORM\ManyToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad", fetch="EAGER")
     * @NotNull
     */
    private $actividad;

    /**
     * @var bool
     * @ORM\Column(name="is_complete", type="boolean", options={"default":0}, nullable=true)
     */
    private $isComplete;

    /**
     * @var Datetime
     * @ORM\Column(type="datetime", name="create_at")
     * @Timestampable(on="create")
     */
    private $create;

    /**
     * @var Datetime
     * @ORM\Column(type="datetime", name="update_at")
     * @Timestampable(on="update")
     */
    private $update;


    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Timestampable(on="change", field="isComplete", value="true")
     */
    private $completedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set asociacion
     *
     * @param Asociacion $asociacion
     *
     * @return DOFA
     */
    public function setAsociacion(Asociacion $asociacion = null)
    {
        $this->asociacion = $asociacion;

        return $this;
    }

    /**
     * Get asociacion
     *
     * @return Asociacion
     */
    public function getAsociacion()
    {
        return $this->asociacion;
    }

    /**
     * Set isComplete
     *
     * @param boolean $isComplete
     * @return DOFA
     */
    public function setIsComplete($isComplete)
    {
        $this->isComplete = $isComplete;

        return $this;
    }

    /**
     * Get isComplete
     *
     * @return boolean
     */
    public function getIsComplete()
    {
        return $this->isComplete;
    }

    /**
     * Set actividad
     *
     * @param DOFAActividad $actividad
     *
     * @return DOFA
     */
    public function setActividad(DOFAActividad $actividad = null)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return DOFAActividad
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Get create
     *
     * @return \DateTime
     */
    public function getCreate()
    {
        return $this->create;
    }

    /**
     * Get update
     *
     * @return \DateTime
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * Get completedAt
     *
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateState()
    {
        if (!$this->isComplete) {
            $this->completedAt = null;
        }
    }
}
