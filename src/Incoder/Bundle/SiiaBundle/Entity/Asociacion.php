<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Accion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFAActividad;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionAsociacion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionElemento;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;
use Symfony\Component\Validator\Constraints\NotBlank;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Asociacion
 *
 * @ORM\Table("asociacion")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\AsociacionRepository")
 * @DForm("caracterizacion_asociacion")
 * @ExclusionPolicy("all")
 */
class Asociacion extends DFormEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Incoder\Bundle\SiiaBundle\Entity\PlanAccion\DOFA", mappedBy="asociacion", cascade={"remove"})
     */
    private $dofa;

    /**
     *
     * @var string
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     * @SyncValue("nombre_asociacion")
     * @NotBlank(message="El nombre de la asociacion no puede estar vacio o nulo")
     * @Expose
     */
    private $nombre;

    /**
     *
     * @var string
     * @ORM\Column(name="sigla", type="string", length=65, nullable=true)
     * @SyncValue("siglas_asociacion")
     * @NotBlank(message="La sigla de la asociacion no puede estar vacia o nula")
     * @Expose
     */
    private $sigla;

    /**
     *
     * @var string
     * @SyncValue("centro_poblado")
     * @ORM\Column(name="centro_poblado", type="string", length=255, nullable=true)
     */
    private $centroPoblado;

    /**
     *
     * @var string
     * @SyncValue("municipio")
     * @ORM\Column(name="municipio", type="string", length=255, nullable=true)
     */
    private $municipio;
    /**
     *
     * @var string
     * @SyncValue("departamento")
     * @ORM\Column(name="departamento", type="string", length=255, nullable=true)
     */
    private $departamento;

    /**
     * @var string
     * @ORM\Column(name="localizacion_asociacion", type="string", length=1024, nullable=true)
     * @SyncValue("localizacion_asociacion")
     * @NotBlank(message="La localizacion no puede estar vacia")
     */
    private $localizacion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DiagnosticoEmpresarial", mappedBy="asociacion", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $diagnosticoEmpresarial;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionAsociacion", mappedBy="asociacion", cascade={"remove"})
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $planAcciones;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DiagnosticoProductivo", mappedBy="asociacion", cascade={"remove"})
     */
    private $diagnosticoProductivo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DiagnosticoTecnico", mappedBy="asociacion", cascade={"remove"})
     */
    private $diagnosticoTecnico;

    /**
     * @ORM\OneToMany(targetEntity="Asociado", mappedBy="asociacion", cascade={"remove"})
     */
    private $asociado;

    /**
     * @var AsociacionProfile
     * @ORM\OneToOne(targetEntity="Incoder\Bundle\SiiaBundle\Entity\AsociacionProfile", mappedBy="asociacion")
     */
    private $perfil;

    /**
     *
     * @var ArrayCollection
     * @ManyToMany(targetEntity="Asesor", mappedBy="asociaciones")
     */
    private $asesores;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct() {
        $this->diagnosticoEmpresarial = new ArrayCollection();
        $this->diagnosticoProductivo = new ArrayCollection();
        $this->asociado = new ArrayCollection();
        $this->asesores = new ArrayCollection();
        $this->planAcciones = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add diagnosticoEmpresarial
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial $diagnosticoEmpresarial
     *
     * @return Asociacion
     */
    public function addDiagnosticoEmpresarial(DiagnosticoEmpresarial $diagnosticoEmpresarial) {
        $this->diagnosticoEmpresarial[] = $diagnosticoEmpresarial;

        return $this;
    }

    /**
     * Remove diagnosticoEmpresarial
     *
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial $diagnosticoEmpresarial
     */
    public function removeDiagnosticoEmpresarial(DiagnosticoEmpresarial $diagnosticoEmpresarial) {
        $this->diagnosticoEmpresarial->removeElement($diagnosticoEmpresarial);
    }

    /**
     * Get diagnosticoEmpresarial
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiagnosticoEmpresarial() {
        return $this->diagnosticoEmpresarial;
    }

    /**
     * Add diagnosticoProductivo
     *
     * @param DiagnosticoProductivo $diagnosticoProductivo
     * @return Asociacion
     */
    public function addDiagnosticoProductivo(DiagnosticoProductivo $diagnosticoProductivo) {
        $this->diagnosticoProductivo[] = $diagnosticoProductivo;

        return $this;
    }

    /**
     * Remove diagnosticoProductivo
     *
     * @param DiagnosticoProductivo $diagnosticoProductivo
     */
    public function removeDiagnosticoProductivo(DiagnosticoProductivo $diagnosticoProductivo) {
        $this->diagnosticoProductivo->removeElement($diagnosticoProductivo);
    }

    /**
     * Get diagnosticoProductivo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiagnosticoProductivo() {
        return $this->diagnosticoProductivo;
    }

    /**
     * Add asociado
     *
     * @param Asociado $asociado
     * @return Asociacion
     */
    public function addAsociado(Asociado $asociado) {
        $this->asociado[] = $asociado;

        return $this;
    }

    /**
     * Remove asociado
     *
     * @param Asociado $asociado
     */
    public function removeAsociado(Asociado $asociado) {
        $this->asociado->removeElement($asociado);
    }

    /**
     * get a diagnostico tecnico object
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiagnosticoTecnico() {
        return $this->diagnosticoTecnico;
    }

    /**
     * Add diagnostico tecnico
     *
     * @param DiagnosticoTecnico
     * @return Asociacion
     */
    public function addDiagnosticoTecnico(DiagnosticoTecnico $diagnostico) {
        $this->diagnosticoTecnico[] = $diagnostico;
        $diagnostico->setAsociacion($this);

        return $this;
    }

    /**
     * Remove asociado
     *
     * @param DiagnosticoTecnico $diagnostico
     */
    public function removeDiagnosticoTecnico(DiagnosticoTecnico $diagnostico) {
        $this->diagnosticoTecnico->removeElement($diagnostico);
    }

    /**
     *
     * @return ArrayCollection
     */
    public function getAsesores() {
        return $this->asesores;
    }

    /**
     *
     * @param Asesor $asesor
     */
    public function addAsesor(Asesor $asesor) {
        $asesor->addAsociacion($this);
        $this->asesores[] = $asesor;
    }

    /**
     *
     * @param Asesor $asesor
     */
    public function removeAsesor(Asesor $asesor) {
        $this->asesores->removeElement($asesor);
    }

    /**
     * Get asociado
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsociado() {
        return $this->asociado;
    }

    public function getMunicipio() {
        return $this->municipio;
    }

    public function getDepartamento() {
        return $this->departamento;
    }

    /**
     * @param mixed $municipio
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;
        $this->data['municipio'] = $municipio;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;
        $this->data['departamento'] = $departamento;
    }

    /**
     * @param string $localizacion
     */
    public function setLocalizacion($localizacion)
    {
        $this->localizacion = $localizacion;
    }

    /**
     * @return string
     */
    public function getLocalizacion()
    {
        return $this->localizacion;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function __toString() {
        if (array_key_exists("siglas_asociacion", $this->data)) {
            return "{$this->data['siglas_asociacion']}";
        }else{
            return "{$this->data['nombre_asociacion']}";
        }

        return "";
    }


    /**
     * Add asesores
     *
     * @param Asesor $asesores
     *
     * @return Asociacion
     */
    public function addAsesore(Asesor $asesores)
    {
        $this->asesores[] = $asesores;

        return $this;
    }

    /**
     * Remove asesores
     *
     * @param Asesor $asesores
     */
    public function removeAsesore(Asesor $asesores)
    {
        $this->asesores->removeElement($asesores);
    }

    public function addPlanAccione(PlanAccionAsociacion $plan)
    {
        if (!$this->planAcciones->contains($plan)) {
            $this->planAcciones[] = $plan;
        }
    }

    public function removePlanAccione(PlanAccionAsociacion $plan)
    {
        if ($this->planAcciones->contains($plan)) {
            $this->planAcciones->removeElement($plan);
        }
    }

    public function getPlanAcciones()
    {
        return $this->planAcciones;
    }

    public function getProductos()
    {
        $productos = [];

        /** @var Asociado $asociado */
        foreach ($this->getAsociado() as $asociado)
        {
            /** @var CuadroPecuario $cp */
            foreach ($asociado->getCuadroPecuario() as $cp)
            {
                $p = $cp->getProducto();
                $productos[$p->getId()] = "{$p}";
            }

            /** @var CuadroAgricola $cp */
            foreach ($asociado->getCuadroAgricola() as $cp)
            {
                $p = $cp->getProducto();
                $productos[$p->getId()] = "{$p}";
            }
        }

        return $productos;
    }

    /**
     * Add dofa
     *
     * @param PlanAccion\DOFA $dofa
     *
     * @return Asociacion
     */
    public function addDofa(PlanAccion\DOFA $dofa)
    {
        $this->dofa[] = $dofa;

        return $this;
    }

    /**
     * Remove dofa
     *
     * @param PlanAccion\DOFA $dofa
     */
    public function removeDofa(PlanAccion\DOFA $dofa)
    {
        $this->dofa->removeElement($dofa);
    }

    /**
     * Get dofa
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDofa()
    {
        return $this->dofa;
    }

    /**
     * @return PlanAccionAsociacion
     */
    public function getLastPlanAccion()
    {
        return $this->getPlanAcciones()->first();
    }

    public function getDebilidades()
    {
        $debilidades = new ArrayCollection();

        /** @var PlanAccionElemento $elemento */
        foreach ($this->getLastPlanAccion()->getElementos() as $elemento) {
            /** @var Accion $accion */
            foreach ($elemento->getAcciones() as $accion) {
                /** @var DOFAActividad $actividad */
                foreach ($accion->getActividades() as $actividad) {
                    foreach ($actividad->getDebilidades() as $debilidad) {
                        if (!$debilidades->contains($debilidad)) {
                            $debilidades->add($debilidad);
                        }
                    }
                }
            }
        }

        return $debilidades;
    }

    /**
     * @param string $sigla
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;
    }

    /**
     * @return string
     */
    public function getSigla()
    {
        return $this->sigla;
    }

    /**
     * @return string
     */
    public function getCentroPoblado()
    {
        return $this->centroPoblado;
    }

    /**
     * @param string $centroPoblado
     */
    public function setCentroPoblado($centroPoblado)
    {
        $this->centroPoblado = $centroPoblado;
        $this->data['centro_poblado'] = $centroPoblado;
    }



    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \Symfony\Component\Validator\Constraints\DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \Symfony\Component\Validator\Constraints\DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return AsociacionProfile
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * @param AsociacionProfile $perfil
     *
     * @return self
     */
    public function setPerfil($perfil)
    {
        $this->perfil = $perfil;

        return $this;
    }
}
