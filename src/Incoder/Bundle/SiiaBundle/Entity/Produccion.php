<?php

namespace Incoder\Bundle\SiiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormAnnotation as DForm;
use RRS\Bundle\DFormBundle\Lib\Annotation\DFormSyncValue as SyncValue;

/**
 * Produccion
 * @package Incoder\Bundle\SiiaBundle\Entity
 * @ORM\Table("dp_produccion")
 * @ORM\Entity(repositoryClass="Incoder\Bundle\SiiaBundle\Repository\DiagnosticoProductivoProduccionRepository")
 * @DForm("diagnostico_productivo_mes_produccion")
 */
class Produccion extends DFormEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * TODO: agregar un index con el mes y el producto
     * @var string mes del año
     * @ORM\Column(name="mes_productivo", type="string")
     * SyncValue("mes_productivo")
     */
    private $mes;

    /**
     * @var \DateTime
     * @ORM\Column(name="anio_productivo", type="date")
     * SyncValue("anio_productivo")
     */
    private $anio;

    /**
     * @var integer
     * @ORM\Column(name="area_sembrada", type="decimal", precision=17, scale=4, nullable=true)
     * @SyncValue("area_sembrada")
     */
    private $areaSembrada;

    /**
     * @var float
     * @ORM\Column(name="volumen_produccion", type="decimal", precision=17, scale=4, nullable=true)
     * @SyncValue("volumen_produccion")
     */
    private $volumenProduccion;

    /**
     *
     * @var string unidad
     * @ORM\Column(name="unidad_produccion", type="string", nullable=true)
     * @SyncValue("unidad_produccion")
     */
    private $unidad_produccion;

    /**
     * @var float
     * @ORM\Column(name="volumen_vendido", type="decimal", precision=17, scale=4, nullable=true)
     * @SyncValue("volumen_vendido")
     */
    private $volumenVendido;

    /**
     *
     * @var string unidad
     * @ORM\Column(name="unidad_venta", type="string", nullable=true)
     * @SyncValue("unidad_venta")
     */
    private $unidad_venta;

    /**
     * @var float
     * @ORM\Column(name="valor_unitario_maximo", type="decimal", precision=19, scale=5, nullable=true)
     * @SyncValue("valor_unitario_maximo")
     */
    private $valorUnitarioMaximo;
    
    /**
     * @var CostoProducto
     *
     * @ORM\ManyToOne(targetEntity="CostoProducto", cascade={"remove"}, inversedBy="mesesProductivos")
     * @ORM\JoinColumn(name="costo_producto_id", referencedColumnName="id", nullable=false)
     */
    private $costoProducto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * TODO: validar mes
     * @param string $mes
     */
    public function setMes($mes) {
        $this->mes = $mes;
    }
    
    /**
     * 
     * @return string
     */
    public function getMes() {
        return $this->mes;
    }
    
    /**
     * 
     * @return CostoProducto
     */
    public function getCostoProducto() {
        return $this->costoProducto;
    }
    
    /**
     * 
     * @param CostoProducto $costoProducto
     */
    public function setCostoProducto(CostoProducto $costoProducto = null) {
        $this->costoProducto = $costoProducto;
    }

    /**
     * Set areaSembrada
     *
     * @param integer $areaSembrada
     * @return Produccion
     */
    public function setAreaSembrada($areaSembrada)
    {
        $this->areaSembrada = $areaSembrada;
    
        return $this;
    }

    /**
     * Get areaSembrada
     *
     * @return integer 
     */
    public function getAreaSembrada()
    {
        return $this->areaSembrada;
    }

    /**
     * Set volumenProduccion
     *
     * @param float $volumenProduccion
     * @return Produccion
     */
    public function setVolumenProduccion($volumenProduccion)
    {
        $this->volumenProduccion = $volumenProduccion;
    
        return $this;
    }

    /**
     * Get volumenProduccion
     *
     * @return float 
     */
    public function getVolumenProduccion()
    {
        return $this->volumenProduccion;
    }

    /**
     * Set volumenVendido
     *
     * @param float $volumenVendido
     * @return Produccion
     */
    public function setVolumenVendido($volumenVendido)
    {
        $this->volumenVendido = $volumenVendido;
    
        return $this;
    }

    /**
     * Get volumenVendido
     *
     * @return float 
     */
    public function getVolumenVendido()
    {
        return $this->volumenVendido;
    }

    /**
     * Set valorUnitarioMaximo
     *
     * @param float $valorUnitarioMaximo
     * @return Produccion
     */
    public function setValorUnitarioMaximo($valorUnitarioMaximo)
    {
        $this->valorUnitarioMaximo = $valorUnitarioMaximo;
    
        return $this;
    }

    /**
     * Get valorUnitarioMaximo
     *
     * @return float 
     */
    public function getValorUnitarioMaximo()
    {
        return $this->valorUnitarioMaximo;
    }

    /**
     * @param \DateTime $anio
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;
    }

    /**
     * @return \DateTime
     */
    public function getAnio()
    {
        return $this->anio;
    }



}
