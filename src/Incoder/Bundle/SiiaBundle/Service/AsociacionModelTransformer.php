<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Service;



use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Elastica\Result;
use FOS\ElasticaBundle\Transformer\ElasticaToModelTransformerInterface;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Symfony\Component\Security\Core\SecurityContext;

class AsociacionModelTransformer implements ElasticaToModelTransformerInterface
{
    private $sc;

    private $om;

    function __construct(SecurityContext $sc, ObjectManager $om)
    {
        $this->sc = $sc;
        $this->om = $om;
    }

    /**
     * Transforms an array of elastica objects into an array of
     * model objects fetched from the doctrine repository
     *
     * @param array $elasticaObjects array of elastica objects
     * @return array of model objects
     **/
    function transform(array $elasticaObjects)
    {
        $qb = $this->om
            ->getRepository($this->getObjectClass())
            ->createQueryBuilder('o');

        $identifiers = array_map(function (Result $elasticaObject) {
            return $elasticaObject->getId();
        }, $elasticaObjects);

        $qb->where($qb->expr()->in('o.id', ':values'))
            ->setParameter('values', $identifiers);

        return $qb->getQuery()->execute();
    }

    function hybridTransform(array $elasticaObjects)
    {
        // TODO: Implement hybridTransform() method.
    }

    /**
     * Returns the object class used by the transformer.
     *
     * @return string
     */
    function getObjectClass()
    {
        return Asociacion::CLASS;
    }

    /**
     * Returns the identifier field from the options
     *
     * @return string the identifier field
     */
    function getIdentifierField()
    {
        return 'id';
    }
}