<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Service\PlanAccion;


use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\OpcionPorAfirmacion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccion;
use JulianReyes\Service\AbstractBaseService;

/**
 * Class MainEntityService
 * @package Incoder\Bundle\SiiaBundle\Service\PlanAccion
 */
class MainEntityService extends AbstractBaseService {

    public function getList($page, $limit, $params = array())
    {
        $entities = $this->om->getRepository('SiiaBundle:PlanAccion\Afirmacion')->findAllWithOptions();
        return $entities;
    }


    /**
     * @return mixed
     */
    public function prepareCreateEntity()
    {
        $entity = parent::prepareCreateEntity();
        $opciones = $this->om->getRepository('SiiaBundle:PlanAccion\Opcion')->findAll();

        $i = 0;
        foreach ($opciones as $opcion) {
            $o = new OpcionPorAfirmacion();
            $o->setOpcion($opcion);
            $o->setValor(++$i);

            $entity->getOpciones()->add($o);
        }

        return $entity;
    }

    protected function postCreateValid($entity)
    {
        $afirmacion = $entity->getAfirmacion();

        foreach ($entity->getOpciones() as $opcion) {
            $opcion->setAfirmacion($afirmacion);
            $this->om->persist($opcion);
        }

        $this->om->flush();
    }


    public function prepareEditEntity($id)
    {
        $afirmacion = $this->om->getRepository('SiiaBundle:PlanAccion\Afirmacion')->find($id);

        if (!$afirmacion) {
            throw $this->createNotFoundException(
                sprintf('Unable to find %s(%s) entity.', 'Afirmacion', $id)
            );
        }

        $entity = parent::prepareCreateEntity();
        $entity->setAfirmacion($afirmacion);
        $entity->setOpciones($afirmacion->getOpcion());

        return $entity;
    }

    /**
     * @param PlanAccion $entity
     */
    protected function postUpdateValid($entity)
    {
        $this->postCreateValid($entity);
    }


    public function createEditForm($entity, $options = array())
    {
        $options['id'] = $entity->getId();
        
        return parent::createEditForm($entity, $options);
    }


} 
