<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Service\PlanAccion;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarial;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Nivel;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionAsociacion;
use Incoder\Bundle\SiiaBundle\Entity\PlanAccion\PlanAccionElemento;
use Incoder\Bundle\SiiaBundle\Service\PlanAccion\Exception\PlanAccionException;

class PlanAccionService {

    /**
     * @var Collection
     */
    private $niveles;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * @param      $id
     * @param bool $force
     *
     * @return PlanAccionAsociacion
     *
     * @throws Exception\PlanAccionException
     */
    public function generateAndSave($id, $force = false)
    {
        $asociacion = $this->em->find('SiiaBundle:Asociacion', $id);

        if (!$asociacion) {
            throw PlanAccionException::asociacionNoExist();
        }

        $plan = $asociacion->getLastPlanAccion();

        if (!$plan || $force) {
            $generate = $this->generatePlanAccion($asociacion);

            $plan = new PlanAccionAsociacion();
            $plan->setAsociacion($asociacion);

            foreach ($generate['aspectos'] as $key => $aspecto)
            {
                $pe = new PlanAccionElemento();
                $pe->setAspecto($aspecto);
                $pe->setNivel($generate['niveles'][$key]);
                $pe->setCalificacion($generate['promedio'][$key]);

                $plan->addElemento($pe);

                $this->em->persist($pe);
            }

            $this->em->flush($plan);
            $this->em->flush();
        }

        return $plan;
    }

    public function generatePlanAccion(Asociacion $asociacion)
    {
        $dgE = $asociacion->getDiagnosticoEmpresarial()->last();

        if (!$dgE) {
            throw PlanAccionException::noDiagnosticoEmpresarial();
        }

        return $this->generatePlanAccionDE($dgE);
    }

    public function generatePlanAccionDE(DiagnosticoEmpresarial $dgE)
    {
        $totales = $max_values = $promedios = $niveles = $aspectos = $respuestas = [];

        /** @var DiagnosticoEmpresarialRespuesta $respuesta */
        foreach ($dgE->getRespuesta() as $respuesta)
        {
            // Inicializa por aspecto
            $aspecto = "{$respuesta->getAfirmacion()->getAspecto()->getId()}";
            if (!array_key_exists($aspecto, $totales)) {
                $aspectos[$aspecto] = $respuesta->getAfirmacion()->getAspecto();
                $totales[$aspecto] = 0;
                $promedios[$aspecto] = 0;
                $max_values[$aspecto] = $respuesta->getAfirmacion()->getMaxPuntaje();
                $respuestas[$aspecto] = 0;
            }

            // Acumula el valor por aspecto
            $totales[$aspecto] += $respuesta->getValorRespuesta();
            $respuestas[$aspecto]++;
        }

        foreach ($totales as $aspecto => $total)
        {
            $promedios[$aspecto] = ($total / $respuestas[$aspecto]) * 100 / $max_values[$aspecto];
            $niveles[$aspecto] = $this->findNivel($promedios[$aspecto]);
        }

        return [
            'promedio' => $promedios,
            'niveles' => $niveles,
            'aspectos' => $aspectos
        ];

    }

    /**
     * Obtiene el nivel basado en un valor entre 0 - 100
     *
     * @param float|integer $promedio valor entre 0 - 100
     *
     * @throws Exception\PlanAccionException
     *
     * @return Nivel|null
     */
    private function findNivel($promedio)
    {
        /** @var Nivel $nivel */
        foreach ($this->getNiveles() as $nivel) {
            if ($promedio >= $nivel->getFrom() && $promedio < $nivel->getTo()) {
                return $nivel;
            } else if ($promedio >= $nivel->getFrom() && $promedio == $nivel->getTo()) {
                return $nivel;
            }
        }

        throw PlanAccionException::accionNoEncontrada();
    }

    /**
     * @return array|Collection|\Incoder\Bundle\SiiaBundle\Entity\PlanAccion\Nivel[]
     */
    public function getNiveles()
    {
        if (!$this->niveles) {
            $this->niveles = $this->em->getRepository('SiiaBundle:PlanAccion\Nivel')->findAll();
        }

        return $this->niveles;
    }
}
