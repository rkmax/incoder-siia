<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Description of DiagnosticoTecnico
 *
 * @author German Rosales
 */
class DiagnosticoTecnicoService extends AbstractBaseWeakService {
    
    /**
     *
     * @var ArrayCollection 
     */
    private $planos;
    /**
     *
     * @var ArrayCollection 
     */
    private $caudales;
    /**
     *
     * @var ArrayCollection 
     */
    private $miembros;

    /**
     * configura el diagnostico tecnico como padre de un arreglo de hijos de este
     * @param array $children
     * @param \Incoder\Bundle\SiiaBundle\Entity\DiagnosticoTecnico $diagnostico
     * @return ArrayCollection 
     */
    protected function setDiagnosticoToChildren($children, DiagnosticoTecnico $diagnostico) {
        $newChildren = new ArrayCollection();
        foreach ($children as $child) {
            $child->setDiagnosticoTecnico($diagnostico);
            $newChildren->add($child);
        }
        return $newChildren;
    }

    /**
     * Elimina los viejos hijos debiles sino estan en el nuevo grupo
     * @param array $oldChildren
     * @param array $newChildren
     */
    protected function removeDiagnosticoChildren($oldChildren, $newChildren) {
        foreach ($oldChildren as $child) {
            if (false === $newChildren->contains($child)) {
                $this->remove($child);
            }
        }
    }
    
    /**
     * 
     * Override
     */
    public function prepareEditEntity($id)
    {
        /** @var DiagnosticoTecnico $entity */
        $entity = parent::prepareEditEntity($id);
        $this->planos = new ArrayCollection();
        $this->caudales = new ArrayCollection();
        $this->miembros = new ArrayCollection();

        foreach ($entity->getPlanos() as $plano) {
            $this->planos->add($plano);
        }
        
        foreach ($entity->getCaudales() as $caudal) {
            $this->caudales->add($caudal);
        }
        
        foreach ($entity->getMiembros() as $miembro) {
            $this->miembros->add($miembro);
        }

        return $entity;
    }

    /**
     * Override
     * @param DiagnosticoTecnico $entity
     */
    protected function postCreateValid($entity)
    {
        $this->quitarMiembrosSinNombre($entity);
        parent::postCreateValid($entity);
    }


    /**
     * Override
     * @param DiagnosticoTecnico $entity
     */
    protected function postUpdateValid($entity) {
        $this->quitarMiembrosSinNombre($entity);

        $asociacion = $this->validateRelatedEntity($this->relatedId);

        parent::postUpdateValid($entity);
        
        $this->removeDiagnosticoChildren($this->planos, $entity->getPlanos());  
        $this->removeDiagnosticoChildren($this->caudales, $entity->getCaudales());
        $this->removeDiagnosticoChildren($this->miembros, $entity->getMiembros());
    }

    public function createCreateForm($entity = null, $options = array()) {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_create', [
            'asociacion' => $this->relatedId
        ]);

        return parent::createCreateForm($entity, $options);
    }

    public function createEditForm($entity, $options = array()) {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_update', [
            'asociacion' => $this->relatedId,
            'id' => $entity->getId()
        ]);

        return parent::createEditForm($entity, $options);
    }

    public function prepareCreateEntity($relatedId = null)
    {
        $entity = parent::prepareCreateEntity($relatedId);
        return $entity;
    }

    /**
     * @param DiagnosticoTecnico $entity
     */
    protected function quitarMiembrosSinNombre(&$entity)
    {
        foreach ($entity->getMiembros() as $miembro) {
            if (!$miembro->getNombre()) {
                $entity->removeMiembro($miembro);
            }
        }
    }
}
