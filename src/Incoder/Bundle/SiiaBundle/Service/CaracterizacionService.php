<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Incoder\Bundle\SiiaBundle\Repository\AsociacionRepository;
use JulianReyes\Service\AbstractBaseService;
use Monolog\Logger;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;

class CaracterizacionService extends AbstractBaseService {

    private $ps;
    /**
     * @var Logger $logger
     */
    private $logger;

    public function __construct(ObjectManager $om, Router $router, FormFactory $factory, SecurityContext $sc, TransactionService $ts, ProfileService $ps, Logger $logger)
    {
        $this->ps = $ps;
        $this->logger = $logger;
        parent::__construct($om, $router, $factory, $sc, $ts);
    }

    protected function postUpdateValid($entity)
    {
        /** @var Asociacion $entity */
        // https://bitbucket.org/rkmax/incoder-siia/issue/276/generador-de-indicadores-falta-campo
        $entity->numero_de_usuarios = $entity->getAsociado()->count();
        parent::postUpdateValid($entity);
    }


    /**
     * @param Asociacion $entity
     */
    protected function postCreateValid($entity)
    {

        if ($this->getUser()->hasRole(BaseProfile::ASESOR)) {
            $asesor = $this->getUser()->getAsesor();
        } else {
            $asesor = null;
        }

        $filter =  function(Asesor $asesor) {
            return $asesor->getSecurityUser() == null;
        };

        if ($this->getUser()->hasRole(BaseProfile::OPERADOR)) {
            $asesorOperador = $this->getUser()->getOperador()->getAsesor()->filter($filter)->first();
        } else {
            $asesorOperador = $asesor->getOperador()->getAsesor()->filter($filter)->first();
        }

        // Asigna la asociacion al que la crea Operador/Asesor
        if ($asesor) {
            $this->om->persist($asesor);
            $entity->addAsesor($asesor);
        }

        if ($asesorOperador) {
            $this->om->persist($asesorOperador);
            $entity->addAsesor($asesorOperador);
        }


        // TODO: Logica para crear el usuario de la asociacion

        parent::postCreateValid($entity);
    }

    public function getList($page, $limit, $params = array())
    {
        $asesores_id = [];
        /* @var AsociacionRepository $repository */
        $repository = $this->getRepository();

        if ($this->getUser()->hasRole(BaseProfile::ADMIN) || $this->getUser()->hasRole(BaseProfile::SUPERVISOR) ) {
            if  (method_exists($repository, 'findAllPaginated')) {
                return $repository->findAllPaginated($page, $limit, $params);
            } else {
                return $repository->findAll();
            }
        } else if ($this->getUser()->hasRole(BaseProfile::OPERADOR)) {
            if (isset($params['asesor']) && !empty($params['asesor'])) {
                $asesores_id[] = $params['asesor'];
            } else {
                foreach ($this->getUser()->getOperador()->getAsesor() as $asesor) {
                    $asesores_id[] = $asesor->getId();
                }
            }
        } else if ($this->getUser()->hasRole(BaseProfile::ASESOR)) {
            $asesores_id[] = $this->getUser()->getAsesor()->getId();
        }

        return $repository->findAllByAsesor($asesores_id);
    }

    /**
     * @return array|\Incoder\Bundle\SiiaBundle\Entity\Asociacion[]
     */
    public function findAll(){
        return $this->getRepository()->findAll();
    }
}
