<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Service\Productivo;


use Incoder\Bundle\SiiaBundle\Entity\Productivo\Producto;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoAgricola;
use Incoder\Bundle\SiiaBundle\Entity\Productivo\ProductoPecuario;
use JulianReyes\Service\AbstractBaseService;

class ProductoService extends AbstractBaseService
{
    public function prepareCreateEntity()
    {
        return new Producto();
    }

    public function prepareEditEntity($id)
    {
        $entity = parent::prepareEditEntity($id);

        return Producto::createFromProductoEspecializado($entity);
    }


    /**
     * @param Producto $entity
     */
    protected function postCreateValid($entity)
    {
        $entity = $entity->createProductoEspecializado();
        parent::postCreateValid($entity);
    }

    /**
     * @param Producto $entity
     *
     * @throws \Exception
     */
    protected function postUpdateValid($entity)
    {
        switch ($entity->getTipo()) {
            case Producto::TAGRICOLA:
                $class = ProductoAgricola::CLASS;
                break;
            case Producto::TPECUARIO:
                $class = ProductoPecuario::CLASS;
                break;
            default:
                throw new \Exception("Tipo de producto no definido");
        }
        $specialProducto = $this->om->find($class, $entity->getId());
        $entity = $entity->createProductoEspecializado($specialProducto);

        parent::postUpdateValid($entity);
    }
}