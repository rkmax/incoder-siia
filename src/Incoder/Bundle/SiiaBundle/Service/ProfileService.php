<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Service;

use FOS\UserBundle\Doctrine\GroupManager;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use FOS\UserBundle\Doctrine\UserManager;

/**
 * Description of ProfileHelper
 *
 * @author chamanx
 */
class ProfileService {
    
    /**
     * @var UserManager
     */
    private $um;
    
    public function __construct(UserManager $userManager) {
        $this->um = $userManager;
    }

    protected function setUp()
    {
        $groupManager = $this->getGroupManager();
        $groups = ['ROLE_OPERADOR', 'ROLE_ASESOR'];

        foreach ($groups as $groupName) {
            $group = $groupManager->findGroupByName($groupName);
            if (!$group) {
                $group = $groupManager->createGroup($groupName);
                // fix: el constructor de la clase abstracta no esta seteando el nombre
                $group->setName($groupName);
                $groupManager->updateGroup($group);
            }
        }
    }

    public function loadUser(BaseProfile $entity)
    {
        $user = $entity->getSecurityUser();
        $entity->setEmail($user->getEmail());
        $entity->setUser($user->getUsername());
    }

    public function createUser(BaseProfile $entity, $roleName)
    {
        //TODO: Los grupos no se han definido, habilitarlos luego
        //$this->setUp();
        //$groupManager = $this->getGroupManager();
        //$group = $groupManager->findGroupBy(['name' => $this->getGroupName()]);

        $user = $this->um->createUser();

        //$user->addGroup($group);

        $user->setUsername($entity->getUser());
        $user->setEmail($entity->getEmail());
        $user->setPlainPassword($entity->getPassword());
        $user->setEnabled(true);
        $user->addRole($roleName);

        $entity->setSecurityUser($user);

        $this->um->updateUser($user);
    }

    public function updateUser(BaseProfile $entity)
    {
        $user = $entity->getSecurityUser();
        $user->setUsername($entity->getUser());
        $user->setEmail($entity->getEmail());
        if (!empty($entity->getPassword())) {
            $user->setPlainPassword($entity->getPassword());
        }

        $this->um->updateUser($user, false);
    }
}
