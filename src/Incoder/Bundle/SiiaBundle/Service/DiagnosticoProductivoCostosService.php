<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Incoder\Bundle\SiiaBundle\Entity\Produccion;
use JulianReyes\Service\AbstractBaseService;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Description of DiagnosticoProductivoCostosService
 *
 * @author chamanx
 */
class DiagnosticoProductivoCostosService extends AbstractBaseWeakService {

    private $meses;

    public function createCreateForm($entity = null, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_create', [
            'diagnosticoProductivo' => $this->relatedId
        ]);

        return parent::createCreateForm($entity, $options);
    }

    public function createEditForm($entity, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_update', [
            'diagnosticoProductivo' => $this->relatedId,
            'id' => $entity->getId()
        ]);

        return parent::createEditForm($entity, $options);
    }

    public function prepareEditEntity($id)
    {
        $entity = parent::prepareEditEntity($id);

        $this->meses = new ArrayCollection();

        foreach ($entity->getMesesProductivos() as $mes) {
            $this->meses->add($mes);
        }

        return $entity;
    }


    protected function postUpdateValid($entity)
    {
        foreach ($this->meses as $mes) {
            if (false === $entity->getMesesProductivos()->contains($mes)) {
                $mes->setCostoProducto();
                $this->remove($mes);
            }
        }

        parent::postUpdateValid($entity);
    }


}
