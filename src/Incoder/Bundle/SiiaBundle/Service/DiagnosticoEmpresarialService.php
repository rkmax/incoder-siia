<?php

namespace Incoder\Bundle\SiiaBundle\Service;
use Incoder\Bundle\SiiaBundle\Entity\DiagnosticoEmpresarialRespuesta;
use JulianReyes\Service\AbstractBaseService;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Service for persistence and query "Diagnostico empresarial" related data
 *
 * @author chamanx
 */
class DiagnosticoEmpresarialService extends AbstractBaseWeakService {

    public function prepareCreateEntity($relatedId = null)
    {
        $entity = parent::prepareCreateEntity($relatedId);
        $afirmaciones = $this->om->getRepository('SiiaBundle:PlanAccion\Afirmacion')->findAllWithOptions();

        foreach ($afirmaciones as $afirmacion) {
            $respuesta = new DiagnosticoEmpresarialRespuesta();
            $respuesta->setAfirmacion($afirmacion);
            $entity->addRespuesta($respuesta);
        }

        return $entity;
    }

    public function createCreateForm($entity = null, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_create', [
            'asociacion' => $this->relatedId
        ]);

        return parent::createCreateForm($entity, $options);
    }

    public function createEditForm($entity, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_update', [
            'asociacion' => $this->relatedId,
            'id' => $entity->getId()
        ]);

        return parent::createEditForm($entity, $options);
    }

    protected function postCreateValid($entity)
    {
        foreach ($entity->getRespuesta() as $respuesta) {
            $respuesta->setDiagnostico($entity);
            $this->persist($respuesta);
        }

        parent::postCreateValid($entity);
    }


}
