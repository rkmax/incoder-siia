<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 3/29/14
 * Time: 4:08 PM
 */

namespace Incoder\Bundle\SiiaBundle\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Incoder\Bundle\SiiaBundle\DataView\ProductivoGeneralDataView;
use Incoder\Bundle\SiiaBundle\Repository\CuadroProductivoRepository;

class ConsolidadoProductivoService {

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param \Doctrine\Common\Persistence\ObjectManager|\Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    /**
     * @param $departamento
     * @param $asociacion
     * @param $municipio
     * @param $fechaini
     * @param $fechafin
     * @return array
     */
    public function getReporteGeneral($departamento, $asociacion, $municipio, $fechaini, $fechafin)
    {
        $viewarray = $this->getCuadrosAgricolasConsolidados($departamento, $asociacion, $municipio, $fechaini, $fechafin);
        //$viewarray[] = $this->getCuadrosPecuariosConsolidados($departamento, $asociacion, $municipio, $fechaini, $fechafin);
        return $viewarray;
    }

    /**
     * @param $departamento
     * @param $asociacion
     * @param $municipio
     * @param $fechaini
     * @param $fechafin
     * @return array
     */
    protected function getCuadrosAgricolasConsolidados($departamento, $asociacion, $municipio, $fechaini, $fechafin)
    {
        $qb = $this->em->createQueryBuilder();
        $qb ->addSelect('p')
            ->join('ca.producto','p');
        /*
            ->addSelect('sum(ca.volumenProduccion) as sumVP')
            ->addSelect('sum(ca.volumenVendido) as sumVV');*/

        /*if($asociacion){
            $qb->addSelect('ca.asociado.asociacion');
        }*/

        /*if($asociacion){
            $qb ->andWhere("cp.asociado.asociacion.nombre like '%?1%'")
                ->setParameter(1, $asociacion);
        }
        if($departamento){
            $qb ->andWhere("cp.asociado.asociacion.localizacion like '%?1%'")
                ->setParameter(1, $departamento);
        }
        if($municipio){
            $qb ->andWhere("cp.asociado.asociacion.localizacion like '%?1%'")
                ->setParameter(1, $municipio);
        }*/
        $qb->addGroupBy('ca.producto');
        $results = $qb->getQuery()->getArrayResult();
        //Convertir al view
        $viewarray = [];
        $view = new ProductivoGeneralDataView();
        foreach($results as $result){
            /*if($asociacion){
                $view->setAsociacion($result['asociacion']);
            }*/
            $view->setProducto($result['producto']);
            /*$view->setVolumenProducido($result['sumVP']);
            $view->setVolumenvendido($result['sumVV']);*/
            $viewarray[] = $view;
        }
        return $viewarray;
    }

}