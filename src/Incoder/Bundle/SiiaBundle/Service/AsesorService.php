<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Service;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Description of AsesorService
 *
 * @author chamanx
 */
class AsesorService extends AbstractBaseWeakService {
    
    /**
     * TODO: deprecar este y usar el del operador service
     * @param int $userid
     * @return \Incoder\Bundle\SiiaBundle\Entity\Operador
     */
    public function getOneOperadorByUser($userid) {
        return $this->om
                ->getRepository('Incoder\Bundle\SiiaBundle\Entity\Operador')
                ->finOneByUser($userid);
    }

    private function getAllAsesores()
    {
        $asesores_id = [];

        foreach ($this->getUser()->getOperador()->getAsesor() as $asesor) {
            $asesores_id[] = $asesor->getId();
        }

        return $asesores_id;
    }

    /**
     * @param Asesor $entity
     * @param array $options
     * @return \Symfony\Component\Form\Form
     */
    public function createEditForm($entity, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_update', [
            'id' => $entity->getId()
        ]);
        $options['password_required'] = false;
        $options['asesores'] = $this->getAllAsesores();

        return parent::createEditForm($entity, $options);
    }

    public function createCreateForm($entity = null, $options = array())
    {
        $options['asesores'] = $this->getAllAsesores();

        return parent::createCreateForm($entity, $options);
    }


    public function prepareEditEntity($id)
    {
        $entity = parent::prepareEditEntity($id);
        $su = $entity->getSecurityUser();
        $entity->setUser($su->getUsername());
        $entity->setEmail($su->getEmail());

        return $entity;
    }

    public function getList($page, $limit, $params = array())
    {
        $repository = $this->getRepository();

        $operador = $this->getUser()->getOperador()->getId();
        return $repository->findAllByOperador($operador);
    }
}
