<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 3/3/14
 * Time: 6:38 AM
 */

namespace Incoder\Bundle\SiiaBundle\Service\Asociado;


use Doctrine\Common\Collections\ArrayCollection;
use Incoder\Bundle\SiiaBundle\Entity\CuadroProductivo;
use Incoder\Bundle\SiiaBundle\Entity\Insumo;
use JulianReyes\Service\AbstractBaseService;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Class CuadroProductivo
 * @package Incoder\Bundle\SiiaBundle\Service\Asociado
 */
abstract class CuadroProductivoService extends AbstractBaseWeakService{
    /**
     * @var ArrayCollection
     */
    protected  $insumos;

    /**
     * Elimina los viejos hijos debiles sino estan en el nuevo grupo
     * @param array $oldChildren
     * @param array $newChildren
     */
    protected function removeChildren($oldChildren, $newChildren) {
        foreach ($oldChildren as $child) {
            if (false === $newChildren->contains($child)) {
                $this->remove($child);
            }
        }
    }

    /**
     * @param int $relatedId
     * @return CuadroProductivo|mixed
     */
    public function prepareCreateEntity($relatedId = null)
    {
        /* @var CuadroProductivo $entity */
        $entity = parent::prepareCreateEntity($relatedId);
        $entity->addInsumo(new Insumo());
        return $entity;
    }

    /**
     *
     * Override
     */
    public function prepareEditEntity($id)
    {
        /** @var CuadroProductivo $entity */
        $entity = parent::prepareEditEntity($id);
        $this->insumos = new ArrayCollection();

        foreach ($entity->getInsumos() as $insumo) {
            $this->insumos->add($insumo);
        }
        //Si aun no tiene insumos
        if($entity->getInsumos() && $entity->getInsumos()->count() == 0){
            $entity->addInsumo(new Insumo());
        }

        return $entity;
    }

    /**
     * Override
     * @param CuadroProductivo $entity
     */
    protected function postUpdateValid($entity) {
        $this->quitarInsumosSinNombre($entity);
        parent::postUpdateValid($entity);
        $this->removeChildren($this->insumos, $entity->getInsumos());
    }

    protected function postCreateValid($entity)
    {
        $this->quitarInsumosSinNombre($entity);
        parent::postCreateValid($entity);
    }


    /**
     * Override
     */
    public function createCreateForm($entity = null, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_create', [
            'asociado' => $this->relatedId
        ]);

        return parent::createCreateForm($entity, $options);
    }

    /**
     * @param $entity
     */
    protected function quitarInsumosSinNombre(&$entity)
    {
        foreach ($entity->getInsumos() as $insumo) {
            if (!$insumo->getNombre()) {
                $entity->removeInsumo($insumo);
            }
        }
    }

} 