<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 2/28/14
 * Time: 10:21 PM
 */

namespace Incoder\Bundle\SiiaBundle\Service\Asociado;


use Doctrine\Common\Collections\ArrayCollection;
use Incoder\Bundle\SiiaBundle\Entity\CuadroPecuario;
use Incoder\Bundle\SiiaBundle\Entity\CuadroProductivo;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Class CuadroPecuarioService
 * @package Incoder\Bundle\SiiaBundle\Service\Asociado
 */
class CuadroPecuarioService extends CuadroProductivoService{
    /**
     * @param CuadroPecuario $entity
     */
    protected function postUpdateValid($entity)
    {
        $entity->setFechaActualizacion($date = date('Y-m-d'));
        parent::postUpdateValid($entity);
    }

    /**
     * @param CuadroPecuario $entity
     */
    protected function postCreateValid($entity)
    {
        $entity->setFechaActualizacion($date = date('Y-m-d'));
        parent::postCreateValid($entity);
    }


} 