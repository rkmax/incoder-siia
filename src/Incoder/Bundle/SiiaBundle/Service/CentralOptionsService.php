<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Service;


use Incoder\Bundle\SiiaBundle\Entity\BaseProfile;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Security\Core\SecurityContext;

class CentralOptionsService extends \Twig_Extension
{
    /**
     * @var SecurityContext
     */
    private $sc;

    /**
     * @var Translator
     */
    private $i10n;

    function __construct(SecurityContext $sc, Translator $tranlator)
    {
        $this->sc = $sc;
        $this->i10n = $tranlator;
    }

    /**
     * Devuelve todas las acciones del panel principal
     * Segun los permisos del usuario
     */
    public function getFilteredActions()
    {
        $context = $this->sc;
        return array_filter($this->getActions(), function($action) use ($context) {
            return $context->isGranted($action['roles']);
        });
    }


    /**
     * Devuelve todos las acciones del panel principal
     *
     * @return array
     */
    public function getActions() {
        $i10n = $this->i10n;

        $actions = [
            [
                'title' => $i10n->trans('menu.g_asociacion'),
                'icon' => 'icn_asociacion',
                'route' => 'asociacion_caracterizacion',
                'roles' => [BaseProfile::ASESOR, BaseProfile::SUPERVISOR]
            ],
        ];
        $actions[]=
            [
                'title' => $i10n->trans('consolidado.productivo.general.ver'),
                'icon' => 'icn_indicadores',
                'route' => 'consolidado_general',
                'roles' => [BaseProfile::ASESOR, BaseProfile::SUPERVISOR],
            ];
        $actions[]=
            [
                'title' => 'Generador de indicadores',
                'icon' => 'icn_generador',
                'route' => 'siia_indicadores_generador',
                'roles' => [BaseProfile::ASESOR, BaseProfile::SUPERVISOR],
            ];
        $actions[]=
            [
                'title' => $i10n->trans('menu.g_operadores'),
                'icon' => 'icn_g_operador',
                'route' => 'operador',
                'roles' => [BaseProfile::ADMIN],
            ];
        $actions[]=
            [
                'title' => $i10n->trans('menu.g_asesores'),
                'icon' => 'icn_g_asesor',
                'route' => 'asesor',
                'roles' => [BaseProfile::OPERADOR],
            ];
        $actions[]=
            [
                'title' => $i10n->trans('menu.a_plan'),
                'icon' => 'icn_g_plan_accion',
                'route' => 'plan-accion',
                'roles' => [BaseProfile::ADMIN],
            ];
        $actions[]=
            [
                'title' => $i10n->trans('menu.g_productiva'),
                'icon' => 'icn_g_productiva',
                'route' => 'productivo_main',
                'roles' => [BaseProfile::ADMIN]
            ];
        $actions[]=
            [
                'title' => $i10n->trans('menu.ver_plan'),
                'icon' => 'icn_v_plan_accion',
                'route' => 'plan_accion_ver',
                'roles' => [BaseProfile::ASESOR, BaseProfile::SUPERVISOR],
            ];

        foreach ($actions as $key => $action) {
            // /bundles/siia/img/{$action['icon']}.png
            $actions[$key]['image'] = "@SiiaBundle/Resources/public/img/{$action['icon']}.png";
        }

        return $actions;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'central_options';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('actions', array($this, 'getActions')),
            new \Twig_SimpleFunction('filtered_actions', array($this, 'getFilteredActions')),
        ];
    }
}