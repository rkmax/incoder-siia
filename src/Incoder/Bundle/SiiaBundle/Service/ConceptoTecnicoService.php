<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 11/03/14
 * Time: 09:55 AM
 */

namespace Incoder\Bundle\SiiaBundle\Service;

use Incoder\Bundle\SiiaBundle\Entity\ConceptoTecnico;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Class ConceptoTecnicoService
 * @package Incoder\Bundle\SiiaBundle\Service
 */
class ConceptoTecnicoService extends AbstractBaseWeakService{

    /**
     * Overridden
     * @param ConceptoTecnico $entity
     * @param array $options
     * @return \Symfony\Component\Form\Form
     */
    public function createCreateForm($entity = null, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_create', [
            'diagnostico' => $this->relatedId
        ]);

        return parent::createCreateForm($entity, $options);
    }

    /**
     * @param ConceptoTecnico $entity
     */
    protected function postCreateValid($entity)
    {
        $now = new \DateTime();
        $now->format('Y-m-d H:i:s');    // MySQL datetime format
        $entity->setFecha($now);
        parent::postCreateValid($entity);
    }

    /**
     * @param ConceptoTecnico $entity
     */
    protected function postUpdateValid($entity)
    {
        $now = new \DateTime();
        $now->format('Y-m-d H:i:s');    // MySQL datetime format
        $entity->setFecha($now);
        parent::postUpdateValid($entity);
    }


} 