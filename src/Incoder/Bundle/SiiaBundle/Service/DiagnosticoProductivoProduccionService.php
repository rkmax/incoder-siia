<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Service;
use JulianReyes\Service\AbstractBaseWeakService;

/**
 * Description of DiagnosticoProductivoProduccion
 *
 * @author chamanx
 */
class DiagnosticoProductivoProduccionService extends AbstractBaseWeakService {
    
    public function createCreateForm($entity = null, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_create', [
            'producto' => $this->relatedId
        ]);

        return parent::createCreateForm($entity, $options);
    }

    public function createEditForm($entity, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_update', [
            'producto' => $this->relatedId,
            'id' => $entity->getId()
        ]);

        return parent::createEditForm($entity, $options);
    }
}
