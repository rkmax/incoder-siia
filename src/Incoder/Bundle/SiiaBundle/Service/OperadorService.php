<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Incoder\Bundle\SiiaBundle\Service;
use JulianReyes\Service\EntityService;

/**
 * Description of OperadorService
 *
 * @author chamanx
 */
class OperadorService extends EntityService {
    
    public function createCreateForm($entity = null, $options = array())
    {
        return parent::createCreateForm($entity, $options);
    }

    public function prepareEditEntity($id)
    {
        $entity = parent::prepareEditEntity($id);
        $su = $entity->getSecurityUser();
        $entity->setUser($su->getUsername());
        $entity->setEmail($su->getEmail());

        return $entity;
    }

    public function createEditForm($entity, $options = array())
    {
        $options['action'] = $this->generateUrl($this->getBaseRoute() . '_update', [
            'id' => $entity->getId()
        ]);
        $options['password_required'] = false;

        return parent::createEditForm($entity, $options);
    }

    /**
     * Obtiene un operador por el nombre de usuario
     *
     * @param $userName
     *
     * @return \Incoder\Bundle\SiiaBundle\Entity\Operador
     */
    public function findOneByUserName($userName) {
        return $this->getRepository()->finOneByUserName($userName);
    }
    
    /**
     * Obtiene un operador por el usuario relacionado
     * @param string $userid
     * @return \Incoder\Bundle\SiiaBundle\Entity\Operador
     */
    public function findOneByUser($userid) {
        return $this->getRepository()->finOneByUser($userid);
    }
}
