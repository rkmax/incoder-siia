<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Incoder\Bundle\SiiaBundle\Listener;


use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use FOS\ElasticaBundle\Persister\ObjectPersister;
use Incoder\Bundle\SiiaBundle\Entity\Asesor;
use Incoder\Bundle\SiiaBundle\Entity\Asociacion;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ListingAsociacionListener
{

    /**
     * @var ObjectPersister
     */
    private $asociacionPersister;

    private $properyAccess;

    function __construct(ObjectPersister $asociacionPersister)
    {
        $this->asociacionPersister = $asociacionPersister;
        $this->properyAccess = PropertyAccess::createPropertyAccessor();
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        $uow = $args->getEntityManager()->getUnitOfWork();

        if (isset($uow->getIdentityMap()[Asociacion::CLASS])) {
            $entities = $uow->getIdentityMap()[Asociacion::CLASS];
            $this->asociacionPersister->replaceMany($entities);
        }
    }
}