<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace JulianReyes\Lib\Listener;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class EntityControllerListener {
    /**
     * @var Reader
     */
    private $annotationReader;

    public function __construct(Reader $reader){
        $this->annotationReader = $reader;
    }

    public function onFilterController(FilterControllerEvent $event)
    {

        list($object, ) = $event->getController();

        $className = ClassUtils::getClass($object);
        $reflectionClass = new \ReflectionClass($className);

        $entityAnnotation = $this->annotationReader->getClassAnnotation(
            $reflectionClass,
            'JulianReyes\Lib\Annotation\EntityControllerAnnotation'
        );

        if ($entityAnnotation) {
            call_user_func([$object, 'setControllerType'], $entityAnnotation->getControllerType());
        }
    }
} 
