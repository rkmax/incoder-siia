<?php

namespace JulianReyes\Lib\Pagination;

use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;

/**
 * @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 */
class Paginator
{
    function __construct(DoctrinePaginator $doctrinePaginator) {
        $this->doctrinePaginator = $doctrinePaginator;
    }

    public function getPageResults($page = 1, $maxPerPage = 10)
    {
        $query = $this->doctrinePaginator->getQuery();

        $this->doctrinePaginator->getQuery()->setMaxResults($maxPerPage);
        $this->total = $total = $this->doctrinePaginator->count();
        $this->totalPages = ceil($total / $maxPerPage);

        $page = min($page, $this->totalPages);
        $offset = ($page - 1) * $maxPerPage;
        $this->doctrinePaginator->getQuery()->setFirstResult($offset);
        $this->page = $page;

        return $this->doctrinePaginator;
    }

    public function getPage()
    {
        return isset($this->page) ? $this->page : 0;
    }

    public function getTotalPages()
    {
        return isset($this->totalPages) ? $this->totalPages : 0;
    }

    public function getTotal()
    {
        return isset($this->total) ? $this->total : 0;
    }

    public function getPagination($maxLinks = 9)
    {
        $counter = 1;
        $pages = array();
        $page = $this->page;
        $middle = floor($maxLinks / 2);
        $direccion = -1;
        $flip = false;

        $inf = $this->page - $middle;
        $sup = $this->page + $middle;

        if (!$this->inBounds($sup)) {
            $inf -= abs(abs($sup) - $this->totalPages);
            $inf = max($inf, 1);
            $sup = $this->totalPages;
        }

        if (!$this->inBounds($inf)) {
            $sup += abs(abs($inf) - 1);
            $sup = min($sup, $this->totalPages);
            $inf = 1;
        }

        for ($page = $inf; $page <= $sup ; $page++) {
            $pages[$page] = array(
                'num' => $page,
                'class' => $page == $this->page ? 'active' : ''
            );
        }

        $prev = max($this->page - 1, 1);
        $next = min($this->page + 1, $this->totalPages);

        return array(
            'prev' => array(
                'num' => $prev,
                'class' => $this->page == 1 ? 'disabled' : ''
            ),
            'next' => array(
                'num' => $next,
                'class' => $this->page == $this->totalPages ? 'disabled' : ''
            ),
            'pages' => $pages
        );
    }

    private function inBounds($page)
    {
        return $page <= $this->totalPages && $page >= 1;
    }
}
