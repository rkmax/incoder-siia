<?php
/**
 * @author Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 */

namespace JulianReyes\Lib;

/**
 * Class ParseUtils
 *
 * @package JulianReyes\Lib\Pagination
 */
class ParseUtils
{
    /**
     * @param array $array
     *
     * @return bool|\stdClass
     */
    public static function array2Object(array $array)
    {
        if (count($array) > 0) {
            $object = new \stdClass();
            foreach ($array as $key => $value) {
                $key = trim($key);
                $object->$key = is_array($value) ? call_user_func(__METHOD__, $value) : $value;
            }

            return $object;
        }

        return false;
    }

    /**
     * Strip accents (àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ) from a given string
     * @param $str
     * @return string
     */
    public static function replaceAccents($str)
    {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    /**
     * comparacion sin acentos ni mayusculas
     * @param $value1
     * @param $value2
     * @param $format
     * @return int
     */
    public static function unsensibleLevenshteinCompare($value1, $value2, $format = 'UTF-8')
    {
        $trnval = ParseUtils::replaceAccents(mb_strtolower($value1, $format));
        $domainval = ParseUtils::replaceAccents(mb_strtolower($value2, $format));
        $lev = levenshtein($trnval, $domainval);
        return $lev;
    }
}
