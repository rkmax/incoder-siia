<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace JulianReyes\Service;


use Doctrine\Common\Persistence\ObjectManager;
use Incoder\Bundle\SiiaBundle\Service\ConfigurableServiceInterface;
use RRS\Bundle\DFormBundle\Entity\DFormEntity;
use RRS\Bundle\DFormBundle\Service\TransactionService;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class AbstractBaseService
 * @package JulianReyes\Service
 */
abstract class AbstractBaseService implements ConfigurableServiceInterface {

    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * @var SecurityContext
     */
    protected $sc;

    /**
     * @var \RRS\Bundle\DFormBundle\Service\TransactionService
     */
    protected $ts;

    /**
     * @var \stdClass
     */
    protected $config;

    /**
     * @param ObjectManager $om
     * @param Router $router
     * @param FormFactory $factory
     * @param \Symfony\Component\Security\Core\SecurityContext $sc
     * @param \RRS\Bundle\DFormBundle\Service\TransactionService $ts
     */
    public function __construct(ObjectManager $om, Router $router, FormFactory $factory, SecurityContext $sc, TransactionService $ts){
        $this->om = $om;
        $this->router = $router;
        $this->formFactory = $factory;
        $this->sc = $sc;
        $this->ts = $ts;
    }

    /**
     * Indica la configuracion del controller al servicio
     *
     * @param \stdClass $configuracion
     */
    public function setConfiguracion(\stdClass $configuracion)
    {
        $this->config = $configuracion;
    }

    public function getEntityClass()
    {
        $entityClass = $this->config->entity;

        if (!class_exists($entityClass)) {
            throw new \InvalidArgumentException(sprintf(
                "Entity class [entity] '%s' class was not defined in config file",
                $entityClass
            ));
        }

        return $entityClass;
    }

    public function getFormTypeClass()
    {
        $formClass = $this->config->form;

        if (!class_exists($formClass)) {
            throw new \InvalidArgumentException(sprintf(
                "FormType class [form] '%s' class was not defined in config file",
                $formClass
            ));
        }

        return $formClass;
    }

    public function getBaseRoute()
    {
        return $this->config->base_route;
    }

    /**
     * Sobreescriba este metodo si desea realizar operacion antes de crear una nueva entidad
     *
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function prepareCreateEntity()
    {
        $entityClass = $this->getEntityClass();

        return new $entityClass;
    }

    /**
     * Sobreescriba este metodo si desea realizar operaciones antes de crear la entidad a editar
     * TODO: el metodo prepareEditEntiy debe ser separado en prepareForEdit y prepareForUpdate
     * @param int $id
     *
     * @return mixed
     */
    public function prepareEditEntity($id)
    {
        $entity = $this->find($id);

        return $entity;
    }

    /**
     *
     * @return \Incoder\Bundle\SiiaBundle\Repository\AbstractEntityRepository
     */
    public function getRepository()
    {
        return $this->om->getRepository($this->getEntityClass());
    }

    /**
     * Persiste la entidad
     *
     * @param mixed $data
     * @param bool  $disableCsrf
     *
     * @return mixed
     */
    public function create($data, $disableCsrf = false)
    {
        $entity = $this->prepareCreateEntity();

        if ($disableCsrf) {
          $options = ['csrf_protection'   => false];
        } else {
          $options = [];
        }

        $form = $this->createCreateForm($entity, $options);

        if ($data instanceof Request) {
            $form->handleRequest($data);
        } else {
            $form->submit($data);
        }

        if ($form->isValid()) {
            $this->postCreateValid($entity);
        }

        return [$entity, $form];
    }

    protected function postCreateValid($entity)
    {
        $this->persist($entity);
        $this->om->flush();
    }

    protected function postUpdateValid($entity)
    {
        $this->persist($entity);
        $this->om->flush();
    }

    /**
     * Actualiza la entidad
     *
     * @param $data
     * @param $id
     *
     * @return array
     */
    public function update($data, $id)
    {
        $entity = $this->prepareEditEntity($id);
        $form = $this->createEditForm($entity);

        if ($data instanceof Request) {
            $form->handleRequest($data);
        } else {
            $form->submit($data);
        }

        if ($form->isValid()) {
            $this->postUpdateValid($entity);
        }

        return [$entity, $form];
    }

    public function persist($entity, $autoflush = false)
    {
        if ($entity instanceof DFormEntity) {
            $entity->updateVersion();
        }

        $this->om->persist($entity);

        if ($autoflush) {
            $this->om->flush();
        }
    }

    public function remove($entity, $autoflush = false)
    {
        $this->om->remove($entity);

        if ($autoflush) {
            $this->om->flush();
        }
    }

    public function find($id)
    {
        return $this->getRepository()->find($id);
    }

    public function getList($page, $limit, $params = array())
    {
        $repository = $this->getRepository();
        if  (method_exists($repository, 'findAllPaginated')) {
            return $repository->findAllPaginated($page, $limit, $params);
        } else {
            return $repository->findAll();
        }
    }

    public function createCreateForm($entity = null, $options = array()) {
        $default_options = array(
            'action' => isset($options['action']) ? $options['action'] : $this->generateUrl($this->getBaseRoute() . '_create'),
            'method' => 'POST'
        );

        $entity = null === $entity ? $this->prepareCreateEntity() : $entity;
        $form = $this->createForm($entity, array_merge($default_options, $options));

        $this->addCreateButton($form);

        return $form;
    }

    public function createEditForm($entity, $options = array()) {

        $default_options = array(
            'action' => isset($options['action']) ? $options['action'] : $this->generateUrl($this->getBaseRoute() . '_update', array('id' => $entity->getId())),
            'method' => 'PUT'
        );

        unset($options['action']);

        try {
            $form = $this->createForm($entity, array_merge($default_options, $options));
        } catch (TransformationFailedException $e) {
            throw new \Exception("La informacion en [{$entity->__type}] tiene campos de informacion no validos. {$e->getMessage()}");
        } catch (\Exception $e) {
            throw $e;
        }

        $this->addEditButton($form);

        return $form;
    }

    public function createDeleteForm($id) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($this->getBaseRoute() . '_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }

    protected function addCreateButton(Form $form) {
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => [
            'class' => 'btn btn-primary'
        ]));
    }

    protected function addEditButton(Form $form) {
        $form->add('submit', 'submit', array('label' => 'Actualizar', 'attr' => [
            'class' => 'btn btn-primary'
        ]));
    }


    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param mixed $data    The initial data for the form
     * @param array $options Options for the form
     *
     * @return Form
     */
    protected function createForm($data = null, array $options = array())
    {
        $formType = $this->getFormTypeClass();
        return $this->formFactory->create(new $formType, $data, $options);
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string         $route         The name of the route
     * @param mixed          $parameters    An array of parameters
     * @param Boolean|string $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->router->generate($route, $parameters, $referenceType);
    }

    protected function createNotFoundException($message = 'Not Found', \Exception $previous = null)
    {
        return new NotFoundHttpException($message, $previous);
    }



    /**
     * @return SecurityUser
     */
    public function getUser()
    {
        return $this->sc->getToken()->getUser();
    }
}
