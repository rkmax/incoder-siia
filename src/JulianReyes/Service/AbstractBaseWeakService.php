<?php
/**
 * The MIT License (MIT)
 * Copyright © 2014 Julian Reyes Escrigas <julian.reyes.escrigas@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the “Software”), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace JulianReyes\Service;


use Doctrine\Common\Util\Inflector;
use Symfony\Component\HttpFoundation\Request;

class AbstractBaseWeakService extends AbstractBaseService {

    private $dqlRelatedName;

    protected $relatedId;

    protected $relatedEntity = null;

    /**
     * @param int $relatedId
     *
     * @return mixed
     */
    public function prepareCreateEntity($relatedId = null)
    {
        $this->relatedId = $relatedId;
        $entity = parent::prepareCreateEntity();
        $method = 'set' . ucfirst($this->getDQLRelatedName());
        $entity->$method($this->validateRelatedEntity($relatedId));

        return $entity;
    }


    /**
     * Persiste la entidad
     *
     * @param mixed $data
     * @param int  $relatedId
     *
     * @return mixed
     */
    public function create($data, $relatedId = null)
    {
        $this->relatedId = $relatedId;
        $entity = $this->prepareCreateEntity($relatedId);
        $form = $this->createCreateForm($entity);

        if ($data instanceof Request) {
            $form->handleRequest($data);
        } else {
            $form->submit($data);
        }

        if ($form->isValid()) {
            $this->postCreateValid($entity);
        }

        return [$entity, $form];
    }

    /**
     * @param $data
     * @param $id
     * @param null $relatedId
     * @return array
     */
    public function update($data, $id, $relatedId = null)
    {
        $this->relatedId = $relatedId;
        $entity = $this->prepareEditEntity($id);
        $form = $this->createEditForm($entity);

        if ($data instanceof Request) {
            $form->handleRequest($data);
        } else {
            $form->submit($data);
        }

        if ($form->isValid()) {
            $this->postUpdateValid($entity);
        }

        return [$entity, $form];
    }


    /**
     * @param int $relatedId
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return mixed
     */
    public function validateRelatedEntity($relatedId)
    {
        $this->relatedEntity = $this->om->find($this->getRelatedEntityClass(), $relatedId);
        $this->relatedId = $relatedId;

        if (!$this->relatedEntity) {
            throw $this->createNotFoundException(
                sprintf("Unable to find '%s' entity.", $this->getRelatedEntityClass())
            );
        }

        return $this->relatedEntity;
    }

    public function getRelatedEntityClass()
    {
        $relatedEntityClass = $this->config->related_entity;

        if (!class_exists($relatedEntityClass)) {
            throw new \InvalidArgumentException(sprintf(
                "Entity class [related_entity] '%s' class was not defined in config file",
                $relatedEntityClass
            ));
        }

        return $relatedEntityClass;
    }

    public function getDQLRelatedName()
    {
        if (!$this->dqlRelatedName) {
            $rclass = new \ReflectionClass($this->getRelatedEntityClass());
            $this->dqlRelatedName = Inflector::camelize($rclass->getShortName());
        }

        return $this->dqlRelatedName;
    }
} 
