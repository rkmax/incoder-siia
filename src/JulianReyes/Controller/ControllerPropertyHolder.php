<?php
/**
 * Created by PhpStorm.
 * User: chamanx
 * Date: 3/30/14
 * Time: 5:29 AM
 */

namespace JulianReyes\Controller;


class ControllerPropertyHolder extends \stdClass{

    function __construct($baseRoute, $relatedBaseRoute, $form, $entity, $service, $relatedEntity, $rolName)
    {
        $this->base_route = $baseRoute;
        $this->related_base_route = $relatedBaseRoute;
        $this->form = $form;
        $this->entity = $entity;
        $this->service = $service;
        $this->related_entity = $relatedEntity;
        $this->rol_name = $rolName;
    }


    public $base_route;
    public $related_base_route;
    public $form;
    public $entity;
    public $service;
    public $related_entity;
    public $rol_name;

    /**
     * @param mixed $baseRoute
     */
    public function setBaseRoute($baseRoute)
    {
        $this->base_route = $baseRoute;
    }

    /**
     * @return mixed
     */
    public function getBaseRoute()
    {
        return $this->base_route;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param mixed $relatedBaseRoute
     */
    public function setRelatedBaseroute($relatedBaseRoute)
    {
        $this->related_base_route = $relatedBaseRoute;
    }

    /**
     * @return mixed
     */
    public function getRelatedBaseroute()
    {
        return $this->related_base_route;
    }

    /**
     * @param mixed $relatedEntity
     */
    public function setRelatedEntity($relatedEntity)
    {
        $this->related_entity = $relatedEntity;
    }

    /**
     * @return mixed
     */
    public function getRelatedEntity()
    {
        return $this->related_entity;
    }

    /**
     * @param mixed $rolName
     */
    public function setRolName($rolName)
    {
        $this->rol_name = $rolName;
    }

    /**
     * @return mixed
     */
    public function getRolName()
    {
        return $this->rol_name;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }




} 