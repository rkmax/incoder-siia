<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/19/14
 * Time: 11:00 PM
 */

namespace JulianReyes\Controller;


interface ControllerNavegationInterface {

    public function getLeftFormNav();

    public function getLeftNav();

    public function getLeftShowNav();
}
