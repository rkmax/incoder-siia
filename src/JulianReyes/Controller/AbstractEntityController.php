<?php


namespace JulianReyes\Controller;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;


abstract class AbstractEntityController extends AbstractBaseController
{
    /**
     *
     * @return Session
     */
    protected function getSession() {
        return $this->get('session');
    }

    /**
     *
     * @return SecurityContext
     */
    protected function getSecurityContext() {
        return $this->get('security.context');
    }

    /**
     * @var mixed
     */
    protected $entity;

    /**
     * Listado general de entidades con paginación
     * @param Request $request
     * @param array $params
     * @param int $page
     * @param int $limit
     * @return array
     */
    function indexAction(Request $request, $params=[], $page=1, $limit=10 )
    {
        $entities = $this->getService()->getList($page, $limit, $params);
        $adapter = new ArrayAdapter($entities);
        $pagerfanta = new Pagerfanta($adapter);

        $page = $request->query->get('page', $page);
        $pagerfanta->setCurrentPage($page);
        $pagerfanta->setMaxPerPage($limit);

        return array(
            'page' => $page,
            'limit' => $limit,
            'leftnav' => $this->getLeftNav(),
            'entities' => $pagerfanta,
        );
    }

    public function newAction()
    {
        $this->entity = $entity = $this->getService()->prepareCreateEntity();
        $form = $this->getService()->createCreateForm();

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'leftnav' => $this->getLeftFormNav(),
        );
    }

    /**
     * Actions to be executed when creating in post-validation
     * @return mixed
     */
    protected function postValidationCreate(){
        return $this->returnAfterCreate($this->entity);
    }

    /**
     * @param null  $entity
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @SuppressWarnings("unused")
     */
    protected function returnAfterCreate($entity = null, $params = [])
    {
        return $this->redirect($this->generateUrl($this->getBaseRoute(), $params));
    }

    /**
     * @param null  $entity
     * @param array $params
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @SuppressWarnings("unused")
     */
    protected function returnAfterUpdate($entity = null, $params = [])
    {
        return $this->redirect($this->generateUrl($this->getBaseRoute(), $params));
    }

    public function createAction(Request $request)
    {
        /** @var Form $form */
        list($entity, $form) = $this->getService()->create($request);
        $this->entity = $entity;

        if ($form->isValid()) {
            return $this->postValidationCreate();
        }

        return array(
            'errors' => $form->getErrors(),
            'entity' => $entity,
            'form'   => $form->createView(),
            'leftnav' => $this->getLeftFormNav(),
        );
    }


    /**
     * @param $id
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @Template()
     */
    public function showAction($id)
    {
        $entity = $this->getService()->find($id);
        $this->entity = $entity;

        return array(
            'leftnav' => $this->getLeftShowNav(),
            'entity' => $entity
        );
    }

    /**
     * @param $id
     *
     * @return array
     *
     * @Template()
     */
    public function editAction($id)
    {
        $entity = $this->getService()->prepareEditEntity($id);
        $editForm = $this->getService()->createEditForm($entity);
        $this->entity = $entity;

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'leftnav' => $this->getLeftFormNav(),
        );
    }

    /**
     * Actions to be executed when updating in post-validation
     * @return mixed
     */
    protected function postValidationUpdate(){
        return $this->returnAfterUpdate($this->entity);
    }

    public function updateAction(Request $request, $id)
    {
        /** @var Form $editForm */
        list($entity, $editForm) = $this->getService()->update($request, $id);
        $this->entity = $entity;

        if ($editForm->isValid()) {
            return $this->postValidationUpdate();
        }

        return array(
            'errors'    => $editForm->getErrors(),
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
            'leftnav'   => $this->getLeftFormNav(),
        );
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    public function getLeftShowNav()
    {
        return $this->getLeftFormNav();
    }

    /*
     * TODO: Este metodo no es usado por ningun controlador, lo eliminamos?
     */
//    public function deleteAction(Request $request, $id)
//    {
//        $form = $this->getService()->createDeleteForm($id);
//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $entity = $em->getRepository($this->getEntityName())->find($id);
//
//            if (!$entity) {
//                throw $this->createNotFoundException(
//                    sprintf('Unable to find %s entity.', $this->getEntityName())
//                );
//            }
//
//            $em->remove($entity);
//            $em->flush();
//        }
//
//        return $this->redirect($this->generateUrl($this->getBaseRoute()));
//    }
}
