<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/20/14
 * Time: 7:21 PM
 */

namespace JulianReyes\Controller;


interface EntityInterface {

    /**
     * Devuelve el FQCN de la entidad
     *
     * @return string
     */
    public function getEntity();

    /**
     * Devuelve el FQCN de el FormType
     *
     * @return string
     */
    public function getFormType();
}
