<?php

namespace JulianReyes\Controller;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractWeakEntityController extends AbstractEntityController {


    /**
     * @var mixed
     */
    private $relatedEntity;

    /**
     * @return \RRS\Bundle\DFormBundle\Entity\DFormEntity
     */
    public function getRelatedEntity()
    {
        return $this->relatedEntity;
    }

    /**
     * @return string related entity class
     */
    protected function getRelatedEntityName() {
        return $this->getConfig($this->getControllerType())->related_entity;
    }

    public function getRelatedBaseRoute() {
        return $this->getConfig($this->getControllerType())->related_base_route;
    }

    /**
     *
     * @return \JulianReyes\Service\AbstractBaseWeakService
     */
    protected function getService() {
        return parent::getService();
    }

    /**
     * validate if related entity exist or throw an exception
     *
     * @param int $relatedId
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function validateRelatedEntity($relatedId) {
        $this->relatedEntity = $this->getService()->find($relatedId, $this->getRelatedEntity());

        if (!$this->relatedEntity) {
            throw $this->createNotFoundException(
                    sprintf('Unable to find %s entity.', $this->getRelatedEntity())
            );
        }
    }

    /**
     * Override
     * Listado general de entidades con paginación
     * @param Request $request
     * @param array $params
     * @param int $page
     * @param int $limit
     * @param int $relatedId
     * @return array
     */
    function indexAction(Request $request, $params=[], $page=1, $limit=10, $relatedId = null )
    {
        $this->relatedEntity = $this->getService()->validateRelatedEntity($relatedId);
        $base_name = $this->getRelatedPropertyName();
        $params = [
            $base_name => $relatedId
        ];
        $entities = $this->getService()->getList($page, $limit, $params);
        $adapter = new ArrayAdapter($entities);
        $pagerfanta = new Pagerfanta($adapter);
        //Obtiene el parametro page del request y si no viene toma el por defecto
        $page = ($request->query->get('page'))? $request->query->get('page') : $page;
        $pagerfanta->setCurrentPage($page);
        $pagerfanta->setMaxPerPage($limit);

        return array(
            'page' => $page,
            'limit' => $limit,
            'leftnav' => $this->getLeftNav(),
            'entities' => $pagerfanta,
            'related_entity' => $this->relatedEntity,
        );
    }

    /**
     * @param int $asociado
     * @return array
     */
    public function newAction($asociado = null) {

        $this->relatedEntity = $this->getService()->validateRelatedEntity($asociado);
        $entity = $this->getService()->prepareCreateEntity($asociado);
        $form = $this->getService()->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'leftnav' => $this->getLeftFormNav(),
            'related_entity' => $this->relatedEntity,
        );
    }

    /**
     * TODO: Documentacion
     * TODO: enviar esta logica a un servicio
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $asociado
     *
     * @return array|mixed
     */
    public function createAction(Request $request, $asociado = null) {
        $this->relatedEntity = $this->getService()->validateRelatedEntity($asociado);
        list($entity, $form) = $this->getService()->create($request, $asociado);
        $this->entity = $entity;
        if ($form->isValid()) {
            return $this->postValidationCreate();
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'leftnav' => $this->getLeftFormNav(),
            'related_entity' => $this->relatedEntity
        );
    }


    /**
     * @param $id
     *
     * @return array
     *
     * @Template()
     */
    public function showAction($id)
    {
        $entity = $this->getService()->prepareEditEntity($id);
        $this->guessRelatedEntity($entity, null);
        $this->entity = $entity;

        return array(
            'leftnav' => $this->getLeftShowNav(),
            'entity' => $entity
        );
    }

    /**
     * TODO: doc
     *
     * @param int  $id
     * @param null $asociado
     *
     * @return array
     */
    public function editAction($id, $asociado = null)
    {
        $entity = $this->getService()->prepareEditEntity($id);
        $this->guessRelatedEntity($entity, $asociado);
        $editForm = $this->getService()->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'leftnav' => $this->getLeftFormNav(),
            'related_entity' => $this->relatedEntity,
        );
    }

    /**
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int                                      $id
     * @param int                                      $asociado
     *
     * @return mixed
     */
    public function updateAction(Request $request, $id, $asociado = null) {
        list($entity, $editForm) = $this->getService()->update($request, $id, $asociado);
        $this->entity = $entity;
        $this->guessRelatedEntity($this->entity, $asociado);
        if ($editForm->isValid()) {
            return $this->postValidationUpdate();
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'leftnav' => $this->getLeftFormNav(),
        );
    }

    protected function guessRelatedEntity($entity, $relatedId)
    {
        if (null != $relatedId) {
            $this->relatedEntity = $this->getService()->validateRelatedEntity($relatedId);
        } else {
            $base_name = (new \ReflectionClass($this->getRelatedEntityName()))->getShortName();
            $method = "get" . $base_name;
            $this->relatedEntity = $entity->$method();
        }
    }

    /**
     * @return string
     */
    protected function getRelatedPropertyName()
    {
        $base_name = (new \ReflectionClass($this->getRelatedEntityName()))->getShortName();

        return lcfirst($base_name);
    }

}
