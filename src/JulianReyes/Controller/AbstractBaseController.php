<?php
/**
 * Created by PhpStorm.
 * User: julian
 * Date: 1/22/14
 * Time: 9:23 AM
 */

namespace JulianReyes\Controller;


use Incoder\Bundle\SiiaBundle\Service\ConfigurableServiceInterface;
use JulianReyes\Service\AbstractBaseService;
use JulianReyes\Lib\ParseUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class AbstractBaseController extends Controller implements ControllerNavegationInterface {

    /**
     * @var string
     */
    private $controllerType;

    /**
     * @var \stdClass
     */
    private $config;

    /**
     * @var ConfigurableServiceInterface
     */
    private $service;

    /**
     * Obtiene la configuracion del controllador indicado por el tipo
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return \stdClass
     */
    protected function getConfig() {
        if (!$this->config) {
            throw $this->createNotFoundException("configuracion no encontrada");
        }

        return $this->config;
    }

    /**
     * get the implemented service
     * @internal param string $type
     * @return AbstractBaseService
     */
    protected function getService() {

        if (!$this->service) {
            $config = $this->getConfig($this->getControllerType());
            $serviceName = $config->service;
            $this->service = $this->get($serviceName);
            $this->service->setConfiguracion($config);
        }

        return $this->service;
    }

    /**
     *
     * @return string
     */
    protected function getEntityName() {
        return $this->getConfig($this->getControllerType())->entity;
    }

    /**
     *
     * @return string form type
     */
    protected function getFormType()
    {
        return $this->getConfig($this->getControllerType())->form;
    }

    protected function getBaseRoute()
    {
        return $this->getConfig($this->getControllerType())->base_route;
    }


    /**
     * @throws \Exception
     * @return String
     */
    function getControllerType()
    {
        if ($this->controllerType) {
            return $this->controllerType;
        }

        throw new \Exception(sprintf(
            "Debe definir la annotacion @EntityController en %s",
            get_class($this)
        ));
    }

    /**
     * @param string $controllerType
     */
    public function setControllerType($controllerType)
    {
        $this->controllerType = $controllerType;
        $config = $this->container->getParameter("siia.controller.$this->controllerType");

        $this->config = ParseUtils::array2Object($config);
    }
} 
