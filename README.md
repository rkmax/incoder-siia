# SIIA - Sistema Integrado de Información Agropecuaria

Estructura general del proyecto

```
/
  app/
  bin/
  src/
  vendor/
  web/
```

## `app/`
Carpeta donde reside la cache, logs y configuracion de toda la aplicacion, asi como recursos (Plantillas, Imagenes, etc) que serán usados por todos los *bundles* que tenga nuestra aplicación

## `bin/`
Comandos utilizados para facilitar el desarrollo de la aplicación

## `src/`
Contiene el codigo fuente de los bundles

## `vendor`
Contiene las librerias externas de PHP, esta carpeta no tiene seguimiento pormel SCM y es administrada por `composer` y no debe editarse a mano

## `web/`
Carpeta publica de la aplicación aqui se colocan los diferentes puntos de entrada de la aplicación, actualmente cuenta con dos (2) uno de produccion (app.php) y otro para el desarrollo (app_dev.php), asi como los archivos publicos (js, css, img, etc), favicon

## Dependencias de desarrollo

El proyecto utiliza un par de componentes de `grunt`, `bower` y `composer` para gestionar tareas, dependencias del lado del cliente y dependencias del lado del servidor respectivamente.

recuerde siempre ejecutar los siguientes comandos en la carpeta del proyecto:

- `$ composer update`
- `$ npm install && grunt`
