module.exports = function(grunt) {
    grunt.initConfig({
        peg: {
            options: { trackLineAndColumn: true },
            ruleparser: {
                src: "src/RRS/Bundle/DFormBundle/Resources/peg/ruleparser.pegjs",
                dest: "src/RRS/Bundle/DFormBundle/Resources/public/js/lib/ruleparser.js",
                options: { exportVar: "DFORMS_ruleparser" }
            }
        },
        bower: {
            install: {}
        },
        shell: {
            dropDB: {
                command: 'php app/console doctrine:database:drop --force',
                options: {
                    stdout: true
                }
            },
            createDB: {
                command: 'php app/console doctrine:database:create',
                options: {
                    stdout: true
                }
            },
            schemaDB: {
                command: 'php app/console doctrine:schema:update --force',
                options: {
                    stdout: true
                }
            },
            mongoDB: {
                command: 'php app/console doctrine:mongodb:schema:drop',
                options: {
                    stdout: true
                }
            },
            loadFixtures: {
                command: 'php app/console david:f:l',
                options: {
                    stdout: true
                }
            },
            randomData: {
                command: 'php app/console data:random',
                options: {
                    stdout: true
                }
            },
            fixtures: {
                command: 'php app/console doctrine:f:l --append',
                options: {
                    stdout: true,
                    stderr: true
                }
            },
            usuarios: {
                command: 'php app/console fos:user:create admin admin@admin productividad && php app/console fos:user:create usuario user@user productividad && php app/console fos:user:promote admin ROLE_ADMIN'
            }
        }
    });

    grunt.loadNpmTasks('grunt-peg');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-shell');
    grunt.registerTask('default', ['peg', 'bower']);
    grunt.registerTask('reload', [
        'shell:dropDB', 'shell:createDB', 'shell:schemaDB', 'shell:mongoDB',
        'shell:loadFormCaracterizacion', 'shell:loadFormRegistro', 'shell:loadFormDgEmpresarial',
        'shell:loadFormPerfil', 'shell:loadFormDgTecnico', 'shell:loadFormDgProductivo',
        'shell:loadFormDgProductivoCuadroAgricola',
        'shell:loadFormDgProductivoCuadroPecuario', 'shell:loadFormDgProductivoInsumo',
        'shell:loadFormDgTecnicoMiembros', 'shell:loadFormDgTecnicoPlano', 'shell:loadFormDgTecnicoCaudales',
        'shell:loadFixtures', 'shell:usuarios'
    ]);
    grunt.registerTask('data', ['shell:randomData', 'shell:usuarios', 'shell:fixtures']);
    grunt.registerTask('clean', ['shell:dropDB', 'shell:createDB', 'shell:schemaDB', 'shell:mongoDB']);
};
